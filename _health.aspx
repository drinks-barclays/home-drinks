<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Continuity.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">
	
	public Continuity c = new Continuity();
	public System.DateTime currentMonth = System.DateTime.Today.AddDays((System.DateTime.Today.Day*-1)+1);
	public DataTable dt = new DataTable();
	
	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Session["operator"] == null) Response.Redirect("/security.aspx?r=" + Server.UrlEncode(Request.Url.ToString()));
		Hashtable p = new Hashtable();
		c.GetContinuity(out dt, p);
		ParsePrograms();
		
		if (Request["id"] != null) {
			p = new Hashtable();
			p.Add("ContinuityConfigurationID", Request["id"]);
			dt = new DataTable();
			c.GetContinuityRaw(out dt, p);
			// parse out current shipments
			ParseMonth();
		}
	}
	
	private void ParsePrograms() {
		foreach(DataRow r in dt.Rows) {
			if (Request["id"] == null || r["ContinuityConfigurationID"].ToString() != Request["id"].ToString()) {
				programs.InnerHtml += "<a href='_health.aspx?id=" + r["ContinuityConfigurationID"] + "'>" + r["Name"].ToString().Trim() + "</a> &nbsp;|&nbsp; ";	
			} else {
				err.InnerHtml += "<div style='font-weight: bold; font-size: 25px; margin-top: 25px;'> Health Report for " + r["Name"].ToString().Trim() + "</div>";	
			}
		}
	}

	private void ParseMonth() {
		int firstShip = 0;
		int secondShipR = 0;
		int secondShipW = 0;
		int secondShipM = 0;
		int secondShip = 0;
		
		int thirdShipR = 0;
		int thirdShipW = 0;
		int thirdShipM = 0;
		int thirdShip = 0;
		
		int fourthShipR = 0;
		int fourthShipW = 0;
		int fourthShipM = 0;
		int fourthShip = 0;
		
		int fifthShip = 0;
		int fifthShipR = 0;
		int fifthShipW = 0;
		int fifthShipM = 0;
		
		int sixthShip = 0;
		int sixthShipR = 0;
		int sixthShipW = 0;
		int sixthShipM = 0;
		
		int seventhShip = 0;
		int seventhShipR = 0;
		int seventhShipW = 0;
		int seventhShipM = 0;
		
		int eighthShip = 0;
		int eighthShipR = 0;
		int eighthShipW = 0;
		int eighthShipM = 0;
		
		int ninthShip = 0;
		int ninthShipR = 0;
		int ninthShipW = 0;
		int ninthShipM = 0;
		
		int tenthShip = 0;
		int tenthShipR = 0;
		int tenthShipW = 0;
		int tenthShipM = 0;
		
		string date = "NextShipDate > '" + currentMonth + "' AND NextShipDate < '" + currentMonth.AddMonths(1) + "'";
		DataRow[] foundRows = dt.Select(date);
		
		
		if (foundRows.Length > 0  || (currentMonth == System.DateTime.Today.AddDays((System.DateTime.Today.Day*-1)+1))) {
			
			// display month
			err.InnerHtml += "<hr style='margin-top: 15px;' /><div style='font-weight: bold; font-size: 18px; margin-bottom: 5px;'>" + currentMonth.Date.ToString("y") + "</div><hr size=1 />";
		
			foreach(DataRow r in foundRows) {
				switch (r["TotalShipments"].ToString()) {
					case "1":
						secondShip++;
						if (r["PreferenceTypeID"].ToString() == "1") secondShipR++;
						if (r["PreferenceTypeID"].ToString() == "2") secondShipW++;
						if (r["PreferenceTypeID"].ToString() == "3") secondShipM++;
						break;
					case "2":
						thirdShip++;
						if (r["PreferenceTypeID"].ToString() == "1") thirdShipR++;
						if (r["PreferenceTypeID"].ToString() == "2") thirdShipW++;
						if (r["PreferenceTypeID"].ToString() == "3") thirdShipM++;
						break;
					case "3":
						fourthShip++;
						if (r["PreferenceTypeID"].ToString() == "1") fourthShipR++;
						if (r["PreferenceTypeID"].ToString() == "2") fourthShipW++;
						if (r["PreferenceTypeID"].ToString() == "3") fourthShipM++;
						break;
					case "4":
						fifthShip++;
						if (r["PreferenceTypeID"].ToString() == "1") fifthShipR++;
						if (r["PreferenceTypeID"].ToString() == "2") fifthShipW++;
						if (r["PreferenceTypeID"].ToString() == "3") fifthShipM++;
						break;
					case "5":
						sixthShip++;
						if (r["PreferenceTypeID"].ToString() == "1") sixthShipR++;
						if (r["PreferenceTypeID"].ToString() == "2") sixthShipW++;
						if (r["PreferenceTypeID"].ToString() == "3") sixthShipM++;
						break;
					case "6":
						seventhShip++;
						if (r["PreferenceTypeID"].ToString() == "1") seventhShip++;
						if (r["PreferenceTypeID"].ToString() == "2") seventhShip++;
						if (r["PreferenceTypeID"].ToString() == "3") seventhShip++;
						break;
					case "7":
						eighthShip++;
						if (r["PreferenceTypeID"].ToString() == "1") eighthShipR++;
						if (r["PreferenceTypeID"].ToString() == "2") eighthShipW++;
						if (r["PreferenceTypeID"].ToString() == "3") eighthShipM++;
						break;
					case "8":
						ninthShip++;
						if (r["PreferenceTypeID"].ToString() == "1") ninthShipR++;
						if (r["PreferenceTypeID"].ToString() == "2") ninthShipW++;
						if (r["PreferenceTypeID"].ToString() == "3") ninthShipM++;
						break;
					default:
						tenthShip++;
						if (r["PreferenceTypeID"].ToString() == "1") tenthShipR++;
						if (r["PreferenceTypeID"].ToString() == "2") tenthShipW++;
						if (r["PreferenceTypeID"].ToString() == "3") tenthShipM++;
						break;
				}
			}
			
			// display counts
			if (secondShip>0)
			err.InnerHtml += "<div>2nd Ship</div><div style='text-align: right; width:" + (5+secondShip) + "px; background-color: green; color: white; padding-right: 5px;'>" + secondShip + "</div>" +
								"<div style='margin-bottom: 10px; margin-right: 10px; font-size: 12px'>" + secondShipR + " Red | " + secondShipW + " White | " + secondShipM + " Mixed</div>";
			if (thirdShip>0)
			err.InnerHtml += "<div>3rd Ship</div><div style='text-align: right; width:" + (5+thirdShip) + "px; background-color: purple; color: white; padding-right: 5px;'>" + thirdShip + "</div>" +
								"<div style='margin-bottom: 10px; margin-right: 10px; font-size: 12px'>" + thirdShipR + " Red | " + thirdShipW + " White | " + thirdShipM + " Mixed</div>";
			if (fourthShip>0)
			err.InnerHtml += "<div>4th Ship</div><div style='text-align: right; width:" + (5+fourthShip) + "px; background-color: orange; color: white; padding-right: 5px;'>" + fourthShip + "</div>" +
								"<div style='margin-bottom: 10px; margin-right: 10px; font-size: 12px'>" + fourthShipR + " Red | " + fourthShipW + " White | " + fourthShipM + " Mixed</div>";
			if (fifthShip>0)
			err.InnerHtml += "<div>5th Ship</div><div style='text-align: right; width:" + (5+fifthShip) + "px; background-color: pink; color: white; padding-right: 5px;'>" + fifthShip + "</div>" +
								"<div style='margin-bottom: 10px; margin-right: 10px; font-size: 12px'>" + fifthShipR + " Red | " + fifthShipW + " White | " + fifthShipM + " Mixed</div>";
			if (sixthShip>0)
			err.InnerHtml += "<div>6th Ship</div><div style='text-align: right; width:" + (5+sixthShip) + "px; background-color: blue; color: white; padding-right: 5px;'>" + sixthShip + "</div>" +
								"<div style='margin-bottom: 10px; margin-right: 10px; font-size: 12px'>" + sixthShipR + " Red | " + sixthShipW + " White | " + sixthShipM + " Mixed</div>";
			if (seventhShip>0)
			err.InnerHtml += "<div>7th Ship</div><div style='text-align: right; width:" + (5+seventhShip) + "px; background-color: blue; color: white; padding-right: 5px;'>" + seventhShip + "</div>" +
								"<div style='margin-bottom: 10px; margin-right: 10px; font-size: 12px'>" + seventhShipR + " Red | " + seventhShipW + " White | " + seventhShipM + " Mixed</div>";
			if (eighthShip>0)
			err.InnerHtml += "<div>8th Ship</div><div style='text-align: right; width:" + (5+eighthShip) + "px; background-color: blue; color: white; padding-right: 5px;'>" + eighthShip + "</div>" +
								"<div style='margin-bottom: 10px; margin-right: 10px; font-size: 12px'>" + eighthShipR + " Red | " + eighthShipW + " White | " + eighthShipM + " Mixed</div>";
			if (ninthShip>0)
			err.InnerHtml += "<div>9th Ship</div><div style='text-align: right; width:" + (5+ninthShip) + "px; background-color: blue; color: white; padding-right: 5px;'>" + ninthShip + "</div>" +
								"<div style='margin-bottom: 10px; margin-right: 10px; font-size: 12px'>" + ninthShipR + " Red | " + ninthShipW + " White | " + ninthShipM + " Mixed</div>";
			if (tenthShip>0)
			err.InnerHtml += "<div>10th+ Ship</div><div style='text-align: right; width:" + (5+tenthShip) + "px; background-color: blue; color: white; padding-right: 5px;'>" + tenthShip + "</div>" +
								"<div style='margin-bottom: 10px; margin-right: 10px; font-size: 12px'>" + tenthShipR + " Red | " + tenthShipW + " White | " + tenthShipM + " Mixed</div>";
			
			// increment
			currentMonth = currentMonth.AddMonths(1);
			
			ParseMonth();
		}
		
		
	}
	
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Continuity Health</title>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>

<body>
<div id="programs" runat="server" style="font-weight; bold: font-size: 20px" />
<div id="err" runat="server" style="width: 800px;" />
</body>
</html>
