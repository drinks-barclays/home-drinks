<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Report.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Net.Mime" %>
<script language="C#" runat="server">

	Report report = new Report();
	DataTable dt;
   	public void Page_Load(Object sender, EventArgs e)
 	{
		string body = "";
		string startDate = System.DateTime.Now.Date.ToString("d");
		string endDate = System.DateTime.Now.Date.AddDays(1).ToString("d");
		double reality = 0;
		string campaignID = Request["campaignID"];
		
		
		body = "<div style='margin-bottom: 10px; font-weight: bold; font-size: 18px;'>Daily Summary</div><table>";
		body += "<tr style='width: 610px; padding: 3px; border-bottom: solid 1px #ccc;' class='clearfix'>";	
		body += "<td style='width: 100px; float: left;'>&nbsp;</td>";		
		body += "<td style='width: 100px; float: left;'>Day</td>";	
		body += "<td style='width: 100px; float: left;'>Orders</td>";
		body += "<td style='width: 100px; float: left;'>Revenue</td>";
		body += "<td style='width: 100px; float: left;'>NetMargin</td>";
		body += "</tr>";
		
		int year = 2013;
		int month = 11;
		int days = DateTime.DaysInMonth(year, month);
		DateTime d;
		for (int day = 1; day <= days; day++)
		{
			 d = new DateTime(year, month, day);
			 startDate = d.ToString("d");
			 endDate = d.AddDays(1).ToString("d");
			 
			 // get bottles
			 report.GetStaticPoolSummaryByDay(out dt, campaignID, startDate, "1001", "0");
			 foreach (DataRow r in dt.Rows) {
				body += "<tr style='width: 610px; padding: 3px;";
				if (Convert.ToDouble(r["NetMargin"]) < 0) body += " color: red;";
				body += "' class='clearfix'>";		
				body += "<td style='width: 100px; float: left;'>Bottle</td>";
				body += "<td style='width: 100px; float: left;'>" +  d.ToString("d") + "</td>";
				body += "<td style='width: 100px; float: left;'>" +  r["Acquisitions"].ToString() + "</td>";
				body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["Revenue"]).ToString("c") + "</td>";
				body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["NetMargin"]).ToString("c") + "</td>";
				reality += Convert.ToDouble(r["NetMargin"]);
				body += "</tr>";
			 }
				
			 // get packs
			 report.GetStaticPoolSummaryByDay(out dt, campaignID, startDate, "1003", "0");
			 foreach (DataRow r in dt.Rows) {
				body += "<tr style='width: 610px; padding: 3px;";
				if (Convert.ToDouble(r["NetMargin"]) < 0) body += " color: red;";
				body += "' class='clearfix'>";		
				body += "<td style='width: 100px; float: left;'>Pack</td>";
				body += "<td style='width: 100px; float: left;'>" +  d.ToString("d") + "</td>";
				body += "<td style='width: 100px; float: left;'>" +  r["Acquisitions"].ToString() + "</td>";
				body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["Revenue"]).ToString("c") + "</td>";
				body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["NetMargin"]).ToString("c") + "</td>";
				reality += Convert.ToDouble(r["NetMargin"]);
				body += "</tr>";
			 }
				
				
			 // get continuity
			 report.GetStaticPoolSummaryByDay(out dt, campaignID, startDate, "1003", "1");
			 foreach (DataRow r in dt.Rows) {
				body += "<tr style='width: 610px; padding: 3px;";
				if (Convert.ToDouble(r["NetMargin"]) < 0) body += " color: red;";
				body += "' class='clearfix'>";		
				body += "<td style='width: 100px; float: left;'>Continuity</td>";
				body += "<td style='width: 100px; float: left;'>" +  d.ToString("d") + "</td>";
				body += "<td style='width: 100px; float: left;'>" +  r["Acquisitions"].ToString() + "</td>";
				body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["Revenue"]).ToString("c") + "</td>";
				body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["NetMargin"]).ToString("c") + "</td>";
				reality += Convert.ToDouble(r["NetMargin"]);
				body += "</tr>";
			 }	
		}
		
		body += "</table>";
		
		Response.Write(body);
		
	}
</script>

