﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/LitlePayment.cs" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="Wine" %>
<script language="C#" runat="server">

	Payment_ p = new Payment_();
	Order o = new Order();
	Customer c = new Customer();
	public void Page_Load(Object sender, EventArgs e)
 	{
		// commit
		if (Request["action"] == "credit") {
			// make sure amount isn't more than the original amount
			if (Convert.ToDouble(Request["originalAmount"].ToString()) >= Convert.ToDouble(Request["amount"].ToString())) {
				// set brand
				SetBrand(Convert.ToInt16(Request["brandID"]));
				
				// do credit
				bool good = p.DoCredit(Request["transactionCode"], Convert.ToDouble(Request["amount"].ToString()).ToString("F"), Request["id"]);
				
				// legacy
				if (good == false) {
					p.MID = "6105109";
					 good = p.DoCredit(Request["transactionCode"], Convert.ToDouble(Request["amount"].ToString()).ToString("F"), Request["id"]);	
				}
			
				if (good) {
					// add credit to invoice
					o.AddInvoiceCredit(Request["id"], Request["amount"]);
					
					// add note to account
					string customerID = o.GetCustomerID(Request["id"]);
					c.AddNote(customerID, "Credit of $" + Request["amount"] + " applied to order #" + Request["id"] + " by " + Session["operator"] + " - Note added by BOT");
					
					// close modal window
					Response.Redirect("/modal/close_modal.html");
				} else {
					err.InnerHtml = "There was an error please contact DevOps with the transaction id of: " + p.trans_id;	
				}
				
			} else {
				// show error
				err.InnerHtml = "Credit amount can't be more than the total amount of the order.";	
			}
		} else if (Request["action"] == "skip") {
			Response.Redirect("/modal/close_modal.html");
		}
		
		// retrieve the order
		DataTable dt = o.Get(Request["id"]);
		
		if (dt.Rows.Count > 0) {
			// get the amount
			originalAmount.Value = Convert.ToDouble(dt.Rows[0]["Total"].ToString()).ToString("F");
			
			// get the brand
			brandID.Value = dt.Rows[0]["BrandID"].ToString().Trim();
			
			// get the transaction code
			transactionCode.Value = dt.Rows[0]["TransactionCode"].ToString().Trim();
			
			// prompt for amount to credit
		}
		
	}
	
	void SetBrand(int id) {
			/*Barclays: 6105109, Heartwood: 6106204, Troon: 6105112*/
			switch  (id) {
				case 2:
					// troon
					p.MID = "6105112";
					break;
				case 7:
					// H&O
					p.MID = "6106204";
					break;
				case 9:
					// afternoon delight
					p.MID = "6106204";
					break;
                case 8:
                    // Franklin Mint
                    p.MID = "7132993";
                    break;
				default:
					// barclays
					p.MID = "6105109";
					break;
			}
	}
	
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
</head>

<body>

<form action="_creditOrder.aspx" method="post">
<div id="err" style="font-weight: bold; color: red; font-size: 18px; margin-bottom: 10px;" runat="server" />
<p>
  <input name="brandID" type="hidden" id="brandID" runat="server"  />
  <input name="id" type="hidden" id="id" value="<%= Request["id"] %>"  />
  <input name="transactionCode" type="hidden" id="transactionCode" runat="server"  />
  Order ID: <%= Request["id"] %><br />
  <input name="originalAmount" type="hidden" id="originalAmount" runat="server"  />
  Amount: 
  <input name="amount" type="text" id="amount" width="8" value="<%= Request["amount"] %>"  />
  <input name="action" type="hidden" id="action" value="credit"  />
  <input type="submit" value="Submit Credit" /> 
  </p>
<p><a href="_creditOrder.aspx?action=skip">Don't Credit</a></p>
</form>
</body>
</html>
