using System.Data;
using Data;
using System.Collections;
using System.Web;
using System.Configuration;

namespace Wine
{
    public class Report
    {
		public DataUtility d = new DataUtility(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
		
		public void GetOrderReport(out DataTable dt, string campaignID, string startDate, string endDate) {
			Hashtable parameters = new Hashtable();
			if (HttpContext.Current.Request["campaign_id"] != null && HttpContext.Current.Request["campaign_id"].ToString() != "") parameters.Add("CampaignID", HttpContext.Current.Request["campaign_id"]);
			parameters.Add("StartDate", startDate);
			parameters.Add("EndDate", endDate);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_OrderReport", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetProfitLossSummary(out DataTable dt, string startDate, string endDate) {
			Hashtable parameters = new Hashtable();
			parameters.Add("StartDate", startDate);
			parameters.Add("EndDate", endDate);
			if (HttpContext.Current.Request["campaign_id"] != null && HttpContext.Current.Request["campaign_id"].ToString() != "") parameters.Add("CampaignID", HttpContext.Current.Request["campaign_id"]);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_ProfitLossSummary", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetProfitLossSummaryByCampaign(out DataTable dt, string startDate, string endDate) {
			Hashtable parameters = new Hashtable();
			parameters.Add("StartDate", startDate);
			parameters.Add("EndDate", endDate);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_ProfitLossSummaryByCampaign", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetOrderReportByDay(out DataTable dt, string startDate, string endDate) {
			Hashtable parameters = new Hashtable();
			parameters.Add("StartDate", startDate);
			parameters.Add("EndDate", endDate);
			
			if (HttpContext.Current.Request["campaign_id"] != null && HttpContext.Current.Request["campaign_id"].ToString() != "") parameters.Add("CampaignID", HttpContext.Current.Request["campaign_id"]);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_OrderReportByDay", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
					
		public DataTable GetLeadReport(out DataTable dt, string startDate, string endDate) {
			Hashtable parameters = new Hashtable();
			parameters.Add("StartDate", startDate);
			parameters.Add("EndDate", endDate);
			
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_LeadReport", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
					
		public DataTable GetCancelReport(out DataTable dt, string startDate, string endDate) {
			Hashtable parameters = new Hashtable();
			parameters.Add("StartDate", startDate);
			parameters.Add("EndDate", endDate);
			
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_CancelReport", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public void GetGrouponExport(out DataTable dt) {
			Hashtable parameters = new Hashtable();
			
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GrouponExport", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

		}
		
		public void GetGrouponAfterMarketExport(out DataTable dt) {
			Hashtable parameters = new Hashtable();
			
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetGrouponAfterMarketExport", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

		}
		
		
		
		public void GetOutOfStock(out DataTable dt) {
			Hashtable parameters = new Hashtable();
			
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetOutStockPacks", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

		}
		
		public DataTable GetInventorySummary(out DataTable dt, string startDate, string endDate) {
			Hashtable parameters = new Hashtable();
			parameters.Add("StartDate", startDate);
			parameters.Add("EndDate", endDate);
			
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetShipmentSummary", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public void GetBinAcquisitionReport(out DataTable dt, string startDate, string endDate, string brandID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("StartDate", startDate);
			parameters.Add("EndDate", endDate);
			parameters.Add("BrandID", brandID);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetBinAcquisitionReport", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetBinOrderReport(out DataTable dt, string startDate, string endDate, string brandID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("StartDate", startDate);
			parameters.Add("EndDate", endDate);
			parameters.Add("BrandID", brandID);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetBinOrderReport", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		
		
		public void GetStaticPoolSummary(out DataTable dt, string campaignID, string monthDate, string productTypeID, string continuity) {
			Hashtable parameters = new Hashtable();
			parameters.Add("CampaignID", campaignID);
			parameters.Add("MonthDate", monthDate);
			parameters.Add("ProductTypeID", productTypeID);
			parameters.Add("Continuity", continuity);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetStaticPoolSummary", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetStaticPoolAfterMarket(out DataTable dt, string campaignID, string monthDate, string productTypeID, string continuity) {
			Hashtable parameters = new Hashtable();
			parameters.Add("CampaignID", campaignID);
			parameters.Add("MonthDate", monthDate);
			parameters.Add("ProductTypeID", productTypeID);
			parameters.Add("Continuity", continuity);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetStaticPoolAfterMarket", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetStaticPoolSummaryByDay(out DataTable dt, string campaignID, string day, string productTypeID, string continuity) {
			Hashtable parameters = new Hashtable();
			parameters.Add("CampaignID", campaignID);
			parameters.Add("Day", day);
			parameters.Add("ProductTypeID", productTypeID);
			parameters.Add("Continuity", continuity);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetStaticPoolSummaryByDay", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetCogsByCampaign(out DataTable dt, string campaignID, string startDate, string endDate) {
			Hashtable parameters = new Hashtable();
			parameters.Add("CampaignID", campaignID);
			parameters.Add("StartDate", startDate);
			parameters.Add("EndDate", endDate);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetCogsByCampaign", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void CreditReportDump(out DataTable dt, Hashtable parameters) {
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_CreditReportDump", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void ContinuityPivot(out DataTable dt, Hashtable parameters) {
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_ContinuityPivot", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetPaintNiteReport(out DataTable dt) {
			Hashtable parameters = new Hashtable();
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetPaintNiteReport", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void VoucherReportByPartner(out DataTable dt, Hashtable parameters) {
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_VoucherReportByPartner", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
	}
}