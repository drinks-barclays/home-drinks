using Data;
using System.Collections;
using System.Data;
using System.Web;

namespace Wine
{
    public class Continuity
    {
        // DataLayer object
        //  NEED: Add connectionstring
        public DataUtility d = new DataUtility(System.Configuration.ConfigurationManager.ConnectionStrings["conn"].ConnectionString);// "Server=localhost;User ID=Wine;password=#3web;Database=WineFolks;Persist Security Info=True");
		
		public void GetContinuity(out DataTable dt, Hashtable parameters) {
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_ContinuityGet", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public void GetEligible(out DataTable dt, string id) {
			Hashtable parameters = new Hashtable();
			parameters.Add("ContinuityConfigurationID", id);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetContinuityEligible", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public void GetEligible(out DataTable dt) {
			Hashtable parameters = new Hashtable();
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetContinuityEligible", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public void GetContinuityRaw(out DataTable dt, Hashtable parameters) {			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetContinuityRaw", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public void GetContinuityPerformance(out DataTable dt, Hashtable parameters) {			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_ContinuityPerformanceByDay", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		public void GetEligibleToClose(out DataTable dt) {
			Hashtable parameters = new Hashtable();
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetContinuityEligibleToClose", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		public void GetEligibleToClose(out DataTable dt, string id) {
			Hashtable parameters = new Hashtable();
			parameters.Add("ContinuityConfigurationID", id);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetContinuityEligibleToClose", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		public void GetEligibleToCloseBonus(out DataTable dt, string campaignID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("CampaignID", campaignID);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetContinuityEligibleToCloseBonus", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public void GetEligibleToClosePrePaid(out DataTable dt) {
			Hashtable parameters = new Hashtable();
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetContinuityEligibleToClosePrePaid", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public void IncrementShipments(string id) {
			Hashtable parameters = new Hashtable();
			parameters.Add("ContinuitySubscriptionID", id);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_ContinuityIncrementShipment", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void IncrementPrePaid(string id) {
			Hashtable parameters = new Hashtable();
			parameters.Add("ContinuitySubscriptionID", id);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_IncrementPrePaid", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetShipments(out DataTable dt, string id) {
			Hashtable parameters = new Hashtable();
			parameters.Add("ContinuityID", id);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_ContinuityGetShipments", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public void GetProductForShipment(out DataTable dt) {
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_ContinuityShipmentProductGet", new Hashtable());
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public void AddShipment(string continuityID, string productID, string preferenceID, string startDate, string endDate, string shipmentBegin, string shipmentEnd) {
			Hashtable parameters = new Hashtable();
			parameters.Add("ContinuityID", continuityID);
			parameters.Add("ProductID", productID);
			parameters.Add("PreferenceID", preferenceID);
			parameters.Add("StartDate", startDate);
			parameters.Add("EndDate", endDate);
			parameters.Add("ShipmentBegin", shipmentBegin);
			parameters.Add("ShipmentEnd", shipmentEnd);

            Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddContinuityProduct", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public void ContinuityProductInactivate(string productID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("ContinuityProductID", productID);

            Result r = new Result();
            r = d.ExecuteNonQuery("ContinuityProductInactivate", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public void GetCancelReasons(out DataTable dt) {
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetCancelReasons", new Hashtable());
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public string GetCustomerEmail(string id) {
			Hashtable parameters = new Hashtable();
			parameters.Add("CustomerID", id);
			
			string email = "";
			Result r = new Result();
            r = d.ExecuteScalar(out email, "sp_GetCustomerEmail", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return email;
		}
		
		public void SkipMembership(string id, string skipSpan) {
			Hashtable parameters = new Hashtable();
			parameters.Add("ContinuitySubscriptionID", id);
			parameters.Add("SkipSpan", skipSpan);

            Result r = new Result();
            r = d.ExecuteNonQuery("sp_SkipMembership", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void SkipMembershipUser(string id, string skipSpan) {
			Hashtable parameters = new Hashtable();
			parameters.Add("UID", id);
			parameters.Add("SkipSpan", skipSpan);

            Result r = new Result();
            r = d.ExecuteNonQuery("sp_SkipMembership", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void CancelMembership(string id, string reasonID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("ContinuitySubscriptionID", id);
			parameters.Add("ReasonID", reasonID);

            Result r = new Result();
            r = d.ExecuteNonQuery("sp_CancelMembership", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void ChangePreference(string id, string preferenceID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("ContinuitySubscriptionID", id);
			parameters.Add("PreferenceTypeID", preferenceID);

            Result r = new Result();
            r = d.ExecuteNonQuery("sp_ChangePreference", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void ChangePreferenceUser(string uid, string preferenceID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("UID", uid);
			parameters.Add("PreferenceTypeID", preferenceID);

            Result r = new Result();
            r = d.ExecuteNonQuery("sp_ChangePreference", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		/* automated close added 7/30/2014 */
		public void GetContinuityConfigsToClose(out DataTable dt) {
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetContinuityConfigsToClose", new Hashtable());
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		/* product details added 7/31/2015 */
		public void GetEligibleProduct(out DataTable dt, string id) {
			Hashtable parameters = new Hashtable();
			parameters.Add("ContinuityConfigurationID", id);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetContinuityEligibleProduct", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
	}
}