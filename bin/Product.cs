using System.Data;
using Data;
using System.Collections;
using System.Web;
using System.Configuration;

namespace Wine
{
    public class Product
    {

        // DataLayer object
        //  NEED: Add connectionstring
		public DataUtility d = new DataUtility(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);

        // different types of product
        public enum ProductType
        {
            Bottle, Case, Pack, Literature, NonWine
        }

        // Add Inventory
        //      sproc: sp_ProductAdd
        //        @BrandID int,
        //        @Code nvarchar(10),
        //        @Name nvarchar(50),
        //        @ProductTypeID int,
        //        @Cost money,
        //        @Price money,
        //        @Discountable bit,
        //        @DateExpires datetime,
        //        @Available bit,
        //        @Quantity int,
        //        @ShortDescription nvarchar(500),
        //        @RedundantProductID int,
        //        @PurchaseOrder nchar(10),
        //        @DateOrdered datetime,
        //        @ArrivalDate datetime,
        //        @LongDescription text,
        //        @VarietalID int,
        //        @RegionID int,
        //        @CountryID int,
        //        @ProducerID int,
        //        @AppellationID int,
        //        @StyleID int,
        //        @AlcoholPercentage int,
        //        @BottleVolume int
        public string Add(int brandID, string code,  string name, string vintage,int productTypeID, double cost, double price,
                            bool discountable, string dateExpires, bool available, int quantity,
                            string shortDescription, int redundantProductID, string purchaseOrder,
                            string dateOrdered, string arrivalDate, string headline, string longDescription,
                            int varietalID, int regionID, int countryID, int producerID, int appellationID,
                            int styleID, double alcoholPercentage, int bottleVolume, bool continuity, bool active)
        {

            Hashtable parameters = new Hashtable();
            parameters.Add("BrandID", brandID);
            parameters.Add("Code", code);
            parameters.Add("Name", name);
            parameters.Add("Vintage", vintage);
            parameters.Add("ProductTypeID", productTypeID);
            parameters.Add("Cost", cost);
            parameters.Add("Price", price);
            parameters.Add("Discountable", discountable);
            parameters.Add("DateExpires", dateExpires);
            parameters.Add("Available", available);
            parameters.Add("Active", active);
            parameters.Add("Quantity", quantity);
            parameters.Add("ShortDescription", shortDescription);
            parameters.Add("RedundantProductID", redundantProductID);
            parameters.Add("PurchaseOrder", purchaseOrder);
            parameters.Add("DateOrdered", dateOrdered);
            parameters.Add("ArrivalDate", arrivalDate);
            parameters.Add("Headline", headline);
            parameters.Add("LongDescription", longDescription);
            parameters.Add("VarietalID", varietalID);
            parameters.Add("RegionID", regionID);
            parameters.Add("CountryID", countryID);
            parameters.Add("ProducerID", producerID);
            parameters.Add("AppellationID", appellationID);
            parameters.Add("StyleID", styleID);
            parameters.Add("AlcoholPercentage", alcoholPercentage);
            parameters.Add("BottleVolume", bottleVolume);
            parameters.Add("Continuity", continuity);

            string productID;
            Result r = new Result();
            r = d.ExecuteScalar(out productID, "sp_ProductAdd", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            return productID;
        }
		
		/* new add - accommodates multiple brands on one product id */
		public string Add(Hashtable parameters)
        {

            string productID;
            Result r = new Result();
            r = d.ExecuteScalar(out productID, "sp_ProductAdd_", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            return productID;
        }
		
		public string AddBrandSku(Hashtable parameters)
        {

            string productID;
            Result r = new Result();
            r = d.ExecuteScalar(out productID, "sp_ProductAddBrandSku", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            return productID;
        }
		
		public string EditBrandSku(Hashtable parameters)
        {

            string productID;
            Result r = new Result();
            r = d.ExecuteScalar(out productID, "sp_ProductEditBrandSku", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            return productID;
        }
		
		public void GetBrandSku(out DataTable dt, Hashtable parameters)
        {
            d.ExecuteDataTable(out dt, "sp_GetBrandSkus", parameters);
        }
		
		public void AddGroupWine(int parentID, int quantity, int childID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("ParentID", parentID);
			parameters.Add("Quantity", quantity);
			parameters.Add("ChildID", childID);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_ProductAddChild", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
				 HttpContext.Current.Response.Write(r.Exception.Message);
				 HttpContext.Current.Response.End();
			}
			
			
		}
		
		public DataSet Get(string id) {
			DataSet ds = new DataSet();
			DataTable t = new DataTable();
			Hashtable parameters = new Hashtable();
			parameters.Add("ProductID", id);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out t, "sp_ProductGet", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			ds.Tables.Add(t);
			
			r = null;
			t = null;
			r = d.ExecuteDataTable(out t, "sp_ProductGetGroup", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			ds.Tables.Add(t);
			
			return ds;
		}
		
		public DataTable GetParents(string id)
        {
            DataTable dt;
			Hashtable parameters = new Hashtable();
			parameters.Add("ProductID", id);
            d.ExecuteDataTable(out dt, "sp_ProductGetParents", parameters);
            return dt;
        }
		
		public DataTable GetPackChildren(string id)
        {
            DataTable dt;
			Hashtable parameters = new Hashtable();
			parameters.Add("ProductID", id);
            d.ExecuteDataTable(out dt, "sp_ProductGetPackChildren", parameters);
            return dt;
        }
		
		public DataTable GetChildren(string id)
        {
			return GetChildren(id, "1");
        }
		
		public DataTable GetChildren(string id, string brandID) {
            DataTable dt;
			Hashtable parameters = new Hashtable();
			parameters.Add("ProductID", id);
			parameters.Add("BrandID", brandID);
            d.ExecuteDataTable(out dt, "sp_ProductGetChildren", parameters);
            return dt;
		}

        public bool Edit(int productID,int brandID, string code,  string name, string vintage,int productTypeID, double cost, double price,
                            bool discountable, string dateExpires, bool available, int quantity,
                            string shortDescription, int redundantProductID, string purchaseOrder,
                            string dateOrdered, string arrivalDate, string headline, string longDescription,
                            int varietalID, int regionID, int countryID, int producerID, int appellationID,
                            int styleID, double alcoholPercentage, int bottleVolume, bool continuity, bool active)
        {

            Hashtable parameters = new Hashtable();
            parameters.Add("ProductID", productID);
            parameters.Add("BrandID", brandID);
            parameters.Add("Code", code);
            parameters.Add("Name", name);
            parameters.Add("Vintage", vintage); 
            parameters.Add("ProductTypeID", productTypeID);
            parameters.Add("Cost", cost);
            parameters.Add("Price", price);
            parameters.Add("Discountable", discountable);
            parameters.Add("DateExpires", dateExpires);
            parameters.Add("Available", available);
            parameters.Add("Active", active);
            parameters.Add("Quantity", quantity);
            parameters.Add("ShortDescription", shortDescription);
            parameters.Add("RedundantProductID", redundantProductID);
            parameters.Add("PurchaseOrder", purchaseOrder);
            parameters.Add("DateOrdered", dateOrdered);
            parameters.Add("ArrivalDate", arrivalDate);
            parameters.Add("Headline", headline);
            parameters.Add("LongDescription", longDescription);
            parameters.Add("VarietalID", varietalID);
            parameters.Add("RegionID", regionID);
            parameters.Add("CountryID", countryID);
            parameters.Add("ProducerID", producerID);
            parameters.Add("AppellationID", appellationID);
            parameters.Add("StyleID", styleID);
            parameters.Add("AlcoholPercentage", alcoholPercentage);
            parameters.Add("BottleVolume", bottleVolume);
            parameters.Add("Continuity", continuity);

            Result r = new Result();
            r = d.ExecuteNonQuery("sp_ProductEdit", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return true;
			
        }
		
		public bool Edit(int productID, string code, string name, string vintage,int productTypeID, double cost, double price,
                            bool discountable, string dateExpires, int quantity,
                            string shortDescription, int redundantProductID, string purchaseOrder,
                            string dateOrdered, string arrivalDate, string headline, string longDescription,
                            int varietalID, int regionID, int countryID, int producerID, int appellationID,
                            int styleID, double alcoholPercentage, int bottleVolume, bool continuity, bool active)
        {

            Hashtable parameters = new Hashtable();
            parameters.Add("ProductID", productID);
            parameters.Add("Code", code);
            parameters.Add("Name", name);
            parameters.Add("Vintage", vintage); 
            parameters.Add("ProductTypeID", productTypeID);
            parameters.Add("Cost", cost);
            parameters.Add("Price", price);
            parameters.Add("Discountable", discountable);
            parameters.Add("DateExpires", dateExpires);
            parameters.Add("Active", active);
            parameters.Add("Quantity", quantity);
            parameters.Add("ShortDescription", shortDescription);
            parameters.Add("RedundantProductID", redundantProductID);
            parameters.Add("PurchaseOrder", purchaseOrder);
            parameters.Add("DateOrdered", dateOrdered);
            parameters.Add("ArrivalDate", arrivalDate);
            parameters.Add("VarietalID", varietalID);
            parameters.Add("RegionID", regionID);
            parameters.Add("CountryID", countryID);
            parameters.Add("ProducerID", producerID);
            parameters.Add("AppellationID", appellationID);
            parameters.Add("StyleID", styleID);
            parameters.Add("AlcoholPercentage", alcoholPercentage);
            parameters.Add("BottleVolume", bottleVolume);
            parameters.Add("Continuity", continuity);

            Result r = new Result();
            r = d.ExecuteNonQuery("sp_ProductEdit", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return true;
			
        }
		
		public void Edit(Hashtable parameters) {
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_ProductEdit", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		
		public DataTable Lookup(string id)
        {
            DataTable dt;
			Hashtable parameters = new Hashtable();
            parameters.Add("ProductID", id);
            d.ExecuteDataTable(out dt, "sp_ProductLookup", parameters);
            return dt;
        }

        public bool Remove(string id)
        {
            return true;
        }

        public bool Group(string name, string price, string description, string whatever, Hashtable products)
        {
            // add concept as a product of type pack

            // add the children

            // kosher
            return true;
        }

        public enum SearchType
        {
            Description, Name, Region, Country, Varietal, Wildcard, Code, Promo, ProductType, All
        }
		
		public DataTable Search() {
			return Search(new Hashtable());
		}

        // Search for product(s)
        //      can be used to display sets by promo as well
        //      sproc: sp_ProductSearch
        public DataTable Search(Hashtable parameters)
        {
            DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_ProductSearch", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
        }
		
		public DataTable SearchInventory() {
			return SearchInventory(new Hashtable());
		}

        // Search for product(s)
        //      can be used to display sets by promo as well
        //      sproc: sp_ProductSearch
        public DataTable SearchInventory(Hashtable parameters)
        {
            DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_InventorySearch", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
        }
		
		public DataTable GetAvailableStyles() {
            DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetAvailableStyles", new Hashtable());
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public DataTable GetAvailableVarietals() {
            DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetAvailableVarietals", new Hashtable());
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;	
		}
		
		public DataTable GetAvailableRegions() {
            DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetAvailableRegions", new Hashtable());
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;	
		}
		
		public DataTable GetAvailableProducers() {
            DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetAvailableProducers", new Hashtable());
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;	
		}
		
        public DataTable GetProducers()
        {
            DataTable dt;
            d.ExecuteDataTable(out dt, "sp_Producers", new Hashtable());
            return dt;
        }

        public DataTable GetVarietals()
        {
            DataTable dt;
            d.ExecuteDataTable(out dt, "sp_ProductVarietals", new Hashtable());
            return dt;
        }

        public DataTable GetStyles()
        {
            DataTable dt;
            d.ExecuteDataTable(out dt, "sp_ProductStyles", new Hashtable());
            return dt;
        }

        public DataTable GetRegions()
        {
            DataTable dt;
            d.ExecuteDataTable(out dt, "sp_ProductRegions", new Hashtable());
            return dt;
        }
		
		public DataTable GetAppellations()
        {
            DataTable dt;
            d.ExecuteDataTable(out dt, "sp_ProductAppellations", new Hashtable());
            return dt;
        }
		
		public DataTable GetBrands()
        {
            DataTable dt;
            d.ExecuteDataTable(out dt, "sp_Brand", new Hashtable());
            return dt;
        }
        public void Dispose()
        {
            d = null;
        }
		
		public string AddVendor(Hashtable parameters)
        {

            string vendorID;
            Result r = new Result();
            r = d.ExecuteScalar(out vendorID, "sp_AddVendor", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            return vendorID;
        }
		
		public void AddVendorState(string state, string vendorID)
        {
			Hashtable parameters = new Hashtable();
            parameters.Add("VendorID", vendorID);
            parameters.Add("State", state);
            Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddVendorState", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
        }
		
		public DataTable GetVendors()
        {
            DataTable dt;
            d.ExecuteDataTable(out dt, "sp_GetVendors", new Hashtable());
            return dt;
        }
		
		public void AddBinProduct(Hashtable parameters)
        {
            Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddBinProduct", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
        }
		
		public DataTable GetBinFeatures(string featureMonth)
        {
            DataTable dt;
			Hashtable parameters = new Hashtable();
            parameters.Add("FeatureMonth", featureMonth);
            d.ExecuteDataTable(out dt, "sp_BinFeatures", parameters);
            return dt;
        }
		
		public DataTable GetBinProductToFeature()
        {
            DataTable dt;
			Hashtable parameters = new Hashtable();
            d.ExecuteDataTable(out dt, "sp_GetBinProductToFeature", parameters);
            return dt;
        }
		
		public void AddBinFeature(string id, string featureDate)
        {
			Hashtable parameters = new Hashtable();
            parameters.Add("BinProductID", id);
            parameters.Add("FeatureDate", featureDate);
            Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddBinFeature", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
        }
		
		
		public DataTable GetInventoryPacks()
        {
            DataTable dt;
			Hashtable parameters = new Hashtable();
            d.ExecuteDataTable(out dt, "sp_InventoryManagementPacks", parameters);
            return dt;
        }
		
		public void AllocateInventory(Hashtable parameters)
        {
            Result r = new Result();
            r = d.ExecuteNonQuery("sp_InventoryAllocation", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
        }
		
		
		public void GetCampaignDetail(out DataTable dt, Hashtable parameters)
        {
            d.ExecuteDataTable(out dt, "sp_GetCampaignOrderDetail", parameters);
        }
		
		public string CloneProduct(string id)
        {
			Hashtable parameters = new Hashtable();
            parameters.Add("ProductID", id);
            Result r = new Result();
			string output = "";
            r = d.ExecuteScalar(out output, "sp_CloneProduct", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return output;
        }
		
		public DataTable GetWarehouseInventory()
        {
            DataTable dt;
			Hashtable parameters = new Hashtable();
            d.ExecuteDataTable(out dt, "sp_ProductGetWarehouseInventory", parameters);
            return dt;
        }
    }
}
