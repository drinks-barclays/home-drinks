using System;
using System.Collections;
using Data;
using System.Web;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Litle.Sdk;
using System.Configuration;

namespace Wine
{
    public class Payment_
    {
		public DataUtility d = new DataUtility(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);

		public string response_code = ""; 
		public string auth_code = "";
		public string AuthCode { get { return auth_code; } set { auth_code = value; }}
		public string avs_response = "";
		public string trans_id = "";
		public string TransactionCode { get { return trans_id; } set { trans_id = value; }}
		public string cvv_response = "";
		public string reason_code = "";
		public string debug = "";
		public string token = "";
		public string DebugString { get { return debug; } set { debug = value; }}
		public string MID = "6105109"; /*Barclays: 6105109, Heartwood: 6106204, Troon: 6105112*/
		public string ReportGroup = "BarclaysWine";
		public LitleOnline litle = new LitleOnline();
		
		public void SetUpLitle()
        {
            Dictionary<string, string> config = new Dictionary<string, string>();
            config.Add("url", "https://transact.litle.com/vap/communicator/online");
            config.Add("reportGroup", ReportGroup);
            config.Add("username", "u817600664037212242");
            config.Add("version", "8.13");
            config.Add("timeout", "65");
            config.Add("merchantId", MID);
            config.Add("password", "oKbJ4NuKHUnqCix");
            config.Add("printxml", "true");
            litle = new LitleOnline(config);
			
			// resets
			response_code = "";
			auth_code = "";
			avs_response = "";
			trans_id = "";
			cvv_response = "";
			reason_code = "";
			debug = "";
			token = "";
			MID = "";
        }
		
		/* note: added cvv as the second arg (need to change all pages calling this) */
		
		public bool DoPayment(string digits, string cvv, string exp_date, string amount, string first_name, string last_name, string postal_code, string x_trans_id, int nill)
		{
			SetUpLitle();
            authorization authorization = new authorization();
			authorization.id = System.DateTime.Now.Ticks.ToString();
            authorization.orderId = (System.DateTime.Now.Ticks+1).ToString();
            authorization.amount = (long)Convert.ToDouble(amount.Replace(".", ""));
            authorization.orderSource = orderSourceType.ecommerce;
	     
			if (IsValidNumber(digits)) {
				contact contact = new contact();
				contact.name = first_name + " " + last_name;
            	if (HttpContext.Current.Request["billing_address"] != null) contact.addressLine1 = HttpContext.Current.Request["billing_address"].ToString();
            	if (HttpContext.Current.Request["billing_city"] != null) contact.city = HttpContext.Current.Request["billing_city"].ToString();
            	if (HttpContext.Current.Request["billing_state"] != null) contact.state = HttpContext.Current.Request["billing_state"].ToString();
				contact.zip = postal_code;
				contact.country = countryTypeEnum.US;
				authorization.billToAddress = contact;
				cardType card = new cardType();
				card.type = GetCardType(digits);
				card.number = digits;
				card.expDate = exp_date;
				card.cardValidationNum = cvv;
				authorization.card = card;
			} else {
				// it's a token
				cardTokenType lToken = new cardTokenType();
				lToken.litleToken = digits;
				lToken.expDate = exp_date;
				authorization.token = lToken;
			}
 
            authorizationResponse response = litle.Authorize(authorization);
			
			response_code = response.response;
            debug = response.message;
            trans_id = response.litleTxnId.ToString();
            auth_code = response.authCode;
			
			try {
				token = response.tokenResponse.litleToken;
			} catch {
				token = digits;	
			}
			
			LogTransaction(auth_code, trans_id, response_code, amount, "0", "AUTH", null);
			
			if (response.message == "Approved") {
				return true;
			} else {
				return false;
			}
		}
		
		public bool DoCapture(string x_trans_id, string amount, string invoiceID) {
			SetUpLitle();
			
			capture capture = new capture();
            capture.litleTxnId = Convert.ToInt64(x_trans_id);
            captureResponse response = litle.Capture(capture);
			
			response_code = response.response;
            debug = response.message;
            trans_id = response.litleTxnId.ToString();
			
			LogTransaction("0", trans_id, response_code, amount, "0","CAPTURE", invoiceID);
			
			if (response.message == "Approved") {
				return true;
			} else {
				return false;
			}
		}
		
		public bool DoCredit(string x_trans_id, string amount, string invoiceID) {
			SetUpLitle();
			
			credit credit = new credit();
            credit.litleTxnId = Convert.ToInt64(x_trans_id);
			credit.amount = (long)Convert.ToDouble(amount.Replace(".", ""));
            creditResponse response = litle.Credit(credit);
			
			response_code = response.response;
            debug = response.message;
            trans_id = response.litleTxnId.ToString();
			
			LogTransaction("0", trans_id, response_code, amount, "0", "CREDIT", invoiceID);
			
			if (response.message == "Approved") {
				// make sure we add it as an invoice credit in the db
				
				return true;
			} else {
				return false;
			}
		}
		
		public bool DoVoid(string x_trans_id) {
			SetUpLitle();
			
			authReversal reversal = new authReversal();
            reversal.litleTxnId = Convert.ToInt64(x_trans_id);
            authReversalResponse response = litle.AuthReversal(reversal);
			
			response_code = response.response;
            debug = response.message;
            trans_id = response.litleTxnId.ToString();
			
			LogTransaction("0", trans_id, response_code, "0", "0", "VOID", null);
			
			if (response.message == "Approved") {
				return true;
			} else {
				return false;
			}
		}
		
		private void LogTransaction(string x_auth_code, string x_tran_id, string x_response_code, string x_amount, string x_cvv_response, string x_tran_type, string x_invoice_id) {
			/* sp_AddPayment
					@AuthCode nchar(10),
					@TransactionCode nchar(30),
					@ResponseCode int,
					@Amount money,
					@CVVResponse nchar(10),
					@TransactionType nchar(10) */
					
			if (x_auth_code == null) x_auth_code = "0";
			Hashtable parameters = new Hashtable();
            parameters.Add("AuthCode", x_auth_code);
            parameters.Add("TransactionCode", x_tran_id);
            parameters.Add("ResponseCode", x_response_code);
            parameters.Add("Amount", x_amount);
            parameters.Add("CVVResponse", x_cvv_response);
            parameters.Add("TransactionType", x_tran_type);
            parameters.Add("InvoiceID", x_invoice_id);

            Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddPayment", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		private const string cardRegex = "^(?:(?<Visa>4\\d{3})|(?<MasterCard>5[1-5]\\d{2})|(?<Discover>6011)|(?<DinersClub>(?:3[68]\\d{2})|(?:30[0-5]\\d))|(?<Amex>3[47]\\d{2}))([ -]?)(?(DinersClub)(?:\\d{6}\\1\\d{4})|(?(Amex)(?:\\d{6}\\1\\d{5})|(?:\\d{4}\\1\\d{4}\\1\\d{4})))$";

		public methodOfPaymentTypeEnum GetCardType(string cardNum)
		{
			Regex cardTest = new Regex(cardRegex);
			GroupCollection gc = cardTest.Match(cardNum).Groups;
			if (gc["Amex"].Success)
			{
				return methodOfPaymentTypeEnum.AX;
			}
			else if (gc["MasterCard"].Success)
			{
				return methodOfPaymentTypeEnum.MC;
			}
			else if (gc["Visa"].Success)
			{
				return methodOfPaymentTypeEnum.VI;
			}
			else if (gc["Discover"].Success)
			{
				return methodOfPaymentTypeEnum.DI;
			}
			else
			{
				return methodOfPaymentTypeEnum.DC;
			}
		}
		
		private static bool IsValidNumber(string number)
		{
			int[] DELTAS = new int[] { 0, 1, 2, 3, 4, -4, -3, -2, -1, 0 };
			int checksum = 0;
			char[] chars = number.ToCharArray();
			for (int i = chars.Length - 1; i > -1; i--)
			{
			   int j = ((int)chars[i]) - 48;
			   checksum += j;
			   if (((i - chars.Length) % 2) == 0)
				   checksum += DELTAS[j];
			}
		
			return ((checksum % 10) == 0);
		}
	}
}	