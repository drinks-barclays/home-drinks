using System;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Collections;
using Data;
using System.Data;
using System.Configuration;
using System.Web;

namespace Wine
{
    public class Payment
    {
		public DataUtility d = new DataUtility(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);

        public string x_login = "6S7LP7dv7";
		public string x_tran_key = "9kqYk48Y69Fbvn6Z"; 
		private string post_url = "https://secure.authorize.net/gateway/transact.dll";
		
		public string response_code = "";
		public string auth_code = "";
		public string AuthCode { get { return auth_code; } set { auth_code = value; }}
		public string avs_response = "";
		public string trans_id = "";
		public string TransactionCode { get { return trans_id; } set { trans_id = value; }}
		public string cvv_response = "";
		public string reason_code = "";
		public string debug = "";
		public string DebugString { get { return debug; } set { debug = value; }}
		
		public bool DoPayment(string digits, string exp_date, string amount, string first_name, string last_name, string postal_code, string x_trans_id, int type)
		{
			Hashtable post_values = new Hashtable();

            //the API Login ID and Transaction Key must be replaced with valid values
            post_values.Add("x_login", x_login);
            post_values.Add("x_tran_key", x_tran_key);

            post_values.Add("x_delim_data", "TRUE");
            post_values.Add("x_delim_char", '|');
            post_values.Add("x_relay_response", "FALSE");
            //post_values.Add("x_invoice_num", HttpContext.Current.Request["id"]);

			string tran_type = "";
			if (type == 1) {
				// authorize
            	post_values.Add("x_type", "AUTH_ONLY");
				tran_type = "AUTH_ONLY";
			} else if (type == 2) {
				// auth and capture
				post_values.Add("x_type", "AUTH_CAPTURE");
				tran_type = "AUTH_CAPTURE";
			}
			
            post_values.Add("x_method", "CC");
            post_values.Add("x_card_num", digits);
            post_values.Add("x_exp_date", exp_date);

            post_values.Add("x_amount", amount);

            post_values.Add("x_first_name", first_name);
            post_values.Add("x_last_name", last_name);
            post_values.Add("x_zip", postal_code);            

            string post_response = sendData(post_values);
            Array response_array = post_response.Split('|');
			
			Debug(response_array);

            // parse the following
			// 1 = response code (1 = approved, 2 = declined, 3 = error, 4 review)
			// 4 = text of reason
			// 5 = auth code
			// 6 = avs response
			// 7 = trans_id
			// 39 = ccv response (M = match, N = no match, P = not processed, S = should have been present, U = issuer unable to process)
            int i = 1;
			foreach (string value in response_array) {
				if (i == 1) response_code = value; // response code
				if (i == 4) reason_code = value; // reason code
				if (i == 5) auth_code = value; // auth code
 				if (i == 6) avs_response =  value; // avs
				if (i == 7) trans_id = value; // transaction id
				if (i == 39) cvv_response = value; // cvv response
				i++;
			}
			
			LogTransaction(auth_code, trans_id, response_code, amount,cvv_response, tran_type);
			
			if (response_code == "1") {
				return true;
			} else {
				return false;
			}
		}
		
		public bool DoCapture(string x_trans_id, string amount) {
			Hashtable post_values = new Hashtable();
            post_values.Add("x_login", x_login);
            post_values.Add("x_tran_key", x_tran_key);
            post_values.Add("x_delim_data", "TRUE");
            post_values.Add("x_delim_char", '|');
            post_values.Add("x_relay_response", "FALSE");
			post_values.Add("x_type", "PRIOR_AUTH_CAPTURE");
			post_values.Add("x_trans_id", x_trans_id); 
			
            string post_response = sendData(post_values);
            Array response_array = post_response.Split('|');
			
			// write debug info
			Debug(response_array);
			
			int i = 1;
			foreach (string value in response_array) {
				if (i == 1) response_code = value; // response code
				if (i == 4) reason_code = value; // reason code
				if (i == 5) auth_code = value; // auth code
				if (i == 7) trans_id = value; // transaction id
				i++;
			}
			
			LogTransaction(auth_code, trans_id, response_code, amount, cvv_response, "PRIOR_AUTH_CAPTURE");
			
			if (response_code == "1") {
				return true;
			} else {
				return false;
			}
		}
		
		
		public bool DoCapture(string x_trans_id, string amount, bool n) {
			Hashtable post_values = new Hashtable();
            post_values.Add("x_login", x_login);
            post_values.Add("x_tran_key", x_tran_key);
            post_values.Add("x_delim_data", "TRUE");
            post_values.Add("x_delim_char", '|');
            post_values.Add("x_relay_response", "FALSE");
			post_values.Add("x_type", "PRIOR_AUTH_CAPTURE");
			post_values.Add("x_trans_id", x_trans_id);
			post_values.Add("x_amount", amount);
			
            string post_response = sendData(post_values);
            Array response_array = post_response.Split('|');
			
			// write debug info
			Debug(response_array);
			
			int i = 1;
			foreach (string value in response_array) {
				if (i == 1) response_code = value; // response code
				if (i == 4) reason_code = value; // reason code
				if (i == 5) auth_code = value; // auth code
				if (i == 7) trans_id = value; // transaction id
				i++;
			}
			
			LogTransaction(auth_code, trans_id, response_code, amount, cvv_response, "PRIOR_AUTH_CAPTURE");
			
			if (response_code == "1") {
				return true;
			} else {
				return false;
			}
		}
		
		
		public bool DoVoid(string x_trans_id) {
			Hashtable post_values = new Hashtable();
            post_values.Add("x_login", x_login);
            post_values.Add("x_tran_key", x_tran_key);
            post_values.Add("x_delim_data", "TRUE");
            post_values.Add("x_delim_char", '|');
            post_values.Add("x_relay_response", "FALSE");
			post_values.Add("x_trans_id", x_trans_id); 
			post_values.Add("x_type", "VOID");
			
            string post_response = sendData(post_values);
            Array response_array = post_response.Split('|');
			
			// write debug info
			Debug(response_array);
			
			int i = 1;
			foreach (string value in response_array) {
				if (i == 1) response_code = value; // response code
				if (i == 4) reason_code = value; // reason code
				if (i == 5) auth_code = value; // auth code
 				if (i == 6) avs_response =  value; // avs
				if (i == 7) trans_id = value; // transaction id
				if (i == 39) cvv_response = value; // cvv response
				i++;
			}
			
			if (response_code == "1") {
				return true;
			} else {
				return false;
			}
							
		}
		
		public bool DoRefund(string x_trans_id, string digits, string amount) {
			Hashtable post_values = new Hashtable();
            post_values.Add("x_login", x_login);
            post_values.Add("x_tran_key", x_tran_key);
            post_values.Add("x_delim_data", "TRUE");
            post_values.Add("x_delim_char", '|');
            post_values.Add("x_relay_response", "FALSE");
			post_values.Add("x_trans_id", x_trans_id); 
            post_values.Add("x_card_num", digits);
			post_values.Add("x_amount", amount);
			post_values.Add("x_type", "CREDIT");
			
            string post_response = sendData(post_values);
            Array response_array = post_response.Split('|');
			
			// write debug info
			Debug(response_array);
			
			int i = 1;
			foreach (string value in response_array) {
				if (i == 1) response_code = value; // response code
				if (i == 4) reason_code = value; // reason code
				if (i == 5) auth_code = value; // auth code
 				if (i == 6) avs_response =  value; // avs
				if (i == 7) trans_id = value; // transaction id
				if (i == 39) cvv_response = value; // cvv response
				i++;
			}
			
			if (response_code == "1") {
				return true;
			} else {
				return false;
			}
							
		}
		
		public void Reset() {
			response_code = "";
			auth_code = "";
			avs_response = "";
			trans_id = "";
			cvv_response = "";
			reason_code = "";
			debug = "";
			x_login = "6S7LP7dv7";
			x_tran_key = "9kqYk48Y69Fbvn6Z";
		}
		
		private void LogTransaction(string x_auth_code, string x_tran_id, string x_response_code, string x_amount, string x_cvv_response, string x_tran_type) {
			/* sp_AddPayment
					@AuthCode nchar(10),
					@TransactionCode nchar(30),
					@ResponseCode int,
					@Amount money,
					@CVVResponse nchar(10),
					@TransactionType nchar(10) */
					
			Hashtable parameters = new Hashtable();
            parameters.Add("AuthCode", x_auth_code);
            parameters.Add("TransactionCode", x_tran_id);
            parameters.Add("ResponseCode", x_response_code);
            parameters.Add("Amount", x_amount);
            parameters.Add("CVVResponse", x_cvv_response);
            parameters.Add("TransactionType", x_tran_type);

            Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddPayment", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		private void Debug(Array response_array) {
			string result = "<OL> \n";
			int i = 1;
			foreach (string value in response_array) {
            	if (i == 1) result += "<li>Response Code: " + value; // response code
            	if (i == 4) result += "<li>Reason: " + value; // reason code
            	if (i == 5) result += "<li>Auth code: " + value; // auth code
            	if (i == 6) result += "<li>AVS Response: " + value; // avs
            	if (i == 7) result += "<li>Transaction ID: " + value; // tran id
            	if (i == 10) result += "<li>Amount: " + value; // amount
            	if (i == 12) result += "<li>Transaction type: " + value; // tran type
            	if (i == 39) result += "<li>CVV Response: " + value; // cvv response
				i++;
			}
            result += "</OL> \n";
			
			this.DebugString = result;
		}
		
		private string sendData(Hashtable post_values) {
			string post_string = "";;
			foreach(DictionaryEntry field in post_values)
            {
                post_string += field.Key + "=" + field.Value + "&";
            }
            post_string = post_string.TrimEnd('&');
            
            // create an HttpWebRequest object to communicate with Authorize.net
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(post_url);
            objRequest.Method = "POST";
            objRequest.ContentLength = post_string.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter  = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(post_string);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            String post_response;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()) )
            {
                post_response = responseStream.ReadToEnd();
                responseStream.Close();
            }
			
			return post_response;
		}
	}
}