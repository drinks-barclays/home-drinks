using System;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Collections;
using Data;
using System.Data;
using System.Web;

namespace Wine
{
    public class Authentication
    {
		string post_url = "https://www.google.com/accounts/ClientLogin";
		public bool Authenticate(string userID, string password) {
			Hashtable post_values = new Hashtable();
			post_values.Add("accountType", "HOSTED");
            post_values.Add("Email", userID.ToLower().Replace("@barclayswine.com", "") + "@barclayswine.com");        
            post_values.Add("Passwd", password);   
            post_values.Add("service", "xapi");      
            post_values.Add("source", "BarclaysWine-Intranet-1.0");         

            try {
				string post_response = sendData(post_values);
			} catch {
				return false;	
			}
			
			return true;
		}
		
		private string sendData(Hashtable post_values) {
			string post_string = "";;
			foreach(DictionaryEntry field in post_values)
            {
                post_string += field.Key + "=" + field.Value + "&";
            }
            post_string = post_string.TrimEnd('&');
            
            // create an HttpWebRequest object to communicate with Authorize.net
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(post_url);
            objRequest.Method = "POST";
            objRequest.ContentLength = post_string.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter  = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(post_string);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            String post_response;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()) )
            {
                post_response = responseStream.ReadToEnd();
                responseStream.Close();
            }
			
			return post_response;
		}
	}
}
		
