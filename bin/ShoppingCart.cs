using System;
using System.Data;
using Data;
using System.Collections;
using System.Web;
using System.Configuration;

namespace Wine
{
    public class ShoppingCart
    {
		public DataUtility d = new DataUtility(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
		
		public string GetShoppingCartId() {
			if (HttpContext.Current.Request.Cookies["XMart_CartID"] != null) {
				// already assigned
				return HttpContext.Current.Request.Cookies["XMart_CartID"].Value;
			} else {
				string tempCartID = System.Guid.NewGuid().ToString();
				HttpCookie cookie = new HttpCookie("XMart_CartID");
				cookie.Value = tempCartID;
				DateTime dtNow = DateTime.Now;
				TimeSpan tsMinute = new TimeSpan(30, 0, 0, 0, 0);
				cookie.Expires = dtNow + tsMinute;
				HttpContext.Current.Response.Cookies.Add(cookie);
				
				return tempCartID;
			}
		}
		
		public void AddItem(string cartID, int productID, int quantity) {
			Hashtable parameters = new Hashtable();
            parameters.Add("CartID", cartID);
            parameters.Add("ProductID", productID);
            parameters.Add("Quantity", quantity);

			Result r = new Result();
            r = d.ExecuteNonQuery("ShoppingCartAddItem", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
		}
		
		
		public void UpdateItem(string cartID, int productID, int quantity) {
			Hashtable parameters = new Hashtable();
            parameters.Add("CartID", cartID);
            parameters.Add("ProductID", productID);
            parameters.Add("Quantity", quantity);

			Result r = new Result();
            r = d.ExecuteNonQuery("ShoppingCartUpdateItem", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
		}
		
		public void EmptyCart(string cartID) {
			Hashtable parameters = new Hashtable();
            parameters.Add("CartID", cartID);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("EmptyCart", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public DataTable GetCart(string cartID) {
			Hashtable parameters = new Hashtable();
            parameters.Add("CartID", cartID);
			if (HttpContext.Current.Request["campaignID"] != null) parameters.Add("CampaignID", HttpContext.Current.Request["campaignID"]);
			parameters.Add("BrandID", 1);
			DataTable dt;
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "GetCart", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return dt;
		}
		
		public double GetShipping(string cartID) {
			Hashtable parameters = new Hashtable();
            parameters.Add("CartID", cartID);
			if (HttpContext.Current.Request["campaignID"] != null) parameters.Add("CampaignID", HttpContext.Current.Request["campaignID"]);
			parameters.Add("BrandID", 1);
			Result r = new Result();
			string amount;
            r = d.ExecuteScalar(out amount, "sp_GetShippingAmount", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return Convert.ToDouble(amount);
		}
		
		public string GetAmount(string cartID) {
			string output = "0";
			Hashtable parameters = new Hashtable();
            parameters.Add("CartID", GetShoppingCartId());
			if (HttpContext.Current.Request["campaignID"] != null) parameters.Add("CampaignID", HttpContext.Current.Request["campaignID"]);
			parameters.Add("BrandID", 1);

			Result r = new Result();
            r = d.ExecuteScalar(out output, "ShoppingCartAmount", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return output;
		}
		
		public string GetTax(string state, double total) {
			string output = "0";
			Hashtable parameters = new Hashtable();
            parameters.Add("State", state);
            parameters.Add("Total", total);

			Result r = new Result();
            r = d.ExecuteScalar(out output, "ShoppingCartTax", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return output; 
		}
		
		public string GetItemCount() {
			string output = "0";
			Hashtable parameters = new Hashtable();
            parameters.Add("CartID", GetShoppingCartId());

			Result r = new Result();
            r = d.ExecuteScalar(out output, "ShoppingCartItemCount", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return output;
		}
		
		public void RemoveSaleItems() {
			Hashtable parameters = new Hashtable();
            parameters.Add("CartID", GetShoppingCartId());
			
			Result r = new Result();
            r = d.ExecuteNonQuery("EmptySaleItems", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		
		
		public string GetGroupon(string grouponID) {
			string output = "0";
			Hashtable parameters = new Hashtable();
            parameters.Add("Groupon", grouponID);

			Result r = new Result();
            r = d.ExecuteScalar(out output, "sp_CheckGroupon", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return output;
		}
		
		public void InvalidateGroupon(string grouponID) {
			Hashtable parameters = new Hashtable();
            parameters.Add("Groupon", grouponID);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_InvalidateGroupon", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void InvalidateAllrecipes(string voucherID) {
			Hashtable parameters = new Hashtable();
            parameters.Add("VoucherID", voucherID);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_InvalidateAllrecipe", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public string CheckAllrecipesVoucher(string code) {
			string output = "0";
			Hashtable parameters = new Hashtable();
			parameters.Add("VoucherID", code.Substring(0, code.Length - 2));
			
			Result r = new Result();
            r = d.ExecuteScalar(out output, "sp_CheckAllrecipeVoucher", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
			return output;
		}
		
		public string CheckVoucher(string code) {
			string output = "0";
			Hashtable parameters = new Hashtable();
			parameters.Add("Code", code);
			
			Result r = new Result();
            r = d.ExecuteScalar(out output, "sp_CheckVoucher", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
			return output;
		}
		
		public string CheckVoucherLead(string email) {
			string output = "0";
			Hashtable parameters = new Hashtable();
			parameters.Add("Email", email);
			
			Result r = new Result();
            r = d.ExecuteScalar(out output, "sp_CheckVoucherLead", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
			return output;
		}
		
		public void AddVoucherLead(string email, string campaignID, string code) {
			Hashtable parameters = new Hashtable();
            parameters.Add("Code", code);
            parameters.Add("CampaignID", campaignID);
            parameters.Add("Email", email);

			Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddVoucherLead", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
		}
		
		public void InvalidateVoucher(string code) {
			InvalidateVoucher(code, null);
		}
		
		public void InvalidateVoucher(string code, string orderID) {
			Hashtable parameters = new Hashtable();
            parameters.Add("VoucherID", code);
			if (orderID != null) parameters.Add("InvoiceID", orderID);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_InvalidateVoucher", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public DataTable GetDiscountDetail(out DataTable dt, string campaignID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("CampaignID", campaignID);
			
			Result r = new Result();
            d.ExecuteDataTable(out dt, "sp_GetDiscountDetail", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
			return dt;
		}
		
		public double GetDiscount(string bottleCount, double subTotal, int campaignID) {
			string output = "0";
			Hashtable parameters = new Hashtable();
			parameters.Add("BottleCount", bottleCount);
			parameters.Add("SubTotal", subTotal);
			parameters.Add("CampaignID", campaignID);
			
			Result r = new Result();
            r = d.ExecuteScalar(out output, "sp_GetDiscount", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
			return Convert.ToDouble(output);
		}
		
		public string GetVoucher(string code) {
			string output = "0";
			Hashtable parameters = new Hashtable();
            parameters.Add("Code", code);

			Result r = new Result();
            r = d.ExecuteScalar(out output, "sp_CheckVoucher", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
				output = "0";
            }
			
			return output;
		}
		
		
		public void GetShippingStates(out DataTable dt) {
			Hashtable parameters = new Hashtable();
			//parameters.Add("CampaignID", campaignID);
			
			Result r = new Result();
            d.ExecuteDataTable(out dt, "sp_GetShippingStates", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
	}
	
}