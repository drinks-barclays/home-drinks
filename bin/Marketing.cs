using System.Data;
using Data;
using System.Collections;
using System.Web;
using System.Configuration;

namespace Wine
{
    public class Marketing
    {
		public DataUtility d = new DataUtility(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
		public string AddCampaign(string campaignType, string partnerID, string cpaPrice, string cpmPrice, string cpmSize, string startDate, string endDate, string discountType, string description) {
			Hashtable parameters = new Hashtable();
			parameters.Add("CampaignTypeID", campaignType);
			parameters.Add("PartnerID", partnerID);
			parameters.Add("Description", description);
			if (cpaPrice != null && cpaPrice != "") parameters.Add("CPAPrice", cpaPrice);
			if (cpmPrice != null && cpmPrice != "") parameters.Add("CPMPrice", cpmPrice);
			if (cpmSize != null && cpmSize != "") parameters.Add("CPMSize", cpmSize);
            parameters.Add("StartDate", startDate);
			parameters.Add("EndDate", endDate);
			parameters.Add("DiscountTypeID", discountType);

            string campaignID;
            Result r = new Result();
            r = d.ExecuteScalar(out campaignID, "sp_AddBaseCampaign", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            return campaignID;
		}
		
		public string AddCampaign(string campaignType, string partnerID, string cpaPrice, string cpmPrice, string cpmSize, string startDate, string endDate, string discountType, string description, string freeShipping, string shippingOverride, string checkSuppression, string pixel) {
			Hashtable parameters = new Hashtable();
			parameters.Add("CampaignTypeID", campaignType);
			parameters.Add("PartnerID", partnerID);
			parameters.Add("Description", description);
			if (cpaPrice != null && cpaPrice != "") parameters.Add("CPAPrice", cpaPrice);
			if (cpmPrice != null && cpmPrice != "") parameters.Add("CPMPrice", cpmPrice);
			if (cpmSize != null && cpmSize != "") parameters.Add("CPMSize", cpmSize);
            parameters.Add("StartDate", startDate);
			parameters.Add("EndDate", endDate); 
			parameters.Add("DiscountTypeID", discountType);
			parameters.Add("FreeShipping", freeShipping);
			parameters.Add("ShippingOverride", shippingOverride);
			parameters.Add("CheckSuppression", checkSuppression);

            string campaignID;
            Result r = new Result();
            r = d.ExecuteScalar(out campaignID, "sp_AddBaseCampaign", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            return campaignID;
		}
		
		public void AddCampaignDiscount(string campaignID, string dollarThreshold, string bottleThreshold, string percentageOff, string dollarOff, string discountType, string voucher) {
			Hashtable parameters = new Hashtable();
			parameters.Add("CampaignID", campaignID);
			if (dollarThreshold != null && dollarThreshold != "") parameters.Add("DollarThreshold", dollarThreshold);
			if (bottleThreshold != null && bottleThreshold != "") parameters.Add("BottleThreshold", bottleThreshold);
			if (percentageOff != null && percentageOff != "") parameters.Add("PercentageOff", percentageOff);
			if (dollarOff != null && dollarOff != "") parameters.Add("DollarOff", dollarOff);
			parameters.Add("DiscountTypeID", discountType);
			parameters.Add("RequireVoucher", voucher);

            Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddDiscount", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void AddPromo(string campaignID, string productID, string price, string free) {
			Hashtable parameters = new Hashtable();
			parameters.Add("CampaignID", campaignID);
			parameters.Add("ProductID", productID);
			parameters.Add("Price", price);
			parameters.Add("Free", free);

            Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddPromo", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}	
		
		public void GetCampaignList(out DataTable dt) {
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetCampaignList", new Hashtable());
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public void GetCampaignList(out DataTable dt, Hashtable parameters) {
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetCampaignList", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public void EditCampaign() {
			
		} 
		
		public void GetCampaignTypes(out DataTable dt) {
			Hashtable parameters = new Hashtable();
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetCampaignTypes", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

		}


        public string GetPixel(int CampaignID)
        {
            Hashtable parameters = new Hashtable();
            parameters.Add("CampaignID", CampaignID);
            Result r = new Result();
            string pixel = "";
            r = d.ExecuteScalar(out pixel, "sp_GetPixel", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            return pixel;

        }

        public void GetPartners(out DataTable dt) {
			Hashtable parameters = new Hashtable();
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetPartners", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

		}
		
		public void GetFollowUp(out DataTable dt) {
			Hashtable parameters = new Hashtable();
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetFollowUp", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public string AddLandingPage(Hashtable parameters) {

            Result r = new Result();
			string landingPageID = "";
            r = d.ExecuteScalar(out landingPageID, "sp_AddLandingPage", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return landingPageID;
		}
		
		public void AddLandingPageProduct(Hashtable parameters) {

            Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddLandingPageProduct", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}	
		
		
		public void GetLandingPage(out DataTable dt, string campaignID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("CampaignID", campaignID);
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetLandingPage", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetLandingPageProduct(out DataTable dt, string lpID, string campaignID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("LandingPageID", lpID);
			parameters.Add("CampaignID", campaignID);
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetLandingPageProduct", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void ReverseLookup(out DataTable dt, string productID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("ProductID", productID);
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_ReverseLPLookup", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
	}
	
}