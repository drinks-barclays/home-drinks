using System;
using System.Data;
using Data;
using System.Collections;
using System.Web;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.Configuration;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;


namespace Wine
{
    public class Order
    {
		public DataUtility d = new DataUtility(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
		public string attachment = null;
		
		public string Add(string billingID, string shippingID, string campaignID, string cartID, string shippingAmount, string taxAmount, 
						  	string total, string discountAmount, string transactionCode) {
			return Add(billingID, shippingID, campaignID, cartID, shippingAmount, taxAmount, 
						  	total, discountAmount, transactionCode, null);
		}
		
		public string Add(string billingID, string shippingID, string campaignID, string cartID, string shippingAmount, string taxAmount, 
						  	string total, string discountAmount, string transactionCode, string rewardNumber) {
			return 	Add(billingID, shippingID, campaignID, cartID, shippingAmount, taxAmount, 
						  	total, discountAmount, transactionCode, rewardNumber, "1");
		}
		
		public string Add(string billingID, string shippingID, string campaignID, string cartID, string shippingAmount, string taxAmount, 
						  	string total, string discountAmount, string transactionCode, string rewardNumber, string brandID) {
			string orderID = "";
			
			Hashtable parameters = new Hashtable();
            parameters.Add("BillingID", billingID);
            parameters.Add("ShippingID", shippingID);
            parameters.Add("CampaignID", campaignID);
            parameters.Add("CartID", cartID);
            parameters.Add("ShippingAmount", shippingAmount);
            parameters.Add("TaxAmount", taxAmount);
            parameters.Add("Total", total);
            parameters.Add("DiscountAmount", discountAmount);
            parameters.Add("TransactionCode", transactionCode);
            parameters.Add("RewardNumber", rewardNumber);
            parameters.Add("BrandID", brandID);
			
			if (HttpContext.Current.Session["operator"] != null) parameters.Add("Operator", HttpContext.Current.Session["operator"]);
			
			if (HttpContext.Current.Request.Cookies["subID"] != null) {
            	parameters.Add("SubID", HttpContext.Current.Request.Cookies["subID"].Value.ToString());
			}

            Result r = new Result();
            r = d.ExecuteScalar(out orderID, "sp_AddOrder", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
				HttpContext.Current.Response.Flush();
            }
			
			return orderID;
		}
		
		public DataTable Get(string orderID) {
			Hashtable parameters = new Hashtable();
			if (HttpContext.Current.Request["uid"] == null) {
				parameters.Add("InvoiceID", orderID);
			} else {
				parameters.Add("UID", orderID);
			}
			
			DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_OrderGet", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public string GetCustomerID(string orderID) {
			Hashtable parameters = new Hashtable();
			if (HttpContext.Current.Request["uid"] == null) {
				parameters.Add("OrderID", orderID);
			} else {
				parameters.Add("UID", orderID);
			}
			
			string customerID = "";      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteScalar(out customerID, "sp_GetCustomerIDFromOrder", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return customerID;
		}
		
		
		public DataTable GetOrdersForReview() {
			Hashtable parameters = new Hashtable();			
			DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetOrdersForReview", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		
		public DataTable GetSuppressions() {
			Hashtable parameters = new Hashtable();			
			DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetSuppressions", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public bool CheckSuppression(string email, string firstName, string lastName, string billingZip, string shippingZip) {
			string val = "0";
			
			Hashtable parameters = new Hashtable();
			parameters.Add("Email", email);
			parameters.Add("FirstName", firstName);
			parameters.Add("LastName", lastName);
			parameters.Add("BillingZip", billingZip);
			parameters.Add("ShippingZip", shippingZip);
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteScalar(out val, "sp_CheckSupression", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			if (val != "0") {
				return true;	
			} else {
				return false;	
			}
		}
		
		public DataTable GetTracking(string orderID) {
			Hashtable parameters = new Hashtable();
			if (HttpContext.Current.Request["uid"] == null) {
				parameters.Add("OrderID", orderID);
			} else {
				parameters.Add("UID", orderID);
			}
			
			DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_OrderGetTracking", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public DataTable GetDupes() {
			Hashtable parameters = new Hashtable();
			
			DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetDuplicateAccounts", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public void MergeAccount(string fromID, string toID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("ToCustomerID", toID);
			parameters.Add("FromCustomerID", fromID);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_MergeAccounts", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void AddGift(string orderID, string giftMessage, string frequency, string amount, string numShipments) {
			Hashtable parameters = new Hashtable();
			parameters.Add("OrderID", orderID);
			parameters.Add("GiftMessage", giftMessage);
			parameters.Add("Frequency", frequency);
			parameters.Add("Amount", amount);
			parameters.Add("NumShipments", numShipments);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddGift", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void AddGroupon(string grouponNumber, string name) {
			Hashtable parameters = new Hashtable();
			parameters.Add("GrouponNumber", grouponNumber);
			parameters.Add("Name", name);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddGroupon", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		// added on 2/9/2012 for new voucher program
		public void AddGrouponVoucher(string voucher, string campaignID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("Voucher", voucher);
			parameters.Add("CampaignID", campaignID);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddGrouponNew", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void AddBinVoucher(string voucher, string campaignID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("Voucher", voucher);
			parameters.Add("CampaignID", campaignID);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddBinVoucher", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		// added 4/10/2012 for generic voucher program
		public void AddVoucher(string voucher, string campaignID, string expirationDate, string value) {
			Hashtable parameters = new Hashtable();
			parameters.Add("Voucher", voucher);
			parameters.Add("CampaignID", campaignID);
			parameters.Add("ExpirationDate", expirationDate);
			parameters.Add("Value", value);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddVoucher", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public string VoidGroupon(string grouponNumber) {
			string output = "0";
			Hashtable parameters = new Hashtable();
			parameters.Add("Groupon", grouponNumber);
			
			Result r = new Result();
            r = d.ExecuteScalar(out output, "sp_VoidGroupon", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return output;
		}
		
		public void AddTracking(string orderID, string trackingNumber) {
			Hashtable parameters = new Hashtable();
			parameters.Add("OrderID", orderID);
			parameters.Add("TrackingNumber", trackingNumber);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddTracking", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void UpdateStatus(string orderID, string status) {
			Hashtable parameters = new Hashtable();
			parameters.Add("InvoiceID", orderID);
			parameters.Add("InvoiceStatusID", status);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_UpdateOrderStatus", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		// for updating the transactionCode
		public void UpdateStatus(string orderID, string status, string transactionCode) {
			Hashtable parameters = new Hashtable();
			parameters.Add("InvoiceID", orderID);
			parameters.Add("InvoiceStatusID", status);
			parameters.Add("TransactionCode", transactionCode);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_UpdateOrderStatus", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetGoogleOrder(out DataTable dt, string orderID) {
            // get the results from the database
			Hashtable parameters = new Hashtable();
			parameters.Add("InvoiceID", orderID);
			
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetGoogleTracking", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetStatusList(out DataTable dt) {
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetStatusList", new Hashtable());
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public DataTable OrderList(Hashtable parameters) {
			DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_Orders", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public DataTable GetShipmentSummary(int statusID) {
			DataTable dt;
			dt = GetShipmentSummary(statusID.ToString(), null, null);

            // bounce back
            return dt;
		}
		
		public DataTable GetShipmentSummary(string statusID, string startDate, string endDate) {
			DataTable dt;
			Hashtable parameters = new Hashtable();
			parameters.Add("OrderStatusID", statusID);
			parameters.Add("StartDate", startDate);
			parameters.Add("EndDate", endDate);
			
            // get the results from the database 
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetShipmentSummary", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public void GetOrdersInProcessing(out DataTable dt) {
			// fetch the orders that are in the processing (1) state
			Result r = new Result();
			Hashtable ht = new Hashtable();
			ht.Add("WarehouseID", 2);
            r = d.ExecuteDataTable(out dt, "sp_OrdersInProcessing", ht);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetOrdersInProcessingScott(out DataTable dt) {  
			// fetch the orders that are in the processing (1) state
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_OrdersInProcessingScott", new Hashtable());
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
			
		public void GetOrdersToShip(out DataTable dt) {
			// fetch the orders that are in the processing (1) state
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_OrdersToShip", new Hashtable());
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetMiscBottlesToShip(out DataTable dt) {
			// fetch the orders that are in the processing (1) state
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetMiscBottlesToShip", new Hashtable());
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetNonWineToShip(out DataTable dt) {
			// fetch the orders that are in the processing (1) state
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetNonWineToShip", new Hashtable());
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetPacksToShip(out DataTable dt) {
			// fetch the orders that are in the processing (1) state
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetPacksToShip", new Hashtable());
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public DataTable GetChildren(string id)
        {
            DataTable dt;
			Hashtable parameters = new Hashtable();
			parameters.Add("ProductID", id);
            d.ExecuteDataTable(out dt, "sp_ProductGetChildren", parameters);
            return dt;
        }
		
		public void SendVoucher(string amount, string orderID, string customerID, string email) {
			string body = "";
			string subject =  "Barclays Wine Voucher";
			
			// create a pdf of the voucher
			//CreateVoucherPDF(orderID + customerID);
			
			body += "Thank you for purchasing a voucher which is good for $" + amount + " at BarclaysWine.com. <br /><br />Your voucher number is " + orderID + customerID + ".<br /><br />";
			body += "Redeem at: http://www.barclayswine.com/voucher.aspx<br /><br />";
			//body += "- You must spend $" + amount + " worth of wine for the voucher to apply<br />";
			body += "- The voucher expires on " + System.DateTime.Now.AddDays(30).Date.ToString("MMMM dd, yyyy") + " but you can always redeem it for the amount you paid<br />";
			body += "- The voucher is valid only in our wine shop or for our mixed cases<br /><br />";
			//body += "For your convenience the voucher is attached to this email in PDF format.<br /><br />";
			body += "If you have any questions, feel free to contact Customer Service at 888-380-2337.<br /><br />";
			body += "In Vino Veritas,<br /><br />";
			body += "Barclays Wine Team";
			
			string toName = email;
			string toEmail = email;
			
			SendMail(toName, toEmail, subject, body);
		}
		
		public void SendFreeVoucher(string amount, string voucherID, string email) {
			string body = "";
			string subject =  "Barclays Wine Voucher";
			
			// create a pdf of the voucher
			//CreateVoucherPDF(orderID + customerID);
			
			body += "Thank you for claiming your voucher which is good for $" + amount + " at BarclaysWine.com. <br /><br />Your voucher number is " + voucherID + ".<br /><br />";
			body += "Redeem at: http://www.barclayswine.com/voucher.aspx<br /><br />";
			//body += "- You must spend $" + amount + " worth of wine for the voucher to apply<br />";
			body += "- The voucher expires on " + System.DateTime.Now.AddDays(30).Date.ToString("MMMM dd, yyyy") + " but you can always redeem it for the amount you paid<br />";
			body += "- The voucher is valid only in our wine shop or for our mixed cases<br /><br />";
			//body += "For your convenience the voucher is attached to this email in PDF format.<br /><br />";
			body += "If you have any questions, feel free to contact Customer Service at 888-380-2337.<br /><br />";
			body += "In Vino Veritas,<br /><br />";
			body += "Barclays Wine Team";
			
			string toName = email;
			string toEmail = email;
			
			SendMail(toName, toEmail, subject, body);
		}
		
		
		private void CreateVoucherPDF(string voucherID) {
			Document document = new Document();
			try
			{
				PdfWriter.GetInstance(document, new FileStream("C:\\Inetpub\\net\\www.bevbistro.com\\documentation\\vouchers\\" + voucherID + ".pdf", FileMode.Create));
				document.Open();
				WebClient wc = new WebClient();
				string htmlText = wc.DownloadString("http://www.barclayswine.com/voucher.html");
				htmlText = htmlText.Replace("@@VOUCHER_ID@@", voucherID);
				htmlText = htmlText.Replace("@@EXPIRATION_DATE@@", System.DateTime.Now.AddDays(90).Date.ToString("MMMM dd, yyyy"));
				System.Collections.Generic.List<IElement> htmlarraylist = HTMLWorker.ParseToList(new StringReader(htmlText), null);
				for (int k = 0; k < htmlarraylist.Count; k++)
				{
					document.Add((IElement)htmlarraylist[k]);
				}
		
				document.Close();
				
				attachment = "C:\\Inetpub\\net\\www.bevbistro.com\\documentation\\vouchers\\" + voucherID + ".pdf";
			}
			catch 
			{
			}
		}
		
		public void SendConfirmation(string orderID) {
			DataTable dt = Get(orderID);
			string body = "<div style='width: 600px; font-family: font-family: \"Cambria\", \"Book Antiqua\", \"Times New Roman\", Times, serif; font-size: 14px'>";
			string subject =  "Barclays Wine Order Confirmation";
			string email = "";
			string emailTo = "";
			if (dt.Rows.Count > 0) {
					// write out basic info
					email = dt.Rows[0]["Email"].ToString();
					body += "<strong>Order ID:</strong> " + dt.Rows[0]["InvoiceID"].ToString() + "<br />";
					
					// write out billing info
					body += "<br /><strong>Billing Information</strong> <hr>";
					emailTo = dt.Rows[0]["BillingFirstName"] + " " + dt.Rows[0]["BillingLastName"];
					body += dt.Rows[0]["BillingFirstName"] + " " + dt.Rows[0]["BillingLastName"] + "<br />";
					body += "(Credit card info hidden for security.)<br />";
					body += String.Format("{0:(###) ###-####}", Convert.ToInt64(dt.Rows[0]["BillingPhoneNumber"])) + "<br />";
					
					// write out address info
					body += "<br /><strong>Shipping Information</strong> <hr>";
					body += dt.Rows[0]["ShippingFirstName"] + " " + dt.Rows[0]["ShippingLastName"] + "<br />";
					body += dt.Rows[0]["ShippingAddress"] + "<br />";
					if (dt.Rows[0]["ShippingAddress2"].ToString().Trim().Length > 0) {
						body += dt.Rows[0]["ShippingAddress2"] + "<br />";
					}
					body += dt.Rows[0]["ShippingCity"] + ", " + dt.Rows[0]["ShippingState"] + " " + dt.Rows[0]["ShippingPostalCode"] + "<br />";
					body += String.Format("{0:(###) ###-####}", Convert.ToInt64(dt.Rows[0]["ShippingPhoneNumber"])) + "<br />";
					
					// write out order info
					body += "<br /><strong>Product Information</strong> <hr>";
					foreach (DataRow r in dt.Rows) {
						body += "<div id='" + r["ProductID"] + "' style='clear: both'>";
						body += "<div style='float: left; width: 400px;'>" + r["ItemName"] + "</div>";	
						body += "<div style='float: left; width: 20px;'>" + r["Quantity"] + "</div>";	
						body += "<div style='float: right;; width: 50px;'>" + Convert.ToDouble(r["ItemPrice"]).ToString("c") + "</div>";	
						body += "</div>";
					}
					
					// totals
					body += "<div style='clear: both; line-height: 130%'><hr size=1>";
					if (Convert.ToDouble(dt.Rows[0]["DiscountAmount"].ToString()) > 0) {
						body += "<div align=right>Discount: " + Convert.ToDouble(dt.Rows[0]["DiscountAmount"]).ToString("c") + "</div>";
					}
					body += "<div align=right>Shipping: " + Convert.ToDouble(dt.Rows[0]["ShippingAmount"]).ToString("c") + "</div>";
					body += "<div align=right>Tax: " + Convert.ToDouble(dt.Rows[0]["TaxAmount"]).ToString("c") + "</div>";
					body += "<div align=right><strong>Total: " + Convert.ToDouble(dt.Rows[0]["Total"]).ToString("c") + "</strong></div>";
					
					body += "</div></div>";
					
			}
		
			string toName = emailTo;
			string toEmail = email;
			
			SendMail(toName, toEmail, subject, body);
		}
		
		public void SendTrackingMails() {
			DataTable dt = 	new DataTable();
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetTrackingEmails", new Hashtable());
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			string body = "";
			string email = "";
			string trackingInfo = "";
			string fromName = "";
			string fromEmail = "";
			foreach(DataRow row in dt.Rows) { 
				
				// make sure we send it from the right brand
				switch (row["BrandID"].ToString()) {
					case "1":
						fromName = "Barclays Wine";
						fromEmail = "tracking@barclayswine.com";
						break;
					case "2":
						fromName = "Troon Wine Club";
						fromEmail = "tracking@barclayswine.com";
						break;
					case "7":
						fromName = "Heartwood & Oak";
						fromEmail = "cs@heartwoodandoak.com";
						break;
					default:
						fromName = "Barclays Wine";
						fromEmail = "tracking@barclayswine.com";
						break;
							
				}
				
				if (row["TrackingNumber"].ToString().Trim().ToLower().IndexOf("1z") > -1) { 
					// ups
					trackingInfo = "<a target='_blank' " +
					"href='http://wwwapps.ups.com/WebTracking/processInputRequest?sort_by=status&tracknums_displayed=1&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=" +
					row["TrackingNumber"].ToString().Trim() + "&track.x=0&track.y=0'>this link</a>";
				} else {
					// fedex
					trackingInfo = "<a target='_blank' " +
					"href='http://www.fedex.com/Tracking?language=english&cntry_code=us&tracknumbers=" +
					row["TrackingNumber"].ToString().Trim() + "'>this link</a>";
				}
				
				body = "<div style='font-family:Georgia, Times, serif'><p>We are pleased to advise that your order " + row["InvoiceID"] + " from " + fromName + " is now being prepared for shipment.  You can track this order by going directly to " + trackingInfo + " and following the instructions.  Remember that our wine orders will not be left without an adult signature so be sure to make the appropriate arrangements.  Thank you again for your business and let us know immediately if you have any questions!</p><p><em>Your tracking number has been assigned but it may take up to a few days before the system shows it is on the way to you.  Please check back on a periodic basis for updated tracking information.</em></p><p>In Vino Veritas,</p><p>" + fromName + " Team</p></div>"; 
				email = row["Email"].ToString().Trim();
				
				try {
					SendMail(row["FirstName"].ToString().Trim() + " " + row["LastName"].ToString().Trim(), email, fromName + " Order Tracking Information", body, fromEmail);
				} catch (Exception ex) {
					;
				}
			}
		}
		
		
		// Allrecipes voucher program
		public string AddAllrecipesVoucher(string customerID, string amount) {
			Hashtable parameters = new Hashtable();
			parameters.Add("CustomerID", customerID);
			parameters.Add("Amount", amount);
			
			string cardID = "";
			
			Result r = new Result();
            r = d.ExecuteScalar(out cardID, "sp_AddAllrecipeVoucher", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return EncodeGiftCard(cardID);
		}
		
		public void CheckAllrecipesVoucher(string code) {
			Hashtable parameters = new Hashtable();
			parameters.Add("VoucherID", code.Substring(0, code.Length - 2));
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_CheckAllrecipeVoucher", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		
		
		public void DepleteInventory(string invoiceID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("InvoiceID", invoiceID);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_DepleteInventory", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		// Gift Card Functionality
		public string AddGiftCard(string customerID, string message, string firstName, string lastName, string amount, string email) {
			Hashtable parameters = new Hashtable();
			parameters.Add("CustomerID", customerID);
			parameters.Add("Message", message);
			parameters.Add("FirstName", firstName);
			parameters.Add("LastName", lastName);
			parameters.Add("Amount", amount);
			parameters.Add("Email", email);
			
			string cardID = "";
			
			Result r = new Result();
            r = d.ExecuteScalar(out cardID, "sp_AddGiftCard", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return EncodeGiftCard(cardID);
		}
		
		public void ApplyGiftCard(string code, string amount) {
			Hashtable parameters = new Hashtable();
			parameters.Add("CardID", code.Substring(0, code.Length - 2));
			parameters.Add("Amount", amount);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_ApplyGiftCard", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public string GiftCardBalance(string code) {
			string balance = "0";
			
			// verify code is good first
			//if (code) {
				// pull the balance available from DB
				Hashtable parameters = new Hashtable();
				parameters.Add("CardID", code.Substring(0, code.Length - 2));
				
				Result r = new Result();
				r = d.ExecuteScalar(out balance, "sp_GetGiftCardBalance", parameters);
				if (r.ResultType == Common.ResultType.Failure)
				{
					HttpContext.Current.Response.Write(r.Exception.Message);
				}
			//} else {
				//return "0";	
			//}
			
			return balance;
		}
		
		public bool CheckGiftCardCode(string code)
        {
            bool good = true;
            int total = 0;

            try
            {
                // separate check digits
                int checkDigitA = Convert.ToInt16(code.Substring(code.Length - 2, 1));
                int checkDigitB = Convert.ToInt16(code.Substring(code.Length - 1, 1));

                // add up digits
                for (int i = 0; i < code.Length - 2; i++)
                {
                    total += Convert.ToInt16(code.Substring(i, 1));
                }

                // check digit A
                if (checkDigitA != (total % 10)) return false;

                // check digit B
                if (checkDigitB != (total % 3)) return false;
            }
            catch
            {
                return false;
            }

            return good;
        }

        public string EncodeGiftCard(string code)
        {
            int total = 0;

            // add up digits
            for (int i = 0; i < code.Length; i++)
            {
                total += Convert.ToInt16(code.Substring(i, 1));
            }

            int checkDigitA = 0;
            checkDigitA = total % 10;

            int checkDigitB = 0;
            checkDigitB = total % 3;

            return code + checkDigitA.ToString() + checkDigitB.ToString();
        }
		
		// end Gift Card functionality
		
		public void SendMail(string toName, string toEmail, string subject, string body)
		{
			SendMail(toName, toEmail, subject, body, "cs@BarclaysWine.com");
		}
		
		public void SendMail(string toName, string toEmail, string subject, string body, string fromAddress)
		{
			MailAddress from = new MailAddress(fromAddress, "Customer Service");
			MailAddress to = new MailAddress(toEmail, toName);
			MailMessage email = new MailMessage(from, to);
			email.Subject = subject;
			email.Body = body;
			email.IsBodyHtml = true;
			
			if (attachment != null) {
				FileStream fs = new FileStream(attachment, FileMode.Open, FileAccess.Read);
				Attachment a = new Attachment(fs, "Voucher.pdf", MediaTypeNames.Application.Octet);
				email.Attachments.Add(a);
			}
			
			NetworkCredential creds = new NetworkCredential(fromAddress, "bevbistro");
			if (fromAddress == "cs@heartwoodandoak.com") creds = new NetworkCredential("cs@heartwoodandoak.com", "1191Eagle");
			SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
			client.EnableSsl = true;
			client.UseDefaultCredentials = false;
			client.Credentials = creds;
			
			try {
				client.Send(email);
			} catch (Exception ex) {
				;	
			}
		}
		
		public void UpdateInventoryItem(Hashtable parameters) {
            // get the results from the database 
            Result r = new Result();
            r = d.ExecuteNonQuery("sp_UpdateInventoryInvoice", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public DataTable DoVoucher() {
			DataTable dt;
			Hashtable parameters = new Hashtable();
			
            // get the results from the database 
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "doVoucher", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public void AddInvoiceCredit(string invoiceID, string amount) {
			Hashtable parameters = new Hashtable();
			parameters.Add("InvoiceID", invoiceID);
			parameters.Add("Amount", amount);
			if (HttpContext.Current.Session["operator"] != null) parameters.Add("Operator", HttpContext.Current.Session["operator"]);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddInvoiceCredit", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void AddNoShipAddress(Hashtable parameters) {
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddNoShipAddress", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		
		
		public string ReversePackLookup(string invoiceID, string code) {
			string oldPackID = "";
			Hashtable parameters = new Hashtable();
			parameters.Add("Code", code);
			parameters.Add("InvoiceID", invoiceID);
		
			Result r = new Result();
			r = d.ExecuteScalar(out oldPackID, "sp_ReversePackLookup", parameters);
			if (r.ResultType == Common.ResultType.Failure)
			{
				HttpContext.Current.Response.Write(r.Exception.Message);
			}
			
			return oldPackID;
		}
		
	}
	
}