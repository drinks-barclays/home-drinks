using Data;
using System.Collections;
using System.Data;
using System.Web;
using System.Configuration;

namespace Wine
{
    public class Customer
    {

        // DataLayer object
        //  NEED: Add connectionstring
		public DataUtility d = new DataUtility(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);

		public string GetID(string uid) {
			Hashtable parameters = new Hashtable();
            parameters.Add("UID", uid);

            string customerID;
            Result r = new Result();
            r = d.ExecuteScalar(out customerID, "sp_GetCustomerID", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            return customerID;
		}
		
		public void Get(out DataSet ds, int customerID) {
			ds = null;
			ds = new DataSet();
			DataTable dt;
			Hashtable parameters = new Hashtable();
            Result r = new Result(); 
			
			// retrieve base information
			parameters.Add("CustomerID", customerID);
			r = d.ExecuteDataTable(out dt, "sp_CustomerLookup", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            } else {
				ds.Tables.Add(dt);	
			}
						
			// retrieve orders
			parameters = new Hashtable();
            r = new Result();
			parameters.Add("CustomerID", customerID);
			r = d.ExecuteDataTable(out dt, "sp_Orders", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            } else {
				ds.Tables.Add(dt);	
			}
			
			// retrieve notes
			parameters = new Hashtable();
            r = new Result();
			parameters.Add("CustomerID", customerID);
			r = d.ExecuteDataTable(out dt, "sp_Notes", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            } else {
				ds.Tables.Add(dt);	
			}
			
			// retrieve memberships
			parameters = new Hashtable();
            r = new Result();
			parameters.Add("CustomerID", customerID);
			r = d.ExecuteDataTable(out dt, "sp_GetMemberships", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            } else {
				ds.Tables.Add(dt);	
			}
			
			
		}
		
		// customer version of get
		public void Get(out DataSet ds, string uid) {
			ds = null;
			ds = new DataSet();
			DataTable dt;
			Hashtable parameters = new Hashtable();
            Result r = new Result();
			
			// retrieve base information
			parameters.Add("UID", uid);
			r = d.ExecuteDataTable(out dt, "sp_CustomerLookup", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            } else {
				ds.Tables.Add(dt);	
			}
			
			// retrieve orders
			parameters = new Hashtable();
            r = new Result();
			parameters.Add("UID", uid);
			r = d.ExecuteDataTable(out dt, "sp_Orders", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            } else {
				ds.Tables.Add(dt);	
			}
			
			// retrieve memberships
			parameters = new Hashtable();
            r = new Result();
			parameters.Add("UID", uid);
			r = d.ExecuteDataTable(out dt, "sp_GetMemberships", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            } else {
				ds.Tables.Add(dt);	
			}
			
			
		}
		
		public void Get(out DataTable dt, string uid) {
			Hashtable parameters = new Hashtable();
            Result r = new Result();
			
			// retrieve base information
			parameters.Add("UID", uid);
			r = d.ExecuteDataTable(out dt, "sp_CustomerLookup", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
		}
		
		public void GetBonusCase (out DataTable dt, string UID) {
            Hashtable parameters = new Hashtable();
			parameters = new Hashtable();
            Result r = new Result();
            parameters.Add("UID", UID);
			r = d.ExecuteDataTable(out dt, "sp_GetBonusCase", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public void GetMemberships (out DataTable dt, string UID) {
            Hashtable parameters = new Hashtable();
			parameters = new Hashtable();
            Result r = new Result();
            parameters.Add("UID", UID);
			r = d.ExecuteDataTable(out dt, "sp_GetMemberships_", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		
		public void GetMembership (out DataTable dt, string UID) {
            Hashtable parameters = new Hashtable();
			parameters = new Hashtable();
            Result r = new Result();
            parameters.Add("MembershipUID", UID);
			r = d.ExecuteDataTable(out dt, "sp_GetMemberships_", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public void GetMembershipFutureShipment (out DataTable dt, string UID) {
            Hashtable parameters = new Hashtable();
			parameters = new Hashtable();
            Result r = new Result();
            parameters.Add("UID", UID);
			r = d.ExecuteDataTable(out dt, "sp_GetFutureShipmentInfo", parameters);
            if (r.ResultType == Common.ResultType.Failure) 
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public string Add(string email, int campaignID, int brandID, string firstName, string lastName)
        {

            Hashtable parameters = new Hashtable();
            parameters.Add("Email", email);
            parameters.Add("CampaignID", campaignID);
            parameters.Add("BrandID", brandID);
            parameters.Add("FirstName", firstName);
            parameters.Add("LastName", lastName);

            string customerID;
            Result r = new Result();
            r = d.ExecuteScalar(out customerID, "sp_AddCustomer", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            return customerID;
		}
		
		
		
		public string Create(Hashtable parameters) {
			string output = "";     
            // get the results from the database
            Result r = new Result();
            parameters.Add("BrandID", 1);
            r = d.ExecuteScalar(out output, "sp_AddCustomerBin", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back 
            return output;
		}
		
		public bool Edit() {
			return false;
		}
		
		public void UpdateEmail(string customerID, string email) {
			Hashtable parameters = new Hashtable();
			if (HttpContext.Current.Request["uid"] != null) {
				parameters.Add("UID", customerID);
			} else {
           		parameters.Add("CustomerID", customerID);
			}
            
			parameters.Add("Email", email);
			
            Result r = new Result();
            r = d.ExecuteNonQuery("sp_UpdateCustomerEmail", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void AddNote(string customerID, string note) {
			Hashtable parameters = new Hashtable();
            parameters.Add("CustomerID", customerID);
            parameters.Add("Note", note);
			
            Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddNote", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public string AddBilling(string customerID, string billingFirstName, string billingLastName,
					   	string billingAddress, string billingPostalCode, string billingPhone, string digits, string month, string year) {
			Hashtable parameters = new Hashtable();
            parameters.Add("CustomerID", customerID);
            parameters.Add("FirstName", billingFirstName);
            parameters.Add("LastName", billingLastName);
            parameters.Add("Address", billingAddress);
            parameters.Add("PostalCode", billingPostalCode);
            parameters.Add("PhoneNumber", billingPhone);
            parameters.Add("Digits", digits);
            parameters.Add("Month", month);
            parameters.Add("Year", year);

            string billingID;
            Result r = new Result();
            r = d.ExecuteScalar(out billingID, "sp_AddCustomerBilling", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            return billingID;
		}
		
		public DataTable GetBilling(string id) {
			Hashtable parameters = new Hashtable();
            parameters.Add("CustomerBillingID", id);
			DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_CustomerBilling", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public void EditBilling(string billingID, string firstName, string lastName,
					   	string postalCode, string digits, string month, string year) {
			
			/* 	UPDATE CustomerBilling SET FirstName = @FirstName, LastName = @LastName, 
					PostalCode = @PostalCode, Month = @Month, Year = @Year
				WHERE CustomerBillingID = @CustomerBillingID */
				
			Hashtable parameters = new Hashtable();
			parameters.Add("CustomerBillingID", billingID);
			parameters.Add("FirstName", firstName);
			parameters.Add("LastName", lastName);
			parameters.Add("PostalCode", postalCode);
			parameters.Add("Digits", digits);
			parameters.Add("Month", month);
			parameters.Add("Year", year);
			
            Result r = new Result();
            r = d.ExecuteNonQuery("sp_UpdateCustomerBilling", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		
		public DataTable GetShipping(string id) {
			Hashtable parameters = new Hashtable();
            parameters.Add("CustomerShippingID", id);
			DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_CustomerShipping", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		
		public string AddShipping(string customerID, string shippingFirstName, string shippingLastName, string shippingAddress, string shippingAddress2,
						string shippingCity, string shippingState, string shippingPostalCode, string shippingPhone) {
			Hashtable parameters = new Hashtable();
            parameters.Add("CustomerID", customerID);
            parameters.Add("FirstName", shippingFirstName);
            parameters.Add("LastName", shippingLastName);
            parameters.Add("Address", shippingAddress);
            parameters.Add("Address2", shippingAddress2);
            parameters.Add("City", shippingCity);
            parameters.Add("State", shippingState);
            parameters.Add("PostalCode", shippingPostalCode);
            parameters.Add("PhoneNumber", shippingPhone);
			
			string shippingID;
            Result r = new Result();
            r = d.ExecuteScalar(out shippingID, "sp_AddCustomerShipping", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            return shippingID;
		}
		
		public void EditShipping(string shippingID, string shippingFirstName, string shippingLastName, string shippingAddress, string shippingAddress2,
						string shippingCity, string shippingState, string shippingPostalCode, string shippingPhone) {
			Hashtable parameters = new Hashtable();
            parameters.Add("CustomerShippingID", shippingID);
            parameters.Add("FirstName", shippingFirstName);
            parameters.Add("LastName", shippingLastName);
            parameters.Add("Address", shippingAddress);
            parameters.Add("Address2", shippingAddress2);
            parameters.Add("City", shippingCity);
            parameters.Add("State", shippingState);
            parameters.Add("PostalCode", shippingPostalCode);
            parameters.Add("PhoneNumber", shippingPhone);
			
            Result r = new Result();
            r = d.ExecuteNonQuery("sp_UpdateCustomerShipping", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public DataTable Lookup(Hashtable parameters) {
			DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_CustomerLookup", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public DataTable VerifyLogin(Hashtable parameters) {
			DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_VerifyLogin", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		
		public DataTable ForgotLogin(Hashtable parameters) {
			DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_ForgotLogin", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public string AuditRegistrion(string id) {
			string check = "";
			Hashtable parameters = new Hashtable();
            parameters.Add("UID", id);
			
			Result r = new Result();
            r = d.ExecuteScalar(out check, "sp_AuditRegistration", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            return check;	
		}
		
		public DataTable ConfirmLogin(Hashtable parameters) {
			DataTable dt;      
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_ConfirmLogin", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public DataTable CustomerLogin(string id, string password, string cartID) {
			DataTable dt;      
			Hashtable parameters = new Hashtable();
            parameters.Add("CustomerID", id);
            parameters.Add("Password", password);
            parameters.Add("CartID", cartID);
			
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_CustomerLogin", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public string GetGroupon(string grouponID, string name) {
			string output = "0";
			Hashtable parameters = new Hashtable();
            parameters.Add("Groupon", grouponID);
            parameters.Add("Name", name);

			Result r = new Result();
            r = d.ExecuteScalar(out output, "sp_GetGroupon", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return output;
		}
		
		public void ValidateGroupon(string grouponID, string val) {
			Hashtable parameters = new Hashtable();
            parameters.Add("Groupon", grouponID);
			
			Result r = new Result();
			if (val == "1") {
            	r = d.ExecuteNonQuery("sp_ValidateGroupon", parameters);
			} else {
				r = d.ExecuteNonQuery("sp_InvalidateGroupon", parameters);
			}
			
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		
		public void ValidateTravelzoo(string grouponID, string val) {
			Hashtable parameters = new Hashtable();
            parameters.Add("VoucherID", grouponID);
            parameters.Add("Pin", "1234");
			
			Result r = new Result();
			if (val == "1") {
            	r = d.ExecuteNonQuery("sp_ValidateTravelzoo", parameters);
			} else {
				r = d.ExecuteNonQuery("sp_InvalidateTravelzoo", parameters);
			}
			
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void ValidateVoucher(string voucherID, string val) {
			Hashtable parameters = new Hashtable();
            parameters.Add("VoucherID", voucherID);
			
			Result r = new Result();
			if (val == "1") {
            	parameters.Add("ExpirationDate", HttpContext.Current.Request["expirationDate"]);
            	r = d.ExecuteNonQuery("sp_ValidateVoucher", parameters);
			} else {
				r = d.ExecuteNonQuery("sp_InvalidateVoucher", parameters);
			}
			
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		
		public DataTable LookupVoucherOrder(string voucherID) {
			DataTable output = new DataTable();
			Hashtable parameters = new Hashtable();
            parameters.Add("VoucherID", voucherID);
			
			Result r = new Result();
			r = d.ExecuteDataTable(out output, "sp_LookupVoucherOrder", parameters);
			
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return output;
		}
		
		// referral functionality begin
		public string CheckReferralEligible(string email) {
			string output = "0";
			Hashtable parameters = new Hashtable();
            parameters.Add("Email", email);
			
			Result r = new Result();
			r = d.ExecuteScalar(out output, "sp_CheckReferralEligible", parameters);
			
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return output;
		}
		
		public DataTable GetReferrals(string custID) {
			DataTable dt;      
			Hashtable parameters = new Hashtable();
            parameters.Add("UID", custID);
			
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetReferrals", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public void AddReferral(string email, string name, string custID) {
			Hashtable parameters = new Hashtable();
            parameters.Add("UID", custID);
            parameters.Add("Email", email);
            parameters.Add("Name", name);
			
			Result r = new Result();
			r = d.ExecuteNonQuery("sp_AddReferral", parameters);
			
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void AddReferralCredit(string custID) {
			Hashtable parameters = new Hashtable();
            parameters.Add("CustomerID", custID);
			
			Result r = new Result();
			r = d.ExecuteNonQuery("sp_AddReferralCredit", parameters);
			
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public DataTable ReferralCredit() {
			DataTable dt;      
			Hashtable parameters = new Hashtable();
			
            // get the results from the database
            Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_ReferralCredit", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }

            // bounce back
            return dt;
		}
		
		public DataTable Impersonate(string custID) {
			DataTable dt;
			Hashtable parameters = new Hashtable();
            parameters.Add("CustomerID", custID);
			
			Result r = new Result();
			r = d.ExecuteDataTable(out dt, "sp_ImpersonateLogin", parameters);
			
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return dt;
		}
		
		public DataTable GetCredits(string custID) {
			DataTable dt;
			Hashtable parameters = new Hashtable();
            parameters.Add("CustomerID", custID);
			
			Result r = new Result();
			r = d.ExecuteDataTable(out dt, "sp_GetCredits", parameters);
			
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return dt;
		}
		
		public DataTable GetRewards(string custID) {
			DataTable dt;
			Hashtable parameters = new Hashtable();
            parameters.Add("CustomerID", custID);
			
			Result r = new Result();
			r = d.ExecuteDataTable(out dt, "sp_GetRewardHistory", parameters);
			
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			return dt;
		}
		
		public void AddCredit(string id, string amount) {
			Hashtable parameters = new Hashtable();
            parameters.Add("CustomerID", id);
            parameters.Add("Amount", amount);
			
			Result r = new Result();
			r = d.ExecuteNonQuery("sp_AddCredit", parameters);
			
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		
		public void Resubscribe(string id) {
			Hashtable parameters = new Hashtable();
            parameters.Add("UID", id);
			
			Result r = new Result();
			r = d.ExecuteNonQuery("sp_Resubscribe", parameters);
			
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void Unsubscribe(string id) {
			Hashtable parameters = new Hashtable();
            parameters.Add("UID", id);
			
			Result r = new Result();
			r = d.ExecuteNonQuery("sp_UnsubscribeMe", parameters);
			
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		
		public void ExportEmailList(out DataTable dt, Hashtable parameters) {
			Result r = new Result();
			r = d.ExecuteDataTable(out dt, "sp_ExportEmailList", parameters);
			
            if (r.ResultType != Common.ResultType.Success)
            {
                //HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
		}
		
	}
	
}