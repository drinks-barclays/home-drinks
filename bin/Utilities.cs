using System;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Security.Cryptography;
using Data;
using System.Collections;
using System.Web;
using System.Configuration;

namespace Wine
{
    public class Util
    {
		public DataUtility d = new DataUtility(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
		
		public void AddEmailLead(string email) {
			Hashtable parameters = new Hashtable();
			parameters.Add("Email", email);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddEmailLead", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		
		
		public void SendMailServer(string toName, string toEmail, string subject, string body)
		{
			SendMailServer(toName, toEmail, subject, body, "CustomerService@BarclaysWine.com", "Customer Service");
		}
		
		public void SendMailServer(string toName, string toEmail, string subject, string body, string fromEmail, string fromName) {
			MailAddress from = new MailAddress(fromEmail, fromName);
			MailAddress to = new MailAddress(toEmail, toName);
			MailMessage email = new MailMessage(from, to);
			if (fromEmail.IndexOf("cs") > -1) email.CC.Add(new MailAddress(fromEmail, "Customer Service"));
			email.Subject = subject;
			email.Body = body;
			email.IsBodyHtml = true;
			
			NetworkCredential creds = new NetworkCredential("auth@voidme.com", "$m@ng0"); 
			SmtpClient client = new SmtpClient("127.0.0.1");
			client.UseDefaultCredentials = false;
			client.Credentials = creds;
			
			try {
				client.Send(email);
			} catch (Exception ex) {
				;	
			}			
		}
		
		public void SendMail(string toName, string toEmail, string subject, string body)
		{
			SendMail(toName, toEmail, subject, body, "CustomerService@BarclaysWine.com", "Customer Service");
		}
		
		public void SendMail(string toName, string toEmail, string subject, string body, string fromEmail, string fromName) {
			MailAddress from = new MailAddress(fromEmail, fromName);
			MailAddress to = new MailAddress(toEmail, toName);
			MailMessage email = new MailMessage(from, to);
			email.Subject = subject;
			email.Body = body;
			email.IsBodyHtml = true;
			
			NetworkCredential creds = new NetworkCredential("communication@barclayswine.com", "bevbistro");
			
			if ((System.DateTime.Now.Ticks % 2) == 0) {
				creds = new NetworkCredential("cs@barclayswine.com", "bevbistro");
			}
			
			SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
			client.EnableSsl = true;
			client.UseDefaultCredentials = false;
			client.Credentials = creds;
			
			try {
				client.Send(email);
			} catch (Exception ex) {
				;	
			}			
		}
	}
	
	 public class cTripleDES
    {
        // define the triple des provider

        private TripleDESCryptoServiceProvider m_des = 
                 new TripleDESCryptoServiceProvider();

        // define the string handler

        private UTF8Encoding m_utf8 = new UTF8Encoding();

        // define the local property arrays

        private byte[] m_key;
        private byte[] m_iv;

        public cTripleDES(byte[] key, byte[] iv)
        {
            this.m_key = key;
            this.m_iv = iv;
        }

        public byte[] Encrypt(byte[] input)
        {
            return Transform(input, 
                   m_des.CreateEncryptor(m_key, m_iv));
        }

        public byte[] Decrypt(byte[] input)
        {
            return Transform(input, 
                   m_des.CreateDecryptor(m_key, m_iv));
        }

        public string Encrypt(string text)
        {
            byte[] input = m_utf8.GetBytes(text);
            byte[] output = Transform(input, 
                            m_des.CreateEncryptor(m_key, m_iv));
            return Convert.ToBase64String(output);
        }

        public string Decrypt(string text) 
        {
            byte[] input = Convert.FromBase64String(text);
            byte[] output = Transform(input, 
                            m_des.CreateDecryptor(m_key, m_iv));
            return m_utf8.GetString(output);
        }

        private byte[] Transform(byte[] input, 
                       ICryptoTransform CryptoTransform)
        {
            // create the necessary streams

            MemoryStream memStream = new MemoryStream();
            CryptoStream cryptStream = new CryptoStream(memStream, 
                         CryptoTransform, CryptoStreamMode.Write);
            // transform the bytes as requested

            cryptStream.Write(input, 0, input.Length);
            cryptStream.FlushFinalBlock();
            // Read the memory stream and

            // convert it back into byte array

            memStream.Position = 0;
            byte[] result = memStream.ToArray();
            // close and release the streams

            memStream.Close();
            cryptStream.Close();
            // hand back the encrypted buffer

            return result;
        }
    }
}