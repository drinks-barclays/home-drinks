using System.Data;
using Data;
using System.Collections;
using System.Web;
using System.Configuration;

namespace Wine
{
    public class Sales
    {
		public DataUtility d = new DataUtility(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
		
		public void AssignLeads(out DataTable dt, Hashtable parameters) {
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_AssignLeads", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetLeadStates(out DataTable dt, string assignedTo) {
			Hashtable parameters = new Hashtable();
			parameters.Add("AssignedTo", assignedTo);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetLeadsByState", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetLeadsByState(out DataTable dt, string assignedTo, string state) {
			Hashtable parameters = new Hashtable();
			parameters.Add("AssignedTo", assignedTo);
			parameters.Add("State", state);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetLeadsByState", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void GetFollowUpLeads(out DataTable dt, string assignedTo) {
			GetFollowUpLeads(out dt, assignedTo, null);	
		}
		
		public void GetFollowUpLeads(out DataTable dt, string assignedTo, string leadID) {
			Hashtable parameters = new Hashtable();
			parameters.Add("AssignedTo", assignedTo);
			if (leadID != null) parameters.Add("LeadID", leadID);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetFollowUpLeads", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		}
		
		public void UpdateLead(string statusID, string leadID, string note, string followUpDate) {
			Hashtable parameters = new Hashtable();
			parameters.Add("LeadAssignmentID", leadID);
			parameters.Add("StatusID", statusID);
			parameters.Add("FollowUpDate", followUpDate);
			
			if (note != "") parameters.Add("@Note", note);
			
			Result r = new Result();
            r = d.ExecuteNonQuery("sp_UpdateLead", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
		
		public void GetEmailOffers(out DataTable dt) {
			GetEmailOffers(out dt, null);	
		}
		
		public void GetEmailOffers(out DataTable dt, string id) {
			Hashtable parameters = new Hashtable();
			if (id != null) parameters.Add("LeadOfferID", id);
			
			Result r = new Result();
            r = d.ExecuteDataTable(out dt, "sp_GetLeadEmailOffers", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }	
		}
	}
}