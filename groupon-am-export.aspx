<%@ Page  Language="c#" %>
<%@ Assembly src="/bin/Report.cs" %>
<%@ Assembly src="/bin/Excel.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<script language="C#" runat="server">
 public void Page_Load(Object sender, EventArgs e)
 {
	Report r = new Report();
	Response.Clear();
	Response.Buffer= true;
	Response.ContentType = "application/vnd.ms-excel";
	Response.AddHeader("Content-Disposition", "inline;filename=GrouponAfterMarketReport.csv"); Response.Charset = "";
	this.EnableViewState = false;
	
	DataTable dt = new DataTable();
	r.GetGrouponAfterMarketExport(out dt);
	DataTableHelper h = new DataTableHelper();
	h.ProduceCSV(dt, Response.Output, true);
 }
</script>