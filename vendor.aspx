<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Product p = new Product();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		DataTable dt = p.GetVendors();
		
		foreach(DataRow r in dt.Rows) {
			results.InnerHtml += "<div style='width: 50px; float: left;'>" + r["VendorID"] + "</div>";
			results.InnerHtml += "<div style='width: 100px; float: left;'> " + r["Name"] + "&nbsp;</div>";
			results.InnerHtml += "<div style='width: 100px; float: left; clear: right' class='clearfix'>" + Convert.ToDateTime(r["DateAdded"]).ToString("d") + "</div>";
		}
	}
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Marketing</title>
<script src="/js/jquery-1.4.2.min.js" type="text/javascript"></script> 
<script type="text/javascript" src="/includes/js/thickbox.js"></script>
<style type="text/css" media="all">@import "/includes/thickbox.css";</style>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>

<body>
<div id="container">
    <div id="navigation">
      <WF:Nav runat="server" id="nav_ctl" navId=2 />
    </div>
    
    <div id="content">
    	<div id="header">Vendors</div>
        <div id="workingArea">
         <a href='/secure/vendor-form.aspx?placeValuesBeforeTB_=savedValues&TB_iframe=true&height=650&width=400&modal=true' class='thickbox'>Add A New Vendor</a>
          <div style="width: 260px; padding: 2px; clear: both; margin-top: 15px;">
          <h4 style="margin-bottom: 3px;">Vendors</h4><hr size="1"  />
            <div style="width: 50px; font-weight: bold; float: left;">ID</div>
            <div style="width: 100px; font-weight: bold; float: left;">Name</div>
            <div style="width: 100px; font-weight: bold; float: left; clear:right" class="clearfix">Date Added</div>
          </div>
          <div style="width: 260px; padding: 2px; clear: both;" id="results" runat="server">
          </div>
        </div>
    </div>
    
    <div id="footer">
    
    </div>
</div>
</body>
</html>
