<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Sales.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

   	public void Page_Load(Object sender, EventArgs e)
 	{
		
	}
	
</script>

<html>
<head>
<style>
body {
	margin: 0px;
}
</style>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<script type="text/javascript" src="https://maps-api-ssl.google.com/maps/api/js?sensor=true"></script>
<script type="text/javascript">
 var geocoder;
  var map;
  function initialize() {
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(-34.397, 150.644);
    var myOptions = {
      zoom: 18,
      center: latlng,
	  mapTypeControl: false,
      mapTypeId: google.maps.MapTypeId.SATELLITE
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	
  }

  function codeAddress() {
    var address = '<%= Request["address"] %>';
    geocoder.geocode( { address: address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK && results.length) {
        // You should always check that a result was returned, as it is
        // possible to return an empty results object.
        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
          map.set_center(results[0].geometry.location);
          var marker = new google.maps.Marker({
              position: results[0].geometry.location,
              map: map
          });
        }
      } else {
        alert("Geocode was unsuccessful due to: " + status);
      }
    });
  }
</script>
</head>


<body onLoad="initialize(); codeAddress();">
	<div id="map_canvas" style="width: 320px; height: 320px;"></div>
</body>