<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Report.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Net.Mime" %>
<%@ Import Namespace="System.IO" %>
<script language="C#" runat="server">

	Report report = new Report();
	DataTable dt;
   	public void Page_Load(Object sender, EventArgs e)
 	{
		string body = "";
		report.GetPaintNiteReport(out dt);
		
		StringBuilder sb = new StringBuilder(); 

		string[] columnNames = dt.Columns.Cast<DataColumn>().
										  Select(column => column.ColumnName).
										  ToArray();
		sb.AppendLine(string.Join(",", columnNames));
		
		foreach (DataRow row in dt.Rows)
		{
			string[] fields = row.ItemArray.Select(field => field.ToString()).
											ToArray();
			sb.AppendLine(string.Join(",", fields));
		}
		string fileName = Server.MapPath("shipping_files/Redemptions_" + System.DateTime.Now.Month + 
										"-" + System.DateTime.Now.Day + 
										"-" + System.DateTime.Now.Year + ".csv");
		File.WriteAllText(fileName, sb.ToString());
		
		
		string subject = "Redemption Report";
		
			MailMessage email = new MailMessage(
				   "drinks-devops@drinks.com",
				   "slessing@paintnite.com",
				   subject,
				   "Report is attached.");
			email.IsBodyHtml = true;
			email.Attachments.Add(new Attachment(fileName, MediaTypeNames.Application.Octet));
				
			NetworkCredential creds = new NetworkCredential("auth@voidme.com", "$m@ng0");
			SmtpClient client = new SmtpClient("127.0.0.1"); 
			client.UseDefaultCredentials = false;
			client.Credentials = creds;
			try {
				client.Send(email);
			} catch (Exception ex) {
				;	
			}
			Response.Write(body);
			
		
	}
</script>

