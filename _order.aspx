﻿<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Assembly src="/bin/LitlePayment.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">
	
	Order o = new Order();
	Customer c = new Customer();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Session["operator"] == null) Response.Redirect("/security.aspx?r=" + Server.UrlEncode(Request.Url.ToString()));
			if (Request["action"] != null && Request["action"] == "update_status") {
				// update the status
				o.UpdateStatus(Request["id"], Request["status"]);
				
				if ((Request["status"] == "6" || Request["status"] == "5") && Request["transactionID"] != null) {
					Payment_ p = new Payment_();
					if (Request["originalStatusID"] != null && 
							Convert.ToInt16(Request["originalStatusID"].ToString()) < 3) {
						bool good = p.DoVoid(Request["transactionID"]);	
						
						if (good) {
							// add note to account
							string customerID = o.GetCustomerID(Request["id"]);
							c.AddNote(customerID, "Order #" + Request["id"] + " cancelled & payment authorization reversed by " + Session["operator"] + " - Note added by BOT");
						}
					} else {
						// redirect to do a credit on the order
						Response.Redirect("_creditOrder.aspx?transactionCode=" + Request["transactionID"] + "&originalStatusID=" + Request["originalStatusID"] + "&id=" + Request["id"] + "");
					}
				}
				
				//  NEED: 
				//  in the future we will want to record the 
				//  disposition if an order is refunded, 
				//  returned or cancelled
				
				Response.Redirect("/modal/close_modal.html");
			}
			
			DataTable dt = o.Get(Request["id"]);
			DataTable dtTracking = o.GetTracking(Request["id"]);
			if (dt.Rows.Count > 0) {
				// write out basic info
				email.InnerHtml = dt.Rows[0]["Email"].ToString();
				sourceID.InnerHtml = "CampaignID: " + dt.Rows[0]["CampaignID"].ToString();
				orderDate.InnerHtml = dt.Rows[0]["DateAdded"].ToString();
				invoiceID.InnerHtml = "InvoiceID: " + dt.Rows[0]["InvoiceID"].ToString();
				transactionCode.InnerText = "Transaction ID: " + dt.Rows[0]["TransactionCode"].ToString().Trim();
				voucherCode.InnerText = "Voucher Code: " + dt.Rows[0]["Voucher"].ToString().Trim();
				warehouse.InnerText += dt.Rows[0]["Warehouse"].ToString().Trim();
				
				// parse out tracking
				foreach(DataRow r in dtTracking.Rows) {
					tracking.InnerHtml += GetTracking(r["TrackingNumber"].ToString());	
				}
				
				// write out billing info
				billing_info.InnerHtml += dt.Rows[0]["BillingFirstName"] + " " + dt.Rows[0]["BillingLastName"] + "<br />";
				billing_info.InnerHtml += "Zip: " + dt.Rows[0]["BillingPostalCode"] + "<br />";
				billing_info.InnerHtml += "CC: " + dt.Rows[0]["Digits"].ToString().Trim() +  "<br />Exp: " + dt.Rows[0]["Month"] + "/" + dt.Rows[0]["Year"] + "<br />";
				billing_info.InnerHtml += String.Format("{0:(###) ###-####}", Convert.ToInt64(dt.Rows[0]["BillingPhoneNumber"])) + "<br />";
				
				// write out address info
				address_info.InnerHtml += dt.Rows[0]["ShippingFirstName"] + " " + dt.Rows[0]["ShippingLastName"] + "<br />";
				address_info.InnerHtml += dt.Rows[0]["ShippingAddress"] + "<br />";
				if (dt.Rows[0]["ShippingAddress2"].ToString().Trim().Length > 0) {
					address_info.InnerHtml += dt.Rows[0]["ShippingAddress2"] + "<br />";
				}
				address_info.InnerHtml += dt.Rows[0]["ShippingCity"] + ", " + dt.Rows[0]["ShippingState"] + " " + dt.Rows[0]["ShippingPostalCode"] + "<br />";
				address_info.InnerHtml += String.Format("{0:(###) ###-####}", Convert.ToInt64(dt.Rows[0]["ShippingPhoneNumber"])) + "<br />";
				
				// write out order info
				foreach (DataRow r in dt.Rows) {
					order.InnerHtml += "<div id='" + r["ProductID"] + "' style='clear: both'>";
					order.InnerHtml += "<div style='float: left; width: 400px;'>" + r["ProductID"] + " - " + r["Vintage"] + " " + r["ItemName"] + "</div>";	
					order.InnerHtml += "<div style='float: left; width: 20px;'>" + r["Quantity"] + "</div>";	
					order.InnerHtml += "<div style='float: right;; width: 50px;'>" + Convert.ToDouble(r["ItemPrice"]).ToString("c") + "</div>";	
					order.InnerHtml += "</div>";
				}
				
				// totals
				order.InnerHtml += "<div style='clear: both; margin-right: 12px; line-height: 130%'><hr size=1>";
				if (Convert.ToDouble(dt.Rows[0]["DiscountAmount"].ToString()) > 0) {
					order.InnerHtml += "<div align=right>Discount: " + Convert.ToDouble(dt.Rows[0]["DiscountAmount"]).ToString("c") + "</div>";
				}if (Convert.ToDouble(dt.Rows[0]["Credit"].ToString()) > 0) {
					order.InnerHtml += "<div align=right style='color: red'>Credit: " + Convert.ToDouble(dt.Rows[0]["Credit"]).ToString("c") + "</div>";
				}
				order.InnerHtml += "<div align=right>Shipping: " + Convert.ToDouble(dt.Rows[0]["ShippingAmount"]).ToString("c") + "</div>";
				order.InnerHtml += "<div align=right>Tax: " + Convert.ToDouble(dt.Rows[0]["TaxAmount"]).ToString("c") + "</div>";
				order.InnerHtml += "<div align=right><strong>Total: " + Convert.ToDouble(dt.Rows[0]["Total"]).ToString("c") + "</strong></div>";
				
				order.InnerHtml += "</div>";
				
				WriteAction(Convert.ToInt32(dt.Rows[0]["OrderStatusID"].ToString()), dt.Rows[0]["TransactionCode"].ToString().Trim());
			}
			o = null;
	}
	
	private void WriteAction(int statusID, string transactionCode) {
		// see what can be done with this order depending on it's status
		action.InnerHtml += "<select id='status' name='status' onchange=\"document.location = '_order.aspx?action=update_status&id=" + Request["id"] + "&originalStatusID=" + statusID;
		if (statusID < 6) action.InnerHtml += "&transactionID=" + transactionCode;
		action.InnerHtml += "&status=' + this.value;\">";
		
		// first retrieve the status options
		DataTable dt;
		o.GetStatusList(out dt);
		
		int i = 0;
		foreach (DataRow r in dt.Rows) {
			if (i > statusID) {
				action.InnerHtml += "<option value='" + r["InvoiceStatusID"] + "'>" + r["Description"] + "</option>";	
			} else if (i == statusID) {
				action.InnerHtml += "<option value='" + r["InvoiceStatusID"] + "' selected>" + r["Description"] + "</option>";
			}
			i++;
		}
		
		action.InnerHtml += "</select>";
		
		if (statusID == 7 || statusID == 8) action.InnerHtml = " <a href='_order.aspx?action=update_status&id=" + Request["id"] + "&status=1' style='color: #cc3300;'>Remove Hold</a>";
		if (statusID == 4 || statusID == 3) action.InnerHtml = action.InnerHtml + " <a href='_creditOrder.aspx?transactionCode=" + transactionCode + "&originalStatusID=" + statusID + "&id=" + Request["id"] + "' style='color: #cc3300;'>Partial Credit</a>";
	}
	
	private string MaskDigits(string x) {
		string output = "";
		string firstDigit = x.Substring(0, 1);
		string lastFour = x.Substring(x.Length - 5, 4);
		
		output = firstDigit;
		for (int i = 0; i <= x.Length - 5; i++) {
			output += "*";	
		}
		
		output += lastFour;
		
		return output;
	}
	
	private string GetTracking(string x) {
		if (x.ToLower().IndexOf("1z") > -1) {
			// ups
			return "<a target='_blank' " +
			"href='http://wwwapps.ups.com/WebTracking/processInputRequest?sort_by=status&tracknums_displayed=1&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=" +
			x + "&track.x=0&track.y=0'>" + x + "</a>";
		} else {
			// fedex
			return "<a target='_blank' " +
			"href='http://www.fedex.com/Tracking?language=english&cntry_code=us&tracknumbers=" +
			x + "'>" + x + "</a>";
		}
	}
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Order</title>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
<style>
	body {
		margin: 0px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
</style>
</head>

<body>
<div id="container" style="width: 90%; margin: 0 auto;">
<div id="tools" align="right" style="display: none;">
      	Close Window
   	<input type="button" name="printOrder" id="printOrder" value="Print Order" runat="server" />
   	<input type="button" name="voidOrder" id="voidOrder" value="Void Order" runat="server" />
      	<input type="button" name="emailInvoice" id="emailInvoice" value="Email Invoice" runat="server" />
  </div>
	<div id="workingArea" style="width: 650px;"> 
    <div style="width: 675px;" align="right">
    	Order Status: <span id="action" runat="server"></span>
    </div> 
        	<fieldset class="fieldSet">
            <legend> Order Information </legend>
            	<div id="orderDate" style="float: right;" runat="server"></div>
                <div id="invoiceID" runat="server"></div>
          		<div id="sourceID" runat="server"></div>  
          		<div id="transactionCode" runat="server"></div>  
          		<div id="voucherCode" runat="server"></div>  
          		<div id="tracking" runat="server" style="margin-top: 10px;">Tracking Number(s): </div>  
          		<div id="warehouse" runat="server" style="margin-top: 10px;">Warehouse: </div>  
          	</fieldset>
            <br />
          	<div style="width: 300px; float: left;">
                <fieldset class="fieldSet" style="width: 300px;" >
                    <legend> Billing Information </legend>
                    <div id="billing" runat="server">
                        <div id="billing_info" style="line-height: 150%; padding: 3px;" runat="server">
                        </div> 
            			<div id="email" style="line-height: 150%; padding: 3px;"  runat="server"></div>    
                    </div>
            	</fieldset>
          	</div>
            
          	<div style="width: 300px; float: right; ">
                <fieldset class="fieldSet" style="width: 300px;" >
                    <legend> Address Information </legend>
                    <div id="address" runat="server">
                        <div id="address_info" style="line-height: 150%; padding: 3px;" runat="server">
                        </div>
                    </div>
                </fieldset>
            </div>
            
            <fieldset class="fieldSet">
                <legend> Product Information</legend>
				<div style='clear: both; font-weight: bold;'>
				<div style='float: left; width: 400px;'>Item Name</div>
				<div style='float: left; width: 20px;'>Quantity</div>
				<div style='float: right;; width: 50px;'>Price</div>
				</div>
                <hr style="clear: both; margin-right: 12px;" size=1 />
                <div id="order" runat="server" />
            </fieldset>
      </div>
</div>
</body>
</html>
