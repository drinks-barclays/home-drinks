<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	DataSet ds = new DataSet();
	Product p = new Product();
   	public void Page_Load(Object sender, EventArgs e)
 	{

		if (Request["action"] == "search") {
			resultSet.Visible = true;
			Hashtable parameters = new Hashtable();
			parameters.Add(Request["by"], Request["k"]);
			ds.Tables.Add(p.Search(parameters)); // add product table
			// write out select for redundant wine
			foreach (DataRow r in ds.Tables[0].Rows) {
				results.InnerHtml += "<tr class='product' id='" + r["ProductID"]  + "' onclick=\"document.location.href = 'inventory_.aspx?id=" + r["ProductID"]  + "';\"><td>" + 
							r["ProductID"] + "</td><td>" + 
							r["Code"] + "</td><td>" + r["Name"] + 
							"</td><td>" + r["Type"] + 
							"</td><td>" + String.Format("{0:c}", r["Price"]) + 
							"</td><td>" + Convert.ToDateTime(r["DateAdded"]).ToString("d") + 
							"</td><td>" + Convert.ToDateTime(r["DateExpires"]).ToString("d") + 
							"</td></tr>\r\n";
			}
		}
	
		
		p.Dispose();
		p = null;
	}
	
</script>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Inventory</title>
<style>
.header {
	font-weight: bold;
	background-color:#B28F57;
}

.product:hover {
	text-decoration: underline;
	cursor: pointer;
}

.product A {
	font-size: 10px;
	color: #0066CC;
	cursor: pointer;
}

.product A:hover {
	text-decoration: none;
}

</style>
<script language="javascript">
	function search() {
		if (document.getElementById("k").value != '') {
			document.location.href = 'inventory.aspx?action=search&by=' +  document.getElementById('by').value
						+ '&k=' + document.getElementById('k').value;
		} else {
			alert('Please enter a value to search for.');
			return false;
		}
	}
</script>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>

<body onLoad="document.getElementById('k').focus()">
<table width="950" border="0" align="center" cellpadding="2" cellspacing="2" style="border: 1px solid #B28F57">
  <tr  style="border: 1px solid #B28F57">
    <td width="250" valign="top"><WF:Nav runat="server" id="navControl" /></td>
    <td width="700" valign="top">
<div id="header">Products</div>

<div id="toolbar" style="float: none">
<div style="float: left; margin: 5px; vertical-align:middle;">
<input name="add"  value="Add New Product"type="submit" onClick="document.location.href = 'inventory_.aspx'" /></div><div  style="float: right; margin: 3px; vertical-align:middle;">
<form onSubmit="return search()"><input name="k" id="k" type="text" />
<select name="by" id="by">
  <option value="Code">Code</option>
  <option value="Name">Name</option>
  <option value="ProductID">Product ID</option>
</select>
<input type="hidden" name="action" value="search" />
<input name="submit" type="submit" value="Search" />
</form></div>
</div>

<div id="resultSet" visible=false runat="server" style="padding: 5px;">
<table width="100%" border="0" cellspacing="2" cellpadding="2" id="products">
  <tr class="header">
    <td>ID</td>
    <td>Code</td>
    <td>Name</td>
    <td>Type</td>
    <td>Price</td>
    <td>Date Added</td>
    <td>Arrival Date</td>
  </tr>
  <span id="results" runat="server"></span>
</table>
</div>
</td>
  </tr>
</table>
</body>
</html>
