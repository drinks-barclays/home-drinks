<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Report.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Net.Mime" %>
<script language="C#" runat="server">

	Report report = new Report();
	DataTable dt;
   	public void Page_Load(Object sender, EventArgs e)
 	{
		string body = "";
		string startDate = Request["startDate"];
		string endDate = Request["endDate"];
		string campaignID = Request["campaignID"];
		
		//Response.Write(startDate + " - " + endDate);
		
		
		body = "<table>";
		
		report.GetCogsByCampaign(out dt, campaignID, startDate, endDate);
		
		foreach (DataRow r in dt.Rows) {
			body += "<tr style='width: 610px; padding: 3px;";
				body += "' class='clearfix'>";	
				if (r["Continuity"].ToString() == "True") { 	
					body += "<td style='width: 100px; float: left;'>Continuity</td>";
				} else if (r["ProductType"].ToString() == "1003") {
					body += "<td style='width: 100px; float: left;'>Pack</td>";
				} else if (r["ProductType"].ToString() == "1001") {
					body += "<td style='width: 100px; float: left;'>Bottle</td>";

				}
				body += "<td style='width: 100px; float: left;'>" + r["Orders"].ToString() + "</td>";
				body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["Revenue"]).ToString("c") + "</td>";
				body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["ProductCost"]).ToString("c") + "</td>";
				body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["ShippingCost"]).ToString("c") + "</td>";
				body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["CCFee"]).ToString("c") + "</td>";
				body += "</tr>";
		}
		
		body += "</table>";
		
		Response.Write(body);
		
	}
</script>

