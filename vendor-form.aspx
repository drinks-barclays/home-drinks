<%@  Page Language="C#" Debug="true" ValidateRequest="false" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "add") {
			Product p = new Product();
			Hashtable parameters = new Hashtable();
            parameters.Add("Name", Request["name"]);
            parameters.Add("Website", Request["website"]);
            parameters.Add("Description", Request["description"]);
            parameters.Add("Terms", Request["terms"]);
			
			string vendorID = p.AddVendor(parameters);
			
			string[] states = Request["state"].ToString().Split(',');
			foreach (string state in states)
			{
				p.AddVendorState(state, vendorID);
			}
			Response.Redirect("modal/close_modal.html");
		}
	}
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="/css/validationEngine.jquery.css" type="text/css" media="screen" charset="utf-8" />
<script src="/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="/js/jquery.validationEngine-en.js" type="text/javascript"></script>
<script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
<script type='text/javascript' src='/js/jquery.simplemodal.1.4.1.min.js'></script>
<script language="javascript">
	
	$(document).ready(function() {
		$("#form1").validationEngine();
	})
	
</script>
<title>Add Vendor</title>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
	<p>Vendor Name<br />
	<input type="textbox" id="name" name="name" class="validate[required,maxSize[50]]" /><br />
    Website<br />
	<input type="textbox"  id="website" name="website" class="validate[required,maxSize[100]]" /><br />
    Bio<br />
    <textarea name="description" cols="50" rows="10" id="description" class="validate[required,maxSize[255]]"></textarea>
    <br />
    Terms of Service<br />
	<textarea  id="terms" cols="50" rows="10" name="terms" class="validate[required,maxSize[4000]]" /></textarea> 
	<br />
    Legal States<br />
	<select name="state" id="state" size="10" multiple="multiple" class="validate[required]"> 
	  <option value="AL">Alabama</option> 
	  <option value="AK">Alaska</option> 
	  <option value="AZ">Arizona</option> 
	  <option value="AR">Arkansas</option> 
	  <option value="CA">California</option> 
	  <option value="CO">Colorado</option> 
	  <option value="CT">Connecticut</option> 
	  <option value="DE">Delaware</option> 
	  <option value="DC">District Of Columbia</option> 
	  <option value="FL">Florida</option> 
	  <option value="GA">Georgia</option> 
	  <option value="HI">Hawaii</option> 
	  <option value="ID">Idaho</option> 
	  <option value="IL">Illinois</option> 
	  <option value="IN">Indiana</option> 
	  <option value="IA">Iowa</option> 
	  <option value="KS">Kansas</option> 
	  <option value="KY">Kentucky</option> 
	  <option value="LA">Louisiana</option> 
	  <option value="ME">Maine</option> 
	  <option value="MD">Maryland</option> 
	  <option value="MA">Massachusetts</option> 
	  <option value="MI">Michigan</option> 
	  <option value="MN">Minnesota</option> 
	  <option value="MS">Mississippi</option> 
	  <option value="MO">Missouri</option> 
	  <option value="MT">Montana</option> 
	  <option value="NE">Nebraska</option> 
	  <option value="NV">Nevada</option> 
	  <option value="NH">New Hampshire</option> 
	  <option value="NJ">New Jersey</option> 
	  <option value="NM">New Mexico</option> 
	  <option value="NY">New York</option> 
	  <option value="NC">North Carolina</option> 
	  <option value="ND">North Dakota</option> 
	  <option value="OH">Ohio</option> 
	  <option value="OK">Oklahoma</option> 
	  <option value="OR">Oregon</option> 
	  <option value="PA">Pennsylvania</option> 
	  <option value="RI">Rhode Island</option> 
	  <option value="SC">South Carolina</option> 
	  <option value="SD">South Dakota</option> 
	  <option value="TN">Tennessee</option> 
	  <option value="TX">Texas</option> 
	  <option value="UT">Utah</option> 
	  <option value="VT">Vermont</option> 
	  <option value="VA">Virginia</option> 
	  <option value="WA">Washington</option> 
	  <option value="WV">West Virginia</option> 
	  <option value="WI">Wisconsin</option> 
	  <option value="WY">Wyoming</option>
	  </select>    

press ctl to select multiple
    </p>
	<p>	  <input type="hidden" name="action" id="action" value="add" />
    <input type="submit" name="button" id="button" value="Submit" />
	</p>
</form>
</body>
</html>
