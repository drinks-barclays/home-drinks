﻿<%@  Control Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	DataSet ds = new DataSet();
	Customer c = new Customer();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "search") {
			resultSet.Visible = true;
			Hashtable parameters = new Hashtable();
			
			if (Request["customerID"] != ""  && Request["customerID"].ToString().Length > 3) {
				parameters.Add("CustomerID", Request["customerID"]);
			}
			
			if (Request["lastName"] != ""  && Request["lastName"].ToString().Length > 3) {
				parameters.Add("LastName", Request["lastName"]);
			}
			
			if (Request["phoneNumber"] != ""  && Request["phoneNumber"].ToString().Length > 9) {
				parameters.Add("PhoneNumber", Request["phoneNumber"]);
			}
			
			if (Request["zipCode"] != ""  && Request["zipCode"].ToString().Length > 3) {
				parameters.Add("ZipCode", Request["zipCode"]);
			}
			
			if (Request["email"] != ""  && Request["email"].ToString().Length > 5) {
				parameters.Add("Email", Request["email"]);
			}
			
			ds.Tables.Add(c.Lookup(parameters)); // add product table
			
			if (ds.Tables[0].Rows.Count > 0) {
				resultSet.InnerHtml += "<table id='myTable' class='tablesorter'><thead><tr><th>Brand</th><th>Name</th><th>Email</th><th>Anniversary</th></thead><tbody>";
										 
				// write out customers found
				foreach (DataRow r in ds.Tables[0].Rows) {
					resultSet.InnerHtml += "<tr class='customer' id='" + r["CustomerID"] + "' onclick=\"document.location.href = '?id=" + r["CustomerID"] + "'\">" +
											"<td>" + r["Name"].ToString().Trim() + "</td>" +
											"<td>" + r["FirstName"].ToString().Trim() + " " + r["LastName"].ToString().Trim() + "</td>" +
											"<td>" + r["Email"].ToString().Trim() + "</td>" +
											"<td>" + r["DateAdded"] + "</td>" +
										 "</tr>";
				}
				resultSet.InnerHtml += "</table>";
			} else {
				resultSet.InnerHtml += "<div class='err'>No results found, please try again.</div>";
			}
		}
	
		
		c = null;
	}
</script>
<style>

	.spaceMe {
		margin-bottom: 10px;
	}
	
	#leftForm {
		width: 200px;
		float: left;
		margin-right: 15px;	
	}
	
	#rightForm {
		width: 200px;
		float: left;
		margin-right: 15px;	
	}
</style>

			<div id="search" runat="server">
            	<form action="" method="post">
                	<div class="clearfix spaceMe" >
                        <div id="leftForm">
                        Customer ID<br />
                        <input type="text" name="customerID" id="customerID" class="spaceMe" /><br />
                        Billing Zip Code<br />
                        <input type="text" name="zipCode" id="zipCode" class="spaceMe" /><br />
                        Phone Number<br />
                        <input type="text" name="phoneNumber" id="phoneNumber" class="spaceMe" />
                        <br class="spaceMe"  />
                        </div>
                        <div id="rightForm">
                        Email<br />
                        <input type="text" name="email" id="email" class="spaceMe" /><br />
                        Last Name<br />
                        <input type="text" name="lastName" id="lastName" class="spaceMe"  /><br /><br />
                        <input type="submit" value="Search" /><input type="hidden" id="action" name="action" value="search" />
                        </div>
					</div>
              </form>
            </div>
            
            <div id="resultSet" class="clearfix" runat="server"></div>