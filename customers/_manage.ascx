<%@  Control Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	DataSet ds = new DataSet();
	Customer c = new Customer();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["id"] != null) {
		// check to see if we have an actual customer
		DataSet ds = new DataSet();
		c.Get(out ds, Convert.ToInt32(Request["id"].ToString()));
		
		if (ds.Tables[0].Rows.Count > 0) {
			// output basic demographic info
			foreach (DataRow r in ds.Tables[0].Rows) {
				customerName.InnerHtml = r["FirstName"] + " " + r["LastName"];
				customerID.InnerHtml += r["Name"].ToString().Trim() + " Customer #" + r["CustomerID"].ToString();
				if (r["BrandID"].ToString() == "5") customerName.InnerHtml += " - <a href='_impersonate.aspx?id=" +
									Request["id"] + "' target=_blank style='font-size: 11px; color:#060;'>[Login to Bin]</a>";
				
				email.InnerHtml = r["email"].ToString().Trim() + " - <a href='modal/_editCustomer.aspx?action=do_update&customer_id=" +
									Request["id"] + "&email=" + r["email"].ToString().Trim() +
									"' class='modal' style='font-size: 11px; color:#060;'>Update</a>";
				memberSince.InnerText += Convert.ToDateTime(r["DateAdded"]).ToString("d");
				sourceID.InnerHtml = "SourceID: " + r["SourceCampaignID"].ToString();
			}
			
			// output order history
			orders.InnerHtml = "<table id='myTable' class='tablesorter'><thead><tr><th>Order ID</th><th>Ship To</th><th>Bill To</th><th>Order Total</th><th>Order Date</th><th>Status</th></thead><tbody>";
			DataTable cOrders = ds.Tables[1].DefaultView.ToTable(true, "InvoiceID", "ShippingFirstName", "ShippingLastName","BillingFirstName", "BillingLastName", "Total", "DateAdded", "InvoiceStatus");
			foreach (DataRow r in cOrders.Rows) {
				orders.InnerHtml += "<tr href='_order.aspx?id=" + r["InvoiceID"] + "' class='modal' style='cursor: pointer'>";
				orders.InnerHtml += "<td>" + r["InvoiceID"] + "</td>";
				orders.InnerHtml += "<td>" + r["ShippingFirstName"] + " " + r["ShippingLastName"] + "</td>";
				orders.InnerHtml += "<td>" + r["BillingFirstName"] + " " + r["BillingLastName"] + "</td>";
				orders.InnerHtml += "<td>" + Convert.ToDouble(r["Total"]).ToString("c") + "</td>";
				orders.InnerHtml += "<td>" + r["DateAdded"] + "</td>";
				orders.InnerHtml += "<td>" + r["InvoiceStatus"] + "</td>";
				orders.InnerHtml += "</tr>";
			}
			orders.InnerHtml += "</table>";
			
			// output credits
			DataTable creditDT = c.GetCredits(Request["id"].ToString());
			foreach (DataRow r in creditDT.Rows) {
				credits.InnerHtml += "<div class='clearfix' style='margin:5px;'>";
				credits.InnerHtml += Convert.ToDouble(r["Amount"]).ToString("c") + " applied by <em>" + r["CampaignName"] + "</em>";
				credits.InnerHtml += "</div>";
					
			}
			
			
			
			/* output rewards
			DataTable rewardDT = c.GetRewards(Request["id"].ToString());
			foreach (DataRow r in rewardDT.Rows) {
				reward.InnerHtml += "<div class='clearfix' style='margin:5px;'>";
				reward.InnerHtml += Convert.ToDouble(r["Amount"]).ToString("c") + " earned on <em>" + r["DateAdded"] + "</em>";
				reward.InnerHtml += "</div>";
					
			}*/
			
			// output notes
			foreach (DataRow r in ds.Tables[2].Rows) {
				notes.InnerHtml += "<div class='note' id='" + r["CustomerNoteID"] + "'>";
				notes.InnerHtml += "<div style='font-size: 10px;'>" + r["DateAdded"] + "</div>";
				notes.InnerHtml += "<div>" + r["Note"] + "</div>";
				notes.InnerHtml += "</div>";
			}
			
			// output memberships
			/* t.PreferenceTypeID, Name, t.Description, s.StartDate, s.Span, Cycle, 
				c.CustomerID, Active, TotalShipments, skip.SkipDate, SkipSpan, cancel.CancelDate, ContinuityCancelReason.Reason */
			
			DataTable membershipsDT = ds.Tables[3].DefaultView.ToTable(true, new string[] { "ContinuitySubscriptionID", "Name", "Description", "Active", "CancelDate", "Reason", "CustomerBillingID", "CustomerShippingID", "CustomerID", "StartDate", "Span", "TotalShipments" });
			foreach (DataRow row in membershipsDT.Rows) {
				memberships.InnerHtml += "<div class='membership' id='" + row["ContinuitySubscriptionID"] + "'>";
				memberships.InnerHtml += "<div style='font-size: 14px; font-weight: bold'>" + row["Name"].ToString().Trim() + 
											" - " + row["Description"].ToString().Trim() + 
											"</div><div style='font-size: 12px;'>";
				
				// show skips
				int skips = 0;
				DataRow[] foundRows = ds.Tables[3].Select("ContinuitySubscriptionID = " + row["ContinuitySubscriptionID"]);
				foreach (DataRow r in foundRows) {
					if (r["SkipSpan"].ToString() != "0") {
						memberships.InnerHtml += "<div>Skipped " + r["SkipSpan"] + " weeks on " + r["SkipDate"] + ".</div>";
						skips += Convert.ToInt32(r["SkipSpan"].ToString());
					}
				}
											
				if (row["Active"].ToString() == "False") {
					// show cancel details
					memberships.InnerHtml += "Cancelled on " + row["CancelDate"] + " - " + row["Reason"];
				} else {
					// show next shipment date
					DateTime startDate = Convert.ToDateTime(row["StartDate"].ToString());
					int span = Convert.ToInt32(row["Span"].ToString());
					int shipments = Convert.ToInt32(row["TotalShipments"].ToString());
					memberships.InnerHtml += "Next shipment will ship on or around " + startDate.AddDays((skips + (span * shipments))*7).ToString("d");
					
					/* show modification link */
					memberships.InnerHtml += "<div style='margin-top: 5px'><a" +
												" href='modal/_billing.aspx?action=do_update&id=" + 
												row["CustomerBillingID"] + "' class='modal'" +
												" style='color: #333;'>Billing Info</a>" +
												"&nbsp; | &nbsp;<a" +
												" href='modal/_shipping.aspx?action=do_update&id=" + 
												row["CustomerShippingID"] + "' class='modal'" +
												" style='color: #333;'>Shipping Info</a>" +
												"&nbsp; | &nbsp;<a" +
												" href='modal/_membership.aspx?action=preference&id=" + 
												row["ContinuitySubscriptionID"] + "' class='modal'" +
												" style='color: #333;'>Change Preference</a>" +
												"&nbsp; | &nbsp;<a href='modal/_membership.aspx?action=skip&id=" + 
												row["ContinuitySubscriptionID"] + "&customer_id=" + 
												row["CustomerID"] + "' class='modal'" +
												" style='color: green;'>Skip</a>" +
												"&nbsp; | &nbsp;<a href='modal/_membership.aspx?action=cancel&id=" + 
												row["ContinuitySubscriptionID"] + "&customer_id=" + 
												row["CustomerID"] + "' class='modal'" +
												" style='color: red;'>Cancel</a></div>";
				}
				
				memberships.InnerHtml += "<div style='margin-top: 5px' /></div></div>";
			}
		}
		}
		c = null;
	}
	
	private string MaskDigits(string x) {
		string output = "";
		string firstDigit = x.Substring(0, 1);
		string lastFour = x.Substring(x.Length - 5, 4);
		
		output = firstDigit;
		for (int i = 0; i <= x.Length - 5; i++) {
			output += "*";	
		}
		
		output += lastFour;
		
		return output;
	}
</script>

          		<div id="sourceID" style="float: right;" runat="server"></div> 
                <div id="customerID" runat="server" style="font-size: 15px;"></div>  
                <div id="customerName" runat="server" style="font-size: 20px; font-weight:bold;"></div> 
                <div id="memberSince" style="font-size: 12px" runat="server">Member since </div>     
            	<span id="email" runat="server"></span>
            <hr />
            <div style="font-size: 20px; margin-bottom: 10px;">Order History</div>
            <div id="orders" runat="server" />
            <hr />
            
            <div style="font-size: 20px; margin-bottom: 10px;">Memberships</div>
            <div id="memberships" runat="server" />
            <hr />
            
            <div style="font-size: 20px; margin-bottom: 10px;">Store Credits</div>
            <div id="credits" runat="server" />
            <hr />
            
            <!--<div style="font-size: 20px; margin-bottom: 10px;">Rewards</div>
            <div id="reward" runat="server" />
            <hr />-->
            
            
            <div style="font-size: 20px; margin-bottom: 10px;">Notes</div>
            <div class="note">
            	<form action="_note.aspx" method="post">
                    <textarea name="note" cols="40" rows="5" id="note"></textarea>
                    <br />
                    <input type="hidden" id="id" name="id" value="<%= Request["id"] %>" />
                    <input type="submit" value="Submit Note" />
              </form>
            </div>
            <div id="notes" runat="server" />