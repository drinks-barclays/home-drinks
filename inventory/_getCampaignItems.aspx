<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.IO" %>
<script language="C#" runat="server">

	Product p = new Product();
	Hashtable parameters = new Hashtable();
	
   	public void Page_Load(Object sender, EventArgs e)
 	{
		Output();
	}
	
	public void Output() {
		DataTable dt = new DataTable();
		Hashtable parameters = new Hashtable();
		parameters.Add("CampaignID", Request["campaignID"]);
		parameters.Add("StartDate", Request["startDate"]);
		parameters.Add("EndDate", Request["endDate"]);
		
		p.GetCampaignDetail(out dt, parameters);
		
		StringBuilder sb = new StringBuilder();

        foreach (DataColumn col in dt.Columns)
        {
            sb.Append(col.ColumnName + ',');
        }

        sb.Remove(sb.Length - 1, 1);
        sb.Append(Environment.NewLine);

        foreach (DataRow row in dt.Rows)
        {
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                sb.Append(row[i].ToString() + ",");
            }

            sb.Append(Environment.NewLine);
        }

        string attachment = "attachment; filename=" + Request["campaignID"] + "-details.csv";
		HttpContext.Current.Response.Clear();
		HttpContext.Current.Response.ClearHeaders();
		HttpContext.Current.Response.ClearContent();
		HttpContext.Current.Response.AddHeader("content-disposition", attachment);
		HttpContext.Current.Response.ContentType = "text/csv";
		HttpContext.Current.Response.AddHeader("Pragma", "public");
		
		Response.Write(sb.ToString());
		Response.End();
	}
	
		
	
	
</script>