<%@  Page Language="C#" Debug="true" ClientIdMode="static" %>
<%@ Register TagPrefix="BB" TagName="BottleInventory" Src="_bottle.ascx" %>
<%@ Assembly src="/bin/Product.cs" %>
<script language="C#" runat="server">
   	public void Page_Load(Object sender, EventArgs e)
 	{
		_bottleInventory.Visible = true;
	}
	 
	
</script>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Inventory</title>


<style>
	
	body {
		margin: 0 auto;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		padding: 0px;
	}
	
	#nav {
		margin: 0 auto;
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
		background-color: #efefef;
		min-width: 400px;
	}
	
	#workingArea {
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
	}
	
	th {
		text-align: left;
		font-weight: bold;
		cursor: s-resize;	
		background-color: #0CF;
		color: white;
	}
	
	th,td {
		padding: 5px;	
		border: 1px solid #efefef;
	}
	
</style>
</head>

<body> 
<BB:BottleInventory runat="server" id="_bottleInventory" visible="false" />
</body>
</html>
 