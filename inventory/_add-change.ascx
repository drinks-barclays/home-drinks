<%@  Control Language="C#" ClientIdMode="static" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	DataSet ds = new DataSet();
	Product p = new Product();
	string _brandID = "0";
	string _productTypeID = "0";
	string _redundantProductID = "0";
	string _styleID = "0";
	string _varietalID = "0";
	string _regionID = "0";
	string _appellationID = "0";
	string _producerID = "0";
	string _active = "0";
	string _available = "0";
	string _continuity = "1";
	string _warehouse = "2";
	string _mystery = "0";
	int groupWineNumber = 1;
	Hashtable parameters = new Hashtable();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		 if (Request["_addChange$subAction"] == "add") {
			AddProduct();
		} else if (Request["_addChange$subAction"] == "update") {
			// edit
			EditProduct();
		}
		
		if (Request["id"] != null && Request["action"] == "add-change") {
			// retrieve product
			DataSet dsProduct = p.Get(Request["id"]);
			code.Value = dsProduct.Tables[0].Rows[0]["Code"].ToString().Trim();
			name.Value = dsProduct.Tables[0].Rows[0]["Name"].ToString().Trim();
			cost.Value = Convert.ToDouble(dsProduct.Tables[0].Rows[0]["Cost"]).ToString("c").Replace("$", "");
			price.Value = Convert.ToDouble(dsProduct.Tables[0].Rows[0]["Price"]).ToString("c").Replace("$", "");
			expirationDate.Value = Convert.ToDateTime(dsProduct.Tables[0].Rows[0]["DateExpires"]).ToString("d");
			quantity.Value = dsProduct.Tables[0].Rows[0]["Quantity"].ToString();
			purchaseOrder.Value = dsProduct.Tables[0].Rows[0]["PurchaseOrder"].ToString().Trim();
			orderDate.Value = Convert.ToDateTime(dsProduct.Tables[0].Rows[0]["DateOrdered"]).ToString("d");
			arrivalDate.Value = Convert.ToDateTime(dsProduct.Tables[0].Rows[0]["ArrivalDate"]).ToString("d");
			shortDescription.Value = dsProduct.Tables[0].Rows[0]["ShortDescription"].ToString().Trim();
			_productTypeID = dsProduct.Tables[0].Rows[0]["ProductTypeID"].ToString();
			_brandID = dsProduct.Tables[0].Rows[0]["BrandID"].ToString();
			_active = dsProduct.Tables[0].Rows[0]["Active"].ToString();
			_available = dsProduct.Tables[0].Rows[0]["Available"].ToString();
			_warehouse = dsProduct.Tables[0].Rows[0]["WarehouseID"].ToString();
			_continuity = dsProduct.Tables[0].Rows[0]["Continuity"].ToString();
			_mystery = dsProduct.Tables[0].Rows[0]["MysteryEligible"].ToString();
			prodType.InnerHtml = "<input type='hidden' id='productTypeID' name='productTypeID' value='" + _productTypeID + "' />" + _productTypeID;
			submitBtn.Value = "Update";
			subAction.Value = "update";
						
			// if it's a product of type wine then show the characteristics
			if (_productTypeID == "1001") {
				_varietalID = dsProduct.Tables[0].Rows[0]["VarietalID"].ToString();
				_redundantProductID = dsProduct.Tables[0].Rows[0]["RedundantProductID"].ToString();
				_producerID = dsProduct.Tables[0].Rows[0]["ProducerID"].ToString();
				_styleID = dsProduct.Tables[0].Rows[0]["StyleID"].ToString();
				_appellationID = dsProduct.Tables[0].Rows[0]["AppellationID"].ToString();
				_regionID = dsProduct.Tables[0].Rows[0]["RegionID"].ToString();
				vintage.Value = dsProduct.Tables[0].Rows[0]["Vintage"].ToString();
				bottleVolume.Value = dsProduct.Tables[0].Rows[0]["BottleVolume"].ToString();
				alcoholPercentage.Value = dsProduct.Tables[0].Rows[0]["AlcoholPercentage"].ToString();
			}
			
			// show the skus for each brand
			brandSkuTable.Visible = true;
			
		}
		
		if (Request["action"] == "add-change") {
			//parameters = new Hashtable();
			//parameters.Add("OrderBy", 2);
			//ds.Tables.Add(p.Search(parameters)); // add product table
			GetSelects();
			
			
			prodSelect.InnerHtml = "<input type='hidden' name='redundantProductID' id='redundantProductID' value='0' />";
			
			// write out select for grouped wine
			// if it is a pack or case show the children...
			if (_productTypeID == "1002" || _productTypeID == "1003") {
				DataTable children = p.GetChildren(Request["id"], _brandID);
				foreach(DataRow cr in children.Rows) {
						productGroup.InnerHtml += "<div id='groupWines'><a href='inventory-management.aspx?action=add-change&id=" + cr["ChildProductID"].ToString() + "'>" + cr["ChildProductID"].ToString() + "</a> - " + cr["Name"].ToString().Trim() + 
						" - Qty: <input type='hidden' name='groupWines_" + groupWineNumber + "' id='groupWines_" + groupWineNumber + "' value='" + 
						cr["ChildProductID"].ToString() + "' /><input type='text' size=3 id='groupWines_" + groupWineNumber + "_qty' name='groupWines_" + groupWineNumber + "_qty' value='" + 
						cr["Quantity"].ToString() + "' /></div>";
						groupWineNumber++;
						
				}
			} else {
				productGroup.InnerHtml += "<div id='groupWines'></div>";
			}	
		}
		

			
		ds = null;
		p.Dispose();
		p = null;
	}
	
	private void GetSelects() {
		DataTable temp = new DataTable();
		// parse out varietals
		temp = p.GetVarietals();
		varietalSelect.InnerHtml = "<select name='varietalID' id='varietalID'>" +
									"<option></option>";
		foreach (DataRow r in temp.Rows) {
			varietalSelect.InnerHtml += "<option value='" + r["VarietalID"] + "'";
			if (r["VarietalID"].ToString() == _varietalID) varietalSelect.InnerHtml += " selected ";
			varietalSelect.InnerHtml += ">" + r["Name"] + "</option>\r\n";
		}
		varietalSelect.InnerHtml += "</select>";
		
		// parse out styles 
		temp = p.GetStyles();
		styleSelect.InnerHtml = "<select name='styleID' id='styleID'>" +
									"<option></option>";
		foreach (DataRow r in temp.Rows) {
			styleSelect.InnerHtml += "<option value='" + r["StyleID"] + "'";
			if (r["StyleID"].ToString() == _styleID) styleSelect.InnerHtml += " selected ";
			styleSelect.InnerHtml += ">" + r["Name"] + "</option>\r\n";
		}
		styleSelect.InnerHtml += "</select>";
		
		// parse out regions
		temp = p.GetRegions();
		regionSelect.InnerHtml = "<select name='regionID' id='regionID'>" +
									"<option></option>";
		foreach (DataRow r in temp.Rows) {
			regionSelect.InnerHtml += "<option value='" + r["RegionID"] + "'";
			if (r["RegionID"].ToString() == _regionID) regionSelect.InnerHtml += " selected ";
			regionSelect.InnerHtml += ">" + r["Name"] + "</option>\r\n";
		}
		regionSelect.InnerHtml += "</select>";
		
		// parse out appellations
		temp = p.GetAppellations();
		appellationSelect.InnerHtml = "<select name='appellationID' id='appellationID'>" +
									"<option></option>";
		foreach (DataRow r in temp.Rows) {
			appellationSelect.InnerHtml += "<option value='" + r["AppellationID"] + "'";
			if (r["AppellationID"].ToString() == _appellationID) appellationSelect.InnerHtml += " selected ";
			appellationSelect.InnerHtml += ">" + r["Name"] + "</option>\r\n";
		}
		appellationSelect.InnerHtml += "</select>";
		
		// parse out producers
		temp = p.GetProducers();
		producerSelect.InnerHtml = "<select name='producerID' id='producerID'>" +
									"<option></option>";
		foreach (DataRow r in temp.Rows) {
			producerSelect.InnerHtml += "<option value='" + r["ProducerID"] + "'";
			if (r["ProducerID"].ToString() == _producerID) producerSelect.InnerHtml += " selected ";
			producerSelect.InnerHtml += ">" + r["Name"].ToString().Trim() + "</option>\r\n";
		}
		temp = null;
	}
	
	private void AddProduct() {
			string productID = "0";
			try {
				parameters.Add("BrandID", 0);
				parameters.Add("Code", Request["_addChange$code"].ToString());
				parameters.Add("Name", Request["_addChange$name"].ToString());
				parameters.Add("ProductTypeID", Request["productTypeID"]);
				parameters.Add("Cost", Request["_addChange$cost"].ToString());
				parameters.Add("Price", Request["_addChange$price"].ToString());
				parameters.Add("Discountable", Request["discountable"]);
				parameters.Add("DateExpires", Request["_addChange$expirationDate"].ToString());
				parameters.Add("Available", 0); // legacy shit, remove this later.
				parameters.Add("Active", Request["active"]);
				parameters.Add("Quantity", Request["_addChange$quantity"].ToString());
				parameters.Add("WarehouseID", Request["warehouse"].ToString());
				//parameters.Add("RedundantProductID", redundantProductID.Value);
				parameters.Add("PurchaseOrder", Request["_addChange$purchaseOrder"].ToString());
				parameters.Add("DateOrdered", Request["_addChange$orderDate"].ToString());
				parameters.Add("ArrivalDate", Request["_addChange$arrivalDate"].ToString());
				parameters.Add("Continuity", Request["continuity"]);
				parameters.Add("ShortDescription", Request["_addChange$shortDescription"].ToString());
				
				if (Request["productTypeID"] == "1001") {
					parameters.Add("Vintage", Request["_addChange$vintage"].ToString());
					parameters.Add("VarietalID", Request["varietalID"].ToString());
					parameters.Add("RegionID", Request["regionID"].ToString());
					//parameters.Add("CountryID", Request["countryID"]);
					parameters.Add("ProducerID", Request["producerID"].ToString());
					parameters.Add("AppellationID", Request["appellationID"].ToString());
					parameters.Add("StyleID", Request["styleID"].ToString());
					parameters.Add("AlcoholPercentage", Request["_addChange$alcoholPercentage"].ToString());
					parameters.Add("BottleVolume", Request["_addChange$bottleVolume"].ToString());	
					parameters.Add("MysteryEligible", Request["mystery"].ToString());	
				}
				
				
				
				productID = p.Add(parameters); // output new product id
				if (Request["groupWines_1"] != null && Request["groupWines_1"].ToString() != "") {
					int g = 1;
					while (Request["groupWines_" + g] != null) {
						p.AddGroupWine(Convert.ToInt32(productID), Convert.ToInt32(Request["groupWines_" + g + "_qty"].ToString()), Convert.ToInt32(Request["groupWines_" + g].ToString()));
						g++;
					}
				}
				
			} catch (Exception ex)
			{
				Response.Write(Request.Form + "<br>" + ex.ToString());
				Response.End();
			}
			Response.Redirect("inventory-management.aspx?action=add-change&id=" + productID);	
	}
	
	private void EditProduct() {
			string productID = "0";
			try {
				parameters.Add("BrandID", 0);
				parameters.Add("ProductID", Request["id"].ToString());
				parameters.Add("Code", Request["_addChange$code"].ToString());
				parameters.Add("Name", Request["_addChange$name"].ToString());
				parameters.Add("ProductTypeID", Request["productTypeID"]);
				parameters.Add("Cost", Request["_addChange$cost"].ToString());
				parameters.Add("Price", Request["_addChange$price"].ToString());
				parameters.Add("Discountable", Request["discountable"]);
				parameters.Add("DateExpires", Request["_addChange$expirationDate"].ToString());
				parameters.Add("Available", 0); // legacy shit, remove this later.
				parameters.Add("Active", Request["active"]);
				parameters.Add("Quantity", Request["_addChange$quantity"].ToString());
				parameters.Add("WarehouseID", Request["warehouse"].ToString());
				//parameters.Add("RedundantProductID", redundantProductID.Value);
				parameters.Add("PurchaseOrder", Request["_addChange$purchaseOrder"].ToString());
				parameters.Add("DateOrdered", Request["_addChange$orderDate"].ToString());
				parameters.Add("ArrivalDate", Request["_addChange$arrivalDate"].ToString());
				parameters.Add("Continuity", Request["continuity"]);
				parameters.Add("ShortDescription", Request["_addChange$shortDescription"].ToString());
				
				if (Request["productTypeID"] == "1001") {
					parameters.Add("Vintage", Request["_addChange$vintage"].ToString());
					parameters.Add("VarietalID", Request["varietalID"].ToString());
					parameters.Add("RegionID", Request["regionID"].ToString());
					//parameters.Add("CountryID", null);
					parameters.Add("ProducerID", Request["producerID"].ToString());
					parameters.Add("AppellationID", Request["appellationID"].ToString());
					parameters.Add("StyleID", Request["styleID"].ToString());
					parameters.Add("AlcoholPercentage", Request["_addChange$alcoholPercentage"].ToString());
					parameters.Add("BottleVolume", Request["_addChange$bottleVolume"].ToString());	
					parameters.Add("MysteryEligible", Request["mystery"].ToString());		
				}
				
				
				
				p.Edit(parameters); // output new product id
				
			} catch (Exception ex)
			{
				Response.Write(Request.Form + "<br>" + ex.ToString());
				Response.End();
			}
			Response.Redirect("inventory-management.aspx?action=add-change&id=" + Request["id"]);	
	}
	
</script>

<script type="text/javascript" src="/js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="/js/jquery.fancybox.pack.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="/js/jquery.fancybox.css?v=2.1.5" media="screen" />

  <script type="text/javascript">
	
	$(document).ready(function() {	
    	 $( document ).tooltip();
		 $("#oForm").validationEngine();
		 $("#sku").validationEngine();
		$(function() {
			$( ".datepicker" ).datepicker();
	 	});
	  	$("#brandSkus").load("inventory/_brandSku.aspx?action=get&id=<%= Request["id"] %>");
	  	$(".modal").fancybox({'autoSize':true, 'type' : 'iframe', 
								afterClose: function() { $("#brandSkus").load("inventory/_brandSku.aspx?action=get&id=<%= Request["id"] %>"); console.log('updated inline content.'); $(".editModal").fancybox({'type' : 'iframe'});}
		});
		$(".editModal").fancybox({'type' : 'iframe', 
								afterClose: function() { $("#brandSkus").load("inventory/_brandSku.aspx?action=get&id=<%= Request["id"] %>"); console.log('updated inline content.'); $(".editModal").fancybox({'type' : 'iframe'});}
		});
	})
	
	function checkWine(v) {
		if (v == 1001) {
			$(".wineOnly").show();
			$(".packOnly").hide();
		} else {
			$(".wineOnly").hide();
			$(".packOnly").hide();
		}
		
		// see if we should show the product grouping too
		if (v == 1002 || v == 1003) {
			$(".packOnly").show();
		}
	}
	
	var groupWineNumber = <%= groupWineNumber %>;
	
	function lookup() {
		$("#sandbox").load("/modal/_lookupInventory.aspx?num=" + groupWineNumber + "&id=" + $("#lookupID").val(), function(response, status, xhr) { 
				if (response != "") {
					$("#groupWines").append(response); 
					$("#lookupID").val(""); 
					groupWineNumber++;
				} else {
					alert("Can't find product.");
					$("#lookupID").focus();
				}
			}
		);
		
	}
	
	$(document).ready(function(){
    	checkWine(document.getElementById('productTypeID').value);
  	});
	
  </script>
<style>
	#sku div {
		margin: 5px;	
	}
	
	tr:hover {
		background: none;	
	}
</style>
<div style="display: none" id="sandbox"></div>
<form action="" method="post" id="oForm">    
        <table width="100%" id="Add">
          <tr> 
            <td width="46%" valign="top"> <table width="100%" border="0" cellspacing="2">
                <tr> 
                  <td colspan="2"><strong><font size="2">Core Product Information</font></strong></td>
                </tr>
                <tr> 
                  <td title="This is a friendly code for the sku, which is shown to the customer.">Code</td>
                  <td><input name="code" id="code" type="text" title="Product code is required" minlength="4" class="validate[required]" size="5" runat="server" /></td>
                </tr>
                <tr>
                  <td title="This is the name of the product.">Name</td>
                  <td><input name="name" id="name" title="Product name is required." class="validate[required]" type="text" runat="server" /></td>
                </tr>
                <tr> 
                  <td>Product Type</td>
                  <td><div id="prodType" runat="server"> <select name="productTypeID" id="productTypeID" onChange="checkWine(this.value)" class="validate[required]">
                      <option></option>
                      <option value="1001">Bottle</option>
                      <option value="1002">Case</option>
                      <option value="1003">Pack</option>
                      <option value="1004">NoneWine</option>
                      <option value="1005">Literature</option>
                      <option value="1006">Service</option>
                      <option value="1007">Voucher</option>
                    </select></div></td>
                </tr> 
                <tr> 
                  <td title="This is the cost per bottle, which is taken from the PO.">Cost</td>
                  <td>$ 
                    <input name="cost" type="text" id="cost" title="Please enter cost as a dollar amount." class="validate[required]" size="5" runat="server" /></td>
                </tr>
                <tr> 
                  <td title="This is MSRP for the product, not the actual sale price.">Price</td>
                  <td>$ 
                    <input name="price" type="text" id="price" title="Please enter price as a dollar amount." class="validate[required]" size="5" runat="server" /></td>
                </tr>
                <tr> 
                  <td title="Is this product discountable? Most likely since only items like fine & rare aren't.">Discountable</td>
                  <td><label> 
                    <input name="discountable" type="radio" class="validate[required]" id="discountable" value="True" checked="checked" />
                    Yes<br />
                    <input type="radio" name="discountable" id="discountable" value="False" />
                    No </label></td>
                </tr>
                <tr> 
                  <td  title="When do we think this product will be no longer safe to consume?">Expiration</td>
                  <td><input name="expirationDate" type="text" title="Expiration date is required." class="datepicker validate[required]" id="expirationDate" runat="server" /></td>
                </tr>
                <tr> 
                  <td title="Dead or alive? This needs to be set to yes if you want to sell the product (by marking this yes, it doesn't mean it's active on any web sites, for that you need to add Brand Skus after you enter this core information).">Active</td>
                  <td><input name="active"  class="validate[required]" type="radio" id="active" value="True" <% if (_active == "True") Response.Write("checked='checked'"); %> />
                    Yes<br /> <input type="radio" name="active" id="active" value="False" <% if (_active == "False") Response.Write("checked='checked'"); %> />
                    No </td>
                </tr>
                <tr> 
                  <td title="Individual unit quantities, not cases.">Quantity</td>
                  <td><input name="quantity" type="text" title="Quantity must be entered as a number." class="validate[required]" id="quantity" size="5" runat="server" /></td>
                </tr>
                <tr> 
                  <td title="The warehouse where this product is.">Warehouse</td>
                  <td>
                  	<select name="warehouse" class="validate[required]" id="warehouse">
                        <option value="2" <% if (_warehouse == "2") { %> selected<% } %>>WineDirect</option>
                    </select>
                    </td>
                </tr>
                <!--<tr> 
                  <td>Backup Product</td>
                  <td><div id="prodSelect" runat="server"></div></td>
                </tr>-->
                <tr> 
                  <td title="PO number this product came in on.">Purchase Order</td>
                  <td><input name="purchaseOrder" type="text" id="purchaseOrder" runat="server" class="validate[required]" size="5" /></td>
                </tr>
                <tr> 
                  <td title="Take this from the PO information.">Date Ordered</td>
                  <td><input name="orderDate" type="text" class="datepicker" id="orderDate" runat="server" />                  </td>
                </tr>
                <tr> 
                  <td title="When did this product arrive at our warehouse?">Arrival Date</td>
                  <td><input name="arrivalDate" type="text" class="datepicker" id="arrivalDate" runat="server" /></td>
                </tr>
                <tr>
                  <td title="Is this a continuity item? If you don't mark this as a continuity item then it will not be available for entry into the continuity system!">Continuity</td>
                  <td><input name="continuity" class="validate[required]" type="radio" id="continuity" value="True" <% if (_continuity == "True") Response.Write("checked='checked'"); %> />
Yes<br />
<input name="continuity" type="radio" id="continuity" value="False"  <% if (_continuity == "False") Response.Write("checked='checked'"); %> />
No </td>
                </tr>
                <tr> 
                  <td colspan="2" title="Internal use only. This is useful for entering information we need, but don't want to show to the customer.">Short Description<br /> <textarea name="shortDescription" cols="30" id="shortDescription" class="validate[required]" runat="server"></textarea></td>
                </tr>
            </table></td>
            <td width="54%" valign="top"> <table width="100%" border="0" cellspacing="2">
                <tr class="wineOnly"> 
                  <td colspan="2"><strong><font size="2">Wine Only Information</font></strong></td>
                </tr>
                <tr class="wineOnly">
                  <td>Vintage</td>
                  <td><input name="vintage" type="text" id="vintage" title="Vintage is required." size="5" maxlength="5" runat="server" /></td>
                </tr>
                <tr class="wineOnly"> 
                  <td>Producer</td>
                  <td><div id="producerSelect" runat="server"></div></td>
                </tr>
                <tr class="wineOnly"> 
                  <td>Style</td>
                  <td><div id="styleSelect" runat="server" ></div></td>
                </tr>
                <tr class="wineOnly"> 
                  <td width="45%">Varietal</td>
                  <td width="55%"><div id="varietalSelect" runat="server"></div></td>
                </tr>
                <tr class="wineOnly"> 
                  <td>Region</td>
                  <td><div id="regionSelect" runat="server"></div></td>
                </tr>
                <tr class="wineOnly"> 
                  <td>Appellation</td>
                  <td><div id="appellationSelect" runat="server"></div></td>
                </tr>
                <tr class="wineOnly"> 
                  <td>Alcohol %</td>
                  <td><input name="alcoholPercentage" type="text" title="Please enter the percentage as a number." id="alcoholPercentage" size="5" runat="server" /></td>
                </tr>
                <tr class="wineOnly"> 
                  <td>Bottle Volume</td>
                  <td><input name="bottleVolume" type="text" title="Please enter the volume as a number in ML" id="bottleVolume" size="5" runat="server" />
                    ml</td>
                </tr>
                <tr class="wineOnly"> 
                  <td>Mystery Eligible</td>
                  <td><input name="mystery"  class="validate[required]" type="radio" id="mystery" value="True" <% if (_mystery == "True") Response.Write("checked='checked'"); %> />
                    Yes<br /> <input type="radio" name="mystery" id="mystery" value="False" <% if (_mystery == "False") Response.Write("checked='checked'"); %> />
                    No </td>
                </tr>
                <tr class="packOnly"> 
                  <td colspan="2"><strong>Items in Pack/Case</strong></td>
                </tr>
                <tr class="packOnly"> 
                  <td colspan="2"><div id="productGroup" runat="server" />
                  		<div style="padding: 5px;">ID: <input type="text" size=3 id="lookupID"> <a href='#' onClick="lookup()">Add Item to Pack</a></div>
                  </td>
                </tr>
              </table>
              <table style="width: 100%" id="brandSkuTable" runat="server" visible="false">
              	<tr>
                	<td colspan="2"><strong>Brand Skus</strong></td>
               	</tr>
              	<tr>
                	<td colspan="2"><div id="brandSkus" runat="server" /></td>
               	</tr>
              	<tr>
                	<td colspan="2" style="padding: 5px;">
                    <a href="inventory/_brandSku.aspx?id=<%= Request["id"] %>" class="modal">Add Sku</a></td>
               	</tr>
              </table>
              </td>
          </tr>
          <tr> 
            <td colspan="2"><input type="hidden" name="subAction" id="subAction" value="add" runat="server" /> 
              <input type="submit" name="submitBtn" id="submitBtn" value="Add Product" runat="server" /></td>
          </tr>
        </table>
</form>