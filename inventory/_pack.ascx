<%@  Control Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Product p = new Product();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["id"] != null && Request["action"] == "packs") {
			// get children
			DataTable table = p.GetChildren(Request["id"]); 
			foreach (DataRow row in table.Rows) {
				string style = "";
				if (Convert.ToInt32(row["InventoryQuantity"].ToString()) < 0) {
					style = "color: red; font-weight: bold";	
				} else if (Convert.ToInt32(row["InventoryQuantity"].ToString()) < 30) {
					style = "color: orange; font-weight: bold";	
				}
				Response.Write("<div style='" + style + "'>" + row["Quantity"].ToString() + " " + 
							" of " + row["Vintage"].ToString().Trim() + " " + 
							row["Name"].ToString().Trim() + 
							" (" + row["InventoryQuantity"].ToString().Trim()  + " available)</div>");	
			}
			table = null;
			p.Dispose();
			p = null;
			Response.End();
		} else  if (Request["action"] == "pack") {
			// see if we need to display results
			DataTable dt = p.GetInventoryPacks(); 
			
			results.InnerHtml = "<table id='myTable' class='tablesorter'><thead><tr><th>ProductID</th><th>Name</th><th>Quantity</th></tr></thead><tbody>";
				foreach (DataRow r in dt.Rows) {
				string style = "";
				if (Convert.ToInt32(r["Quantity"].ToString()) < 0) {
					style = "color: red; font-weight: bold";	
				} else if (Convert.ToInt32(r["Quantity"].ToString()) < 30) {
					style = "color: orange; font-weight: bold";	
				}
					results.InnerHtml += "<tr class='product' id='" + r["ProductID"]  + "' style='" + style + "'><td onclick=\"document.location.href = '?action=add-change&id=" + r["ProductID"]  + "';\">" + 
								r["ProductID"] + "</td><td>" + r["Name"] + 
								" - [<a href='?action=clone&sub-action=get&id=" + r["ProductID"]  + "'>Clone</a>]</td><td><a href='?id=" + r["ProductID"]  + "' rel='?action=packs&id=" + r["ProductID"]  + "' class='tag'>" + r["Quantity"].ToString() + 
								"</a></td></tr>\r\n";
				}
			results.InnerHtml += "</tbody></table>";
			dt = null;
		}
		
		p.Dispose();
		p = null;
	}

		
</script>

<script src="/js/jquery.tablesorter.min.js"></script>
<script src="/js/opentip.js"></script>
<script type="text/javascript">
	
	$(document).ready(function(){
		$("#myTable").tablesorter(); 
		
		$('a.tag').cluetip({showTitle: false, width: '400px'});
  	});
	
  </script>
<link rel="stylesheet" href="/js/cluetip.css" type="text/css" />

<div id="results" runat="server" />