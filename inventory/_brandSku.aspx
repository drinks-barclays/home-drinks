﻿<%@  Page Language="C#"  validateRequest="false"%>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">
   	
	Product p = new Product();
	DataTable dt = new DataTable();
	string _brandID = "0";
	string _available = "0";
	public void Page_Load(Object sender, EventArgs e)
 	{
		
		id.Value = Request["id"];
		
		if (Request["id"] != null && Request["action"] == "get") {
			// show the brand skus
			Hashtable parameters = new Hashtable();
			parameters.Add("ProductID", Request["id"]);
			p.GetBrandSku(out dt, parameters);
			
			if (dt.Rows.Count > 0) {
				Response.Write("<table><thead><tr><th>Brand</th><th>Price</th><th>Available</th><th></th></thead></tr>");
				foreach (DataRow r in dt.Rows) {
					Response.Write("<tr>");
					Response.Write("<td>" + r["Brand"] + "</td>");
					Response.Write("<td>" + Convert.ToDouble(r["Price"].ToString()).ToString("c").Replace("$", "") + "</td>");
					Response.Write("<td>" + r["Available"] + "</td>");
					Response.Write("<td><a href='inventory/_brandSku.aspx?action=edit&skuID=" + r["ProductBrandID"] + "' class='editModal'>[Edit]</a></td>");
					Response.Write("</tr>");
				}
				Response.Write("</table>");
			} else {
				Response.Write("(no skus found)");	
			}
			dt = null;
			p.Dispose();
			p = null;
			Response.End();
		} else if (Request["id"] != null && Request["action"] == "add") {
			// add a brand sku
			try {
				Hashtable parameters = new Hashtable();
				parameters.Add("ProductID", Request["id"]);
				parameters.Add("BrandID", Request["brandID"]);
				parameters.Add("Price", Request["price"]);
				parameters.Add("Available", Request["available"]);
				parameters.Add("Headline", Request["headline"]);
				parameters.Add("Description", Request["description"]);
				
				p.AddBrandSku(parameters); // output new product id
				Response.Write("Sku added successfully!");
				
			} catch (Exception ex)
			{
				Response.Write(Request.Form + "<br>" + ex.ToString());
				Response.End();
			}
				
		} else if (Request["skuID"] != null && Request["action"] == "edit") {
			Edit();
		} else  if (Request["skuID"] != null && Request["action"] == "update") {
			try {
				Hashtable parameters = new Hashtable();
				parameters.Add("ProductBrandID", Request["skuID"]);
				parameters.Add("BrandID", Request["brandID"]);
				parameters.Add("Price", Request["price"]);
				parameters.Add("Available", Request["available"]);
				parameters.Add("Headline", Request["headline"]);
				parameters.Add("Description", Request["description"]);
				
				p.EditBrandSku(parameters); // output new product id
				Response.Write("Sku updated successfully!");
				Edit();
			} catch (Exception ex)
			{
				Response.Write(Request.Form + "<br>" + ex.ToString());
				Response.End();
			}
		}
		
		dt = p.GetBrands();
		/* parse out brands*/
		brandSelect.InnerHtml = "<select name='brandID' id='brandID' class=\"validate[required]\" title='Brand is required.'>" +
									"<option></option>";
		foreach (DataRow r in dt.Rows) {
			brandSelect.InnerHtml += "<option value='" + r["BrandID"] + "'";
			if (r["BrandID"].ToString() == _brandID) brandSelect.InnerHtml += " selected ";
			brandSelect.InnerHtml += ">" + r["Name"] + "</option>\r\n";
		}
		brandSelect.InnerHtml += "</select>";
		
			dt = null;
			p.Dispose();
			p = null;
	}
	
	void Edit() {
			action.Value = "update";
			btn.Value = "Update Sku";
			Hashtable parameters = new Hashtable();
			parameters.Add("ProductBrandID", Request["skuID"]);
			p.GetBrandSku(out dt, parameters);
			
			foreach(DataRow r in dt.Rows) {
				price.Value = Convert.ToDouble(r["Price"].ToString()).ToString("c").Replace("$", "");
				headline.Value = r["Headline"].ToString();
				description.Value = r["Description"].ToString();
				_brandID = r["BrandID"].ToString();
				_available = r["Available"].ToString();
			}	
	}
	
</script>

<html lang="en" style="width: 250px; height: 400px;">
<head>

<script src="//code.jquery.com/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/pepper-grinder/jquery-ui.min.css" media="screen" />
<script src="/js/jquery.validationEngine-en.js" type="text/javascript"></script>
<script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
<link rel="stylesheet" href="//heartwoodandoak.com/assets/css/validationEngine.jquery.css" type="text/css" media="screen" charset="utf-8" />

<link rel="stylesheet" type="text/css" href="style.css" media="screen" />

<script type="text/javascript">
	$(document).ready(function() {	
    	 $( document ).tooltip();
		 $("#oForm").validationEngine();
		 $("#sku").validationEngine();
	});
</script>
</head>

<body>
<div id="addSku" runat="server">
						<form id="sku" action="_brandSku.aspx" method="post">                        
                        <table>
                            <tr>
                            	<td title="Which brand do you want this product to be sold on?">
                                <label for="brandID">Brand: </label><br /><span id="brandSelect" runat="server" />
                            	</td>
                            </tr>
                            <tr>
                            	<td title="How much do you want to sell this item for on the web site? This is regular pricing, not a sale price, for that you would discount it through the campaign entry system!">
                            	<label for="price">Price: </label>
                            	<br />
                            	$ <input type="text"  name="price" id="price" size="5" class="validate[required]" runat="server" />
                                </td>
                            </tr>
                            <tr>
                            	<td title="Do you want this to be shown on the main web site? For landing pages only or continuity only, mark this as NO!">
                                Available:<br/>
                            	<input type="radio" id="available1" name="available" value="1" class="validate[required]" runat="server" /><label for="available1">Yes</label>
                                <input type="radio" id="available0" name="available" value="0" runat="server" /><label for="available0">No</label>
                                </td>
                            </tr>
                            <tr>
                            	<td title="This is the line that is shown on the catalog page and right under the title on the detail page. Be wise and witty when entering this, this is a call-out!">
                            	<label for="headline">Headline: </label><br />
                            	<input type="text" id="headline" name="headline" class="validate[required]" runat="server" />
                                </td>
                            </tr>
                            <tr>
                            	<td title="This is the description that is shown on the product detail page.">
                                <label for="description">Description: </label><br />
                            	<textarea id="description" name="description" class="validate[required]" runat="server"></textarea>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                <input type="hidden" name="id" value="" id="id" runat="server" />
                                <input type="hidden" name="skuID" value="<%= Request["skuID"] %>" id="skuID" />
                                <input type="hidden" name="action" value="add" id="action" runat="server" />
                                <input type="submit" value="Add Sku" id="btn" runat="server" />
                                </td>
                            </tr>
                        </table>
                        </form>
</div>
</body>
</html>