<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Assembly src="/bin/Utilities.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.IO" %>
<script language="C#" runat="server">

	Product p = new Product();
							Hashtable parameters = new Hashtable();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "upload") {
			using (StreamReader sr = new StreamReader(file.PostedFile.InputStream)) 
			{
				string line;
				string[] splitted;
				char[] splitter = { ',' };
						
				int i = 0;
				while ((line = sr.ReadLine()) != null) 
				{					
					splitted = line.Split(splitter);
					try {
						if (splitted[0] != null && splitted[1] != null) {
							string sSubject = subject.InnerHtml;
							string sBody = body.InnerText;
							
							// 0-BrandID	1-Brand	2-InvoiceID	3-CustomerID	4-Email	5-FirstName	6-DateAdded	7-Voucher
							sSubject = sSubject.Replace("[ORDERDATE]", splitted[6].ToString()); // ORDERDATE
							sBody = sBody.Replace("[BRANDNAME]", splitted[1].ToString());// BRANDNAME
							sBody = sBody.Replace("[VOUCHERCODE]", splitted[7].ToString());// VOUCHERCODE
							sBody = sBody.Replace("[FIRSTNAME]", splitted[5].ToString().Trim());// FIRSTNAME
							sBody = sBody.Replace("[BRANDPHONE]", CSNumber(splitted[0].ToString()));// BRANDPHONE
							
							Util u = new Util();
							u.SendMailServer(splitted[5].ToString().Trim(), splitted[4].ToString().Trim(), sSubject.Replace("\r\n", ""), sBody, CSEmail(splitted[0].ToString()), CSEmail(splitted[0].ToString()));	
						}
					} catch (Exception ex) {
						Response.Write(ex.ToString());	
					}
				}
			}
		}
		p.Dispose();
		p = null;
	}
	
	public string CSNumber(string brandID) {
		string s = "";
		switch (brandID) {
		 case "2":
		 	s = "(877) 998-7666"; // troon
			break;	
		 case "7":
		 	s = "(888) 661-1246"; // heartwood
			break;	
		 default:
		 	s = "(888) 380-2337"; // barclays
			break;	
		}
		
		return s;
	}
	public string CSEmail(string brandID) {
		string s = "";
		switch (brandID) {
		 case "2":
		 	s = "customerservice@troonwineclub.com"; // troon
			break;	
		 case "7":
		 	s = "cs@heartwoodandoak.com"; // heartwood
			break;	
		 default:
		 	s = "cs@barclayswine.com"; // barclays
			break;	
		}
		
		return s;
	}
	
	
	
</script>
<body style="padding: 5px;">
<form action="" method="post" enctype="multipart/form-data" runat="server" style="padding: 5px;">
	Select a file to upload:<br />
	<input id="file" name="file" type="file" runat="server" style="margin: 5px;">
	<br>
    <input type="hidden" value="upload" name="action" id="action" />
<input type="submit" value="Upload" style="margin: 5px;" />
</form>

<div id="subject" runat="server" visible=false>Whoops! We had a problem with your order on [ORDERDATE]!</div>

<div id="body" runat="server" visible=false>
<P>Dear [FIRSTNAME],</P>
 
<P>Thank you for your order with [BRANDNAME]. We really appreciate your business.</P>
 
<P>Unfortunately, we had some system errors and were not able to process your order successfully. Your credit card was authorized (which may show as "pending" in your online banking application) but was not charged, and we have reversed the authorization. In addition, if you used a voucher to place your order your voucher code has been re-activated and you should now be able to choose your wines again.</P>
 
<P>If you have further trouble please give us a call at [BRANDPHONE] and our customer service representatives will happily place the order for you.</P>
 
<P>Please accept this voucher, good for 90 days, that allows you to take $25 off your next order (excluding shipping):
[VOUCHERCODE]</P>
 
<P>Once again, thank you for your business and please accept our sincere apologies for the extra trouble we've caused you.</P>
 
 
<P>Cheers,</P>
 
<P>[BRANDNAME]</P>
</div>
</body>