﻿<%@  Control Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Product p = new Product();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "clone") {
			switch (Request["sub-action"]) {
				case "get":
					DataTable table = p.GetChildren(Request["id"]); 
					foreach (DataRow row in table.Rows) {
						data.InnerHtml += "<div style='cursor: e-resize; margin-bottom: 5px;' class='child' id='" + row["Quantity"].ToString() + "' data-id='" + row["ChildProductID"].ToString() + "' data-quantity='" + row["Quantity"].ToString() + "' data-name='" + row["Name"].ToString().Trim() + "'>" + row["Quantity"].ToString() + " " + 
									" of [" + row["ChildProductID"].ToString().Trim() + "] - " + row["Vintage"].ToString().Trim() + " " + 
									row["Name"].ToString().Trim() + "</div>";	
					}
					table = null;
					p.Dispose();
					p = null;
					break;
				case "add":
					Add();
					break;
				default:
					break;	
			}
		}
	}
	
	void Add() {
				string productID = p.CloneProduct(Request["id"]); // output new product id
				if (Request["groupWines_1"] != null) {
					int g = 1;
					while (Request["groupWines_" + g] != null) {
						p.AddGroupWine(Convert.ToInt32(productID), Convert.ToInt32(Request["groupWines_" + g + "_qty"].ToString()), Convert.ToInt32(Request["groupWines_" + g].ToString()));
						g++;
					}
				}	
				Response.Redirect("inventory-management.aspx?action=add-change&id=" + productID);
	}
	
</script>

<script type="text/javascript">
	var groupWineNumber = 1;
	
	function lookup() {
		$("#sandbox").load("/modal/_lookupInventory.aspx?num=" + groupWineNumber + "&id=" + $("#lookupID").val(), function(response, status, xhr) { 
				if (response != "") {
					$("#children").append(response); 
					$("#lookupID").val(""); 
					groupWineNumber++;
				} else {
					alert("Can't find product.");
					$("#lookupID").focus();
				}
			}
		);
		
	}

	$(document).ready(function(){
		$(".child").click(
					function () 
					{ 
						// add item
						var x = "<input type=hidden name='groupWines_" + groupWineNumber + "' value='" + $(this).data('id') + "' />"; // id
						x += " - Qty: <input type=text size=3 name='groupWines_" + groupWineNumber + "_qty' value='" + $(this).data('quantity') + "' />"; // quantity
						$("#children").append($(this).data('id') + " - " + $(this).data('name') + x + '<br />'); // text
						groupWineNumber++;
					}
		);		
  	});
</script>
<div id="sandbox" style='display: none'></div>
<div align="center">Click on an item on the left to add it to the new sku and/or add using the form on the right.</div>
<form action="inventory-management.aspx?action=clone&id=<%= Request["id"] %>" method="post">
<table>
	<thead>
  <th>Original Items</th>
        <th>New Items</th>
    </thead>
    <tr>
   	  <td width="50%"><div id="data" runat="server"></div></td>
        <td>
        	<div id="children">
            	<div style="padding: 5px;">ID: <input type="text" size=3 id="lookupID"> <a href='#' onClick="lookup()">Add Item to Pack</a></div>
            </div>
        </td>
    </tr>
</table>
<div align="center"><input type="submit" value="Clone" /><input type="hidden" name="sub-action" value="add" /></div>
</form>

