<%@  Control Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Product p = new Product();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["amount"] != null) {
			Hashtable parameters = new Hashtable();
			parameters.Add("FromID", Request["fromID"]);
			parameters.Add("ToID", Request["toID"]);
			parameters.Add("Amount", Request["amount"]);
			p.AllocateInventory(parameters);	
		}
		
		p.Dispose();
		p = null;
	}
	
	
</script>
<form action="" method="post">
<div>
	Allocate <input width=4 type="text" name="amount" value="" /> bottles from <input width=4 type="text" name="fromID" value="" /> to <input width=4 type="text" name="toID" value="" /> <input type="submit" value="Submit" />
</div>
</form>