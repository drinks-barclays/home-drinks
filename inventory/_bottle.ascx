<%@  Control Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Product p = new Product();
   	public void Page_Load(Object sender, EventArgs e) 
 	{
		
		if (Request["sku"] != null) {
			// get children
			DataTable table = p.GetParents(Request["sku"]); 
			foreach (DataRow row in table.Rows) {
				Response.Write("<div>" + row["Quantity"].ToString().Trim() + " " + 
							" inside of [" + row["ParentProductID"].ToString() + "] " + row["Name"].ToString().Trim() + "</div>");	
			} 
			
			if (table.Rows.Count < 1) {
				Response.Write("<div>Not in any packs.</div>");	
			}
			table = null;
			p.Dispose();
			p = null;
			Response.End();
		} else if (Request["action"] == "bottle" || Request["action"] == null) {
			
			GetSelects();
			
			// see if we need to display results
			Hashtable parameters = new Hashtable();
			if (Request["varietalID"] != null && Request["varietalID"] != "") parameters.Add("VarietalID", Request["varietalID"]);
			if (Request["regionID"] != null && Request["regionID"] != "") parameters.Add("RegionID", Request["regionID"]);
			if (Request["styleID"] != null && Request["styleID"] != "") parameters.Add("StyleID", Request["styleID"]);
			if (Request["k"] != null && Request["k"] != "") {
				parameters.Add(Request["by"], Request["k"]);
			} else {
				parameters.Add("Active", 1);
				parameters.Add("ProductTypeID", 1001);
			}
			
			DataTable dt = p.Search(parameters);
			DataTable warehouses = p.GetWarehouseInventory();
			
			dt = dt.DefaultView.ToTable(true, new string[] { "ProductID", "Name", "Cost", "Code", "Quantity", "DateAdded", "Warehouse", "MysteryEligible" });
			
			results.InnerHtml = "<table id='myTable' class='tablesorter'><thead><tr><th>ProductID</th><th>Code</th><th>Name</th><th>Cost</th><th>Mystery</th><th>DateAdded</th><th>AC Qty</th><th>GW Qty</th><th>Avail Qty</th></tr></thead><tbody>";
				foreach (DataRow r in dt.Rows) {
					string style = "";
					if (Convert.ToInt32(r["Quantity"].ToString()) < 0) {
						style = "color: red; font-weight: bold";	
					} else if (Convert.ToInt32(r["Quantity"].ToString()) < 60) {
						style = "color: orange; font-weight: bold";	
					}
					results.InnerHtml += "<tr class='product' id='" + r["ProductID"]  + "' style='" + style + "'><td onclick=\"document.location.href = '?action=add-change&id=" + r["ProductID"]  + "';\">" + 
								r["ProductID"] + "</td><td>" + 
								r["Code"] + "</td><td>" + r["Name"] + 
								"</td><td>" + String.Format("{0:c}", r["Cost"]) + 
								"</td><td>" + r["MysteryEligible"] + 
								"</td><td>" + String.Format("{0:d}", r["DateAdded"]) + 
								"</td>";
					// display warehouse counts
					DataRow[] foundrows = warehouses.Select("Warehouse LIKE 'AmericanCanyon%' AND ProductID = " + r["ProductID"]);
					if (foundrows.Length > 0) { results.InnerHtml += "<td>" + foundrows[0]["Quantity"] + "</td>"; } else { results.InnerHtml += "<td>0</td>"; }
					foundrows = warehouses.Select("Warehouse LIKE 'Glenwillow%' AND ProductID = " + r["ProductID"]);
					if (foundrows.Length > 0) { results.InnerHtml += "<td>" + foundrows[0]["Quantity"] + "</td>"; } else { results.InnerHtml += "<td>0</td>"; }
					
					results.InnerHtml += "<td><a href='?sku=" + r["ProductID"]  + "' rel='?sku=" + r["ProductID"]  + "' class='tag'>" + r["Quantity"].ToString() + 
								"</a></td></tr>\r\n";
				}
			results.InnerHtml += "</tbody></table>";
			dt = null;
		}
		
		p.Dispose();
		p = null;
	}
	
	private void GetSelects() {
		DataTable dt;

		// parse out varietals
		dt = p.GetVarietals();
		varietalSelect.InnerHtml = "<select name='varietalID' id='varietalID'>" +
									"<option value=''>Varietal</option>";
		foreach (DataRow r in dt.Rows) {
			varietalSelect.InnerHtml += "<option value='" + r["VarietalID"] + "'";
			if (r["VarietalID"].ToString() ==  Request["varietalID"]) varietalSelect.InnerHtml += " selected ";
			varietalSelect.InnerHtml += ">" + r["Name"] + "</option>\r\n";
		}
		varietalSelect.InnerHtml += "</select>";
		
		// parse out styles 
		dt = p.GetStyles();
		styleSelect.InnerHtml = "<select name='styleID' id='styleID'>" +
									"<option value=''>Style</option>";
		foreach (DataRow r in dt.Rows) {
			styleSelect.InnerHtml += "<option value='" + r["StyleID"] + "'";
			if (r["StyleID"].ToString() ==  Request["styleID"]) styleSelect.InnerHtml += " selected ";
			styleSelect.InnerHtml += ">" + r["Name"] + "</option>\r\n";
		}
		styleSelect.InnerHtml += "</select>";
		
		// parse out regions
		dt = p.GetRegions();
		regionSelect.InnerHtml = "<select name='regionID' id='regionID'>" +
									"<option value=''>Region</option>";
		foreach (DataRow r in dt.Rows) {
			regionSelect.InnerHtml += "<option value='" + r["RegionID"] + "'";
			if (r["RegionID"].ToString() ==  Request["regionID"]) regionSelect.InnerHtml += " selected ";
			regionSelect.InnerHtml += ">" + r["Name"] + "</option>\r\n";
		}
		regionSelect.InnerHtml += "</select>";
		dt = null;
	}
	
</script>

<script src="/js/jquery.tablesorter.min.js"></script>
<script src="/js/opentip.js"></script>
<script type="text/javascript">
	
	$(document).ready(function(){
		$("#myTable").tablesorter({sortList: [[5,0]]}); 
		
		$('a.tag').cluetip({showTitle: false, width: '350px'});
  	});
	
  </script>
<link rel="stylesheet" href="/js/cluetip.css" type="text/css" />
 <% if (Request["bare"] == null) { %>
<form action="" method="post">
<div id="search" style="margin-bottom: 10px;">
Search: <input name="k" id="k" type="text" />
<select name="by" id="by">
  <option value="Code">Code</option>
  <option value="Name">Name</option>
  <option value="ProductID">Product ID</option>
</select> <input type="submit" value="Search" /></form><br />
<form action="" method="post">
	<span class="sel" id="varietalSelect" runat="server" /> <span class="sel" id="styleSelect" runat="server" /> <span class="sel" id="regionSelect" runat="server" /> 
    <span class="sel" id="appellationSelect" runat="server" /> <span class="sel" id="producerSelect" runat="server" /> <input type="submit" value="Filter" />
</div>
</form>
<% } %>
<div id="results" runat="server" />