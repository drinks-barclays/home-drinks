﻿<%@  Page Language="C#" Debug="true" %>
<%@ Import Namespace="ZendeskApi_v2" %>
<%@ Import Namespace="ZendeskApi_v2.Models.Tickets" %>
<%@ Import Namespace="ZendeskApi_v2.Models.Constants" %>
<%@ Import Namespace="ZendeskApi_v2.Models" %>
<script language="C#" runat="server">

	public void Page_Load(Object sender, EventArgs e)
 	{
		try {
		ZendeskApi api = new ZendeskApi("https://drinks.zendesk.com/api/v2", "cintra@drinks.com", "", "I6WpWIZa9T3Ix2ZqFqDajrw5NWe47SMPme4iHQyE"); 
		
		//create the user if they don't already exist
		var u = api.Users.SearchByEmail("drinks-devops@drinks.com");
		if (u == null || u.Users.Count < 1)
			api.Users.CreateUser(new ZendeskApi_v2.Models.Users.User()
									 {
										 Name = "Drinks DevOps",
										 Email = "drinks-devops@drinks.com"
									 });
									 
    	var ticket = new Ticket()
                             {
                                 Subject = "DevOps Test",
                                 Comment = new Comment(){Body = "This is a test, discard please."},
                                 Priority = TicketPriorities.Normal,
								 GroupId = 21814425,
								 Requester = new Requester() {Email = "drinks-devops@drinks.com"}
                             };
		var res = api.Tickets.CreateTicket(ticket).Ticket;
		Response.Write(res.Id.Value);
		} catch (Exception ex) {
			Response.Write(ex.ToString());	
		}
	}
	

</script>