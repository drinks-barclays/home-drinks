﻿<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Net.Mime" %>
<script language="C#" runat="server"> 

	Product product = new Product();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		InventorySnapshot();	
	}

	private void InventorySnapshot() {
		Hashtable parameters = new Hashtable();
			parameters.Add("Active", 1);
			parameters.Add("ProductTypeID", 1001);
			DataTable dt = product.Search(parameters);
			dt = dt.DefaultView.ToTable(true, new string[] { "ProductID", "Name", "Cost", "Code", "Quantity" });
			
		StringBuilder sb = new StringBuilder(); 
		sb.Append("ProductID,Code,Name,Cost,Quantity\r\n");
				foreach (DataRow r in dt.Rows) {
					sb.Append(r["ProductID"] + "," + 
								r["Code"] + "," + r["Name"] + 
								"," + String.Format("{0:c}", r["Cost"]) + 
								"," + r["Quantity"].ToString() + 
								"\r\n");
				}
			
		SendEmail("Attached is the message.", "Inventory Snapshot", sb);
	}
	
	private void SendEmail(string s, string subject, StringBuilder sb) {
			string fileName = subject + "_" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + 
								"-" + DateTime.Now.Year.ToString() + ".csv";
			fileName = fileName.Replace(@"\", "");
			fileName = fileName.Replace(@"/", "");
			try {
				TextWriter tw = new StreamWriter(Server.MapPath("~/shipping_files/") + fileName);
				tw.Write(sb.ToString());
				tw.Close();
			}
			catch (Exception ex) {
				Response.Write(ex.ToString());	
			}
			
			try {
				Attachment data = new Attachment((Server.MapPath("~/shipping_files/") + fileName), MediaTypeNames.Application.Octet);
				MailMessage email = new MailMessage(
					   "inventory-robot@barclayswine.com",
					   "drinks-devops@drinks.com, tyson@drinks.com, josiah@drinks.com",
					   subject,
					   s);
				email.Attachments.Add(data);
				email.IsBodyHtml = true;
					
				NetworkCredential creds = new NetworkCredential("auth@voidme.com", "$m@ng0"); 
				SmtpClient client = new SmtpClient("127.0.0.1");
				client.UseDefaultCredentials = false;
				client.Credentials = creds;
				client.Send(email);
			} catch (Exception ex) {
				Response.Write(ex.ToString());	
			}
	}
	
</script>