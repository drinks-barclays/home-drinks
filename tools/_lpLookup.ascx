<%@  Control Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Marketing.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Marketing m = new Marketing();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["productID"] != null && Request["productID"].ToString() != "") {
			DataTable dt = new DataTable();
			m.ReverseLookup(out dt, Request["productID"]);
			
			if (dt.Rows.Count > 0) {
				resultSet.InnerHtml += "<table border=0 cellpadding=5 style='margin: 10px'>";
				resultSet.InnerHtml += "<tr><td><a href='/lp?campaign_id=" + dt.Rows[0]["CampaignID"].ToString() + "'>" + dt.Rows[0]["CampaignID"].ToString() + "</a></td></tr>";
				resultSet.InnerHtml += "</table>";
				
			} else {
				resultSet.InnerHtml += "<strong style='color: red; font-weight: bold'>No LPs found.</strong>";
			}
		}
		
	}
</script>

	<div style="font-size: 20px; margin-bottom: 10px;">LP Lookup</div> 
        <form action="" method="post" id="iForm">
			Product ID: 
			<input id="productID" name="productID" type="text" class="required" />
       	  <input name="submit" value="Submit" type="submit" />
        </form>
        <hr size="1" />
        <div id="resultSet" runat="server">	</div>