<%@  Control Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	DataSet ds = new DataSet();
	Order o = new Order();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		
			// show open orders
			resultSet.Visible = true;
			ds.Tables.Add(o.GetSuppressions());	
			ds.Tables.Add(o.GetOrdersForReview());			
			Display();
	
		
		o = null;
	}
	
	private void Display() {
			if (ds.Tables[0].Rows.Count > 0) {
				resultSet.InnerHtml += "<table><thead><tr><th>CampaignID</th><th>Customer Name</th><th>Fraud Risk</th><th>Order Date</th></tr></thead><tbody>";
				foreach (DataRow r in ds.Tables[0].Rows) {
					resultSet.InnerHtml += "<tr id='" + r["InvoiceID"] + "' style='margin:5px;'>";
					resultSet.InnerHtml += "<td>" + r["CampaignID"] + "</td><td><a href='customer.aspx?id=" + r["CustomerID"] + "'>";
					resultSet.InnerHtml += r["FirstName"] + " " + r["LastName"] + "</a></td>";
					resultSet.InnerHtml += "<td>" + ShowRiskFriendly(Convert.ToDouble(r["RiskScore"].ToString()))+ "</td><td>" + r["DateAdded"] + "</td>";
					resultSet.InnerHtml += "</tr>";
				}
				resultSet.InnerHtml += "</tbody></table>";
			} else {
				resultSet.InnerHtml = "<div class='err'>No orders to review.</div>";
			}	
			
			
			if (ds.Tables[1].Rows.Count > 0) {
				cpaReview.InnerHtml = "<table><thead><tr><th>CampaignID</th><th>Customer Name</th><th>Order Date</th></tr></thead><tbody>";
				foreach (DataRow r in ds.Tables[1].Rows) {
					cpaReview.InnerHtml += "<tr id='" + r["InvoiceID"] + "' style='margin:5px;'>";
					cpaReview.InnerHtml += "<td>" + r["CampaignID"] + "</td><td><a href='customer.aspx?id=" + r["CustomerID"] + "'>";
					cpaReview.InnerHtml += r["FirstName"] + " " + r["LastName"] + "</a></td>";
					//cpaReview.InnerHtml += "<td>" + ShowRiskFriendly(Convert.ToDouble(r["RiskScore"].ToString())) + "</td>";
					cpaReview.InnerHtml += "<td>" + r["DateAdded"] + "</td>";
					cpaReview.InnerHtml += "</tr>";
				}
				cpaReview.InnerHtml += "</tbody></table>";
			} else {
				cpaReview.InnerHtml = "<div class='err'>No orders to review.</div>";
			}	
	}
	
	private string ShowRiskFriendly(double score) {
		string output = "";
		if (score < 3) output = "<span style='color: green'>Low</span>";
		if (score >= 3 && score < 6) output = "<span style='color: orange; font-weight: bold;'>Moderate</span>";
		if (score >= 6) output = "<span style='color: red; font-weight: bold;'>High</span>";
		return output;
	}
</script>

			
                <div style="font-size: 20px; margin-bottom: 10px;">Suppressions</div> 
                <div id="resultSet" runat="server">	</div>
                <hr size="1" style="margin: 20px;" />
                <div style="font-size: 20px; margin-bottom: 10px;">CPA Review</div> 
                <div id="cpaReview" runat="server"></div>
            
            