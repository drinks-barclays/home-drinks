﻿<%@  Control Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Customer c = new Customer();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["grouponID"] != null && Request["grouponID"].ToString() != "") {
			c.ValidateVoucher(Request["grouponID"].ToString(), Request["val"].ToString());
			resultSet.InnerHtml += "<strong>Voucher changed successfully!</strong><br />";
		} else if (Request["voucherID"] != null && Request["voucherID"].ToString() != "") {
			
			DataTable dt = c.LookupVoucherOrder(Request["voucherID"]);
			string orderID = "0";
			
			if (dt.Rows.Count > 0) {
				resultSet.InnerHtml += "<table border=0 cellpadding=5 style='margin: 10px'>";
				resultSet.InnerHtml += "<tr><td>Voucher:</td><td>" + dt.Rows[0]["Code"] + "</td></tr>";
				resultSet.InnerHtml += "<tr><td>Order ID:</td><td>" + dt.Rows[0]["InvoiceID"] + "</td></tr>";
				resultSet.InnerHtml += "<tr><td>Valid:</td><td>" + dt.Rows[0]["Valid"].ToString() + "</td></tr>";
				resultSet.InnerHtml += "<tr><td>Value:</td><td>" + dt.Rows[0]["DollarOff"].ToString() + "</td></tr>";
				resultSet.InnerHtml += "<tr><td>Expires:</td><td>" + dt.Rows[0]["ExpirationDate"].ToString() + "</td></tr>";
				resultSet.InnerHtml += "<tr><td>CampaignID:</td><td>" + dt.Rows[0]["CampaignID"].ToString() + "</td></tr>";
				resultSet.InnerHtml += "</table>";
				
			} else {
				resultSet.InnerHtml += "<strong style='color: red; font-weight: bold'>No voucher found.</strong>";
			}
		}
		
	}
</script>

	<div style="font-size: 20px; margin-bottom: 10px;">Voucher Management</div> 
        <form action="" method="post" id="iForm">
        	<strong style="font-size: 14px;">Activate/Update Voucher</strong>
        	<hr size="1" style="margin-top: 2px;" />
			Voucher ID: 
			<input id="grouponID" name="grouponID" type="text" class="required" />
            Expires: <input type="date" id="expirationDate" name="expirationDate" />
            <select name="val">
            	<option value="1">Activate</option>
            	<option value="0">De-activate</option>
            </select>
       	  <input name="submit" value="Submit" type="submit" />
        </form>
        <br />
        <form action="" method="post" id="iForm2">
        	<strong style="font-size: 14px;">Voucher Lookup</strong><hr size="1" style="margin-top: 2px;" />
			Voucher ID: <input id="voucherID" name="voucherID" type="text" class="required" />
       	  <input name="submit" value="Submit" type="submit" />
        </form>
        <hr size="1" />
        <div id="resultSet" runat="server">	</div>