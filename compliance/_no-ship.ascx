<%@  Control Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Assembly src="/bin/ShoppingCart.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	public DataUtility d = new DataUtility(ConfigurationSettings.AppSettings["conn"]);
   	public void Page_Load(Object sender, EventArgs e)
 	{
		/* need to move everything into a Compliance.cs file
			- states datatable
			- methods/functions from the test page
		*/
		
		if (Request["action"] == "add") Add();
		
			ShoppingCart cart = new ShoppingCart();
			DataTable states;
			cart.GetShippingStates(out states);
			state.InnerHtml = "<select name='state' id='state' class='form-control' required><option value=''></option>";
			foreach(DataRow r in states.Rows) {
				state.InnerHtml += "<option value='" + r["State"].ToString().Trim() + "'>" + r["Name"].ToString().Trim() + "</option>\r\n";	
			}
			state.InnerHtml += "</select>";
		
	}
	
	public void Add() {
			Hashtable parameters = new Hashtable();
            parameters.Add("Street", Request["street"]);
            parameters.Add("City", Request["city"]);
            parameters.Add("PostalCode", Request["zip"]);
            parameters.Add("State", Request["state"]);
            parameters.Add("Hard", Request["hard"]);

			Result r = new Result();
            r = d.ExecuteNonQuery("sp_AddNoShipAddress", parameters);
            if (r.ResultType != Common.ResultType.Success)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			
			err.InnerText = "Address added!";
		}
</script>

	<div style="font-size: 20px; margin-bottom: 10px;">Add No Ship Address</div> 
        <form action="" method="post" id="iForm">
        	<div id="err" runat="server" />
        	<table>
            <tr>
            	<td>Street:</td>
                <td><input id="street" name="street" type="text" class="required" /></td>
            </tr>
            <tr>
            	<td>City:</td>
                <td><input id="city" name="city" type="text" class="required" /></td>
            </tr>
            <tr>
            	<td>State:</td>
                <td><div id="state" runat="server" /></td>
            </tr>
            <tr>
            	<td>Postal Code:</td>
                <td><input id="zip" name="zip" type="text" class="required" /></td>
            </tr>
            <tr>
            	<td>Type:</td>
                <td><select id="hard" name="hard">
            	<option value="0">Soft - Notify Customer</option>
                <option value="1">Hard - Don't Notify Customer</option>
            </select></td>
            </tr>
            </table>
            <br />
       	  <input name="submit" value="Submit" type="submit" />
          <input name="action" type="hidden" value="add" />
        </form>
        <hr size="1" />
        <div id="resultSet" runat="server">	</div>