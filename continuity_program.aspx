﻿<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Continuity.cs" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	public Product p = new Product();
	public Continuity c = new Continuity();
	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["id"] == null) {
			if (Request["action"] == "add") {
				AddProgram();	
			} else {
				// new program
				GetBrands("");
				GetRecurrence("");
				newCampaign.Visible = true;
			}
		} else {
			if (Request["action"] == "update") {
				EditProgram();
			} else {
				// get values to edit
				program.Visible = true;
				GetProgram();
			}
		}
	}
	
	private void AddProgram() {
		
	}
	
	private void EditProgram() {
		
	}
	
	private void GetProgram() {
		DataTable dt = new DataTable();
		Hashtable parameters = new Hashtable();
		parameters.Add("ContinuityID", Request["id"]);
		c.GetContinuity(out dt, parameters);
		
		foreach(DataRow r in dt.Rows) {
			brand.InnerText = r["BrandName"].ToString();
			programName.InnerText = r["Name"].ToString();
			shipSpan.InnerHtml = "Ships every <strong>" + r["Span"] + " </strong>weeks.";
			pricing.InnerHtml = "<strong>Price:</strong> " + Convert.ToDouble(r["Price"]).ToString("c") + "&nbsp; &nbsp;<strong>Shipping:</strong> " + Convert.ToDouble(r["Shipping"]).ToString("c");
			activeMembers.InnerText = "Active Members: " +  r["ActiveMembers"];
		}
		
		// get shipments
		dt = new DataTable();
		c.GetShipments(out dt, Request["id"]);
		
		// parse out current shipments
		string date = "StartDate <= '" + System.DateTime.Now.Date + "' AND EndDate >= '" + System.DateTime.Now.Date + "'";
		DataRow[] foundRows = dt.Select(date);
		currentShipments.InnerHtml = "<table style='border: dashed 1px #ccc; padding: 5px; width: 100%;' cellspacing='5'><thead><tr>";
		currentShipments.InnerHtml += "<th><strong>Type</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Product</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Ship Date</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Cycle</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Eligible</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Sent</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Cancels</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Unsubs</strong></th></tr></thead>";
		
		foreach(DataRow r in foundRows) {
			currentShipments.InnerHtml += "<tr><td>" + r["PreferenceType"] + "</td>";
			currentShipments.InnerHtml += "<td>" + r["ProductName"] + "</td>";
			currentShipments.InnerHtml += "<td>" + Convert.ToDateTime(r["StartDate"]).ToString("d") + " thru " + Convert.ToDateTime(r["EndDate"]).ToString("d") + "</td>";
			currentShipments.InnerHtml += "<td>" + r["ShipmentBegin"] + " to " + r["ShipmentEnd"] + "</td>";
			currentShipments.InnerHtml += "<td><strong>" + r["Going"] + "</strong></td>";
			currentShipments.InnerHtml += "<td><strong>" + r["Sent"] + "</strong></td>";
			currentShipments.InnerHtml += "<td><strong>" + r["Cancels"] + "</strong></td>";
			currentShipments.InnerHtml += "<td><strong>" + r["Unsubs"] + "</strong></td></tr>";
		}
		currentShipments.InnerHtml += "</table>";
		
		
		
	}
	
	private void GetBrands(string x) {
		// parse out brands
		DataTable dt = p.GetBrands();
		brandSelect.InnerHtml = "<select name='brandID' id='brandID' class=\"validate-not-first\" title='Brand is required.'>" +
									"<option value='0'></option>";
		foreach (DataRow r in dt.Rows) {
			brandSelect.InnerHtml += "<option value='" + r["BrandID"] + "'";
			if (r["BrandID"].ToString() == x) brandSelect.InnerHtml += " selected ";
			brandSelect.InnerHtml += ">" + r["Name"] + "</option>\r\n";
		}
		brandSelect.InnerHtml += "</select>";	
	}
	
	private void GetRecurrence(string x) {
		recurrenceSelect.InnerHtml = "<select name='span' id='span' class=\"validate-not-first\" title='Brand is required.'>" +
									"<option></option>";
		for (int i = 1; i < 53; i++) {
			recurrenceSelect.InnerHtml += "<option value='" + i + "'";
			if (i.ToString() == x) brandSelect.InnerHtml += " selected ";
			recurrenceSelect.InnerHtml += ">" + i + "</option>\r\n";
		}
		recurrenceSelect.InnerHtml += "</select>";
	}
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Continuity</title>
<script language="javascript" src="/includes/yui/build/yahoo/yahoo-min.js" type="text/javascript"></script>
<script language="javascript" src="/includes/yui/build/event/event-min.js" type="text/javascript"></script>
<script language="javascript" src="/includes/jsvalidate.js" type="text/javascript"></script>
<script type="text/javascript" src="/includes/mootools.js"></script>
<script type="text/javascript" src="/includes/js/jquery.js"></script> 
<script type="text/javascript" src="/includes/js/thickbox.js"></script>
<style type="text/css" media="all">@import "/includes/thickbox.css";</style>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />

<style>
	th {
	 text-align: left;	
	}
</style>

</head>

<body>
<div id="container">
    <div id="navigation">
      <WF:Nav runat="server" id="nav_ctl" navId=2 />
    </div>
    
    <div id="content">
    	<div id="header">Continuity</div>
        <div id="workingArea">
    		<div id="newCampaign" style="width: 650px; clear: both; padding: 3px;" runat="server" visible=false>
                <form method="post" action="">
                <div>
                	<strong>Brand:</strong><br />
                    <span id="brandSelect" runat="server"></span>           
                </div>
                <br />
                <div>
                	<strong>Name of Program:</strong><br />
                    <input name="name" id="name" type="text" title="Product name is required." class="required" runat="server" />           
                </div>
                <br />
                <div>
                	<strong>Recurrence of Shipments:</strong><br />
                    Ship every <span id="recurrenceSelect" runat="server"></span> weeks.      
                </div>
                <br />
                <div>
                	<strong>Price:</strong><br />
                    $
                    <input name="price" id="price" type="text" size="4" title="Price is required." class="required validate-currency-dollar" runat="server" />           
                </div>
                <br />
                <div>
                	<strong>Shipping:</strong><br />
                    $
                    <input name="shipping" id="shipping" type="text" size="4" title="Shipping is required." class="required validate-currency-dollar" runat="server" />           
                </div>
                <br />
                <input name="submit" type="submit" value="Add Program" />
            	</form>
        	</div>
            
            <div id="program" runat="server" visible=false>
            	<div id="brand" runat="server" />
                <div id="programName" style="font-size: 20px; font-weight:bold; margin-top: 3px; margin-bottom: 3px;" runat="server" />
                <div id="activeMembers" style="font-size: 13px; font-weight:bold; margin-top: 3px; margin-bottom: 3px; float: right;" runat="server" />
                <div id="pricing" style="font-size: 12px; margin-top: 3px; margin-bottom: 3px;" runat="server" />
                <div id="shipSpan" style="font-size: 10px; font-weight:bold; margin-top: 3px; margin-bottom: 3px;" runat="server" />
                <hr size="1" />
                <div style="float: right" align="right"><a style="color: green; font-weight: bold;" href='modal/_continuity.aspx?id=<%= Request["id"] %>&placeValuesBeforeTB_=savedValues&TB_iframe=true&height=200&width=400&modal=true' class='thickbox'>+ Add Shipment</a></div>
                <h3>Current Shipments</h3>
                <div id="currentShipments" runat="server">
                
                </div>
                
            </div>
        </div>
    </div>
    
    <div id="footer">
    
    </div>
</div>
</body>
</html>
</html>
