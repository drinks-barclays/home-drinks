<%@  Page Language="C#" Debug="true" validateRequest="false"  %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="../_nav.ascx" %>
<%@ Assembly src="/bin/Report.cs" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Assembly src="/bin/Marketing.cs" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<script language="C#" runat="server">

	Report report = new Report();
	Order o = new Order();
	Marketing m = new Marketing();
	Product p = new Product();
	DataTable dt;
	public int counter = 1;
   	public void Page_Load(Object sender, EventArgs e)
 	{
		
		if (Request["action"] == "add") {
			Hashtable parameters = new Hashtable();
			parameters.Add("CampaignID", Request["campaign_id"]);
			parameters.Add("Summary", Request["summary"]);
			parameters.Add("Details", Request["body"]);
			parameters.Add("UpsellDescription", Request["upsellDescription"].ToString().Replace("<p>", "").Replace("</p>", ""));	
			string lpID = m.AddLandingPage(parameters);
			
			// parse through products to add
			int i = 1;
			while (Request["_sku" + i] != null) {
				parameters = new Hashtable();
				parameters.Add("ProductID", Request["_sku" + i]);
				parameters.Add("Label", Request["_sku" + i + "_Description"]);
				parameters.Add("Upsell", Request["_sku" + i + "_Upsell"]);
				parameters.Add("LandingPageID", lpID);
				m.AddLandingPageProduct(parameters);
				i++;	
			}
			
			err.InnerHtml = "Page successfully created: <a href='/special-offer/?campaignID=" + Request["campaign_id"] + "'>View</a>";
		}
		
		DataTable campaigns = new DataTable();
		m.GetCampaignList(out campaigns);
		
		campaign.InnerHtml = "<select name='campaign_id' id='campaign_id'><option value='' selected>Please select a campaign ID</option>";
		foreach (DataRow r in campaigns.Rows) {
			campaign.InnerHtml += "<option value='" + r["CampaignID"] + "'";
			if (Request["campaign_id"] == r["CampaignID"].ToString()) campaign.InnerHtml += " selected";
			campaign.InnerHtml += ">" + r["CampaignID"] + ": " + r["PartnerName"].ToString().Trim() + " - " + r["Description"] + "</option>";	
		}
		campaign.InnerHtml += "</select>";
		
		if (Request["campaign_id"]!= null && Request["campaign_id"].ToString() != "") {
			DataTable dt = new DataTable();
					
			m.GetLandingPage(out dt, Request["campaign_id"]);
			if (dt.Rows.Count > 0) 
			{
				/* display data
				title.InnerHtml = "<span id='topic'>" + GetTopics(dt.Rows[0]["TopicID"].ToString()) + "</span> ";
				title.InnerHtml += " / <input type='text' id='title' name='title' value='" + dt.Rows[0]["Title"].ToString().Trim() + "'></input>";
				description.InnerHtml += "<input type='text' id='description' name='description' value='" + dt.Rows[0]["Description"].ToString().Trim() + "' style='width: 97%'></input>";
				author.InnerHtml += "by " + GetAuthors(dt.Rows[0]["AuthorID"].ToString());
				body.InnerHtml += "<textarea class='ckeditor' cols='80' id='body' name='body' rows='10'>" + dt.Rows[0]["Body"].ToString() + "</textarea>";
				tags.InnerHtml += "<input type='text' id='tags' name='tags' value='" + dt.Rows[0]["Tags"].ToString().Trim() + "' style='width: 97%'></input>";*/
				summary.InnerHtml = "<textarea class='ckeditor' cols='30' id='summary' name='summary' rows='10'>" + dt.Rows[0]["Summary"].ToString().Trim() + "</textarea>";
				details.InnerHtml = "<textarea class='ckeditor' cols='80' id='body' name='body' rows='10'>" + dt.Rows[0]["Details"].ToString().Trim() + "</textarea>";
				upsellDescription.InnerHtml = dt.Rows[0]["UpsellDescription"].ToString().Trim();
				action.Value = "add";
				
				// grab skus
				DataTable s = new DataTable();
				m.GetLandingPageProduct(out s,dt.Rows[0]["LandingPageID"].ToString(), Request["campaignID"]);
				
				skusInner.InnerHtml += GetPacks();		
				foreach (DataRow r in s.Rows) {
					skus.InnerHtml += "<input type='hidden' name='_sku" + counter + "' value='" + r["ProductID"] + "' />";
					skus.InnerHtml += "<input type='hidden' name='_sku" + counter + "_Upsell' value='" + r["Upsell"] + "' />";
					skus.InnerHtml += "<input type='hidden' name='_sku" + counter + "_Description' value='" + r["Label"] + "' /><div>" + r["ProductID"] + " - " + r["Label"] + "</div>";
					counter++;	
				}		
				
			} else {
				summary.InnerHtml = "<textarea class='ckeditor' cols='30' id='summary' name='summary' rows='10'></textarea>";
				details.InnerHtml = "<textarea class='ckeditor' cols='80' id='body' name='body' rows='10'>" + details.InnerHtml + "</textarea>";
				skusInner.InnerHtml += GetPacks();
				action.Value = "add";
			}
		} else {
				summary.InnerHtml = "<textarea class='ckeditor' cols='30' id='summary' name='summary' rows='10'></textarea>";
				details.InnerHtml = "<textarea class='ckeditor' cols='80' id='body' name='body' rows='10'>" + details.InnerHtml + "</textarea>";
				skusInner.InnerHtml += GetPacks();
				action.Value = "add";
			}
		
	}
	
	private string GetPacks() 
	{
			string output = "";
			DataTable dt = p.GetInventoryPacks();
			
			output = "<select id='productID'><option selected></option>";
				foreach (DataRow r in dt.Rows) {
					output += "<option value='" + r["ProductID"]  + "' title='" +  r["Name"].ToString().Trim() + "'>" + 
								r["ProductID"] + "-" + r["Name"].ToString().Trim() + 
								"</option>\r\n";
				}
			output += "</select> <br />Title:<input id='productName' type='text' /><input type='checkbox' id='upsell' /> Upsell <input type='button' id='addSku' value='Add Sku' /> <hr size=1 />";
			return output;
	}
	
</script>

<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>LP Editor</title>
<link rel="stylesheet" href="/css/validationEngine.jquery.css" type="text/css" media="screen" charset="utf-8" />
<script src="//code.jquery.com/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="/js/jquery.validationEngine-en.js" type="text/javascript"></script>
<script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
<script type="text/javascript" src="/fancybox/jquery.mousewheel-3.0.2.pack.js"></script>
<script src="/js/editor/ckeditor.js"></script>


<script type="text/javascript">
	var counter = <%= counter %>;
	$(document).ready(function(){
		$("#productID").change(function() { 
			$("#productName").val($('option:selected', this).prop("title")); 
		});
		
		$("#hypResetSkus").click(function() { 
			$("#skus").html("");
			counter = 1;
		});
		
		$("#campaign_id").change(function() {
			document.location = '?campaign_id=' + $(this).val();
			
		});
		
		$("#addSku").click(function () {
				$("#skus").append("<input type='hidden' name='_sku" + counter + "' value='" + $("#productID").val() + "' />");
				$("#skus").append("<input type='hidden' name='_sku" + counter + "_Upsell' value='" + $("#upsell").prop('checked') + "' />");
				$("#skus").append("<input type='hidden' name='_sku" + counter + "_Description' value='" + $("#productName").val() + "' /><div>" + $("#productName").val() + "</div>");
				counter++;
				
				$("#productName").val(""); 
				$("#productID").val("");
				$('#upsell').prop('checked', false);
			}); 
  	});
	
	
</script>
</head>

<body>
<div id="err" runat="server" />

                <form method="post" id="form1">
                <div>Campaign:</div>
                <div id="campaign" runat="server"></div>
                <hr />
                <table cellpadding="10">
                <tr>
                    <td style="width: 300px;" valign="top">
                        <h1>Details HTML</h1>
                        <div id="details" style="width: 300px;" runat="server"><div style="background-color: #fcfcfc; border: 2px solid #efefef; padding: 19px; color: #4f4f4f; margin: 76px 40px 0 0;">
<div style="color: #9d0b0e; font-size: 22px;">Syrah, Shiraz, Hurrah!</div>

<ul style="padding-left: 15px;">
	<li style="margin-bottom: 10px;">Three bottles each of four stunning Syrahs or Shiraz from around the world, <span style="color: #9d0b0e; font-weight: bold;"><em>yours for just $9 per bottle!</em></span> (<a href="/_detailPack.aspx?id=5976" class="modal" style="color: #003399">View Wines</a>).</li>
	<li style="margin-bottom: 10px;">Every bottle is guaranteed delicious — Return any wine you don't like!</li>
	<li style="margin-bottom: 10px;">One time purchase: no clubs or further obligation</li>
</ul>
<img src="https://www.barclayswine.com/syrah/upsell.jpg" width="225px">
<div style="text-align: center">&nbsp;</div>
</div></div>                
                    </td>
                	<td style="width: 510px;" valign="top">
                        <h1 style="display:none">Summary HTML</h1>
                        <div id="summary"  style="width: 510px;display:none" runat="server"></div>
                        
                        <h1>Skus</h1>
                        <div id="resetSkus"><a id="hypResetSkus" href="javascript: void(0);">Reset</a></div>
                        <div id="skusInner" runat="server"></div>
                        <div id="skus" runat="server"></div>
                        
                        <h1>Upsell HTML</h1>
                        <textarea class="ckeditor" type="text" id="upsellDescription" name="upsellDescription" runat="server"><span style="font-family: Cambria, 'Book Antiqua', 'Times New Roman', Times, serif; font-size: 14px; line-height: 21px; color: rgb(157, 11, 14); font-weight: bold;"><em>Upgrade Your Pack:</em></span><span style="color: rgb(58, 54, 52); font-family: Cambria, 'Book Antiqua', 'Times New Roman', Times, serif; font-size: 14px; line-height: 21px;">&nbsp;Add 3 bottles of the stunning Firestarter Red for the price of 1,&nbsp;</span><strong style="color: rgb(58, 54, 52); font-family: Cambria, 'Book Antiqua', 'Times New Roman', Times, serif; font-size: 14px; line-height: 21px;">just $29.95 + FREE Shipping, a 67% savings</strong><span style="color: rgb(58, 54, 52); font-family: Cambria, 'Book Antiqua', 'Times New Roman', Times, serif; font-size: 14px; line-height: 21px;">.</span></textarea>
                    <td>
                </tr>
                </table>
                <input type="submit" value="Save" />
                <input type="hidden" name="action" id="action" value="update" runat="server" />
                <input type="hidden" name="id" value="<%= Request["id"] %>" />
                </form>
</body>
</html>
