<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Report.cs" %>
<%@ Register TagPrefix="BB" TagName="Nav" Src="_navMain.ascx" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">
	
	public Report report = new Report();
	public System.DateTime currentMonth = Convert.ToDateTime("6/1/2009");
	public DataTable dt = new DataTable();
	public double revenue = 0;
	public int orders = 0;
	public string holder = "";
	
	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Session["operator"] == null) Response.Redirect("/security.aspx?r=" + Server.UrlEncode(Request.Url.ToString()));
		if (Request["start"] != null) currentMonth = Convert.ToDateTime(Request["start"]);
		
		Hashtable p = new Hashtable();
		report.GetOrderReportByDay(out dt, Request["start"], Request["end"]);
		ParseMonth();
		
		err.InnerHtml = "<div style='font-size: 20px; font-weight: bold'>" + Request["start"] + " to " + Request["end"] + "</div>" + 
							"<div style='font-size: 15px;'>" + String.Format("{0:$#,##0.00;($#,##0.00);Zero}", revenue) + " from " + orders + " orders." + "<div>" +			holder;
	}

	private void ParseMonth() {
		string date = "OrderDate >= '" + currentMonth + "' AND OrderDate < '" + currentMonth.AddMonths(1) + "'";
		double totalRevenue = 0;
		int totalOrders = 0;
		string temp = "";
		
		DataRow[] foundRows = dt.Select(date);
		
		
		if (foundRows.Length > 0) {
		
			foreach(DataRow r in foundRows) {
				// write out data
				temp += "<div class='clearfix' style='font-weight: bold; margin: 5px; margin-left: 0px;'>" + Convert.ToDateTime(r["OrderDate"]).ToString("d") + "</div><div  class='clearfix'><div style='margin-left: 10px; text-align: right; width:" + Convert.ToInt16(r["Orders"].ToString()) / 2 + "px; background-color: green; color: white; padding-right: 5px; float: left;'>&nbsp;</div><div style='margin-left: 10px; float: left; clear: right;' class='clearfix'>" + Convert.ToDouble(r["Revenue"]).ToString("c") + " from " + r["Orders"].ToString() + " orders.</div></div>";
				totalRevenue += Convert.ToDouble(r["Revenue"]);
				totalOrders += Convert.ToInt16(r["Orders"].ToString());
			}
			
			holder += "<hr style='margin-top: 15px;' class='clearfix' /><div style='font-weight: bold; font-size: 18px; margin-bottom: 5px;' class='clearfix'>" + 
								currentMonth.Date.ToString("y") + " - " + String.Format("{0:$#,##0.00;($#,##0.00);Zero}", totalRevenue) + " from " + totalOrders + " orders.</div><hr size=1 />" + temp;
			
			// increment
			currentMonth = currentMonth.AddMonths(1);
			revenue += totalRevenue;
			orders += totalOrders;
			
			ParseMonth();
		}
		
		
	}
	
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Daily Order Report</title>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>

<body>
<div id="programs" runat="server" style="font-weight; bold: font-size: 20px" />
<div id="err" runat="server" style="width: 800px;" />
</body>
</html>
