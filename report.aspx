<%@  Page Language="C#" Debug="true" ClientIdMode="static" %>
<%@ Register TagPrefix="BB" TagName="Nav" Src="_navMain.ascx" %>
<%@ Assembly src="/bin/Report.cs" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Assembly src="/bin/Marketing.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Report report = new Report();
	Order o = new Order();
	Marketing m = new Marketing();
	DataTable dt;
	string startDate = System.DateTime.Now.Date.ToString("d");
	string endDate = System.DateTime.Now.Date.AddDays(1).ToString("d");
	
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["startDate"] != null) {
			startDate = Request["startDate"];
			endDate = Request["endDate"];
		}
		
		// show partners
		getPartners();
		
		DataTable campaigns = new DataTable();
		m.GetCampaignList(out campaigns);
		
		campaign.InnerHtml = "<select name='campaign_id' id='campaign_id'><option value='' selected>[optional] Please select a campaign ID</option>";
		foreach (DataRow r in campaigns.Rows) {
			campaign.InnerHtml += "<option value='" + r["CampaignID"] + "'>" + r["CampaignID"] + ": " + r["PartnerName"].ToString().Trim() + " - " + r["Description"] + "</option>";	
		}
		campaign.InnerHtml += "</select>";
		
		switch (Request["report"]) {
			case (null):
				report.GetOrderReport(out dt, Request["Campaign_ID"], startDate, endDate);
				dateSpan.InnerText = "Order Report for " + startDate + " to " + endDate;
				DisplayOrderReport();
				break;
			case "order":
				report.GetOrderReport(out dt, Request["Campaign_ID"], startDate, endDate);
				dateSpan.InnerText = "Order Report for " + startDate + " to " + endDate;
				DisplayOrderReport();
				break;
			case "inventory":
				dt = o.GetShipmentSummary(null, Request["startDate"], Request["endDate"]);
				dateSpan.InnerText = "Inventory Sold Report for " + startDate + " to " + endDate;
				DisplayInventoryReport();
				break;
			case "cancel":
				report.GetCancelReport(out dt, startDate, endDate);
				dateSpan.InnerText = "Cancel Report for " + startDate + " to " + endDate;
				DisplayCancelReport();
				break;
			case "pnl":
				report.GetProfitLossSummaryByCampaign(out dt, startDate, endDate);
				dateSpan.InnerText = "Profit/Loss Summary for " + startDate + " to " + endDate;
				DisplayProfitLossSummary();
				break;
			case "credit-report":
				CreditReportDump();
				break;
			case "voucher-report":
				VoucherReportDump();
				break;
			default:
				break;
		}
	}
	
	public void getPartners() {
		partners.InnerHtml = "<select id='partnerID' name='partnerID' title='Please select a partner.' class='required validate-not-first'><option>Please select a partner: </option>";
		DataTable dt = new DataTable();
		m.GetPartners(out dt);
		
		foreach(DataRow r in dt.Rows) {
			partners.InnerHtml += "<option value='" + r["PartnerID"] + "'>" + r["PartnerName"] + "</option>";
		}
		
		partners.InnerHtml += "</select>";			
	}
	
	public void CreditReportDump() {
		DataTable dt = new DataTable();
		Hashtable parameters = new Hashtable();
		parameters.Add("StartDate", Request["startDate"]);
		parameters.Add("EndDate", Request["endDate"]);
		
		report.CreditReportDump(out dt, parameters);
		
		StringBuilder sb = new StringBuilder();

        foreach (DataColumn col in dt.Columns)
        {
            sb.Append(col.ColumnName + ',');
        }

        sb.Remove(sb.Length - 1, 1);
        sb.Append(Environment.NewLine);

        foreach (DataRow row in dt.Rows)
        {
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                sb.Append(row[i].ToString() + ",");
            }

            sb.Append(Environment.NewLine);
        }

        string attachment = "attachment; filename=credit-dump-" + System.DateTime.Now.Date + ".csv";
		HttpContext.Current.Response.Clear();
		HttpContext.Current.Response.ClearHeaders();
		HttpContext.Current.Response.ClearContent();
		HttpContext.Current.Response.AddHeader("content-disposition", attachment);
		HttpContext.Current.Response.ContentType = "text/csv";
		HttpContext.Current.Response.AddHeader("Pragma", "public");
		
		Response.Write(sb.ToString());
		Response.End();
	}
	
	public void VoucherReportDump() {
		DataTable dt = new DataTable();
		Hashtable parameters = new Hashtable();
		parameters.Add("StartDate", Request["startDate"]);
		parameters.Add("EndDate", Request["endDate"]);
		parameters.Add("PartnerID", Request["partnerID"]);
		
		report.VoucherReportByPartner(out dt, parameters);
		
		StringBuilder sb = new StringBuilder();

        foreach (DataColumn col in dt.Columns)
        {
            sb.Append(col.ColumnName + ',');
        }

        sb.Remove(sb.Length - 1, 1);
        sb.Append(Environment.NewLine);

        foreach (DataRow row in dt.Rows)
        {
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                sb.Append(row[i].ToString() + ",");
            }

            sb.Append(Environment.NewLine);
        }

        string attachment = "attachment; filename=voucher-dump-" + System.DateTime.Now.Date + ".csv";
		HttpContext.Current.Response.Clear();
		HttpContext.Current.Response.ClearHeaders();
		HttpContext.Current.Response.ClearContent();
		HttpContext.Current.Response.AddHeader("content-disposition", attachment);
		HttpContext.Current.Response.ContentType = "text/csv";
		HttpContext.Current.Response.AddHeader("Pragma", "public");
		
		Response.Write(sb.ToString());
		Response.End();
	}
	
	private void DisplayCancelReport() {
		int nCount = 1;
		int nTotal = 0;
		foreach (DataRow r in dt.Rows) {
			results.InnerHtml += "<div style='width: 580px; padding: 3px;' class='clearfix'>";	
			results.InnerHtml += "<div style='width: 40px; float: left;'>" + r["CampaignID"].ToString().Trim() + "</div>";
			results.InnerHtml += "<div style='width: 165px; float: left;'>" + r["Name"].ToString().Trim() + "</div>";
			results.InnerHtml += "<div style='width: 300px; float: left;'>" + r["Reason"].ToString().Trim() + "</div>";
			results.InnerHtml += "<div style='width: 75px; float: left;'>" + r["Total"].ToString() + "</div>";
			results.InnerHtml += "</div>";
						
			nTotal += Convert.ToInt32(r["Total"].ToString());
						
			if (nCount == dt.Rows.Count) {
				results.InnerHtml += "<div style='width: 580px; padding: 3px; border-top: 1px solid #333' class='clearfix'>";
			results.InnerHtml += "<div style='width: 40px; float: left;'>&nbsp;</div>";	
				results.InnerHtml += "<div style='width: 165px; float: left;'>&nbsp;</div>";	
				results.InnerHtml += "<div style='width: 300px; float: left;'>&nbsp;</div>";
				results.InnerHtml += "<div style='width: 75px; float: left; font-weight: bold; font-size: 15px'>" + nTotal + "</div>";
				results.InnerHtml += "</div>";	
			}
			nCount++;
		}		
	}
	
	private void DisplayInventoryReport() {
				int nCount = 1;
				int nTotal = 0;
				if (Request["k"] == null) {
					results.InnerHtml += "<table id='myTable' class='tablesorter'><thead><tr><th>Product ID</th><th>Code</th><th>Name</th><th>Cost</th><th>Items</th></tr></thead><tbody>";	
					foreach (DataRow r in dt.Rows) {
						results.InnerHtml += "<tr><td>" + r["ProductID"].ToString().Trim() + "</td>";
						results.InnerHtml += "<td>" + r["Code"].ToString().Trim() + "</td>";
						results.InnerHtml += "<td>" + r["Name"].ToString().Trim() + "</td>";
						results.InnerHtml += "<td>" + Convert.ToDouble(r["Cost"]).ToString("c") + "</td>";
						results.InnerHtml += "<td>" + r["Items"].ToString().Trim() + "</td></tr>";
						
						nTotal += Convert.ToInt32(r["Items"].ToString());
						nCount++;
					}
					results.InnerHtml += "</tbody></table>";
				}		
	}
	
	private void DisplayLeadReport() {
		int nCount = 1;
		int nTotal = 0;
		foreach (DataRow r in dt.Rows) {
			results.InnerHtml += "<div style='width: 450px; padding: 3px;' class='clearfix'>";	
			results.InnerHtml += "<div style='width: 150px; float: left;'>" + r["Status"].ToString().Trim() + "</div>";
			results.InnerHtml += "<div style='width: 110px; float: left;'>" + r["AssignedTo"].ToString().Trim() + "</div>";
			results.InnerHtml += "<div style='width: 75px; float: left;'>" + r["Leads"].ToString().Trim() + "</div>";
			results.InnerHtml += "<div style='width: 100px; float: left;'>" + r["ContactDate"].ToString().Trim() + "</div>";
			results.InnerHtml += "</div>";
						
			nTotal += Convert.ToInt32(r["Leads"].ToString());
						
			if (nCount == dt.Rows.Count) {
				results.InnerHtml += "<div style='width: 450px; padding: 3px; border-top: 1px solid #333' class='clearfix'>";	
				results.InnerHtml += "<div style='width: 150px; float: left;'>&nbsp;</div>";
				results.InnerHtml += "<div style='width: 110px; float: left;'>&nbsp;</div>";
				results.InnerHtml += "<div style='width: 75px; float: left; font-weight: bold; font-size: 15px'>" + nTotal + "</div>";
				results.InnerHtml += "<div style='width: 100px; float: left;'>&nbsp;</div>";
				results.InnerHtml += "</div>";	
			}
			nCount++;
		}		
	}
	
	private void DisplayProfitLossSummary() {
		results.InnerHtml += "<table id='myTable' class='tablesorter'><thead><tr><th>Campaign ID</th><th>Product Cost</th><th>Shipping Cost</th><th>CC Fees</th><th>Taxes</th><th>Credits</th><th>Billed</th><th>Net</th><th>%</th></tr></thead><tbody>";		
		foreach (DataRow r in dt.Rows) {
			results.InnerHtml += "<tr>";	
			results.InnerHtml += "<td>" + r["CampaignID"].ToString().Trim() + "</td>";
			//results.InnerHtml += "<td>" +  r["Warehouse"] + "</td>";
			results.InnerHtml += "<td>" +  Convert.ToDouble(r["ProductCost"]).ToString("c") + "</td>";
			results.InnerHtml += "<td>" +  Convert.ToDouble(r["ShippingCost"]).ToString("c") + "</td>";
			results.InnerHtml += "<td>" +  Convert.ToDouble(r["CCFee"]).ToString("c") + "</td>";
			results.InnerHtml += "<td>" +  Convert.ToDouble(r["Taxes"]).ToString("c") + "</td>";
			results.InnerHtml += "<td>" +  Convert.ToDouble(r["Credits"]).ToString("c") + "</td>";
			results.InnerHtml += "<td>" +  Convert.ToDouble(r["Billed"]).ToString("c") + "</td>";
			results.InnerHtml += "<td>" +  Convert.ToDouble(r["Net"]).ToString("c") + "</td>";
			results.InnerHtml += "<td>" +  (Convert.ToDouble(r["Net"])/Convert.ToDouble(r["Billed"])).ToString("P")  + "</td>";
			results.InnerHtml += "</tr>";
		}		
		results.InnerHtml += "<tfoot><tr style='font-weight: bold'><td>&nbsp;</td><td>" + Convert.ToDouble(dt.Compute("Sum(ProductCost)", "")).ToString("c") + "</td>" +
								"<td>" + Convert.ToDouble(dt.Compute("Sum(ShippingCost)", "")).ToString("c") + "</td>" +
								"<td>" + Convert.ToDouble(dt.Compute("Sum(CCFee)", "")).ToString("c") + "</td>" +
								"<td>" + Convert.ToDouble(dt.Compute("Sum(Taxes)", "")).ToString("c") + "</td>" +
								"<td>" + Convert.ToDouble(dt.Compute("Sum(Credits)", "")).ToString("c") + "</td>" +
								"<td>" + Convert.ToDouble(dt.Compute("Sum(Billed)", "")).ToString("c") + "</td>" +
								"<td>" + Convert.ToDouble(dt.Compute("Sum(Net)", "")).ToString("c") + "</td><td>&nbsp;</td>" +
								"</tr></tfoot></tbody></table>";
	}
	
	private void DisplayOrderReport() {
		int orderTotal = 0;
		double revenueTotal = 0;
		double creditTotal = 0;
		double billedTotal = 0;
		results.InnerHtml += "<table id='myTable' class='tablesorter'><thead><tr><th>ID</th><th>Brand</th><th>Description</th><th>Orders</th><th>Billed</th><th>Credits</th><th>Revenue</th><th>Date</th></tr></thead><tbody>";	
		foreach(DataRow r in dt.Rows) {
			results.InnerHtml += "<tr><td><a href='inventory/_getCampaignItems.aspx?campaignID=" + r["CampaignID"] + "&startDate=" + startDate + "&endDate=" + endDate + "'>" + r["CampaignID"] + "</a></td>";
			results.InnerHtml += "<td>" + r["BrandName"] + "</td>";
			results.InnerHtml += "<td> " + r["CampaignType"] + "&nbsp;";
			results.InnerHtml += "- " + r["PartnerName"].ToString().Trim() + " " + r["Description"] + "</td>";
			results.InnerHtml += "<td>" + r["Orders"] + "</td>";
			orderTotal += Convert.ToInt32(r["Orders"]);
			results.InnerHtml += "<td>" + Convert.ToDouble(r["Billed"]).ToString("c") + "</td>";
			billedTotal += Convert.ToDouble(r["Billed"]);
			results.InnerHtml += "<td>" + Convert.ToDouble(r["Credits"]).ToString("c") + "</td>";
			creditTotal += Convert.ToDouble(r["Credits"]);
			results.InnerHtml += "<td>" + Convert.ToDouble(r["Revenue"]).ToString("c") + "</td>";
			revenueTotal += Convert.ToDouble(r["Revenue"]);
			results.InnerHtml += "<td>" + Convert.ToDateTime(r["OrderDate"]).ToString("d") + "</td></tr>";
		}
		results.InnerHtml += "</tbody>";
		results.InnerHtml += "<tfoot><tr><td colspan=3></td>" +
								"<td><strong>" + orderTotal + "</strong></td>" +
								"<td>" + billedTotal.ToString("c") + "</td>" +
								"<td>" + creditTotal.ToString("c") + "</td>" +
								"<td><strong>" + revenueTotal.ToString("c") + 
								"</strong></td><td></td></tr>";	
		results.InnerHtml += "</tfoot></table>";	
	}
</script>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Reporting</title>

<script src="//code.jquery.com/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/pepper-grinder/jquery-ui.min.css" media="screen" />
<script src="/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
	
	$(document).ready(function(){
		$("#myTable").tablesorter(); 
    	$(".datepicker" ).datepicker();
		
		$("#report").on('change', function() {
			if ($(this).val() == 'voucher-report') {
				$("#campaign").hide();
				$("#partners").show();
			} else {
				$("#campaign").show();
				$("#partners").hide();
			}
		});
		
  	});
	
  </script>
<link rel="icon" type="image/ico" href="favicon.png" />
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
<style>
	
	body {
		margin: 0 auto;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		padding: 0px;
	}
	
	#nav {
		margin: 0 auto;
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
		background-color: #efefef;
		min-width: 400px;
	}
	
	#workingArea {
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
	}
	
	th {
		text-align: left;
		font-weight: bold;
		cursor: s-resize;	
		background-color: #0CF;
		color: white;
	}
	
	th,td {
		padding: 5px;	
		border: 1px solid #efefef;
	}
	
</style>
</head>

<body> 
<BB:Nav runat="server" id="nav_ctl" navId=1 />
    <div id="bodyWrapper">
        <div id="nav">
            <div style="font-size: 20px; margin-bottom: 10px;">Reporting</div> 
            <div>
            <form action="">
                Start Date <input name="startDate" type="text" id="startDate" size="12" class="datepicker" />
                End Date   <input name="endDate" type="text" id="endDate" class="datepicker" size="12" />
	            	<select name="report" id="report">
                    <option value="order">Order Report</option>
                    <option value="inventory">Inventory Report</option>
                    <option value="cancel">Cancel Report</option>
                    <option value="pnl">Profit/Loss Summary</option>
                    <option value="credit-report">Invoice/Credit Dump</option>
                    <option value="voucher-report">Voucher Dump</option>
                </select>
	            <span name="campaign" id="campaign" runat="server" style="margin-top: 16px;">
                	CampaignID: 
                </span>
                <span name="partners" id="partners" runat="server" style="margin-top: 16px; display: none">
                	Partner: 
                </span>
				<input name="" type="submit" value="Generate Report" class="clearfix" />
            </form>
            </div>
        </div>
        
        <div id="workingArea">
        	<div style="font-weight: bold; font-size: 15px;" id="dateSpan" runat="server"></div>
          	<hr size="1"  />
          	<div  id="results" runat="server"></div>
        </div>
    </div>
</body>
</html>
 