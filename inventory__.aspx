<%@  Page Language="C#" Debug="true" validaterequest="false" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	DataSet ds = new DataSet();
	Product p = new Product();
	string _brandID = "0";
	string _productTypeID = "0";
	string _redundantProductID = "0";
	string _styleID = "0";
	string _varietalID = "0";
	string _regionID = "0";
	string _appellationID = "0";
	string _producerID = "0";
	string _active = "0";
	string _available = "0";
	int groupWineNumber = 1;
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "add") {
			string productID = "0";
			try {
			if (Request["styleID"].ToString() != "0") {
				productID = p.Add(Convert.ToInt32(Request["brandID"].ToString()), Request["code"].ToString(), Request["name"].ToString(), Request["vintage"], Convert.ToInt32(Request["productTypeID"].ToString()), Convert.ToDouble(Request["cost"].ToString()), 
								Convert.ToDouble(Request["price"].ToString()), Convert.ToBoolean(Request["discountable"].ToString()), Request["expirationDate"].ToString(), 
								Convert.ToBoolean(Request["available"].ToString()), Convert.ToInt32(Request["quantity"].ToString()),
								Request["shortDescription"], Convert.ToInt32(Request["redundantProductID"].ToString()), Request["purchaseOrder"].ToString(),
								Request["orderDate"].ToString(), Request["arrivalDate"].ToString(), Request["headline"].ToString(), Request["longDescription"],
								Convert.ToInt32(Request["varietalID"].ToString()), Convert.ToInt32(Request["regionID"]), 0, 
								Convert.ToInt32(Request["producerID"].ToString()), Convert.ToInt32(Request["appellationID"].ToString()),
								Convert.ToInt32(Request["styleID"].ToString()), Convert.ToDouble(Request["alcoholPercentage"].ToString()), 
								Convert.ToInt32(Request["bottleVolume"].ToString()), Convert.ToBoolean(Request["continuity"].ToString()), Convert.ToBoolean(Request["active"].ToString()));
			} else {
					productID = p.Add(Convert.ToInt32(Request["brandID"].ToString()), Request["code"].ToString(), Request["name"].ToString(), Request["vintage"], Convert.ToInt32(Request["productTypeID"].ToString()), Convert.ToDouble(Request["cost"].ToString()), 
								Convert.ToDouble(Request["price"].ToString()), Convert.ToBoolean(Request["discountable"].ToString()), Request["expirationDate"].ToString(), 
								Convert.ToBoolean(Request["available"].ToString()), Convert.ToInt32(Request["quantity"].ToString()),
								Request["shortDescription"], Convert.ToInt32(Request["redundantProductID"].ToString()), Request["purchaseOrder"].ToString(),
								Request["orderDate"].ToString(), Request["arrivalDate"].ToString(), Request["headline"], Request["longDescription"],
								0, 0, 0, 0, 0, 0, 0, 0, Convert.ToBoolean(Request["continuity"].ToString()), Convert.ToBoolean(Request["active"].ToString()));
				}
				
				if (Request["groupWines_1"].ToString() != "") {
					int g = 1;
					while (Request["groupWines_" + g] != null) {
						p.AddGroupWine(Convert.ToInt32(productID), Convert.ToInt32(Request["groupWines_" + g + "_qty"].ToString()), Convert.ToInt32(Request["groupWines_" + g].ToString()));
						g++;
					}
				}
				
			} catch (Exception ex)
			{
				Response.Write(Request.Form + "<br>" + ex.ToString());
				Response.End();
			}
			Response.Redirect("inventory.aspx");
		} else if (Request["action"] == "update") {
			// edit
			try {
			if (Request["styleID"].ToString() != "0") {
				p.Edit(Convert.ToInt32(Request["id"].ToString()), Convert.ToInt32(Request["brandID"].ToString()), Request["code"].ToString(), Request["name"].ToString(), Request["vintage"], Convert.ToInt32(Request["productTypeID"].ToString()), Convert.ToDouble(Request["cost"].ToString()), 
								Convert.ToDouble(Request["price"].ToString()), Convert.ToBoolean(Request["discountable"].ToString()), Request["expirationDate"].ToString(), 
								Convert.ToBoolean(Request["available"].ToString()), Convert.ToInt32(Request["quantity"].ToString()),
								Request["shortDescription"], Convert.ToInt32(Request["redundantProductID"].ToString()), Request["purchaseOrder"].ToString(),
								Request["orderDate"].ToString(), Request["arrivalDate"].ToString(), Request["headline"].ToString(), Request["longDescription"],
								Convert.ToInt32(Request["varietalID"].ToString()), Convert.ToInt32(Request["regionID"]), 0, 
								Convert.ToInt32(Request["producerID"].ToString()), Convert.ToInt32(Request["appellationID"].ToString()),
								Convert.ToInt32(Request["styleID"].ToString()), Convert.ToDouble(Request["alcoholPercentage"].ToString()), 
								Convert.ToInt32(Request["bottleVolume"].ToString()),  Convert.ToBoolean(Request["continuity"].ToString()), Convert.ToBoolean(Request["active"].ToString()));

			} else {
				p.Edit(Convert.ToInt32(Request["id"].ToString()), Convert.ToInt32(Request["brandID"].ToString()), Request["code"].ToString(), Request["name"].ToString(), Request["vintage"], Convert.ToInt32(Request["productTypeID"].ToString()), Convert.ToDouble(Request["cost"].ToString()), 
								Convert.ToDouble(Request["price"].ToString()), Convert.ToBoolean(Request["discountable"].ToString()), Request["expirationDate"].ToString(), 
								Convert.ToBoolean(Request["available"].ToString()), Convert.ToInt32(Request["quantity"].ToString()),
								Request["shortDescription"], Convert.ToInt32(Request["redundantProductID"].ToString()), Request["purchaseOrder"].ToString(),
								Request["orderDate"].ToString(), Request["arrivalDate"].ToString(), Request["headline"], Request["longDescription"],
								0, 0, 0, 0, 0, 0, 0, 0,  Convert.ToBoolean(Request["continuity"].ToString()), Convert.ToBoolean(Request["active"].ToString()));
			}
				
			} catch (Exception ex)
			{
				Response.Write(Request.Form + "<br>" + ex.ToString());
				Response.End();
			}
			Response.Redirect("inventory.aspx");
		} else if (Request["id"] != null) {
			// retrieve product
			DataSet dsProduct = p.Get(Request["id"]);
			code.Value = dsProduct.Tables[0].Rows[0]["Code"].ToString();
			name.Value = dsProduct.Tables[0].Rows[0]["Name"].ToString();
			cost.Value = Convert.ToDouble(dsProduct.Tables[0].Rows[0]["Cost"]).ToString("c").Replace("$", "");
			price.Value = Convert.ToDouble(dsProduct.Tables[0].Rows[0]["Price"]).ToString("c").Replace("$", "");
			expirationDate.Value = Convert.ToDateTime(dsProduct.Tables[0].Rows[0]["DateExpires"]).ToString("d");
			quantity.Value = dsProduct.Tables[0].Rows[0]["Quantity"].ToString();
			purchaseOrder.Value = dsProduct.Tables[0].Rows[0]["PurchaseOrder"].ToString();
			orderDate.Value = Convert.ToDateTime(dsProduct.Tables[0].Rows[0]["DateOrdered"]).ToString("d");
			arrivalDate.Value = Convert.ToDateTime(dsProduct.Tables[0].Rows[0]["ArrivalDate"]).ToString("d");
			shortDescription.Value = dsProduct.Tables[0].Rows[0]["ShortDescription"].ToString();
			_productTypeID = dsProduct.Tables[0].Rows[0]["ProductTypeID"].ToString();
			_brandID = dsProduct.Tables[0].Rows[0]["BrandID"].ToString();
			_active = dsProduct.Tables[0].Rows[0]["Active"].ToString();
			_available = dsProduct.Tables[0].Rows[0]["Available"].ToString();
			prodType.InnerHtml = "<input type='hidden' id='productTypeID' name='productTypeID' value='" + _productTypeID + "' />" + _productTypeID;
			submitBtn.Value = "Update";
			action.Value = "update";
			
			// show description if present
			longDescription.Value = dsProduct.Tables[0].Rows[0]["LongDescription"].ToString();
			headline.Value = dsProduct.Tables[0].Rows[0]["Headline"].ToString();
			
			// if it's a product of type wine then show the characteristics
			if (_productTypeID == "1001") {
				_varietalID = dsProduct.Tables[0].Rows[0]["VarietalID"].ToString();
				_redundantProductID = dsProduct.Tables[0].Rows[0]["RedundantProductID"].ToString();
				_producerID = dsProduct.Tables[0].Rows[0]["ProducerID"].ToString();
				_styleID = dsProduct.Tables[0].Rows[0]["StyleID"].ToString();
				_appellationID = dsProduct.Tables[0].Rows[0]["AppellationID"].ToString();
				_regionID = dsProduct.Tables[0].Rows[0]["RegionID"].ToString();
				vintage.Value = dsProduct.Tables[0].Rows[0]["Vintage"].ToString();
				bottleVolume.Value = dsProduct.Tables[0].Rows[0]["BottleVolume"].ToString();
				alcoholPercentage.Value = dsProduct.Tables[0].Rows[0]["AlcoholPercentage"].ToString();
			}
			
		}
		
		Hashtable parameters = new Hashtable();
		parameters.Add("OrderBy", 2);
		ds.Tables.Add(p.Search(parameters)); // add product table
		GetSelects();
		
		
		prodSelect.InnerHtml = "<input type='hidden' name='redundantProductID' id='redundantProductID' value='0' />";
		
		// write out select for grouped wine
		// if it is a pack or case show the children...
		if (_productTypeID == "1002" || _productTypeID == "1003") {
			DataTable children = p.GetChildren(Request["id"]);
			foreach(DataRow cr in children.Rows) {
					productGroup.InnerHtml += "<div id='groupWines'>" + cr["Name"].ToString().Trim() + 
					" - Qty: <input type='hidden' name='groupWines_" + groupWineNumber + "' id='groupWines_" + groupWineNumber + "' value='" + 
					cr["ChildProductID"].ToString() + "' /><input type='text' size=3 id='groupWines_" + groupWineNumber + "_qty' name='groupWines_" + groupWineNumber + "_qty' value='" + 
					cr["Quantity"].ToString() + "' /></div>";
					groupWineNumber++;
					
			}
		} else {
			productGroup.InnerHtml += "<div id='groupWines'></div>";
		}

			
		
		p.Dispose();
		p = null;
	}
	
	private void GetSelects() {

		// parse out varietals
		ds.Tables.Add(p.GetVarietals());
		varietalSelect.InnerHtml = "<select name='varietalID' id='varietalID'>" +
									"<option value='0'></option>";
		foreach (DataRow r in ds.Tables[1].Rows) {
			varietalSelect.InnerHtml += "<option value='" + r["VarietalID"] + "'";
			if (r["VarietalID"].ToString() == _varietalID) varietalSelect.InnerHtml += " selected ";
			varietalSelect.InnerHtml += ">" + r["Name"] + "</option>\r\n";
		}
		varietalSelect.InnerHtml += "</select>";
		
		// parse out styles 
		ds.Tables.Add(p.GetStyles());
		styleSelect.InnerHtml = "<select name='styleID' id='styleID'>" +
									"<option value='0' selected></option>";
		foreach (DataRow r in ds.Tables[2].Rows) {
			styleSelect.InnerHtml += "<option value='" + r["StyleID"] + "'";
			if (r["StyleID"].ToString() == _styleID) styleSelect.InnerHtml += " selected ";
			styleSelect.InnerHtml += ">" + r["Name"] + "</option>\r\n";
		}
		styleSelect.InnerHtml += "</select>";
		
		// parse out regions
		ds.Tables.Add(p.GetRegions());
		regionSelect.InnerHtml = "<select name='regionID' id='regionID'>" +
									"<option value='0' selected></option>";
		foreach (DataRow r in ds.Tables[3].Rows) {
			regionSelect.InnerHtml += "<option value='" + r["RegionID"] + "'";
			if (r["RegionID"].ToString() == _regionID) regionSelect.InnerHtml += " selected ";
			regionSelect.InnerHtml += ">" + r["Name"] + "</option>\r\n";
		}
		regionSelect.InnerHtml += "</select>";
		
		// parse out appellations
		ds.Tables.Add(p.GetAppellations());
		appellationSelect.InnerHtml = "<select name='appellationID' id='appellationID'>" +
									"<option value='0' selected></option>";
		foreach (DataRow r in ds.Tables[4].Rows) {
			appellationSelect.InnerHtml += "<option value='" + r["AppellationID"] + "'";
			if (r["AppellationID"].ToString() == _appellationID) appellationSelect.InnerHtml += " selected ";
			appellationSelect.InnerHtml += ">" + r["Name"] + "</option>\r\n";
		}
		appellationSelect.InnerHtml += "</select>";
		
		// parse out brands
		ds.Tables.Add(p.GetBrands());
		brandSelect.InnerHtml = "<select name='brandID' id='brandID' class=\"validate-not-first\" title='Brand is required.'>" +
									"<option value='0' selected></option>";
		foreach (DataRow r in ds.Tables[5].Rows) {
			brandSelect.InnerHtml += "<option value='" + r["BrandID"] + "'";
			if (r["BrandID"].ToString() == _brandID) brandSelect.InnerHtml += " selected ";
			brandSelect.InnerHtml += ">" + r["Name"] + "</option>\r\n";
		}
		brandSelect.InnerHtml += "</select>";
		
		// parse out producers
		ds.Tables.Add(p.GetProducers());
		producerSelect.InnerHtml = "<select name='producerID' id='producerID'>" +
									"<option value='0' selected></option>";
		foreach (DataRow r in ds.Tables[6].Rows) {
			producerSelect.InnerHtml += "<option value='" + r["ProducerID"] + "'";
			if (r["ProducerID"].ToString() == _producerID) producerSelect.InnerHtml += " selected ";
			producerSelect.InnerHtml += ">" + r["Name"].ToString().Trim() + "</option>\r\n";
		}
	}
	
</script>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Inventory</title>
<script language="javascript" src="/includes/yui/build/yahoo/yahoo-min.js" type="text/javascript"></script>
<script language="javascript" src="/includes/yui/build/event/event-min.js" type="text/javascript"></script>
<script language="javascript" src="/includes/jsvalidate.js" type="text/javascript"></script>
<script type="text/javascript" src="/includes/mootools.js"></script>
<script type="text/javascript" src="/includes/calendar.js"></script>
<script src="/includes/js/jquery.js"></script>
  <script type="text/javascript">
    window.addEvent('domready', function() { myCal = new Calendar({ orderDate: 'm/d/Y' }); });
	window.addEvent('domready', function() { myCal2 = new Calendar({ arrivalDate: 'm/d/Y' }); });
	window.addEvent('domready', function() { myCal3 = new Calendar({ expirationDate: 'm/d/Y' }); });
	function getElementsByClassName(className, tag, elm){
		var testClass = new RegExp("(^|\\s)" + className + "(\\s|$)");
		var tag = tag || "*";
		var elm = elm || document;
		var elements = (tag == "*" && elm.all)? elm.all : elm.getElementsByTagName(tag);
		var returnElements = [];
		var current;
		var length = elements.length;
		for(var i=0; i<length; i++){
			current = elements[i];
			if(testClass.test(current.className)){
				returnElements.push(current);
			}
		}
		return returnElements;
	}

	
	function checkWine(v) {
		var arr = getElementsByClassName('wineOnly');
		if (v == 1001) {
			for(i=0; i <arr.length;i++) 
			{
				arr[i].style.display = 'block';
			}
			
			// make the wine characteristics reguired.
			document.getElementById('styleID').className = 'required validate-not-first';
			document.getElementById('varietalID').className = 'required validate-not-first';
			document.getElementById('regionID').className = 'required validate-not-first';
			document.getElementById('bottleVolume').className = 'required validate-number';
			document.getElementById('alcoholPercentage').className = 'required validate-currency-dollar';
		} else {
			for(i=0; i <arr.length;i++) 
			{
				arr[i].style.display = 'none';
			}
			
			arr = getElementsByClassName('packOnly');
			for(i=0; i <arr.length;i++) 
			{
				arr[i].style.display = 'none';
			}
			document.getElementById('styleID').className = '';
			document.getElementById('varietalID').className = '';
			document.getElementById('alcoholPercentage').className = '';
			document.getElementById('bottleVolume').className = '';
		}
		
		// see if we should show the product grouping too
		if (v == 1002 || v == 1003) {
			arr = getElementsByClassName('packOnly');
			for(i=0; i <arr.length;i++) 
			{
				arr[i].style.display = 'block';
			}
		}
	}
	
	var groupWineNumber = <%= groupWineNumber %>;
	
	function lookup() {
		$("#sandbox").load("/secure/modal/_lookupInventory.aspx?num=" + groupWineNumber + "&id=" + $("#lookupID").val(), function(response, status, xhr) { 
				if (response != "") {
					$("#groupWines").append(response); 
					$("#lookupID").val(""); 
					groupWineNumber++;
				} else {
					alert("Can't find product.");
					$("#lookupID").focus();
				}
			}
		);
		
	}
	
	$(document).ready(function(){
    	checkWine(document.getElementById('productTypeID').value);
  	});
	
  </script>
<link rel="stylesheet" type="text/css" href="/includes/calendar.css" media="screen" />
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>

<body>
<div style="display: none" id="sandbox"></div>
<form action="" method="post">
<table width="950" border="0" align="center" cellpadding="2" cellspacing="2" style="border: 1px solid #B28F57">
  <tr  style="border: 1px solid #B28F57">
    <td width="250" valign="top"><WF:Nav runat="server" id="navControl" /></td>
    <td width="700" valign="top">
<div id="header">Products > New Product</div>      
        <table width="100%" id="Add">
          <tr> 
            <td width="46%" valign="top"> <table width="100%" border="0" cellspacing="2">
                <tr> 
                  <td colspan="2"><strong><font size="2">Core Product Information</font></strong></td>
                </tr>
                <tr> 
                  <td width="34%">Brand</td>
                  <td width="66%"><div id="brandSelect" runat="server"></div></td>
                </tr>
                <tr> 
                  <td>Code</td>
                  <td><input name="code" id="code" type="text" title="Product code is required" minlength="4" class="required" size="5" runat="server" /></td>
                </tr>
                <tr>
                  <td>Name</td>
                  <td><input name="name" id="name" title="Product name is required." class="required" type="text" runat="server" /></td>
                </tr>
                <tr> 
                  <td>Product Type</td>
                  <td><div id="prodType" runat="server"> <select name="productTypeID" id="productTypeID" onChange="checkWine(this.value)" class="validate-not-first">
                      <option></option>
                      <option value="1001">Bottle</option>
                      <option value="1002">Case</option>
                      <option value="1003">Pack</option>
                      <option value="1004">NoneWine</option>
                      <option value="1005">Literature</option>
                      <option value="1006">Service</option>
                    </select></div></td>
                </tr> 
                <tr> 
                  <td>Cost</td>
                  <td>$ 
                    <input name="cost" type="text" id="cost" title="Please enter cost as a dollar amount." class="required validate-currency-dollar" size="5" runat="server" /></td>
                </tr>
                <tr> 
                  <td>Price</td>
                  <td>$ 
                    <input name="price" type="text" id="price" title="Please enter price as a dollar amount." class="required validate-currency-dollar" size="5" runat="server" /></td>
                </tr>
                <tr> 
                  <td>Discountable</td>
                  <td><label> 
                    <input name="discountable" type="radio" id="discountable" value="True" checked="checked" />
                    Yes<br />
                    <input type="radio" name="discountable" id="discountable" value="False" />
                    No </label></td>
                </tr>
                <tr> 
                  <td>Expiration</td>
                  <td><input name="expirationDate" type="text" title="Expiration date is required." class="calendar required" id="expirationDate" runat="server" /></td>
                </tr>
                <tr> 
                  <td>Wine Shop</td>
                  <td><input name="available" type="radio" id="available" value="True" <% if (_available == "True") Response.Write("checked='checked'"); %> />
                    Yes<br /> <input type="radio" name="available" id="available" value="False" <% if (_available == "False") Response.Write("checked='checked'"); %> />
                    No </td>
                </tr>
                <tr> 
                  <td>Active</td>
                  <td><input name="active" type="radio" id="active" value="True" <% if (_active == "True") Response.Write("checked='checked'"); %> />
                    Yes<br /> <input type="radio" name="active" id="active" value="False" <% if (_active == "False") Response.Write("checked='checked'"); %> />
                    No </td>
                </tr>
                <tr> 
                  <td>Quantity</td>
                  <td><input name="quantity" type="text" title="Quantity must be entered as a number." class="required validate-number" id="quantity" size="5" runat="server" /></td>
                </tr>
                <tr> 
                  <td>Backup Product</td>
                  <td><div id="prodSelect" runat="server"></div></td>
                </tr>
                <tr> 
                  <td>Purchase Order</td>
                  <td><input name="purchaseOrder" type="text" id="purchaseOrder" runat="server" size="5" /></td>
                </tr>
                <tr> 
                  <td>Date Ordered</td>
                  <td><input name="orderDate" type="text" class="calendar" id="orderDate" runat="server" />                  </td>
                </tr>
                <tr> 
                  <td>Arrival Date</td>
                  <td><input name="arrivalDate" type="text" class="calendar" id="arrivalDate" runat="server" /></td>
                </tr>
                <tr>
                  <td>Continuity</td>
                  <td><input name="continuity" type="radio" id="continuity" value="True" />
Yes<br />
<input name="continuity" type="radio" id="continuity" value="False" checked="checked" />
No </td>
                </tr>
                <tr> 
                  <td colspan="2">Short Description<br /> <textarea name="shortDescription" cols="30" id="shortDescription" runat="server"></textarea></td>
                </tr>
                <tr> 
                  <td colspan="2"><strong><br />
                    <font size="2">Marketing Information</font></strong></td>
                </tr>
                <tr> 
                  <td>Headline</td>
                  <td><input name="headline" id="headline" type="text" runat="server" /></td>
                </tr>
                <tr> 
                  <td colspan="2">Long Description<br /> <textarea name="longDescription" cols="30" id="longDescription" runat="server"></textarea>                  </td>
                </tr>
            </table></td>
            <td width="54%" valign="top"> <table width="100%" border="0" cellspacing="2">
                <tr class="wineOnly"> 
                  <td colspan="2"><strong><font size="2">Wine Only Information</font></strong></td>
                </tr>
                <tr class="wineOnly"> 
                  <td>Vintage</td>
                  <td><input name="vintage" type="text" id="vintage" title="Vintage is required." size="5" maxlength="5" runat="server" /></td>
                </tr>
                <tr class="wineOnly"> 
                  <td>Producer</td>
                  <td><div id="producerSelect" runat="server"></div></td>
                </tr>
                <tr class="wineOnly"> 
                  <td>Style</td>
                  <td><div id="styleSelect" runat="server" ></div></td>
                </tr>
                <tr class="wineOnly"> 
                  <td width="45%">Varietal</td>
                  <td width="55%"><div id="varietalSelect" runat="server"></div></td>
                </tr>
                <tr class="wineOnly"> 
                  <td>Region</td>
                  <td><div id="regionSelect" runat="server"></div></td>
                </tr>
                <tr class="wineOnly"> 
                  <td>Appellation</td>
                  <td><div id="appellationSelect" runat="server"></div></td>
                </tr>
                <tr class="wineOnly"> 
                  <td>Alcohol %</td>
                  <td><input name="alcoholPercentage" type="text" title="Please enter the percentage as a number." id="alcoholPercentage" size="5" runat="server" /></td>
                </tr>
                <tr class="wineOnly"> 
                  <td>Bottle Volume</td>
                  <td><input name="bottleVolume" type="text" title="Please enter the volume as a number in ML" id="bottleVolume" size="5" runat="server" />
                    ml</td>
                </tr>
                <tr class="wineOnly"> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class="packOnly"> 
                  <td colspan="2">Items in Pack/Case</td>
                </tr>
                <tr class="packOnly"> 
                  <td colspan="2"><div id="productGroup" runat="server" />
                  		<div style="padding: 5px;">ID: <input type="text" size=3 id="lookupID"> <a href='#' onClick="lookup()">Add Item to Pack</a></div>
                  </td>
                </tr>
              </table></td>
          </tr>
          <tr> 
            <td colspan="2"><input type="hidden" name="action" id="action" value="add" runat="server" /> 
              <input type="submit" name="submitBtn" id="submitBtn" value="Add Product" runat="server" /></td>
          </tr>
        </table></td>
  </tr>
</table>
</form>

</body>
</html>
 