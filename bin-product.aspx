<%@  Page Language="C#" Debug="true" ValidateRequest="false" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

   	public void Page_Load(Object sender, EventArgs e)
 	{
		Product p = new Product();
		Hashtable parameters = new Hashtable();
		parameters.Add("Available", 1);
		DataTable dt = p.Search(parameters);
		
		product.InnerHtml = "<select name='productID' id='productID' class='validate[required]'><option value=''>Select a Product</option>";
		foreach (DataRow r in dt.Rows) {
			product.InnerHtml += "<option value='" + r["ProductID"] + "'>" + r["Code"] + " - " + r["Name"] + "</option>\r\n";
		}
		product.InnerHtml += "</select>";
		
		dt = p.GetVendors();
		vendor.InnerHtml = "<select name='vendorID' id='vendorID' class='validate[required]'><option value=''>Select a Vendor</option>";
		foreach (DataRow r in dt.Rows) {
			vendor.InnerHtml += "<option value='" + r["VendorID"] + "'>" + r["Name"] + "</option>\r\n";
		}
		vendor.InnerHtml += "</select>";
		
		
		if (Request["action"] == "add") {
			Hashtable param = new Hashtable();
            param.Add("ProductID", Request["productID"]);
            param.Add("VendorID", Request["vendorID"]);
            param.Add("MinUnits", Request["units"]);
            param.Add("RetailPrice", Request["retailPrice"]);
            param.Add("Price", Request["price"]);
            param.Add("ShortDescription", Request["shortDescription"]);
            param.Add("LongDescription", Request["longDescription"]);
            param.Add("ExpirationDate", Request["expireDate"]);
			
			p.AddBinProduct(param);
			//Response.Redirect("modal/close_modal.html");
		}
	}
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="/css/validationEngine.jquery.css" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="/includes/calendar.css" media="screen" />
<script src="/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="/js/jquery.validationEngine-en.js" type="text/javascript"></script>
<script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
<script type="text/javascript">
	
	$(document).ready(function() {
		$("#form1").validationEngine();
	})
	
</script>
<title>Add Bin Product</title>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
	<p>
    <div id="vendor" runat="server" />
	<div id="product" runat="server" />
    
    <select id="units" name="units" class="validate[required]">
    	<option value='' selected>Minimum Units</option>
    	<%
			for (int i = 1; i < 13; i++) {
				Response.Write("<option value='" + i + "'>" + i + "</option>");
			}
		%>
    </select><br />
    
    <br />Retail Price<br />
	<input type="textbox"  id="retailPrice" name="retailPrice" class="validate[required]" /><br />
    Sale Price<br />
	<input type="textbox"  id="price" name="price" class="validate[required]" /><br />
    Short Description (450 character max)<br />
    <textarea name="shortDescription" cols="50" rows="10" id="shortDescription" class="validate[required,maxSize[450]]"></textarea>
    <br />
    Long Description (2000 character max)<br />
	<textarea  id="longDescription" cols="50" rows="10" name="longDescription" class="validate[required,maxSize[2000]]" /></textarea> 
	<br />
    Expiration Date<br />
  <input name="expireDate" type="text" class="validate[custom[date],future[NOW]]" id="expireDate" /> 
	</p>
	<p>	  
    <input type="hidden" name="action" id="action" value="add" />
    <input type="submit" name="button" id="button" value="Submit" />
	</p>
</form>
</body>
</html>
