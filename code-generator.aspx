﻿<%@  Page Language="C#" Debug="true" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Hashtable codes = new Hashtable();
	string output = "";
	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["qty"] != null) {
			int numCodes = Convert.ToInt32(Request["qty"].ToString());
			for (int i = 0; i < numCodes; i++) {
				GetCode(i);
			}
			Response.ContentType = "text";
			Response.Clear();
			
			byte[] bytes = System.Text.Encoding.UTF8.GetBytes(output);
			 
			Response.OutputStream.Write(bytes, 0, bytes.Length);
			Response.End(); 
		}
		
	}
	
	Random r = new Random();
	void GetCode(int i) {
		string year = System.DateTime.Now.Year.ToString().Substring(2,2);
		string day = System.DateTime.Now.DayOfYear.ToString().PadLeft(3,'0');
		
		int rand = r.Next(100000, 999999);
		string random = rand.ToString();
		
		string finalCode = Request["brand"] + year + day + random + CheckDigit(random);
		
		if (codes.ContainsValue(finalCode)) {
			GetCode(i); // make sure we don't have dupes
		} else {
			codes.Add(i, finalCode);
			output += finalCode + "\r\n";
		}
			
	}
	
	int CheckDigit(string  x) {
		int len = x.Length;
		int checkMe = 0;
		
		for (int i = 0; i < len; i++) {
			if (i%2 ==0) {
				// get the number to check
				checkMe += Convert.ToInt16(x.Substring(i,1));
			}
		}
		
		return (checkMe % 10);
	}
	
</script>

<form method="post" action="">
Brand: <select name="brand" id="brand">
	<option value="BAR">Barclays</option>
    <option value="HEA">Heartwood & Oak</option>
    <option value="AFD">Afternoon Delight</option>
    <option value="TRO">Troon</option>
    <option value="FMW">Franklin Mint Wine</option>
</select>
<br/>
Quantity: <select name="qty" id="qty">
	<option value="50">50</option>
	<option value="100">100</option>
	<option value="250">250</option>
	<option value="500">500</option>
	<option value="1000">1,000</option>
    <option value="2500">2,500</option>
    <option value="5000">5,000</option>
    <option value="10000">10,000</option>
    <option value="25000">25,000</option>
    <option value="50000">50,000</option>
</select>
<br/>
<input type="submit" value="Submit" />

</form>