<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Marketing.cs" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	DataSet ds = new DataSet();
	Marketing m = new Marketing();
	Product p = new Product();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "commit") {
			string campaignID = m.AddCampaign(Request["campaign_type"], Request["partner"], Request["cpa_cost"], Request["cpm_cost"], Request["cpm_volume"], Request["start_date"], Request["end_date"], Request["offer_type"], Request["description"]);
			
			// check to see if it is an offer that we need to enter a threshold for
			if ((Request["threshold_type"] != null && Request["threshold_type"] != "") || Request["offer_type"] == "1") {
				string dollarThreshold = null,bottleThreshold = null;
				if (Request["threshold_type"] == "1") {
					bottleThreshold = Request["threshold_amount"];	
				} else if (Request["threshold_type"] == "2"){
					dollarThreshold = Request["threshold_amount"];	
				} else {
					// default to $50
					dollarThreshold = "50";	
				}
				
				m.AddCampaignDiscount(campaignID, dollarThreshold, bottleThreshold, Request["percentage_off"], Request["dollar_off"], Request["offer_type"]);
			}
			
			// check to see if we should enter catalog discounts
			if (Request["prod_1"] != null && Request["prod_1"] != "") {
				// loop through products and add them to promo table
				int i = 1;
				while (Request["prod_" + i] != null && Request["prod_" + i]  != "") {
					m.AddPromo(campaignID, Request["prod_" + i] , Request["prod_" + i + "_price"], "0");
					i++;	
				}
			}
			
			// check to see if we should enter a free item
			if (Request["free_product"] != null && Request["free_product"] != "") {
				m.AddPromo(campaignID, Request["free_product"] ,"0", "1");	
			}
		}
		getCampaignTypes();
		getPartners();
		getProducts();
		
	}
	
	private void getProducts() {
		Hashtable parameters = new Hashtable();
		parameters.Add("Active", 1);
		DataTable dt = p.Search(parameters);
		products.InnerHtml = "<br />Please select a product: <br /><select id='prod_1' name='prod_1' title='Please select a product.' class='required validate-not-first'><option></option>";
		products2.InnerHtml = "<br />Please select a product: <br /><select id='free_product' name='free_product' title='Please select a product.' class='required validate-not-first'><option></option>";
		
		foreach(DataRow r in dt.Rows) {
			products.InnerHtml += "<option value='" + r["ProductID"] + "'>" + r["Code"] + " - " + r["Name"] + " - " + Convert.ToDouble(r["Price"]).ToString("c") + "</option>";
			products2.InnerHtml += "<option value='" + r["ProductID"] + "'>" + r["Code"] + " - " + r["Name"] + " - " + Convert.ToDouble(r["Price"]).ToString("c") + "</option>";			
		}
		
		products.InnerHtml += "</select> Sale Price: $<input size=2 name='prod_1_price' id='prod_1_price' title='Please enter a price.' class='required validate-currency-dollar' />";	
		products2.InnerHtml += "</select>";
	}
	
	public void getCampaignTypes() {
		campaignType.InnerHtml = "<select id='campaign_type' name='campaign_type' onchange='checkType(this.value)' title='Please select a type for this campaign.' class='required validate-not-first'><option></option>";
		DataTable dt = new DataTable();
		m.GetCampaignTypes(out dt);
		
		foreach(DataRow r in dt.Rows) {
			campaignType.InnerHtml += "<option value='" + r["CampaignTypeID"] + "'>" + r["Description"] + "</option>";
		}
		
		campaignType.InnerHtml += "</select>";		
	}
	
	public void getPartners() {
		partners.InnerHtml = "Please select a partner: <br /><select id='partner' name='partner' title='Please select a partner.' class='required validate-not-first'><option></option>";
		DataTable dt = new DataTable();
		m.GetPartners(out dt);
		
		foreach(DataRow r in dt.Rows) {
			partners.InnerHtml += "<option value='" + r["PartnerID"] + "'>" + r["PartnerName"] + "</option>";
		}
		
		partners.InnerHtml += "</select>";			
	}
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Marketing</title>
<script language="javascript" src="/includes/yui/build/yahoo/yahoo-min.js" type="text/javascript"></script>
<script language="javascript" src="/includes/yui/build/event/event-min.js" type="text/javascript"></script>
<script language="javascript" src="/includes/jsvalidate.js" type="text/javascript"></script>
<script type="text/javascript" src="/includes/mootools.js"></script>
<script type="text/javascript" src="/includes/calendar.js"></script>
<script language="javascript">
    window.addEvent('domready', function() { myCal = new Calendar({ start_date: 'm/d/Y' }); });
	window.addEvent('domready', function() { myCal2 = new Calendar({ end_date: 'm/d/Y' }); });
	function checkType(x) {
		// check for type partner (7)
		if (x == "7") {
			// show partner list
			document.getElementById('partners').style.display = 'block';
		}
		
		// check for type affiliate (6)
		if (x == "6") {
			// show affiliate list
			
		}
	}
	
	function checkStructure(x) {
		if (x == "CPA") {
			document.getElementById('structureDetailCPA').style.display = 'block';	
		} else {
			document.getElementById('structureDetailCPA').style.display = 'none';
		}
		
		if (x == "CPM") {
			document.getElementById('structureDetailCPM').style.display = 'block';	
		} else {
			document.getElementById('structureDetailCPM').style.display = 'none';
		}
	}
	
	function checkOffer(x) {
		document.getElementById('threshold').style.display = 'none';
		document.getElementById('dollarOff').style.display = 'none';
		document.getElementById('percentageOff').style.display = 'none';
		document.getElementById('catalogDiscount').style.display = 'none';
		document.getElementById('freeProduct').style.display = 'none';
		
		if (x == 1) {
			// free shipping
			document.getElementById('threshold').style.display = 'block';
		} else if (x == 2) {
			// dollar off
			document.getElementById('threshold').style.display = 'block';
			document.getElementById('dollarOff').style.display = 'inline';
		} else if (x == 3) {
			// percentage off
			document.getElementById('threshold').style.display = 'block';
			document.getElementById('percentageOff').style.display = 'inline';
		} else if (x == 4) {
			// catalog discounts
			document.getElementById('catalogDiscount').style.display = 'block';
		} else if (x == 5) {
			// catalog discounts
			document.getElementById('freeProduct').style.display = 'block';
		}
	}
	
	var prod = 1;
	function addProduct() {
		prod++;
		document.getElementById('products').innerHTML += document.getElementById('products2').innerHTML.replace(/free_product/g, "prod_" + prod);
		document.getElementById('products').innerHTML += " Sale Price: $<input size=2 name='prod_" + prod + "_price' id='prod_" + prod + "_price' class='required validate-currency-dollar' />";
	}
</script>

<style type="text/css" media="all">@import "/includes/thickbox.css";</style>
<link rel="stylesheet" type="text/css" href="/includes/calendar.css" media="screen" />
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>

<body>
<div id="container">
    <div id="navigation">
      <WF:Nav runat="server" id="nav_ctl" navId=2 />
    </div>
    
    <div id="content">
    	<div id="header">Marketing &gt; New Campaign</div>
        <div id="workingArea">
          <div id="newCampaign" style="width: 650px; clear: both; padding: 3px;">
          	<form method="post" action="">
            <div>
           	  Enter type of campaign:<br />
                <span id="campaignType" runat="server"></span>           
            </div>
            <br />
            <div id="partners" style="display: none;" runat="server">
            </div>
            <br />
            <div>
            	What is the cost structure for this campaign?<br />
			  <select id="cost_structure" onChange="checkStructure(this.value)" title="Please enter a cost structure." class="required validate-not-first">
       	    		<option></option>
                    <option value="CPA">CPA</option>
                    <option value="CPM">CPM</option>
                    <option value="None">No Cost</option>
              	</select>
                <span id="structureDetailCPM" style="display: none;">
                	<br />Cost: $<input id="cpm_cost" name="cpm_cost" size="4" title='Please enter the cost.' class='required validate-currency-dollar' /><br />
                    Volume: <input id="cpm_volume" name="cpm_volume" size="4"  title='Please enter the volume.' class='required validate-number' />
                </span>
                <span id="structureDetailCPA" style="display: none;">
                	<br />Cost: $<input id="cpa_cost" name="cpa_cost" size="4" title='Please enter the cost.' class='required validate-currency-dollar' />
                </span>
            </div>
            <br />
            <div>
            Describe the campaign:<br />
			<input id="description" name="description" size="20" class="required" /><br />
<br />

            	What is the start date?<br />
            	<input name="start_date" type="text" id="start_date" class="calendar" class="required validate-date" /> 
                <br />
                <br />
              What is the end date?<br />
              <input name="end_date" type="text" id="end_date" class="calendar" class="required validate-date" /> 
            </div><br />
			<div>
            	What type of offer is this?<br />
            	<select id="offer_type" name="offer_type" onChange="checkOffer(this.value)" title='Please select the offer type.' class='required validate-not-first'>
            	  <option></option>
            	  <option value="1">Free Shipping</option>
            	  <option value="2">Dollar Off</option>
            	  <option value="3">Percentage Off</option>
            	  <option value="4">Catalog Item(s) Discount</option>
            	  <option value="5">Free Item</option>
          	  </select>
            	<span id="dollarOff" style="display: none">
                	Amount: $<input name="dollar_off" id="dollar_off" size="2" title='Please enter the dollar off amount.' class='required validate-currency-dollar'  />
                </span>
                <span id="percentageOff" style="display: none">
                	Amount: <input name="percentage_off" id="percentage_off" size="2" title='Please enter the percentage off amount.' class='required validate-number'  />%
                </span>
                <span id="threshold" style="display: none">
                	<br />What is the threshold?<br />
					<select name="threshold_type" id="threshold_type" onChange="checkThresholdType(this.value)"  title='Please select the threshold type.' class='required validate-not-first' >
                        <option></option>
                        <option value="1">Bottle</option>
                        <option value="2">Dollar</option>
                    </select>
                    How many/much? <input id="threshold_amount" name="threshold_amount" title='Please enter the threshold amount.' class='required validate-currency-dollar' size=2 />
                </span>
                <span id="catalogDiscount" style="display: none">
                	<span id="products" runat="server"></span><br />
                    <a href="javascript: void();" onClick="addProduct()">Add Another Item</a>
                </span>
                <span id="freeProduct" style="display: none">
                	<span id="products2" runat="server"></span>
                </span>
            </div>
            <br/>
            <div>
            	<br/><input type="submit" value="Add Campaign" /><input type="hidden" id="action" name="action" value="commit" />
            </div>
            </form>
          </div>
        </div>
    </div>
    
    <div id="footer">
    
    </div>
</div>
</body>
</html>
