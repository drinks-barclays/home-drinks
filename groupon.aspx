<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Customer c = new Customer();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["grouponID"] != null && Request["grouponID"].ToString() != "") {
			c.ValidateGroupon(Request["grouponID"].ToString(), Request["val"].ToString());
			workingArea.InnerHtml += "<strong>Groupon changed successfully!</strong><br />";
		} else if (Request["voucherID"] != null && Request["voucherID"].ToString() != "") {
			string orderID = c.LookupVoucherOrder(Request["voucherID"]);
			
			if (orderID != "0") {
				workingArea.InnerHtml += "<strong style='color: red; font-weight: bold'>Order ID: " + orderID + "</strong>";
			} else {
				workingArea.InnerHtml += "<strong style='color: red; font-weight: bold'>No order found.</strong>";
			}
		}
		
		
		
	}
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Groupon</title>
<script type="text/javascript" src="/includes/js/jquery.js"></script> 
<script type="text/javascript" src="/includes/js/thickbox.js"></script>
<style type="text/css" media="all">@import "/includes/thickbox.css";</style>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>

<body>
<div id="container">
    <div id="navigation">
      <WF:Nav runat="server" id="nav_ctl" navId=2 />
    </div>
    
    <div id="content">
    	<div id="header" runat="server">Groupon</div>
        <div id="workingArea" runat="server"> 
        <form action="" method="post" id="iForm">
        	<strong style="font-size: 14px;">Activate Groupon</strong><hr size="1" style="margin-top: 2px;" />
			Groupon ID: <input id="grouponID" name="grouponID" type="text" class="required" />
            <select name="val">
            	<option value="1">Activate</option>
            	<option value="0">De-activate</option>
            </select>
       	  <input name="submit" value="Submit" type="submit" />
        </form>
        </div>
    </div>
    
    <div id="footer">
    
    </div>
</div>
</body>
</html>
