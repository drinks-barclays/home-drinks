<%@  Page Language="C#" Debug="true" ClientIdMode="static" validateRequest="false" %>
<%@ Register TagPrefix="BB" TagName="PackInventory" Src="inventory/_pack.ascx" %>
<%@ Register TagPrefix="BB" TagName="AllocateInventory" Src="inventory/_allocate.ascx" %>
<%@ Register TagPrefix="BB" TagName="BottleInventory" Src="inventory/_bottle.ascx" %>
<%@ Register TagPrefix="BB" TagName="AddChange" Src="inventory/_add-change.ascx" %>
<%@ Register TagPrefix="BB" TagName="Clone" Src="inventory/_clone.ascx" %>
<%@ Register TagPrefix="BB" TagName="Nav" Src="_navMain.ascx" %>
<%@ Assembly src="/bin/Product.cs" %>
<script language="C#" runat="server">
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Session["operator"] == null) Response.Redirect("/security.aspx?r=" + Server.UrlEncode(Request.Url.ToString()));
		
		switch (Request["action"]) {
			case "pack":
				_packInventory.Visible = true;
				break;
			case "allocate":
				_allocateInventory.Visible = true;
				break;
			case "add-change":
				_addChange.Visible = true;
				break;
			case "clone":
				_clone.Visible = true;
				break;
			default:
				_bottleInventory.Visible = true;
				break;	
		}
	}
	 
	
</script>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Inventory Management</title>

<script src="//code.jquery.com/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/pepper-grinder/jquery-ui.min.css" media="screen" />
<script src="/js/jquery.validationEngine-en.js" type="text/javascript"></script>
<script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
<link rel="stylesheet" href="//heartwoodandoak.com/assets/css/validationEngine.jquery.css" type="text/css" media="screen" charset="utf-8" />

<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
<style>
	
	body {
		margin: 0 auto;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		padding: 0px;
	}
	
	#nav {
		margin: 0 auto;
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
		background-color: #efefef;
		min-width: 400px;
	}
	
	#workingArea {
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
	}
	
	th {
		text-align: left;
		font-weight: bold;
		cursor: s-resize;	
		background-color: #0CF;
		color: white;
	}
	
	th,td {
		padding: 5px;	
		border: 1px solid #efefef;
	}
	
</style>
</head>

<body> 
    <BB:Nav runat="server" id="nav_ctl" navId=2 />
    <div id="bodyWrapper">
        <div id="nav">
            <div style="font-size: 20px; margin-bottom: 10px;">Inventory Management</div> 
            <div>
                <a href="?action=bottle">View Bottle Inventory</a> | <a href="?action=pack">View Pack Inventory</a> | <a href="?action=allocate">Allocate Inventory</a> | <a href="?action=add-change">Add Inventory</a>
            </div>
        </div>
        
        <div id="workingArea">
             <BB:PackInventory runat="server" id="_packInventory" visible="false" />
             <BB:BottleInventory runat="server" id="_bottleInventory" visible="false" />
             <BB:AllocateInventory runat="server" id="_allocateInventory" visible="false" />
             <BB:AddChange runat="server" id="_addChange" visible="false" />
             <BB:Clone runat="server" id="_clone" visible="false" />
        </div>
    </div>
</body>
</html>
 