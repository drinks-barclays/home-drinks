﻿<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Assembly src="/bin/LitlePayment.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Net.Mime" %>
<script language="C#" runat="server">

	Order o = new Order();
	DataTable dt = new DataTable();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		o.GetOrdersInProcessing(out dt);		
		foreach(DataRow r in dt.Rows) {
			// deplete
			o.DepleteInventory(r["InvoiceID"].ToString());
		}
		
		dt = null;
		o = null;
	}
	
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
</body>
</html>
