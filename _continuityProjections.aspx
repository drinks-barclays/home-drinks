<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Continuity.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">
	
	public Continuity c = new Continuity();
	public System.DateTime currentMonth = System.DateTime.Today.AddDays((System.DateTime.Today.Day*-1)+1);
	public DataTable dt = new DataTable();
	
	public void Page_Load(Object sender, EventArgs e)
 	{
		Hashtable p = new Hashtable();
		
		//if (Request["id"] != null) {
			p = new Hashtable();
			//p.Add("ContinuityConfigurationID", Request["id"]);
			dt = new DataTable();
			c.GetContinuityRaw(out dt, p);
			// parse out current shipments
			ParseMonth();
		//}
	}

	private void ParseMonth() {
		int firstShip = 0;
		int secondShip = 0;
		int thirdShip = 0;
		int fourthShip = 0;
		int fifthShip = 0;
		int sixthShip = 0;
		int seventhShip = 0;
		int eighthShip = 0;
		int ninthShip = 0;
		int aboveShip = 0;
		
		
		double firstShipRevenue = 0;
		double secondShipRevenue = 0;
		double thirdShipRevenue = 0;
		double fourthShipRevenue = 0;
		double fifthShipRevenue = 0;
		double sixthShipRevenue = 0;
		double seventhShipRevenue = 0;
		double eighthShipRevenue = 0;
		double ninthShipRevenue = 0;
		double aboveShipRevenue = 0;
		double totalRevenue = 0;
		
		string date = "NextShipDate > '" + currentMonth + "' AND NextShipDate < '" + currentMonth.AddMonths(1) + "'";
		DataRow[] foundRows = dt.Select(date);
		
		
		if (foundRows.Length > 0  || (currentMonth == System.DateTime.Today.AddDays((System.DateTime.Today.Day*-1)+1))) {
			
			// display month
			err.InnerHtml += "<hr style='margin-top: 15px;' /><div style='font-weight: bold; font-size: 18px; margin-bottom: 5px;'>" + currentMonth.Date.ToString("y") + "</div><hr size=1 />";
		
			foreach(DataRow r in foundRows) {
				switch (r["TotalShipments"].ToString()) {
					case "1":
						secondShip++;
						secondShipRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						totalRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						break;
					case "2":
						thirdShip++;
						thirdShipRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						totalRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						break;
					case "3":
						fourthShip++;
						fourthShipRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						totalRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						break;
					case "4":
						fifthShip++;
						fifthShipRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						totalRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						break;
					case "5":
						sixthShip++;
						sixthShipRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						totalRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						break;
					case "6":
						seventhShip++;
						seventhShipRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						totalRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						break;
					case "7":
						eighthShip++;
						eighthShipRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						totalRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						break;
					case "8":
						ninthShip++;
						ninthShipRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						totalRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						break;
					default:
						aboveShip++;
						aboveShipRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						totalRevenue += (Convert.ToDouble(r["Price"].ToString()) + Convert.ToDouble(r["Shipping"].ToString()));
						break;
				}
			}
			
			
			err.InnerHtml += "<table style='padding: 5px; border: 0px;'><tr style='padding: 3px;'><td>&nbsp;</td><td>2nd</td><td>3rd</td><td>4th</td><td>5th</td><td>6th</td><td>7th</td><td>8th</td><td>9th</td><td>10+</td><td>&nbsp;</td></tr>";
			err.InnerHtml += "<tr class='item'><td style='font-weight: bold;'>Potential Orders:<br />Potential Revenue:</td>" +
								"<td>" + secondShip + "<br />" + secondShipRevenue.ToString("c") + "</td>" +
								"<td>" + thirdShip + "<br />" + thirdShipRevenue.ToString("c") + "</td>" +
								"<td>" + fourthShip + "<br />" + fourthShipRevenue.ToString("c") + "</td>" + 
								"<td>" + fifthShip + "<br />" + fifthShipRevenue.ToString("c") + "</td>" +
								"<td>" + sixthShip + "<br />" + sixthShipRevenue.ToString("c") + "</td>" +
								"<td>" + seventhShip + "<br />" + seventhShipRevenue.ToString("c") + "</td>" +
								"<td>" + eighthShip + "<br />" + eighthShipRevenue.ToString("c") + "</td>" +
								"<td>" + ninthShip + "<br />" + ninthShipRevenue.ToString("c") + "</td>" +
								"<td>" + aboveShip + "<br />" + aboveShipRevenue.ToString("c") + "</td><td>&nbsp;</td></tr>" +
								"<tr><td colspan=10>&nbsp;</td><td style='font-weight: bold'>" + totalRevenue.ToString("c") + "</td></tr>"; // build display table
			err.InnerHtml += "</table>";
			
			// increment
			currentMonth = currentMonth.AddMonths(1);
			
			ParseMonth();
		}
		
		
	}
	
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Continuity Projections</title>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
<style>
	table {
		font-family:Arial, Helvetica, sans-serif;
		font-size: 13px;	
	}

	.item {
		padding: 5px;
		margin: 0px;	
	}
	
	.item td {
		padding-right: 10px;	
	}
</style>
</head>

<body>
<div id="programs" runat="server" style="font-weight; bold: font-size: 20px" />
<div id="err" runat="server" style="width: 800px;" />
</body>
</html>
