<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">
	
	Customer c = new Customer();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["note"] != null && Request["note"] != "") {
			c.AddNote(Request["id"], Request["note"]);	
			Response.Redirect("customer.aspx?id=" + Request["id"]);
		}
	}

</script>