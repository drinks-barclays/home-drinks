<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Customer c = new Customer();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["id"] != null && Request["id"].ToString() != "") {
			c.AddCredit(Request["id"].ToString(), Request["amount"].ToString());
			workingArea.InnerHtml += "<strong>Credit Applied!</strong><br />";
		}
		
	}
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Credit</title>
<script type="text/javascript" src="/includes/js/jquery.js"></script> 
<script type="text/javascript" src="/includes/js/thickbox.js"></script>
<style type="text/css" media="all">@import "/includes/thickbox.css";</style>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>

<body>
<div id="container">
    <div id="navigation">
      <WF:Nav runat="server" id="nav_ctl" navId=2 />
    </div>
    
    <div id="content">
    	<div id="header" runat="server">Credit</div>
        <div id="workingArea" runat="server"> 
        <form action="" method="post" id="iForm">
        	<strong style="font-size: 14px;">Apply Credit</strong>
       	  <hr size="1" style="margin-top: 2px;" />
			Customer ID: 
			<input id="id" name="id" type="text" class="required" width="5" /> 
			Amount: 
		  <input id="amount" name="amount" type="text" class="required" width="5" /> 
       	  <input name="submit" value="Apply Credit" type="submit" />
        </form>
        </div>
    </div>
    
    <div id="footer">
    
    </div>
</div>
</body>
</html>
