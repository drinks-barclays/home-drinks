<%@  Control Language="C#" ClientIdMode="static" %>
<%@ Assembly src="/bin/Continuity.cs" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	public Product p = new Product();
	public Continuity c = new Continuity();
	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["subAction"] == "add") {
				AddProgram();	
		} else if (Request["subAction"] == "remove") {
			c.ContinuityProductInactivate(Request["cid"]);
		}
		
		if (Request["id"] != null)  {
			if (Request["subAction"] == "update") {
				EditProgram();
			} else {
				// get values to edit
				program.Visible = true;
				GetProgram();
			}
		} else {
			// new program
			GetBrands("");
			GetRecurrence("");
			newProgram.Visible = true;
		}
	}
	
	private void AddProgram() {
		
	}
	
	private void EditProgram() {
		
	}
	
	private void GetProgram() {
		DataTable dt = new DataTable();
		Hashtable parameters = new Hashtable();
		parameters.Add("ContinuityID", Request["id"]);
		c.GetContinuity(out dt, parameters);
		
		foreach(DataRow r in dt.Rows) {
			brand.InnerText = r["BrandName"].ToString();
			programName.InnerText = r["Name"].ToString();
			shipSpan.InnerHtml = "Ships every <strong>" + r["Span"] + " </strong>weeks.";
			pricing.InnerHtml = "<strong>Price:</strong> " + Convert.ToDouble(r["Price"]).ToString("c") + "&nbsp; &nbsp;<strong>Shipping:</strong> " + Convert.ToDouble(r["Shipping"]).ToString("c");
			activeMembers.InnerText = "Active Members: " +  r["ActiveMembers"];
		}
		
		// get shipments
		dt = new DataTable();
		c.GetShipments(out dt, Request["id"]);
		
		// parse out current shipments
		string date = "StartDate <= '" + System.DateTime.Now.AddDays(30).Date + "' AND EndDate >= '" + System.DateTime.Now.AddDays(-30).Date + "'";
		DataRow[] foundRows = dt.Select(date);
		currentShipments.InnerHtml = "<table style='border: dashed 1px #ccc; padding: 5px; width: 100%;' cellspacing='5' id='myTable'><thead><tr>";
		currentShipments.InnerHtml += "<th><strong>ID</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Type</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Product</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Ship Date</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Cycle</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Eligible</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Sent</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Cancels</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Unsubs</strong></th>";
		currentShipments.InnerHtml += "<th><strong>Active</strong></th></tr></thead>";
		
		foreach(DataRow r in foundRows) {
			currentShipments.InnerHtml += "<tr><td>" + r["ContinuityProductID"] + "</td>";
			currentShipments.InnerHtml += "<td>" + r["PreferenceType"] + "</td>";
			currentShipments.InnerHtml += "<td>" + r["ProductID"] + " - " + r["ProductName"] + "</td>";
			currentShipments.InnerHtml += "<td>" + Convert.ToDateTime(r["StartDate"]).ToString("d") + " thru " + Convert.ToDateTime(r["EndDate"]).ToString("d") + "</td>";
			currentShipments.InnerHtml += "<td>" + r["ShipmentBegin"] + " to " + r["ShipmentEnd"] + "</td>";
			currentShipments.InnerHtml += "<td><strong>" + r["Going"] + "</strong></td>";
			currentShipments.InnerHtml += "<td><strong>" + r["Sent"] + "</strong></td>";
			currentShipments.InnerHtml += "<td><strong>" + r["Cancels"] + "</strong></td>";
			currentShipments.InnerHtml += "<td><strong>" + r["Unsubs"] + "</strong></td>";
			currentShipments.InnerHtml += "<td>";
			if (r["Active"].ToString() == "True") {
				 currentShipments.InnerHtml += "<a href='?id=" + Request["id"] + "&action=manage&subAction=remove&cid=" + r["ContinuityProductID"] + "' style='color: red'><strong>" + r["Active"] + "</strong></a>";
			} else {
				currentShipments.InnerHtml += "<strong>" + r["Active"] + "</strong>";	
			}
			currentShipments.InnerHtml += "</td></tr>";
		}
		currentShipments.InnerHtml += "</table>";
		
		
		
	}
	
	private void GetBrands(string x) {
		// parse out brands
		DataTable dt = p.GetBrands();
		brandSelect.InnerHtml = "<select name='brandID' id='brandID' class=\"validate-not-first\" title='Brand is required.'>" +
									"<option value='0'></option>";
		foreach (DataRow r in dt.Rows) {
			brandSelect.InnerHtml += "<option value='" + r["BrandID"] + "'";
			if (r["BrandID"].ToString() == x) brandSelect.InnerHtml += " selected ";
			brandSelect.InnerHtml += ">" + r["Name"] + "</option>\r\n";
		}
		brandSelect.InnerHtml += "</select>";	
	}
	
	private void GetRecurrence(string x) {
		recurrenceSelect.InnerHtml = "<select name='span' id='span' class=\"validate-not-first\" title='Brand is required.'>" +
									"<option></option>";
		for (int i = 1; i < 53; i++) {
			recurrenceSelect.InnerHtml += "<option value='" + i + "'";
			if (i.ToString() == x) brandSelect.InnerHtml += " selected ";
			recurrenceSelect.InnerHtml += ">" + i + "</option>\r\n";
		}
		recurrenceSelect.InnerHtml += "</select>";
	}
	
</script>



			<div id="newProgram" style="width: 650px; clear: both; padding: 3px;" runat="server" visible=false>
                <form method="post" action="">
                <div>
                	<strong>Brand:</strong><br />
                    <span id="brandSelect" runat="server"></span>           
                </div>
                <br />
                <div>
                	<strong>Name of Program:</strong><br />
                    <input name="name" id="name" type="text" title="Product name is required." class="required" runat="server" />           
                </div>
                <br />
                <div>
                	<strong>Recurrence of Shipments:</strong><br />
                    Ship every <span id="recurrenceSelect" runat="server"></span> weeks.      
                </div>
                <br />
                <div>
                	<strong>Price:</strong><br />
                    $
                    <input name="price" id="price" type="text" size="4" title="Price is required." class="required validate-currency-dollar" runat="server" />           
                </div>
                <br />
                <div>
                	<strong>Shipping:</strong><br />
                    $
                    <input name="shipping" id="shipping" type="text" size="4" title="Shipping is required." class="required validate-currency-dollar" runat="server" />           
                </div>
                <br />
                <input name="submit" type="submit" value="Add Program" />
            	</form>
        	</div>
            
            
            
            <div id="program" runat="server" visible=false>
            	<div id="brand" runat="server" />
                <div id="programName" style="font-size: 20px; font-weight:bold; margin-top: 3px; margin-bottom: 3px;" runat="server" />
                <div id="activeMembers" style="font-size: 13px; font-weight:bold; margin-top: 3px; margin-bottom: 3px; float: right;" runat="server" />
                <div id="pricing" style="font-size: 12px; margin-top: 3px; margin-bottom: 3px;" runat="server" />
                <div id="shipSpan" style="font-size: 10px; font-weight:bold; margin-top: 3px; margin-bottom: 3px;" runat="server" />
                <hr size="1" />
                <div style="float: right" align="right"><a style="color: purple; font-weight: bold;" href='modal/_sendShipmentNotification.aspx?id=<%= Request["id"] %>' class='modalStrict'>+ Send ASN</a> | <a style="color: blue; font-weight: bold;" href='modal/_continuityClose.aspx?id=<%= Request["id"] %>' class='modalStrict'>+ Charge</a> | <a style="color: green; font-weight: bold;" href='modal/_continuity.aspx?id=<%= Request["id"] %>' class='modal'>+ Add Shipment</a></div>
                <h3>Current Shipments</h3>
                <div id="currentShipments" runat="server">
                
                </div>
                
            </div>
        </div>