﻿<%@  Control Language="C#" ClientIdMode="static" %>
<%@ Assembly src="/bin/Continuity.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Continuity c = new Continuity();
	public void Page_Load(Object sender, EventArgs e)
 	{
		DataTable dt;
		Hashtable parameters = new Hashtable();
		c.GetContinuity(out dt, parameters);
		
		results.InnerHtml += "<table id='myTable' class='tablesorter'><thead><tr><th>ID</th><th>Brand</th><th>Name</th><th>Recurs</th><th>Price</th><th>Shipping</th><th>Members</th></tr></thead><tbody>";	
		foreach(DataRow r in dt.Rows) {
			results.InnerHtml += "<tr style='cursor:pointer' onclick=\"document.location = '?action=manage&id=" + r["ContinuityConfigurationID"] + "'\">";
			results.InnerHtml += "<td>" + r["ContinuityConfigurationID"] + "</td>";
			results.InnerHtml += "<td>" + r["BrandName"] + "</td>";
			results.InnerHtml += "<td>" + r["Name"] + "</td>";
			results.InnerHtml += "<td>" + r["Span"] + " wks</td>";
			results.InnerHtml += "<td>" + Convert.ToDouble(r["Price"]).ToString("c") + "</td>";
			results.InnerHtml += "<td>" + Convert.ToDouble(r["Shipping"]).ToString("c") + "</td>";
			results.InnerHtml += "<td>" + r["ActiveMembers"] + "</td>";
			results.InnerHtml += "</tr>";
		}
		results.InnerHtml += "</table>";
	}
	
</script>
          	 <h4 style="margin-bottom: 3px;">Current Programs</h4><hr size="1"  />
             <div id="results" runat="server">
             
             </div>