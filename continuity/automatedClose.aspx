<%@ Page Language="C#" ClientIdMode="static" %>
<%@ Assembly src="/bin/ShoppingCart.cs" %>
<%@ Assembly src="/bin/Continuity.cs" %>
<%@ Assembly src="/bin/LitlePayment.cs" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Assembly src="/bin/Utilities.cs" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.Net" %>
<script language="C#" runat="server">

	Util u = new Util();
	public Continuity c = new Continuity();
	public Customer customer = new Customer();
		int declines = 0;
		int charges = 0;
		double revenue = 0;
		string adminBody = "";
	public void Page_Load(Object sender, EventArgs e)
 	{
		Continuity c = new Continuity();
		DataTable dt = new DataTable();
		c.GetContinuityConfigsToClose(out dt);
		foreach (DataRow row in dt.Rows) {
			DoClose(row["ContinuityConfigurationID"].ToString());
			adminBody += "<p><strong>" + row["ContinuityConfigurationID"].ToString() + " - " + row["Name"].ToString() + "</strong><br/>" + charges + " charges for " + revenue.ToString("c") + " revenue and " + declines + " declines.</p>";
			declines = 0;
			charges = 0;
			revenue = 0;
		}
		
			string adminSubject = "Automated Continuity Charge Summary";
		
			MailMessage email = new MailMessage(
				   "continuity+robot@barclayswine.com",
				   "drinks-devops@drinks.com, josiah@drinks.com, zac@drinks.com, tyson@drinks.com",
				   adminSubject,
				   adminBody);
			email.IsBodyHtml = true;
				
			NetworkCredential creds = new NetworkCredential("auth@voidme.com", "$m@ng0"); 
			SmtpClient client = new SmtpClient("127.0.0.1");
			client.UseDefaultCredentials = false;
			client.Credentials = creds;
			try {
				client.Send(email);
			} catch (Exception ex) {
				;	
			}	
	}
	
	public void DoClose(string id) {
		Continuity c = new Continuity();
		DataTable dt;
		Payment_ p = new Payment_();
		Order order = new Order();
		bool goodPayment;
		ShoppingCart cart = new ShoppingCart();
		string cartID = cart.GetShoppingCartId();
		
		// roll through pre-paids
		try {
			c.GetEligibleToClosePrePaid(out dt);
			
			foreach(DataRow row in dt.Rows) {
				// add order
				cart.EmptyCart(cartID);
				cart.AddItem(cartID, Convert.ToInt32(row["ProductID"].ToString()), 1);
				order = new Order();
				order.Add(row["CustomerBillingID"].ToString(), row["CustomerAddressID"].ToString(), "3655", cartID, 
									"0", "0", "0", 
									"0", "0", Request["reward_number"], row["BrandID"].ToString());
						
				// incremement shipments in their subscription
				c.IncrementShipments(row["ContinuitySubscriptionID"].ToString());
				c.IncrementPrePaid(row["ContinuitySubscriptionID"].ToString());
			}
		} catch (Exception ex) {
			;	
		}
		dt = null;
		
		// grab eligible
		if (id != null && id != "") {
			c.GetEligibleToClose(out dt, id); 
		} else {
			c.GetEligibleToClose(out dt); 
		}

		// parse through and charge
		if (dt.Rows.Count > 0) {
			//main.Visible = false;
			foreach(DataRow row in dt.Rows) {
				// try to charge
				p = new Payment_(); 
				
				/*Barclays: 6105109, Heartwood: 6106204, Troon: 6105112*/
				switch  (row["BrandID"].ToString()) {
					case "2":
						// troon
						p.MID = "6105112";
						p.ReportGroup = "TroonContinuity";
						break;
					case "7":
						// H&O
						p.MID = "6106204";
						p.ReportGroup = "HeartwoodContinuity";
						break;
					default:
						p.MID = "6105109";
						p.ReportGroup = "BarclaysContinuity";
						break;
				}
				
				double tax = Convert.ToDouble(cart.GetTax(row["State"].ToString(), Convert.ToDouble(row["Total"].ToString())));
				double total = Convert.ToDouble(row["Total"].ToString()) + tax;
				
				goodPayment = p.DoPayment(row["Digits"].ToString().Trim(), "",
											(row["Month"].ToString().Trim().PadLeft(2, '0') + row["Year"].ToString().Trim().Substring(2,2)), 
											total.ToString("F"), row["BillingFirstName"].ToString().Trim(), 
											row["BillingLastName"].ToString().Trim(), row["BillingPostalCode"].ToString().Trim(), 
											"", 1);
				
				if (goodPayment) {
					// add order
					cart.EmptyCart(cartID);
					cart.AddItem(cartID, Convert.ToInt32(row["ProductID"].ToString()), 1);
					order = new Order();
					order.Add(row["CustomerBillingID"].ToString(), row["CustomerAddressID"].ToString(), "3655", cartID, 
										row["Shipping"].ToString(), tax.ToString("F"), total.ToString("F"), 
										"0", p.TransactionCode, Request["reward_number"], row["BrandID"].ToString());
										
					// if there's a new token, update it
					
					// incremement shipments in their subscription
					c.IncrementShipments(row["ContinuitySubscriptionID"].ToString());
					
					revenue += total;
					charges++;
				} else {
					declines++;
					// bad
					Response.Write(row["CustomerID"] + " - " + p.debug + "<br />");
					string CSPhone = "";
					string brandName = "";
					string body;
					string fromEmail;
					string fromName;
					
					// increment counters
					switch  (row["BrandID"].ToString()) {
						case "2":
							// troon
							//troonDecline += 1;
							fromEmail = "cs@troonwineclub.com";
							fromName = "Wine Concierge";
							brandName = "Troon Wine Club";
							CSPhone = "877-998-7666";
							break;
						case "7":
							// H&O
							//heartwoodDecline += 1;
							fromEmail = "cs@heartwoodandoak.com";
							fromName = "Wine Concierge";
							brandName = "Heartwood & Oak";
							CSPhone = "888-661-1246";
							break;
						default:
							//barclaysDecline += 1;
							fromEmail = "cs@barclayswine.com";
							fromName = "Wine Concierge";
							brandName = "Barclays Wine";
							CSPhone = "888-380-2337";
							break;
					}
					
					body = File.ReadAllText("c:\\inetpub\\net\\www.bevbistro.com\\secure\\common\\decline\\default.html");
					body = body.Replace("*|CS_PHONE|*", CSPhone);
					body = body.Replace("*|CS_EMAIL|*", fromEmail);
					body = body.Replace("*|FIRST_NAME|*", row["BillingFirstName"].ToString().Trim());
					body = body.Replace("*|BRAND|*", brandName);
					body = body.Replace("*|CLUB_NAME|*", row["ClubName"].ToString().Trim());
					body = body.Replace("*|CUSTOMER_ID|*", row["CustomerID"].ToString().Trim());
				
					
					// send decline email
					u.SendMailServer(row["BillingFirstName"].ToString().Trim() + " " + row["BillingLastName"].ToString().Trim(), row["Email"].ToString(), "Oh no! We couldn't process your order. Please call us ASAP.", body, fromEmail, fromName);
					
					// notate account
					customer.AddNote(row["CustomerID"].ToString(), "Continuity Decline: " + p.debug + " - automated note from BOT");
				}
				
			}	
		} else {
				// bad
		}
	}
</script>