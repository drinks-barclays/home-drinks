﻿<%@  Page Language="C#" Debug="true" validaterequest="false" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Product p = new Product();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		int id;
		if (Request["id"] != null && Request["id"] != "" && int.TryParse(Request["id"].ToString(), out id)) {
			// see if we have an item to return
			// name, id, qty box
			DataTable dt = p.Lookup(Request["id"]);
				
			if (dt.Rows.Count > 0) {
					Response.Write("<div>" + dt.Rows[0]["ProductID"].ToString() + " - " + dt.Rows[0]["Name"].ToString().Trim() + 
						"<input type='hidden' name='groupWines_" + Request["num"] + "' id='groupWines_" + Request["num"] + "' value='" + 
						dt.Rows[0]["ProductID"].ToString() + "' /> - Qty: <input type='text' size=3 name='groupWines_" + Request["num"] + "_qty' id='groupWines_" + Request["num"] + "_qty' value='1' /></div>");	
			}
		}
	}
	
</script>