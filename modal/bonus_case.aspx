﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Utilities.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="Data" %>
<%@ Import Namespace="System.Data" %>
<script language="C#" runat="server">
	public DataUtility d = new DataUtility("Server=DrinksWeb01;User ID=Wine;password=#3web;Database=WineFolks;Persist Security Info=True");
   	public void Page_Load(Object sender, EventArgs e)
 	{
		Hashtable parameters = new Hashtable();
        Result r = new Result();
		
		if (Request["action"] == "decline") {
			parameters = new Hashtable();
			parameters.Add("ContinuitySubscriptionID", Request["id"]);
			r = d.ExecuteNonQuery("sp_DeclineShipment", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
			Response.Redirect("bonus_case.aspx");	
			//Response.End();
		}
		
		DataTable dt;
			parameters = new Hashtable();
			r = d.ExecuteDataTable(out dt, "sp_GetBonusCasePeople", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		
		results.InnerHtml = "<table align=center cellpadding=2 style='border: 1px solid #ccc; margin: 10px;'>" +
							"<tr style='font-weight: bold; margin: 5px;'><td>Customer" +
							" ID</td><td>Name</td><td>Phone Number</td><td>&nbsp;</td></tr>";
							
		foreach (DataRow row in dt.Rows) {
			results.InnerHtml += "<tr style='margin: 5px'><td>" + row["CustomerID"] +
									" </td><td> " + row["FirstName"].ToString().Trim() + " " + 
									row["LastName"].ToString().Trim() + " </td><td> " + 
									String.Format("{0:(###) ###-####}", Convert.ToInt64(row["PhoneNumber"].ToString())) +
									"</td><td style='padding-left: 25px;'> " +
									"<a href='bonus_case.aspx?action=decline&id=" +
									row["ContinuitySubscriptionID"] + "' style='color: red'>[Decline]</a>" +
									"</td></tr>";
		}
		results.InnerHtml += "</table>";
	}

</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bonus Case Leads</title>	
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 15px;
	}
	
	.field {
		width: 325px;
		clear: both;
		padding: 3px;
	}
	
	.label {
		width: 125px;
		float: left;
		padding: 3px;
	}
	
	.input {
		width: 175px;
		float: left;
	}
</style>
</head>

<body>
<div id="results" runat="server" />
</body>
</html>
