﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Marketing.cs" %>
<%@ Assembly src="/bin/Utilities.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.IO" %>
<script language="C#" runat="server">
	
	Marketing m = new Marketing();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "upload") {
			int counter = 0;
			using (StreamReader sr = new StreamReader(file.PostedFile.InputStream)) 
			{
				string line;
				string[] splitted;
				char[] splitter = { ',' };
						
				while ((line = sr.ReadLine()) != null) 
				{					
					splitted = line.Split(splitter);
					try {
						if (splitted[0] != null && splitted[1] != null && splitted[2] != null) {
							m.AddPromo(splitted[1].ToString().Trim(), splitted[0].ToString().Trim(), splitted[2].ToString().Trim(), "False"); 
							counter++;
						}
					} catch {
						;	
					}
				}
				
				Response.Write(counter + " discounted items added.");
			}
		}
	}
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload Discounts</title>
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 0px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
</style>
</head>

<body style="padding: 5px;">
CSV FORMAT: ProductID,CampaignID,Price<hr />
<form action="" method="post" enctype="multipart/form-data" runat="server" style="padding: 5px;">
	Select a file to upload:<br />
	<input id="file" name="file" type="file" runat="server" style="margin: 5px;">
	<br>
    <input type="hidden" value="upload" name="action" id="action" />
<input type="submit" value="Uploads" style="margin: 5px;" />
</form>
</body>
</html>
