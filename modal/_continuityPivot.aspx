﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Report.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<script language="C#" runat="server">

	public void Page_Load(Object sender, EventArgs e)
 	{
		DataTable dt = new DataTable();
		Report report = new Report();
		Hashtable parameters = new Hashtable();
		parameters.Add("ConfigID", Request["configID"]);
		parameters.Add("CampaignID", Request["campaignID");
		
		report.ContinuityPivot(out dt, parameters);
		
		string json = JsonConvert.SerializeObject(dt, Formatting.Indented);
		output.InnerHtml = json;
	}
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Continuity Pivot</title>
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body { 
		margin: 20px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 14px;
	}
	
	.header {
		font-size: 20px;
		font-weight: bold;	
		border-bottom: 1px solid;
	}
	
	table {
		font-family: Arial, Helvetica, sans-serif;
		margin: 10px;
		margin-bottom: 40px;
	}
	
	table th {
		margin: 7px;	
		font-size: 16px;
		font-weight: bold;
		border-bottom: 1px solid;
		border-right: 1px solid;
	}
	table td {
		margin: 5px;
		font-size: 14px;	
		border-bottom: 1px solid;
		border-right: 1px solid;
	}
	
	.descriptor {
		margin: 7px;
	}
	
	.subscriber {
		color: #060	;
	}
	
	.unsubscribe {
		color: #900;	
	}
	
	.bounce {
		color: #F60;
	}
	
	.revenue {
		color: #060	;
	}
	
	.credit {
		color: #900;	
	}
	
	.campaign {
		color: #666;	
	}
	
	#dateRange {
		font-size: 15px;
		margin-bottom: 20px;
		border-bottom: 1px dashed #999;
		padding: 3px;	
	}
</style>
</head>

<body style="padding: 5px;">
<div id="output" runat="server" />
</body>
</html>
