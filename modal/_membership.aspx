﻿<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="../_nav.ascx" %>
<%@ Assembly src="/bin/Continuity.cs" %>
<%@ Assembly src="/bin/Utilities.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "cancel") {
			CancelMember();
		} else if (Request["action"] == "skip") {
			SkipMember();
		} else if (Request["action"] == "preference") {
			ChangePreference();	
		}
	}
	
	private void SkipMember() {
		Continuity c = new Continuity();
		if (Request["commit"] == "true") {
			// cancel member
			c.SkipMembership(Request["id"], Request["skip_span"]);
			
			// redirect
			Response.Redirect("close_modal.html");
		} else {
			// show cancel dialog
			skip.Visible = true;
			
			// parse out skip weeks			
			string selectSkip = "<select id='skip_span' name='skip_span'>";
			for (int i = -3; i < 25; i++) {
				selectSkip += "<option value='" + i + "'>" + i + " weeks</option>";
			}
			selectSkip += "</select>";
			skip.InnerHtml += "<form action='_membership.aspx' method='post' id='oForm' name='oForm'>" + selectSkip + 
								"<input type='hidden' id='id' name='id' value='" + Request["id"] + "' />" +
								"<input type='hidden' id='commit' name='commit' value='true' />" +
								"<input type='hidden' id='action' name='action' value='skip' />" +
								"<input type='submit' /></form>";
		}
	}
	
	private void ChangePreference() {
		if (Request["commit"] == "true") {
			Continuity c = new Continuity();
			// change preference
			c.ChangePreference(Request["id"], Request["preference"]);
			
			// redirect
			Response.Redirect("close_modal.html");
		}
		
		_preference.Visible = true;
	}
	
	private void CancelMember() { 
		Continuity c = new Continuity();
		if (Request["commit"] == "true") {
			// cancel member
			c.CancelMembership(Request["id"], Request["reason_id"]);
			
			if (Request["email_"] == "yes") {
				string email = c.GetCustomerEmail(Request["customer_id"]);
				string subject = "Message from BarclaysWine.com";
				string body = "<span style='font-family: verdana; font-size: 11px;'>" +
								"<p>This will confirm that we have processed the request to cancel your membership in Barclay’s Wine Experience.  We have been proud to serve you and we want to thank you for the opportunity.  Remember that, at Barclay’s Wine, our goal is to make sure you always receive great value and that you never pay for a wine you don’t like.   We want to thank you for the opportunity to serve you in the past and we will be pleased to welcome you back at any time.  If for any reason, this cancellation was processed in error, contact us and we will reinstate your membership immediately.</p>" +  
								"<p>To encourage you to give us another try sooner rather than later, we are delighted to be able to make you an offer, effective for 30 days from the receipt of this email, to receive $25.00 off any purchase of six bottles or more in our on-line Wine Shop.  No membership is required and your full satisfaction is guaranteed. Check out the shop at www.barclayswine.com and, if you see something you like,  simply call Customer Service at 888-380-2337 (this offer is not available on the website) and we will  be pleased to accommodate your request.</p>" +
								"<p>Again, thank you for the privilege of allowing us to serve you.</p>" +
								"<p>In Vino Veritas,</p>" +
								"<p>The Barclays Wine Team</p></span>";
				
				Util u = new Util();
				u.SendMail(email, email, subject, body);
			}
			
			// redirect
			Response.Redirect("close_modal.html");
		} else {
			// show cancel dialog
			cancel.Visible = true;
			
			// get cancel reasons
			DataTable dt;
			c.GetCancelReasons(out dt);
			
			string selectCancel = "<select id='reason_id' name='reason_id'>";
			foreach (DataRow r in dt.Rows) {
				selectCancel += "<option value='" + r["ContinuityCancelReasonID"] + "'>" + r["Reason"] + "</option>";
			}
			selectCancel += "</select>";
			cancel.InnerHtml += "<form action='_membership.aspx' method='post' id='oForm' name='oForm'>" + selectCancel + 
								"<input type='hidden' id='id' name='id' value='" + Request["id"] + "' />" +
								"<input type='hidden' id='customer_id' name='customer_id' value='" + Request["customer_id"] + "' />" +
								"<br /><input type='checkbox' id='email_' name='email_' value='yes' checked  />Send email to Customer? <br />" +
								"<input type='hidden' id='commit' name='commit' value='true' />" +
								"<input type='hidden' id='action' name='action' value='cancel' />" +
								"<input type='submit' /></form>";
		}	
	}
	
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Membership Modification</title>
<script type="text/javascript" src="/includes/js/jquery.js"></script> 
<script src="/includes/js/jquery.validate.js"></script>
<script>
	$(document).ready(function(){
		$("#oForm").validate();
	});
</script>
</head>

<body>
<div id="cancel" runat="server" visible="false">      
	Please enter the reason for cancellation: <br />
</div>
<div id="skip" runat="server" visible="false">      
	Please enter the amount of weeks you want to skip: <br />
</div>
<div id="_preference" runat="server" visible="false">
	<div class="label">Preference: </div><div class="input">
    <form action='_membership.aspx' method='post' id="oForm" name="oForm">
    <select name="preference" class="required">
      <option> </option>
	  <option value="1">Red</option>
	  <option value="2">White</option>
	  <option value="3">Mixed</option>
	</select>
    <input type="submit" value="Change Preference" />
    <input type='hidden' id='id' name='id' value='<%= Request["id"]  %>' />
	<input type='hidden' id='commit' name='commit' value='true' />
	<input type='hidden' id='action' name='action' value='preference' />
    </form>
    </div>
</div>
</body>
</html>
