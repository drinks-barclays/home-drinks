﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Report.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<script language="C#" runat="server">

	string jSon = "";
	public void Page_Load(Object sender, EventArgs e)
 	{
		DataTable dt = new DataTable();
		Report report = new Report();
		Hashtable parameters = new Hashtable();
		if (Request["configID"] != null && Request["configID"] != "") parameters.Add("ConfigID", Request["configID"].ToString());
		if (Request["campaign_ID"] != null && Request["campaign_ID"] != "") parameters.Add("CampaignID", Request["campaign_ID"].ToString());
		
		//Response.Write(Request.QueryString);
		
		report.ContinuityPivot(out dt, parameters);
		
		//Response.Write(dt.Rows.Count);
		
		string json = JsonConvert.SerializeObject(dt, Formatting.Indented);
		jSon = json;
	}
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Continuity Pivot</title>


        <!-- external libs from cdnjs -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

        <!-- PivotTable.js libs from ../dist -->
        <link rel="stylesheet" type="text/css" href="//nicolas.kruchten.com/pivottable/dist/pivot.css">
        <script type="text/javascript" src="//nicolas.kruchten.com/pivottable/dist/pivot.js"></script>
        
        <script language="javascript">
			$(function(){
				var json = <%= jSon %>;
                var sum = $.pivotUtilities.aggregatorTemplates.countUnique;
                var numberFormat = $.pivotUtilities.numberFormat;
                var intFormat = numberFormat({digitsAfterDecimal: 0}); 
                        $("#shipments").pivot(
							json,
							{
								rows: ["StartDate"],
								cols: ["TotalShipments"],
                        		aggregator: sum(intFormat)(["CustomerID"])

							}
						);
						
						$("#margin").pivot(
							json,
							{
								rows: ["StartDate"],
								cols: ["OrderDate"],
                        		aggregator: $.pivotUtilities.aggregatorTemplates.sum(numberFormat({digitsAfterDecimal: 2}))(["Margin"])

							}
						);
			});
			
		</script>
        
        
<style>
	body {
		margin: 20px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 14px;
	}
	
	.header {
		font-size: 20px;
		font-weight: bold;	
		border-bottom: 1px solid;
	}
	
	table {
		font-family: Arial, Helvetica, sans-serif;
		margin: 10px;
		margin-bottom: 40px;
	}
	
	table th {
		margin: 7px;	
		font-size: 16px;
		font-weight: bold;
		border-bottom: 1px solid;
		border-right: 1px solid;
	}
	table td {
		margin: 5px;
		font-size: 14px;	
		border-bottom: 1px solid;
		border-right: 1px solid;
	}
	
	.descriptor {
		margin: 7px;
	}
	
	.subscriber {
		color: #060	;
	}
	
	.unsubscribe {
		color: #900;	
	}
	
	.bounce {
		color: #F60;
	}
	
	.revenue {
		color: #060	;
	}
	
	.credit {
		color: #900;	
	}
	
	.campaign {
		color: #666;	
	}
	
	#dateRange {
		font-size: 15px;
		margin-bottom: 20px;
		border-bottom: 1px dashed #999;
		padding: 3px;	
	}
</style>
</head>

<body style="padding: 5px;">
<div id="shipments" runat="server" />
<div id="margin" runat="server" />
</body>
</html>
