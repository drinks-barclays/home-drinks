﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Assembly src="/bin/Utilities.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.IO" %>
<script language="C#" runat="server">
	
	Customer c = new Customer();
	Util u = new Util();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		DataTable dt = c.ReferralCredit();
		foreach (DataRow r in dt.Rows) {
			string body = "<p>" + r["ReferrerName"] + ",</p>" +
				"<p>Thank you for referring " + r["ReferralName"] + "! As promised, you now" +
				" have a twenty five dollar credit applied to your account.</p>" +
				"<p>In Vino Veritas!</p>" +
				"<p>Wine Concierge<br />" +
				"  www.barclaysbin.com" +
				"</p>";
			// send email
			u.SendMail(r["ReferrerName"].ToString(), r["ReferrerEmail"].ToString(), "Your referral joined!", body);
			
			// apply credit
			c.AddReferralCredit(r["Referrer"].ToString());
		}
	}
	
</script>
