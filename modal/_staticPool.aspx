﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Report.cs" %>
<%@ Assembly src="/bin/Marketing.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Globalization" %>
<script language="C#" runat="server">
   	
	string output;
	Report report = new Report();
	public void Page_Load(Object sender, EventArgs e)
 	{
		// switch to see which report we are on
		switch (Request["action"]) {
			case "summary":
				showSummary();
				break;
			case "after-market":
				showAfterMarket();
				break;
			default:
				break;	
		}
		
		Marketing m = new Marketing();
		DataTable dt;
		m.GetCampaignList(out dt);
		
		campaign.InnerHtml = "<select name='campaignID' id='campaignID'><option value='' selected>Please select a campaign ID</option>";
		foreach (DataRow r in dt.Rows) {
			campaign.InnerHtml += "<option value='" + r["CampaignID"] + "'>" + r["CampaignID"] + ": " + r["PartnerName"] + " - " + r["Description"] + "</option>";	
		}
		campaign.InnerHtml += "</select>";
		
	}
	
	public void showAfterMarket() {
		afterMarket.Visible = true;
		headLine.InnerText = "After Market Sales";
		DataTable dt = new DataTable();
		
		// bottles
		report.GetStaticPoolAfterMarket(out dt, Request["campaignID"], Request["month"], Request["productTypeID"], Request["continuity"]);
		
		foreach (DataRow r in dt.Rows) {
			output += "<tr><td>" + r["MonthDate"].ToString().Replace(" 12:00:00 AM", "") + "</td><td>" + r["Orders"] + "</td><td align='center'>" + r["Total"] + "</td><td align='center' class='credit'>" + r["Net"] + "</td></tr>";
		}
	}
	
	public void showSummary() {
		summary.Visible = true;
		DataTable dt = new DataTable();
		
		// bottles
		report.GetStaticPoolSummary(out dt, Request["campaignID"], Request["month"], "1001", "0");
		
		foreach (DataRow r in dt.Rows) {
			output += "<tr><td><a href='_staticPool.aspx?action=after-market&month=" +
					Request["month"] + "&campaignID=" + Request["campaignID"] + 
					"&productTypeID=1001&continuity=0" + 
					"'>Bottle</a></td><td>" + r["Acquisitions"] + 
					"</td><td align='center'>" + r["Revenue"] + 
					"</td><td align='center' class='credit'>" + r["NetMargin"] + 
					"</td><td>" +
					Convert.ToDouble(r["NetMargin"]) / Convert.ToInt16(r["Acquisitions"])+ "</td></tr>";
		}
		
		// cases
		report.GetStaticPoolSummary(out dt, Request["campaignID"], Request["month"], "1002", "0");
		
		foreach (DataRow r in dt.Rows) {
			output += "<tr><td><a href='_staticPool.aspx?action=after-market&month=" +
					Request["month"] + "&campaignID=" + Request["campaignID"] + 
					"&productTypeID=1002&continuity=0" + 
					"'>Cases</a></td><td>" + r["Acquisitions"] + 
					"</td><td align='center'>" + r["Revenue"] + 
					"</td><td align='center' class='credit'>" + r["NetMargin"] + 
					"</td><td>" +
					Convert.ToDouble(r["NetMargin"]) / Convert.ToInt16(r["Acquisitions"])+ "</td></tr>";;
		}
		
		// packs
		report.GetStaticPoolSummary(out dt, Request["campaignID"], Request["month"], "1003", "0");
		
		foreach (DataRow r in dt.Rows) {
			output += "<tr><td><a href='_staticPool.aspx?action=after-market&month=" +
					Request["month"] + "&campaignID=" + Request["campaignID"] + 
					"&productTypeID=1003&continuity=0" + 
					"'>Packs</a></td><td>" + r["Acquisitions"] + 
					"</td><td align='center'>" + r["Revenue"] + 
					"</td><td align='center' class='credit'>" + r["NetMargin"] + 
					"</td><td>" +
					Convert.ToDouble(r["NetMargin"]) / Convert.ToInt16(r["Acquisitions"])+ "</td></tr>";
		}
		
		// continuity
		report.GetStaticPoolSummary(out dt, Request["campaignID"], Request["month"], "1003", "1");
		
		foreach (DataRow r in dt.Rows) {
			output += "<tr><td><a href='_staticPool.aspx?action=after-market&month=" +
					Request["month"] + "&campaignID=" + Request["campaignID"] + 
					"&productTypeID=1003&continuity=1" + 
					"'>Continuity</a></td><td>" + r["Acquisitions"] + 
					"</td><td align='center'>" + r["Revenue"] + 
					"</td><td align='center' class='credit'>" + r["NetMargin"] + 
					"</td><td>" +
					Convert.ToDouble(r["NetMargin"]) / Convert.ToInt16(r["Acquisitions"])+ "</td></tr>";
		}
	}
	
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Static Pool</title>
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 20px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 14px;
	}
	
	.header {
		font-size: 20px;
		font-weight: bold;	
		border-bottom: 1px solid;
	}
	
	table {
		font-family: Arial, Helvetica, sans-serif;
		margin: 10px;
		margin-bottom: 40px;
	}
	
	table th {
		margin: 7px;	
		font-size: 16px;
		font-weight: bold;
		border-bottom: 1px solid;
		border-right: 1px solid;
	}
	table td {
		margin: 5px;
		font-size: 14px;	
		border-bottom: 1px solid;
		border-right: 1px solid;
	}
	
	.descriptor {
		margin: 7px;
	}
	
	.subscriber {
		color: #060	;
	}
	
	.unsubscribe {
		color: #900;	
	}
	
	.bounce {
		color: #F60;
	}
	
	.revenue {
		color: #060	;
	}
	
	.credit {
		color: #900;	
	}
	
	.campaign {
		color: #666;	
	}
	
	#dateRange {
		font-size: 15px;
		margin-bottom: 20px;
		border-bottom: 1px dashed #999;
		padding: 3px;	
	}
</style>
</head>

<body style="padding: 5px;">

<div id="dateRange" align="right">
<form method="get" action="_staticPool.aspx">
	<input type="hidden" value="summary" name="action" />
    <span id="campaign" runat="server" />
    <select name="month">
    	<%
			DateTime d = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
			for (int i = 1; i<36; i++) {
				Response.Write("<option>" + d.AddMonths(i*-1) + "</option>");
			}
		%>
    </select>
    <input type="submit" />
</form>
</div>



<div class="header" id="headLine" runat="server">Cost Per Acquisition</div>
<div  id="summary" runat="server" visible="false">
<table border="0" cellpadding="5" cellspacing="0">
	<tr align="center" style="border: 1px solid;">
    	<th class="campaign"></th>
    	<th>Acquisitions</th>
    	<th>Revenue</th>
    	<th>NetMargin</th>
    	<th>CPA</th>
    </tr>
    <%= output %>
</table>
</div>
<div  id="afterMarket" runat="server" visible="false">
<table border="0" cellpadding="5" cellspacing="0">
	<tr align="center" style="border: 1px solid;">
    	<th>Month</th>
    	<th>Orders</th>
    	<th>Revenue</th>
    	<th>NetMargin</th>
    </tr>
    <%= output %>
</table>
</div>

</body>
</html>
