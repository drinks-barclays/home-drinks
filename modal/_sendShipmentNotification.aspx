﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Continuity.cs" %>
<%@ Assembly src="/bin/Utilities.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<script language="C#" runat="server">

   	public void Page_Load(Object sender, EventArgs e)
 	{
		//if (Session["operator"] == null) Response.Redirect("/security.aspx?r=" + Server.UrlEncode(Request.Url.ToString()));
		
		if (Request["action"] == "send") {
			DoSend();	
		}
	}
	
	public void DoSend() {
		Util u = new Util();
		Continuity c = new Continuity();
		DataTable dt;
		DataTable product;
		// grab eligible
		if (Request["id"] != null && Request["id"].ToString() != "") {
			c.GetEligible(out dt, Request["id"]); 
			c.GetEligibleProduct(out product, Request["id"]);
		} else {
			c.GetEligible(out dt); 
			c.GetEligibleProduct(out product, null);
		}
		string body;
		string name;
		string fromEmail;
		string fromName;
		
		string club = "";
		double clubPrice = 0;
		double clubShipping = 0;
		
		foreach (DataRow row in dt.Rows) {
			club = row["Name"].ToString().Trim();
			clubPrice = Convert.ToDouble(row["Price"].ToString());
			clubShipping = Convert.ToDouble(row["Shipping"].ToString());
			
			StringBuilder sb = new StringBuilder();
			StringWriter sw = new StringWriter(sb);
			using (JsonWriter writer = new JsonTextWriter(sw))
			{
				writer.Formatting = Formatting.Indented;
				writer.WriteStartObject();
				
				// write out core
				foreach (DataColumn col in dt.Columns)  //loop through the columns. 
				{
					writer.WritePropertyName(col.ColumnName);
					writer.WriteValue(row[col.ColumnName]);
				}
				
				// manually add ship date
				writer.WritePropertyName("ShipDate");
				writer.WriteValue(System.DateTime.Now.AddDays(3).Date.ToString("d"));
				
				// write out product
				writer.WritePropertyName("product");
				writer.WriteStartArray();
				DataRow[] children = product.Select("ParentProductID = " + row["ProductID"]);
				foreach(DataRow p in children) {
					writer.WriteStartObject();
					foreach (DataColumn col in product.Columns)  //loop through the columns. 
					{						
						writer.WritePropertyName(col.ColumnName);
						writer.WriteValue(p[col.ColumnName]);
					}
					writer.WriteEndObject();
				}
				
				writer.WriteEnd();
				writer.WriteEndObject();
			}
			
			Response.Write("<pre>"+sb.ToString()+"</pre>");
			MailMessage mail = new MailMessage(
				   "cs@" + row["Domain"],
				   row["Email"].ToString(),
				   "",
				   "");
			mail.Bcc.Add("cs@" + row["Domain"].ToString()); // bcc customer service for their records
			
			mail.Headers.Add( "X-MC-MergeVars", sb.ToString());
			mail.Headers.Add( "X-MC-Template", row["BrandName"].ToString().Replace("& ", "").Trim().Replace(" ", "-") + "-asn");
				
			NetworkCredential creds2 = new NetworkCredential("mandrill@drinks.com", "RyaDdy6YtIeZoG8roFAw2Q");
			SmtpClient client2 = new SmtpClient("smtp.mandrillapp.com", 587); 
			client2.UseDefaultCredentials = false;
			client2.Credentials = creds2;
			try {
				client2.Send(mail);
			} catch (Exception ex) {
				ex.ToString();	
			}
		}
		
		
		string adminSubject = "ASNs sent: "+  dt.Rows.Count + " - " + club;
		string adminBody = "Club: " + club + "<br />";
		adminBody += "ASNs sent: " + dt.Rows.Count + "<br />";
		adminBody += "Price: " + clubPrice.ToString("c") + "<br />";
		adminBody += "Shipping: " + clubShipping.ToString("c") + "<br />";
		adminBody += "Potential Revenue: " + ((clubPrice+clubShipping)*dt.Rows.Count).ToString("c") + "<br />";
		Response.Write(dt.Rows.Count + " ASN emails sent.");
		Response.Write("<p>" + adminBody + "</p>");
		
			MailMessage email = new MailMessage(
				   "asn+robot@barclayswine.com",
				   "drinks-devops@drinks.com, josiah@drinks.com, zac@drinks.com, tyson@drinks.com, jupin.chamanara@drinks.com, cintra@drinks.com",
				   adminSubject,
				   adminBody);
			email.IsBodyHtml = true;
				
			NetworkCredential creds = new NetworkCredential("auth@voidme.com", "$m@ng0");
			SmtpClient client = new SmtpClient("127.0.0.1"); 
			client.UseDefaultCredentials = false;
			client.Credentials = creds;
			try {
				client.Send(email);
			} catch (Exception ex) {
				;	
			}
	}
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload Tracking</title>
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 0px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
</style>
<script src="/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script>
	function hideMe() {
		$("#main").html("Processing, please wait...");
		$("#main").load("_sendShipmentNotification.aspx?action=send&id=<%= Request["id"] %>");
	}
</script>
</head>

<body>
<div id="main">
Are you sure you want to send the ASN? <br>
<a href="javascript: void()" onClick="hideMe();">Yes</a> &nbsp; or&nbsp; <a href="javascript: parent.$.fancybox.close();">No</a>
</div>
</body>
</html>
