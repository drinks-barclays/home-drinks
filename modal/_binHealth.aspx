﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Report.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="createsend_dotnet" %>
<%@ Import Namespace="System.Globalization" %>
<script language="C#" runat="server">
   	public string today;
	public string thisWeek;
	public string thisMonth;
	public string thisYear;
	Report report = new Report();
	public void Page_Load(Object sender, EventArgs e)
 	{
		today = "fromDate=" + System.DateTime.Now.Date + "&toDate="  + System.DateTime.Now.AddDays(1).Date;
		
		CultureInfo cultureInfo = CultureInfo.CurrentCulture;
		DayOfWeek firstDay = cultureInfo.DateTimeFormat.FirstDayOfWeek;
		DateTime firstDayInWeek = System.DateTime.Now.Date;
		while (firstDayInWeek.DayOfWeek != firstDay)
		firstDayInWeek = firstDayInWeek.AddDays(-1);
		thisWeek = "fromDate=" + firstDayInWeek + "&toDate="  + firstDayInWeek.AddDays(7);
		
		thisMonth = "fromDate=" + System.DateTime.Now.Month + "/1/" + System.DateTime.Now.Year + "  12:00:00 AM &toDate="  + System.DateTime.Now.AddMonths(1).Month + "/1/" + System.DateTime.Now.AddMonths(1).Year + " 12:00:00 AM";
		
		thisYear = "fromDate=1/1/" + System.DateTime.Now.Year + " 12:00:00 AM&toDate=1/1/" + System.DateTime.Now.AddYears(1).Year + " 12:00:00 AM";
		
		ShowEmailHealth();
		if (Request["range"] != null) ShowAcquisitionHealth();
		
	}
	
	private void ShowEmailHealth() {
		switch (Request["range"]) {
			case "today":
				tToday.Visible = true;
				reportDate.InnerText = today.Replace("fromDate=", "").Replace("&toDate=", " - ");
				break;
			case "week":
				tWeek.Visible = true;
				reportDate.InnerText = thisWeek.Replace("fromDate=", "").Replace("&toDate=", " - ");
				break;
			case "month":
				tMonth.Visible = true;
				reportDate.InnerText = thisMonth.Replace("fromDate=", "").Replace("&toDate=", " - ");
				break;
			case "year":
				tYear.Visible = true;
				reportDate.InnerText = thisYear.Replace("fromDate=", "").Replace("&toDate=", " - ");
				break;
			default:
				break;
		}
		
		
		List list = new List("43fdbee4c6a6371122816951b390f051");
		
		ListStats listDetails = list.Stats();
		totalActive.InnerText = listDetails.TotalActiveSubscribers.ToString("n").Replace(".00" , ""); 
		totalUnsubscribes.InnerText = listDetails.TotalUnsubscribes.ToString("n").Replace(".00" , "");
		totalBounces.InnerText = listDetails.TotalBounces.ToString("n").Replace(".00" , ""); 
		
		todayActive.InnerText = listDetails.NewActiveSubscribersToday.ToString("n").Replace(".00" , ""); 
		todayUnsubscribes.InnerText = listDetails.UnsubscribesToday.ToString("n").Replace(".00" , "");
		todayBounces.InnerText = listDetails.BouncesToday.ToString("n").Replace(".00" , ""); 
		
		weekActive.InnerText = listDetails.NewActiveSubscribersThisWeek.ToString("n").Replace(".00" , ""); 
		weekUnsubscribes.InnerText = listDetails.UnsubscribesThisWeek.ToString("n").Replace(".00" , "");
		weekBounces.InnerText = listDetails.BouncesThisWeek.ToString("n").Replace(".00" , "");
		
		monthActive.InnerText = listDetails.NewActiveSubscribersThisMonth.ToString("n").Replace(".00" , ""); 
		monthUnsubscribes.InnerText = listDetails.UnsubscribesThisMonth.ToString("n").Replace(".00" , "");
		monthBounces.InnerText = listDetails.BouncesThisMonth.ToString("n").Replace(".00" , "");
		
		yearActive.InnerText = listDetails.NewActiveSubscribersThisYear.ToString("n").Replace(".00" , ""); 
		yearUnsubscribes.InnerText = listDetails.UnsubscribesThisYear.ToString("n").Replace(".00" , "");
		yearBounces.InnerText = listDetails.BouncesThisYear.ToString("n").Replace(".00" , "");	
	}
	
	
	public string acqReport = "";
	public string ordReport = "";
	private void ShowAcquisitionHealth() {
		DataTable dt;
		report.GetBinAcquisitionReport(out dt, Request["fromDate"].ToString(),  Request["toDate"].ToString(), "5");
		foreach (DataRow r in dt.Rows) {
			acqReport += "<tr><td class='campaign'>" + r["Description"] + "</td><td align='center'>" + r["Customers"] + "</td></tr>";
		}
		
		
		report.GetBinOrderReport(out dt, Request["fromDate"].ToString(),  Request["toDate"].ToString(), "5");
		foreach (DataRow r in dt.Rows) {
			ordReport += "<tr><td class='campaign'>" + r["Description"] + "</td><td align='center'>" + r["Orders"] + "</td><td align='center' class='credit'>" + r["Credits"] + "</td><td align='center' class='revenue'>" + r["Revenue"] + "</td></tr>";
		}
	}
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bin Health Report</title>
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 20px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 14px;
	}
	
	.header {
		font-size: 20px;
		font-weight: bold;	
		border-bottom: 1px solid;
	}
	
	table {
		font-family: Arial, Helvetica, sans-serif;
		margin: 10px;
		margin-bottom: 40px;
	}
	
	table th {
		margin: 7px;	
		font-size: 16px;
		font-weight: bold;
		border-bottom: 1px solid;
		border-right: 1px solid;
	}
	table td {
		margin: 5px;
		font-size: 14px;	
		border-bottom: 1px solid;
		border-right: 1px solid;
	}
	
	.descriptor {
		margin: 7px;
	}
	
	.subscriber {
		color: #060	;
	}
	
	.unsubscribe {
		color: #900;	
	}
	
	.bounce {
		color: #F60;
	}
	
	.revenue {
		color: #060	;
	}
	
	.credit {
		color: #900;	
	}
	
	.campaign {
		color: #666;	
	}
	
	#dateRange {
		font-size: 15px;
		margin-bottom: 20px;
		border-bottom: 1px dashed #999;
		padding: 3px;	
	}
</style>
</head>

<body style="padding: 5px;">

<div id="dateRange" align="right"><div id="reportDate" style="float: left;" runat="server" /><a href="_binHealth.aspx?range=today&<%= today %>">Today</a> | <a href="_binHealth.aspx?range=week&<%= thisWeek %>">This Week</a> | <a href="_binHealth.aspx?range=month&<%= thisMonth %>">This Month</a> | <a href="_binHealth.aspx?range=year&<%= thisYear %>">This Year</a> </div>

<div class="header">Email List Health</div>
<table border="0" cellpadding="5" cellspacing="0">
	<tr align="center">
    	<td></td>
    	<th class="subscriber">Subscribers</th>
        <th class="unsubscribe">Unsubscribes</th>
        <th class="bounce">Bounces</th>
    </tr>
	<tr>
	  <td class="descriptor">Total</td>
	  <td align="center" runat="server" id="totalActive" class="subscriber">&nbsp;</td>
	  <td align="center" runat="server" id="totalUnsubscribes" class="unsubscribe">&nbsp;</td>
	  <td align="center" runat="server" id="totalBounces" class="bounce">&nbsp;</td>
  	</tr>
	<tr runat="server" id="tToday" visible=False>
	  <td class="descriptor">Today</td>
	  <td align="center" runat="server" id="todayActive" class="subscriber">&nbsp;</td>
	  <td align="center" runat="server" id="todayUnsubscribes" class="unsubscribe">&nbsp;</td>
	  <td align="center" runat="server" id="todayBounces" class="bounce">&nbsp;</td>
  	</tr>
	<tr runat="server" id="tWeek" visible=False>
	  <td class="descriptor">This week</td>
	  <td align="center" runat="server" id="weekActive" class="subscriber">&nbsp;</td>
	  <td align="center" runat="server" id="weekUnsubscribes" class="unsubscribe">&nbsp;</td>
	  <td align="center" runat="server" id="weekBounces" class="bounce">&nbsp;</td>
  	</tr>
	<tr runat="server" id="tMonth" visible=False>
	  <td class="descriptor">This month</td>
	  <td align="center" runat="server" id="monthActive" class="subscriber">&nbsp;</td>
	  <td align="center" runat="server" id="monthUnsubscribes" class="unsubscribe">&nbsp;</td>
	  <td align="center" runat="server" id="monthBounces" class="bounce">&nbsp;</td>
  	</tr>
	<tr runat="server" id="tYear" visible=False>
	  <td class="descriptor">This year</td>
	  <td align="center" runat="server" id="yearActive" class="subscriber">&nbsp;</td>
	  <td align="center" runat="server" id="yearUnsubscribes" class="unsubscribe">&nbsp;</td>
	  <td align="center" runat="server" id="yearBounces" class="bounce">&nbsp;</td>
  	</tr>
</table>


<div class="header">Acquisitions</div>
<table border="0" cellpadding="5" cellspacing="0">
	<tr align="center" style="border: 1px solid;">
    	<th class="campaign">Source Campaign</th>
    	<th>Customers</th>
    </tr>
    <%= acqReport %>
</table>

<div class="header">Orders by Acquisition Campaign</div>
<table border="0" cellpadding="5" cellspacing="0">
  <tr align="center" style="border: 1px solid;">
    	<th class="campaign">Source Campaign</th>
    	<th>Orders</th>
        <th class="credit">Credits</th>
        <th class="revenue">Revenue</th>
    </tr>
	<tr>
    <%= ordReport %>
  	</tr>
</table>


</body>
</html>
