﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Globalization" %>
<script language="C#" runat="server">

	public DateTime thisMonth;
	Product product = new Product();
	public string schedule;
	public void Page_Load(Object sender, EventArgs e)
 	{
		thisMonth = Convert.ToDateTime(System.DateTime.Now.Month + "/1/" + System.DateTime.Now.Year + "  12:00:00 AM");
		
		DataTable dt = product.GetBinFeatures(thisMonth.ToString());
		DataRow[] foundRows;
		
		int daysInMonth = System.DateTime.DaysInMonth(thisMonth.Year, thisMonth.Month);
		
		for (int i = 1; i <= daysInMonth; i++) {
			foundRows = dt.Select("FeatureDate = #" + thisMonth.AddDays(i-1) + "#");
			
			if (foundRows.Length > 0) {
				// show scheduled
				schedule += "<tr><td style='font-weight: bold'>" + i + "</td><td style='font-weight: bold'><a class='featureMe' href='_binToFeature.aspx?featureDate=" + thisMonth.AddDays(i-1) + "' style='color: blue; font-style: italic'>" + foundRows[0]["Name"].ToString() + "</a></td></tr>";
			} else {
				// nothing scheduled
				schedule += "<tr><td>" + i + "</td><td><a class='featureMe' href='_binToFeature.aspx?featureDate=" + thisMonth.AddDays(i-1) + "' style='color: blue; font-style: italic'>Empty</a></td></tr>";
			}
		}
	}
	
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bin Schedule</title>
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<script src="/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/fancybox/jquery.mousewheel-3.0.2.pack.js"></script>
<script type="text/javascript" src="/fancybox/jquery.fancybox-1.3.1.js"></script>
<link rel="stylesheet" type="text/css" href="/fancybox/jquery.fancybox-1.3.1.css" media="screen" />
<script language="javascript">
	$(document).ready(function() {
		
		$(".featureMe").fancybox({
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'padding'			: '0',
				'width'				: 650,
				'height'			: 400,
				'type'				: 'iframe'
			});
	})
</script>
<style>
	body {
		margin: 20px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 14px;
	}
	
	.header {
		font-size: 20px;
		font-weight: bold;	
		border-bottom: 1px solid;
	}
	
	table {
		font-family: Arial, Helvetica, sans-serif;
		margin: 10px;
		margin-bottom: 40px;
	}
	
	table th {
		margin: 7px;	
		font-size: 16px;
		font-weight: bold;
		border-bottom: 1px solid;
		border-right: 1px solid;
	}
	table td {
		margin: 5px;
		font-size: 14px;	
		border-bottom: 1px solid;
		border-right: 1px solid;
	}
	
	.descriptor {
		margin: 7px;
	}
	
	.subscriber {
		color: #060	;
	}
	
	.unsubscribe {
		color: #900;	
	}
	
	.bounce {
		color: #F60;
	}
	
	.revenue {
		color: #060	;
	}
	
	.credit {
		color: #900;	
	}
	
	.campaign {
		color: #666;	
	}
	
	#dateRange {
		font-size: 15px;
		margin-bottom: 20px;
		border-bottom: 1px dashed #999;
		padding: 3px;	
	}
</style>
</head>

<body style="padding: 5px;"> 

<div id="dateRange" align="right"><div id="reportDate" style="float: left;" runat="server" /></div>

<div class="header">Bin Schedule for <%= CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(thisMonth.Month) + " " + thisMonth.Year %></div>
<table border="0" cellpadding="5" cellspacing="0">
  <tr align="center" style="border: 1px solid;">
    	<th class="campaign">Day</th>
    	<th>Product</th>
    </tr>
    	<%= schedule %>
</table>


</body>
</html>
