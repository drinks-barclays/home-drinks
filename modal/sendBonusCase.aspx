﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Utilities.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="Data" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Net.Mime" %>
<script language="C#" runat="server">
	public DataUtility d = new DataUtility("Server=DrinksWeb01;User ID=Wine;password=#3web;Database=WineFolks;Persist Security Info=True");
   	public void Page_Load(Object sender, EventArgs e)
 	{
		DataTable dt;
		string body;
		string name;
		Util u = new Util();
            
			Hashtable parameters = new Hashtable();
			parameters = new Hashtable();
            Result r = new Result();
			r = d.ExecuteDataTable(out dt, "sp_GetBonusCasePeople", parameters);
            if (r.ResultType == Common.ResultType.Failure)
            {
                HttpContext.Current.Response.Write(r.Exception.Message);
            }
		
		foreach (DataRow row in dt.Rows) {
			name = row["FirstName"].ToString().Trim() + " " + row["LastName"].ToString().Trim();
			body = "<p>Twice a year—winter and summer—we offer our members a great opportunity to get an extra case of wine because those are the times that you are likely to need it most. Well, the Summer Bonus Case is finally here—and what a case it is.  First, you get 12 spectacular bottles that were selected for the season.  Second, we are going to include a BONUS magic decanter with your shipment—that is a 39.95 value.  And finally, we will pay shipping on your bonus case, saving you another 19.95." + 
					"</p>" +
					"<p><strong><a href='https://www.barclayswine.com/bonus_case.aspx?" +
					"uid=" + row["UID"].ToString() + "'>Click here</a> to check out the wines " +
					"in the bonus case and to confirm that you want to receive it. We will get it rolling " +
					" as soon as we hear from you!</strong></p><p>Remember that a sip is " +
					"worth a thousand words- have a great summer!</p><p>In vino veritas!</p>" +
					"<p>Barclay's Wine Team</p>";
			//Response.Write(name + " - " + row["email"] + "<br>" + body + "<hr>");
			
			try {
				u.SendMailServer(name, row["email"].ToString().Trim(), "Members Only Holiday Bonus Case!", body);
			} catch (Exception ex) {
				;
			}
		}
		
	}

</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Close Continuity</title>	
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 0px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	
	.field {
		width: 325px;
		clear: both;
		padding: 3px;
	}
	
	.label {
		width: 125px;
		float: left;
		padding: 3px;
	}
	
	.input {
		width: 175px;
		float: left;
	}
</style>
</head>

<body>
</body>
</html>
