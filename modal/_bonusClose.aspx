﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/ShoppingCart.cs" %>
<%@ Assembly src="/bin/Continuity.cs" %>
<%@ Assembly src="/bin/LitlePayment.cs" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Assembly src="/bin/Utilities.cs" %>
<%@ Assembly src="/bin/Marketing.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.IO" %>
<script language="C#" runat="server">

	Marketing m = new Marketing();
	Util u = new Util();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Session["operator"] == null) Response.Redirect("/security.aspx?r=" + Server.UrlEncode(Request.Url.ToString()));
		
		
		if (Request["action"] == "close" && Request["campaign_ID"] != null) {
			DoClose();	
		} else {
			DataTable campaigns = new DataTable();
			m.GetCampaignList(out campaigns);
			
			campaign.InnerHtml = "<select name='campaign_id' id='campaign_id'><option value='' selected>Please select a campaign ID</option>";
			foreach (DataRow r in campaigns.Rows) {
				campaign.InnerHtml += "<option value='" + r["CampaignID"] + "'>" + r["CampaignID"] + ": " + r["PartnerName"].ToString().Trim() + " - " + r["Description"] + "</option>";	
			}
			campaign.InnerHtml += "</select>";	
		}
		
	}
	
	public void DoClose() {
		Continuity c = new Continuity();
		DataTable dt;
		Payment_ p = new Payment_();
		Order order = new Order();
		bool goodPayment;
		ShoppingCart cart = new ShoppingCart();
		string cartID = cart.GetShoppingCartId();
		
		// grab eligible
		c.GetEligibleToCloseBonus(out dt, Request["campaign_id"]);
		
		// counters
		int troon = 0;
		int barclays = 0;
		int heartwood = 0;
		int franklinmint = 0;

		int troonDecline = 0;
		int barclaysDecline = 0;
		int heartwoodDecline = 0;
		int franklinmintDecline = 0;
		
		double troonRev = 0;
		double barclaysRev = 0;
		double heartwoodRev = 0;
		double franklinmintRev = 0;
		
		// parse through and charge
		foreach(DataRow row in dt.Rows) {			
			// try to charge
			p = new Payment_();
				
				/*Barclays: 6105109, Heartwood: 6106204, Troon: 6105112*/
				switch  (row["BrandID"].ToString()) {
					case "2":
						// troon
						p.MID = "6105112";
						p.ReportGroup = "TroonContinuityBonus";
						break;
					case "7":
						// H&O
						p.MID = "6106204";
						p.ReportGroup = "HeartwoodContinuityBonus";
						break;
					case "8":
						// Franklin Mint
						p.MID = "7132993";
						p.ReportGroup = "FranklinMintContinuity";
						break;
					default:
						p.MID = "6105109";
						p.ReportGroup = "BarclaysContinuityBonus";
						break;
				}
				
			
			string totalAmount = (Convert.ToDouble(row["Price"].ToString()) + Convert.ToDouble(row["Shipping"].ToString())).ToString();
			string shippingAmount = row["Shipping"].ToString();
			
			double tax = Convert.ToDouble(cart.GetTax(row["State"].ToString(), Convert.ToDouble(totalAmount)));
			double total = Convert.ToDouble(totalAmount) + tax;
				
			goodPayment = p.DoPayment(row["Digits"].ToString().Trim(), "",
											(row["Month"].ToString().Trim().PadLeft(2, '0') + row["Year"].ToString().Trim().Substring(2,2)), 
											total.ToString("F"), row["BillingFirstName"].ToString().Trim(), 
											row["BillingLastName"].ToString().Trim(), row["BillingPostalCode"].ToString().Trim(), 
											"", 1);
			
			if (goodPayment) {
				// add order
				cart.EmptyCart(cartID);
				int productID = Convert.ToInt32(row["ProductID"].ToString());
				
				/*switch (row["PreferenceTypeID"].ToString()) {
					//m 6776	 r 6779	w 6781
					// 6793	6787	6788
					case "1": //red
						productID = 6787;
						break;
					case "2":  // white
						productID = 6788;
						break;
					case "3":  // mixed
						productID = 6793;
						break;
					default:
						break;
				}*/
				
				
				cart.AddItem(cartID, productID, 1);
				order = new Order();
				order.Add(row["CustomerBillingID"].ToString(), row["CustomerShippingID"].ToString(), Request["campaign_id"], cartID, 
									shippingAmount, tax.ToString("F"), total.ToString("F"), 
									"0", p.TransactionCode, Request["reward_number"], row["BrandID"].ToString());
									
				// increment counters
				
				switch  (row["BrandID"].ToString()) {
					case "2":
						// troon
						troon += 1;
						troonRev += total;
						break;
					case "7":
						// H&O
						heartwood += 1;
						heartwoodRev += total;
						break;
					case "8":
						// Franklin Mint
						franklinmint += 1;
						franklinmintRev += total;
						break;
					default:
						barclays += 1;
						barclaysRev += total;
						break;
				}
										
			} else {
				// bad
				Response.Write(row["CustomerID"] + " - " + p.debug + "<br />");
				
				string CSPhone = "";
				string brandName = "";
				string body;
				string fromEmail;
				string fromName;
				
				// increment counters
				switch  (row["BrandID"].ToString()) {
					case "2":
						// troon
						troonDecline += 1;
						fromEmail = "cs@troonwineclub.com";
						fromName = "Wine Concierge";
						brandName = "Troon Wine Club";
						CSPhone = "877-998-7666";
						break;
					case "7":
						// H&O
						heartwoodDecline += 1;
						fromEmail = "cs@heartwoodandoak.com";
						fromName = "Wine Concierge";
						brandName = "Heartwood & Oak";
						CSPhone = "888-661-1246";
						break;
					case "8":
						// Franklin Mint
						franklinmintDecline += 1;
						fromEmail = "cs@franklinmintwine.com";
						fromName = "Wine Concierge";
						brandName = "Franklin Mint Wine Merchants";
						CSPhone = "855-784-3488";
						break;
					default:
						barclaysDecline += 1;
						fromEmail = "cs@barclayswine.com";
						fromName = "Wine Concierge";
						brandName = "Barclays Wine";
						CSPhone = "888-380-2337";
						break;
				}
				
				body = File.ReadAllText("c:\\inetpub\\net\\home.drinks.com\\common\\decline\\default.html");
				body = body.Replace("*|CS_PHONE|*", CSPhone);
				body = body.Replace("*|CS_EMAIL|*", fromEmail);
				body = body.Replace("*|FIRST_NAME|*", row["BillingFirstName"].ToString().Trim());
				body = body.Replace("*|BRAND|*", brandName);
				body = body.Replace("*|CLUB_NAME|*", row["ClubName"].ToString().Trim());
				body = body.Replace("*|CUSTOMER_ID|*", row["CustomerID"].ToString().Trim());
			
				
				// send decline email
				u.SendMailServer(row["BillingFirstName"].ToString().Trim() + " " + row["BillingLastName"].ToString().Trim(), row["Email"].ToString(), "Oh no! We couldn't process your order. Please call us ASAP.", body, fromEmail, fromName);
			}
			
		}	
		
		string adminSubject = "Bonus Case Summary";
		string adminBody = "Barclays: " + barclays + " charges for " + barclaysRev.ToString("c") + " revenue and " + barclaysDecline + " declines<br />";
		adminBody += "H&O: " + heartwood + " charges for " + heartwoodRev.ToString("c") + " revenue and " + heartwoodDecline + " declines<br />";
		adminBody += "Troon: " + troon + " charges for " + troonRev.ToString("c") + " revenue and " + troonDecline + " declines<br />";
		adminBody += "Franklin Mint: " + franklinmint + " charges for " + franklinmintRev.ToString("c") + " revenue and " + franklinmintDecline + " declines<br />";
		
			MailMessage email = new MailMessage(
				   "asn+robot@barclayswine.com",
				   "drinks-devops@drinks.com, josiah@drinks.com, zac@drinks.com",
				   adminSubject,
				   adminBody);
			email.IsBodyHtml = true;
				
			NetworkCredential creds = new NetworkCredential("auth@voidme.com", "$m@ng0");
			SmtpClient client = new SmtpClient("127.0.0.1"); 
			client.UseDefaultCredentials = false;
			client.Credentials = creds;
			try {
				client.Send(email);
			} catch (Exception ex) {
				;	
			}	
	}

</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Close Continuity</title>	
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 0px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	
	.field {
		width: 325px;
		clear: both;
		padding: 3px;
	}
	
	.label {
		width: 125px;
		float: left;
		padding: 3px;
	}
	
	.input {
		width: 175px;
		float: left;
	}
</style>
<script src="/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script>
	function hideMe() {
		if ($("#campaign_id").val() != "") {
			$("#main").html("Charging, please wait...");
			$("#main").load("_bonusClose.aspx?action=close&campaign_id=" + $("#campaign_id").val());
		} else {
			alert("enter a campaign");	
		}
	}
</script>
</head>

<body>
<div id="campaign" runat="server" />
<div id="main"><a href="javascript: void()" onClick="hideMe();">Close Bonus</a></div>
</body>
</html>
