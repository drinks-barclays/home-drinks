﻿<%@  Page Language="C#" Debug="true" %>
<%@ Import Namespace="Data" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.IO" %>
<script language="C#" runat="server">
	
	public DataUtility d = new DataUtility("Server=DrinksWeb01;User ID=Wine;password=#3web;Database=WineFolks;Persist Security Info=True");
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "upload") {
			int counter = 0;
			using (StreamReader sr = new StreamReader(loFile.PostedFile.InputStream)) 
			{
				string line;
				string[] splitted;
				char[] splitter = { ',' };
						
				while ((line = sr.ReadLine()) != null) 
				{					
					splitted = line.Split(splitter);
					try {
						// make sure there is a number
						if (splitted[2] != null && splitted[2].ToString().Trim() != "") {
							Hashtable parameters = new Hashtable();
							parameters.Add("Email", splitted[0].ToString().Trim());
							parameters.Add("RewardNumber", splitted[2].ToString().Trim());
							parameters.Add("CampaignID", splitted[3].ToString().Trim());
							
							Result r = new Result();
							r = d.ExecuteNonQuery("sp_AddAmtrak", parameters);
							if (r.ResultType != Common.ResultType.Success)
							{
								Response.Write(r.Exception.Message);
								Response.Flush();
							}
							counter++;
						}
					} catch {
						;	
					}
				}
				
				Response.Write(counter + " amtrak numbers added.");
			}
		}
	}
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload Tracking</title>
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 0px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
</style>
</head>

<body style="padding: 5px;">
<form action="" method="post" enctype="multipart/form-data" runat="server" style="padding: 5px;">
	Select a file to upload:<br />
	<input id="loFile" name="loFile" type="file" runat="server" style="margin: 5px;">
	<br>
    <input type="hidden" value="upload" name="action" id="action" />
<input type="submit" value="Upload Amtrak Numbers" style="margin: 5px;" />
</form>
</body>
</html>
