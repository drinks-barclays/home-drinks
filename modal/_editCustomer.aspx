﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

   	Customer c = new Customer();
	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "do_update") {
			action.Value = "update";
		} else if (Request["action"] == "do_add") {
			action.Value = "add";
		} else if (Request["action"] == "update") {
			c.UpdateEmail(Request["customer_id"], Request["email"]);
			Response.Redirect("close_modal.html");
		}
	}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Billing Information</title>	
<script type="text/javascript" src="/includes/js/jquery.js"></script> 
<script src="/includes/js/jquery.validate.js"></script>
<script>
	$(document).ready(function(){
		$("#oForm").validate();
	});
</script>
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 0px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	
	.field {
		width: 325px;
		clear: both;
		padding: 3px;
	}
	
	.label {
		width: 125px;
		float: left;
		padding: 3px;
	}
	
	.input {
		width: 175px;
		float: left;
	}
</style>
</head>

<body>
<form action="_editCustomer.aspx" method="post" id="oForm">
<div class="field">
	<div class="label">Email: </div>
	<div class="input"><input value="<%= Request["email"] %>" name="email" id="email" type="text" size="30" class="required" title="Email is required." /></div>
</div>
<div class="field">
	<div class="label"> </div><div class="input"><input name="" type="submit" value="Submit" />
	  <input type="hidden" name="action" id="action" value="" runat="server">
      <input type="hidden" name="customer_id" id="customer_id" value="<%= Request["customer_id"] %>">
	</div>
</div>
</form>
</body>
</html>