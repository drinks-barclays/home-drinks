﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Continuity.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Continuity c = new Continuity();
	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "add") {
			c.AddShipment(Request["id"], Request["product_id"], Request["preference"], Request["startDate"], Request["endDate"], Request["spanBegin"], Request["spanEnd"]);	
			Response.Redirect("close_modal.html");
		}
		
		DataTable dt;
		c.GetProductForShipment(out dt);
		
		product.InnerHtml += "<select id='product_id' name='product_id' class='validate-not-first'><option></option>";
		foreach(DataRow r in dt.Rows) {
			product.InnerHtml += "<option value='" + r["ProductID"] + "'>" + r["ProductID"] + " - " + r["Name"].ToString().Trim() + "</option>";
		}
		product.InnerHtml += "</select>";
	}

</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Billing Information</title>	
<script language="javascript" src="/includes/yui/build/yahoo/yahoo-min.js" type="text/javascript"></script> 
<script language="javascript" src="/includes/yui/build/event/event-min.js" type="text/javascript"></script> 
<script type="text/javascript" src="/includes/js/jsvalidate.js"></script>
<script type="text/javascript" src="/includes/mootools.js"></script>
<script type="text/javascript" src="/includes/calendar.js"></script>
<script type="text/javascript">
    window.addEvent('domready', function() { myCal = new Calendar({ startDate: 'm/d/Y' }); });
	window.addEvent('domready', function() { myCal2 = new Calendar({ endDate: 'm/d/Y' }); });
</script>
<link rel="stylesheet" type="text/css" href="/includes/calendar.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 0px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	
	.field {
		width: 325px;
		clear: both;
		padding: 3px;
	}
	
	.label {
		width: 125px;
		float: left;
		padding: 3px;
	}
	
	.input {
		width: 175px;
		float: left;
	}
</style>
</head>

<body>
<form action="" method="post">
<div class="field">
	<div class="label">Preference: </div><div class="input"><select name="preference" class="validate-not-first">
      <option> </option>
	  <option value="1">Red</option>
	  <option value="2">White</option>
	  <option value="3">Mixed</option>
	</select></div>
</div>
<div class="field">
  <div class="label">Product: </div><div class="input">
    <span id="product" runat="server"></span>
  </div>
</div>
<div class="field">
  <div class="label">Start Date: </div><div class="input">
    <input name="startDate" id="startDate" type="text" size="20" maxlength="20" class="required" title="Start date is required." runat="server" />
  </div>
</div>
<div class="field">
  <div class="label">End Date: </div><div class="input">
    <input name="endDate" id="endDate" type="text" size="20" maxlength="20" class="required" title="End date is required." runat="server" />
  </div>
</div><br />

<div class="field" style="margin: 5px;">
  For shipments between  
  <select name="spanBegin" class="validate-not-first">
      <option> </option>
	  <%
	  	for (int i = 1; i < 100; i++) {
		%>
			<option value="<%= i %>"><%= i %></option>
		<%	
		}
	  %>
	</select>
    &nbsp;and 
    <select name="spanEnd" class="validate-not-first">
      <option> </option>
	  <%
	  	for (int i = 1; i < 100; i++) {
		%>
			<option value="<%= i %>"><%= i %></option>
		<%	
		}
	  %>
	</select>
    .
</div>
<input style="margin: 5px;" name="" type="submit" value="Add Shipment" />
<input type="hidden" name="action" id="action" value="add" />
</form>
</body>
</html>
