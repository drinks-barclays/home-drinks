﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.IO" %>
<script language="C#" runat="server">
	
	Order o = new Order();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		o.SendTrackingMails();
	}
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload Tracking</title>
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 0px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
</style>
</head>

<body style="padding: 5px;">
<form action="" method="post" enctype="multipart/form-data" runat="server" style="padding: 5px;">
	Select a file to upload:<br />
	<input id="loFile" name="loFile" type="file" runat="server" style="margin: 5px;">
	<br>
    <input type="hidden" value="upload" name="action" id="action" />
<input type="submit" value="Upload Tracking Numbers" style="margin: 5px;" />
</form>
</body>
</html>
