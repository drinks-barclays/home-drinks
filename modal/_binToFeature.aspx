﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Product.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Globalization" %>
<script language="C#" runat="server">
	
	Product product = new Product();
	public string output;
	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "add") {
			product.AddBinFeature(Request["id"], Request["featureDate"]);	
			Response.Redirect("close_modal.html");
		}
		
		DataTable dt = product.GetBinProductToFeature();
		
		foreach (DataRow r in dt.Rows) {
			output += "<tr><td>" + r["ProductID"] + "</td><td><a href='_binToFeature.aspx?action=add&featureDate=" + Request["featureDate"] + "&id=" + r["BinProductID"] + "'>"  + r["Vintage"] + " " + r["Name"] + "</a></td></tr>";
		}
	}
	
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bin Products To Feature</title>
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 20px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 14px;
	}
	
	.header {
		font-size: 20px;
		font-weight: bold;	
		border-bottom: 1px solid;
	}
	
	table {
		font-family: Arial, Helvetica, sans-serif;
		margin: 10px;
		margin-bottom: 40px;
	}
	
	table th {
		margin: 7px;	
		font-size: 16px;
		font-weight: bold;
		border-bottom: 1px solid;
		border-right: 1px solid;
	}
	table td {
		margin: 5px;
		font-size: 14px;	
		border-bottom: 1px solid;
		border-right: 1px solid;
	}
	
	.descriptor {
		margin: 7px;
	}
	
	.subscriber {
		color: #060	;
	}
	
	.unsubscribe {
		color: #900;	
	}
	
	.bounce {
		color: #F60;
	}
	
	.revenue {
		color: #060	;
	}
	
	.credit {
		color: #900;	
	}
	
	.campaign {
		color: #666;	
	}
	
	#dateRange {
		font-size: 15px;
		margin-bottom: 20px;
		border-bottom: 1px dashed #999;
		padding: 3px;	
	}
</style>
</head>

<body style="padding: 5px;"> 
<table border="0" cellpadding="5" cellspacing="0">
  <tr align="center" style="border: 1px solid;">
    	<th>Product ID</th>
    	<th>Product</th>
    </tr>
    	<%= output %>
</table>


</body>
</html>
