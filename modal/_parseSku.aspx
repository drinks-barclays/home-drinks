﻿<%@  Page Language="C#" Debug="true" ClientIdMode="static" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	string path = "c:\\inetpub\\net\\";
	string search;
   	public void Page_Load(Object sender, EventArgs e)
 	{
		search = ".val(" + Request["id"];
        if(Directory.Exists(path)) 
        {
            ProcessDirectory(path);
        }
        else 
        {
            //
        }        
                
    }


    // Process all files in the directory passed in, recurse on any directories  
    // that are found, and process the files they contain. 
    public void ProcessDirectory(string targetDirectory) 
    {
        // Process the list of files found in the directory. 
        string [] fileEntries = Directory.GetFiles(targetDirectory);
        foreach(string fileName in fileEntries) {
            if (fileName.IndexOf(".aspx") > -1 && targetDirectory.IndexOf("secure") == -1) ProcessFile(fileName);
		}

        // Recurse into subdirectories of this directory. 
        string [] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
        foreach(string subdirectory in subdirectoryEntries)
            ProcessDirectory(subdirectory);
    }

    // Insert logic for processing found files here. 
    public void ProcessFile(string p) 
    {
        //Console.WriteLine("Processed file '{0}'.", path);
		string s = File.ReadAllText(p);   
		if (s.IndexOf(search) > -1) Response.Write("- " + p.Replace(path, "") + "<br />");
		s = null;
    }
	
</script>