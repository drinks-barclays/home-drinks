﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/ShoppingCart.cs" %>
<%@ Assembly src="/bin/Continuity.cs" %>
<%@ Assembly src="/bin/LitlePayment.cs" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Assembly src="/bin/Utilities.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.IO" %>
<script language="C#" runat="server">

	Util u = new Util();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		//if (Session["operator"] == null) Response.Redirect("/security.aspx?r=" + Server.UrlEncode(Request.Url.ToString()));
		if (Request["action"] == "close") {
			DoClose();	
		}
		
	}
	
	public void DoClose() {
		Continuity c = new Continuity();
		DataTable dt;
		Payment_ p = new Payment_();
		Order order = new Order();
		bool goodPayment;
		ShoppingCart cart = new ShoppingCart();
		string cartID = cart.GetShoppingCartId();
		
		// roll through pre-paids
		try {
			c.GetEligibleToClosePrePaid(out dt);
			
			foreach(DataRow row in dt.Rows) {
				// add order
				cart.EmptyCart(cartID);
				cart.AddItem(cartID, Convert.ToInt32(row["ProductID"].ToString()), 1);
				order = new Order();
				order.Add(row["CustomerBillingID"].ToString(), row["CustomerAddressID"].ToString(), "3655", cartID, 
									"0", "0", "0", 
									"0", "0", Request["reward_number"], row["BrandID"].ToString());
						
				// incremement shipments in their subscription
				c.IncrementShipments(row["ContinuitySubscriptionID"].ToString());
				c.IncrementPrePaid(row["ContinuitySubscriptionID"].ToString());
			}
		} catch (Exception ex) {
			;	
		}
		dt = null;
		
		// grab eligible
		if (Request["id"] != null && Request["id"].ToString() != "") {
			c.GetEligibleToClose(out dt, Request["id"]); 
		} else {
			c.GetEligibleToClose(out dt); 
		}

		// parse through and charge
		if (dt.Rows.Count > 0) {
			main.Visible = false;
			foreach(DataRow row in dt.Rows) {
				// try to charge
				p = new Payment_(); 
				
				//Barclays: 6105109, Heartwood: 6106204, Troon: 6105112
				switch  (row["BrandID"].ToString()) {
					case "2":
						// troon
						p.MID = "6105112";
						p.ReportGroup = "TroonContinuity";
						break;
					case "7":
						// H&O
						p.MID = "6106204";
						p.ReportGroup = "HeartwoodContinuity";
						break;
					case "8":
						// Franklin Mint
						p.MID = "7132993";
						p.ReportGroup = "FranklinMintContinuity";
						break;
					default:
						p.MID = "6105109";
						p.ReportGroup = "BarclaysContinuity";
						break;
				}
				
				double tax = Convert.ToDouble(cart.GetTax(row["State"].ToString(), Convert.ToDouble(row["Total"].ToString())));
				double total = Convert.ToDouble(row["Total"].ToString()) + tax;
				
				goodPayment = p.DoPayment(row["Digits"].ToString().Trim(), "",
											(row["Month"].ToString().Trim().PadLeft(2, '0') + row["Year"].ToString().Trim().Substring(2,2)), 
											total.ToString("F"), row["BillingFirstName"].ToString().Trim(), 
											row["BillingLastName"].ToString().Trim(), row["BillingPostalCode"].ToString().Trim(), 
											"", 1);
				
				if (goodPayment) {
					// add order
					cart.EmptyCart(cartID);
					cart.AddItem(cartID, Convert.ToInt32(row["ProductID"].ToString()), 1);
					order = new Order();
					order.Add(row["CustomerBillingID"].ToString(), row["CustomerAddressID"].ToString(), "3655", cartID, 
										row["Shipping"].ToString(), tax.ToString("F"), total.ToString("F"), 
										"0", p.TransactionCode, Request["reward_number"], row["BrandID"].ToString());
										
					// if there's a new token, update it
					
					// incremement shipments in their subscription
					c.IncrementShipments(row["ContinuitySubscriptionID"].ToString());
				} else {
					// bad
					Response.Write(row["CustomerID"] + " - " + p.debug + "<br />");string CSPhone = "";
				string brandName = "";
				string body;
				string fromEmail;
				string fromName;
				
				// increment counters
				switch  (row["BrandID"].ToString()) {
					case "2":
						// troon
						//troonDecline += 1;
						fromEmail = "cs@troonwineclub.com";
						fromName = "Wine Concierge";
						brandName = "Troon Wine Club";
						CSPhone = "877-998-7666";
						break;
					case "7":
						// H&O
						//heartwoodDecline += 1;
						fromEmail = "cs@heartwoodandoak.com";
						fromName = "Wine Concierge";
						brandName = "Heartwood & Oak";
						CSPhone = "888-661-1246";
						break;
					default:
						//barclaysDecline += 1;
						fromEmail = "cs@barclayswine.com";
						fromName = "Wine Concierge";
						brandName = "Barclays Wine";
						CSPhone = "888-380-2337";
						break;
				}
				
				body = File.ReadAllText("c:\\inetpub\\net\\www.bevbistro.com\\secure\\common\\decline\\default.html");
				body = body.Replace("*|CS_PHONE|*", CSPhone);
				body = body.Replace("*|CS_EMAIL|*", fromEmail);
				body = body.Replace("*|FIRST_NAME|*", row["BillingFirstName"].ToString().Trim());
				body = body.Replace("*|BRAND|*", brandName);
				body = body.Replace("*|CLUB_NAME|*", row["ClubName"].ToString().Trim());
				body = body.Replace("*|CUSTOMER_ID|*", row["CustomerID"].ToString().Trim());
			
				
				// send decline email
				u.SendMailServer(row["BillingFirstName"].ToString().Trim() + " " + row["BillingLastName"].ToString().Trim(), row["Email"].ToString(), "Oh no! We couldn't process your order. Please call us ASAP.", body, fromEmail, fromName);
				}
				
			}	
		} else {
				// bad
		}
		
		/*string adminSubject = "Continuity Charge Summary";
		string adminBody = "Barclays: " + barclays + " charges for " + barclaysRev.ToString("c") + " revenue and " + barclaysDecline + " declines<br />";
		adminBody += "H&O: " + heartwood + " charges for " + heartwoodRev.ToString("c") + " revenue and " + heartwoodDecline + " declines<br />";
		adminBody += "Troon: " + troon + " charges for " + troonRev.ToString("c") + " revenue and " + troonDecline + " declines<br />";
		
			MailMessage email = new MailMessage(
				   "asn+robot@barclayswine.com",
				   "drinks-devops@drinks.com, josiah@drinks.com, zac@drinks.com",
				   adminSubject,
				   adminBody);
			email.IsBodyHtml = true;
				
			NetworkCredential creds = new NetworkCredential("auth@voidme.com", "$m@ng0"); 
			SmtpClient client = new SmtpClient("127.0.0.1");
			client.UseDefaultCredentials = false;
			client.Credentials = creds;
			try {
				client.Send(email);
			} catch (Exception ex) {
				;	
			}	*/
	}

</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Close Continuity</title>	
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 0px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	
	.field {
		width: 325px;
		clear: both;
		padding: 3px;
	}
	
	.label {
		width: 125px;
		float: left;
		padding: 3px;
	}
	
	.input {
		width: 175px;
		float: left;
	}
</style>
<script src="/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script>
	function hideMe() {
		$("#main").html("Processing, please wait...");
		$("#main").load("_continuityClose.aspx?action=close&id=<%= Request["id"] %>");
	}
</script>
</head>

<body>
<div id="main" runat="server">
Are you sure you want to close? <br>
<a href="javascript: void()" onClick="hideMe();">Yes</a> &nbsp; or&nbsp; <a href="javascript: parent.$.fancybox.close();">No</a>
</div>
</body>
</html>
