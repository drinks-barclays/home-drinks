﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

   	Customer c = new Customer();
	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "do_update") {
			action.Value = "update";
		} else if (Request["action"] == "update") {
			c.EditBilling(Request["id"], Request["first_name"], Request["last_name"],
					   	Request["postal_code"], Request["ccnum"], Request["cc_mo"], Request["cc_yr"]);
			Response.Redirect("close_modal.html");
		}
		
		if (Request["id"] != null && Request["id"] != "") {
			// update
			DataTable dt = c.GetBilling(Request["id"]);
			first_name.Value = dt.Rows[0]["FirstName"].ToString().Trim();
			last_name.Value = dt.Rows[0]["LastName"].ToString().Trim();
			postal_code.Value = dt.Rows[0]["PostalCode"].ToString().Trim();
			ccnum.Value = dt.Rows[0]["Digits"].ToString().Trim();
			ccExpiration.InnerHtml = GetExpiration(dt.Rows[0]["Month"].ToString().Trim(), dt.Rows[0]["Year"].ToString().Trim());
		} else {
			ccExpiration.InnerHtml = GetExpiration("", "");	
		}
		
		
	}
		
	
	private string MaskDigits(string x) {
		string output = "";
		string firstDigit = x.Substring(0, 1);
		string lastFour = x.Substring(x.Length - 5, 4);
		
		output = firstDigit;
		for (int i = 0; i <= x.Length - 5; i++) {
			output += "*";	
		}
		
		output += lastFour;
		
		return output;
	}
	
	private string GetExpiration(string month, string year) {
		string output = "<select id='cc_mo' name='cc_mo' class='validate-not-first' title='Expiration month is required.'><option>Month</option>";
		for (int i = 1; i < 13; i++) {
			output += "<option value='" + i + "'";
			if (i.ToString() == month.Trim()) output += " selected ";
			output += ">" + i + "</option>";	
		}
		output += "</select>";
		
		output += " <select id='cc_yr' name='cc_yr' class='validate-not-first' title='Expiration year is required.'><option>Year</option>";
		for (int i = System.DateTime.Now.Year; i < (System.DateTime.Now.Year + 15); i++) {
			
			output += "<option value='" + i + "'";
			if (i.ToString() == year.Trim()) output += " selected ";
			output += ">" + i + "</option>";	
		}
		output += "</select>";
		
		return output;
		
	}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Billing Information</title>	
<script language="javascript" src="/includes/yui/build/yahoo/yahoo-min.js" type="text/javascript"></script> 
<script language="javascript" src="/includes/yui/build/event/event-min.js" type="text/javascript"></script> 
<script type="text/javascript" src="/includes/js/jsvalidate.js"></script>
<script language="javascript" type="text/javascript">
	function clearCard(x) {
		if (x.value.indexOf("*") > -1) x.value = '';	
	}
	
	function validateCard(x) {
		x.value = x.value.replace(/[^0-9]+/g, '');
		if (x.value.indexOf("*") == -1) x.className = 'required validate-cc';
	}
</script>
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 0px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	
	.field {
		width: 325px;
		clear: both;
		padding: 3px;
	}
	
	.label {
		width: 125px;
		float: left;
		padding: 3px;
	}
	
	.input {
		width: 175px;
		float: left;
	}
</style>
</head>

<body>
<form action="_billing.aspx" method="post">
<div class="field">
  <div class="label">First Name: </div><div class="input">
    <input name="first_name" id="first_name" type="text" size="20" maxlength="20" class="required" title="First name is required." runat="server" />
  </div>
</div>
<div class="field">
	<div class="label">Last Name: </div><div class="input"><input name="last_name" id="last_name" type="text" size="20" maxlength="20" class="required" title="Last name is required." runat="server" /></div>
</div>
<div class="field">
	<div class="label">Postal Code: </div><div class="input"><input name="postal_code" id="postal_code" type="text" size="6" maxlength="5"  class="required validate-number" title="Postal Code is required." runat="server" /></div>
</div>
<div class="field">
	<div class="label">Credit Card #: </div><div class="input"><input name="ccnum" id="ccnum" onClick="clearCard(this);" type="text" title="Credit card is required." onBlur="validateCard(this) " runat="server" /></div>
</div>
<div class="field">
	<div class="label">Credit Card Exp: </div><div class="input" id="ccExpiration" runat="server"></div>
</div>
<div class="field">
	<div class="label"> </div><div class="input" style="margin-top:25px;"><input name="" type="submit" value="Submit" />
	  <input type="hidden" name="action" id="action" value="" runat="server">
      <input type="hidden" name="id" id="id" value="<%= Request["id"] %>" />
	</div>
</div>
</form>
</body>
</html>
