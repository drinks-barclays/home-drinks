﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

   	Customer c = new Customer();
	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "do_update") {
			action.Value = "update";
		} else if (Request["action"] == "update") {
			c.EditShipping(Request["id"], Request["first_name"], Request["last_name"], Request["address"], Request["address2"],
						Request["city"], Request["state"], Request["postal_code"], Request["phone"]);
			Response.Redirect("close_modal.html");
		}
		
		if (Request["id"] != null && Request["id"] != "") {
			// update
			DataTable dt = c.GetShipping(Request["id"]);
			first_name.Value = dt.Rows[0]["FirstName"].ToString().Trim();
			last_name.Value = dt.Rows[0]["LastName"].ToString().Trim();
			address.Value = dt.Rows[0]["Address"].ToString().Trim();
			address2.Value = dt.Rows[0]["Address2"].ToString().Trim();
			city.Value = dt.Rows[0]["City"].ToString().Trim();
			postal_code.Value = dt.Rows[0]["PostalCode"].ToString().Trim();
			phone.Value = dt.Rows[0]["PhoneNumber"].ToString().Trim();
			state.Value = dt.Rows[0]["State"].ToString().Trim();
		}
		
		
	}

</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Billing Information</title>	
<script language="javascript" src="/includes/yui/build/yahoo/yahoo-min.js" type="text/javascript"></script> 
<script language="javascript" src="/includes/yui/build/event/event-min.js" type="text/javascript"></script> 
<script type="text/javascript" src="/includes/js/jsvalidate.js"></script>
<script language="javascript" type="text/javascript">
	function clearCard(x) {
		if (x.value.indexOf("*") > -1) x.value = '';	
	}
	
	function validateCard(x) {
		x.value = x.value.replace(/[^0-9]+/g, '');
		if (x.value.indexOf("*") == -1) x.className = 'required validate-cc';
	}
</script>
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 0px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	
	.field {
		width: 325px;
		clear: both;
		padding: 3px;
	}
	
	.label {
		width: 125px;
		float: left;
		padding: 3px;
	}
	
	.input {
		width: 175px;
		float: left;
	}
</style>
</head>

<body>
<form action="_shipping.aspx" method="post">
<div class="field">
  <div class="label">First Name: </div><div class="input">
    <input name="first_name" id="first_name" type="text" size="20" maxlength="20" class="required" title="First name is required." runat="server" />
  </div>
</div>
<div class="field">
	<div class="label">Last Name: </div><div class="input"><input name="last_name" id="last_name" type="text" size="20" maxlength="20" class="required" title="Last name is required." runat="server" /></div>
</div>
<div class="field">
	<div class="label">Address: </div><div class="input"><input name="address" id="address" type="text" maxlength="40"  class="required" title="Address is required." runat="server" /></div>
</div>
<div class="field">
	<div class="label">&nbsp;</div><div class="input"><input name="address2" id="address2" type="text" maxlength="40"  runat="server" /></div>
</div>
<div class="field">
	<div class="label">City: </div><div class="input"><input name="city" id="city" type="text" maxlength="40"  class="required" title="City is required." runat="server" /></div>
</div>
<div class="field">
	<div class="label">State: </div><div class="input"><select name='state' id='state' title="State is required." class="validate-not-first" runat="server" ><option></option><option value='AK'>Alaska</option><option value='AL'>Alabama</option><option value='AR'>Arkansas</option><option value='AZ'>Arizona</option><option value='CA'>California</option><option value='CO'>Colorado</option><option value='CT'>Connecticut</option><option value='DC'>District of Columbia</option><option value='DE'>Delaware</option><option value='FL'>Florida</option><option value='GA'>Georgia</option><option value='HI'>Hawaii</option><option value='IA'>Iowa</option><option value='ID'>Idaho</option><option value='IL'>Illinois</option><option value='IN'>Indiana</option><option value='KS'>Kansas</option><option value='KY'>Kentucky</option><option value='LA'>Louisiana</option><option value='MA'>Massachusetts</option><option value='MD'>Maryland</option><option value='ME'>Maine</option><option value='MI'>Michigan</option><option value='MN'>Minnesota</option><option value='MO'>Missouri</option><option value='MS'>Mississippi</option><option value='MT'>Montana</option><option value='NC'>North Carolina</option><option value='ND'>North Dakota</option><option value='NE'>Nebraska</option><option value='NH'>New Hampshire</option><option value='NJ'>New Jersey</option><option value='NM'>New Mexico</option><option value='NV'>Nevada</option><option value='NY'>New York</option><option value='OH'>Ohio</option><option value='OK'>Oklahoma</option><option value='OR'>Oregon</option><option value='PA'>Pennsylvania</option><option value='RI'>Rhode Island</option><option value='SC'>South Carolina</option><option value='SD'>South Dakota</option><option value='TN'>Tennesee</option><option value='TX'>Texas</option><option value='UT'>Utah</option><option value='VA'>Virginia</option><option value='VT'>Vermont</option><option value='WA'>Washington</option><option value='WI'>Wisconsin</option><option value='WV'>West Virginia</option><option value='WY'>Wyoming</option></select></div>
</div>
<div class="field">
	<div class="label">Postal Code: </div><div class="input"><input name="postal_code" id="postal_code" type="text" size="6" maxlength="5"  class="required validate-number" title="Postal Code is required." runat="server" /></div>
</div>
<div class="field">
	<div class="label">Phone Number: </div><div class="input"><input name="phone" id="phone" type="text" size="12" maxlength="10"  class="required validate-number" title="Phone number is required." runat="server" /></div>
</div>
<div class="field">
	<div class="label"> </div><div class="input"><br /><input name="" type="submit" value="Submit" />
	  <input type="hidden" name="action" id="action" value="" runat="server">
      <input type="hidden" name="id" id="id" value="<%= Request["id"] %>">
	</div>
</div>
</form>
</body>
</html>
