﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.IO" %>
<script language="C#" runat="server">
	
	Order o = new Order();
	StringBuilder sb = new StringBuilder();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "upload") {
			int counter = 0;
			using (StreamReader sr = new StreamReader(file.PostedFile.InputStream)) 
			{
				string line;
				string[] splitted;
				char[] splitter = { ',' };
						
				while ((line = sr.ReadLine()) != null) 
				{					
					splitted = line.Split(splitter);
					try {
						if (splitted[0] != null && splitted[1] != null) {
							if (counter != 0) sb.Append(Environment.NewLine);
							sb.Append(splitted[0].ToString() + "," + o.ReversePackLookup(splitted[0].ToString(), splitted[1].ToString()));
							counter++;
						}
					} catch {
						;	
					}
				}
				
				string attachment = "attachment; filename=output.csv";
				HttpContext.Current.Response.Clear();
				HttpContext.Current.Response.ClearHeaders();
				HttpContext.Current.Response.ClearContent();
				HttpContext.Current.Response.AddHeader("content-disposition", attachment);
				HttpContext.Current.Response.ContentType = "text/csv";
				HttpContext.Current.Response.AddHeader("Pragma", "public");
				
				Response.Write(sb.ToString());
				Response.End();
			}
		}
	}
		
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sku Lookup Utility</title>
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 0px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
</style>
</head>

<body style="padding: 5px;">
<p style="margin-bottom: 20px; font-weight: bold">
Input File format (CSV): InvoiceID, OutOfStockSku<br />
Example: 406126,M05803</p>

<p style="margin-bottom: 20px; font-weight: bold">
Output File format (CSV): InvoiceID, OldPackID<br />
Example: 406126,123456</p>
<form action="" method="post" enctype="multipart/form-data" runat="server" style="padding: 5px;">
	Select a file to upload:<br />
	<input id="file" name="file" type="file" runat="server" style="margin: 5px;">
	<br>
    <input type="hidden" value="upload" name="action" id="action" />
<input type="submit" value="Upload Orders" style="margin: 5px;" />
</form>
</body>
</html>
