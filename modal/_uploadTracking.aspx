﻿<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Assembly src="/bin/Utilities.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.IO" %>
<script language="C#" runat="server">
	
	Order o = new Order();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "upload") {
			int counter = 0;
			using (StreamReader sr = new StreamReader(file.PostedFile.InputStream)) 
			{
				string line;
				string[] splitted;
				char[] splitter = { ',' };
						
				while ((line = sr.ReadLine()) != null) 
				{					
					splitted = line.Split(splitter);
					try {
						if (splitted[0] != null && splitted[1] != null) {
							string orderID = splitted[0].ToString();
							if (orderID.IndexOf("-") > 0) orderID = orderID.Substring(0, orderID.IndexOf("-"));
							Response.Write(orderID + "<br />");
							o.AddTracking(orderID, splitted[1].ToString().Replace(" ", "").ToUpper());
							// if (splitted[1].ToString().Replace(" ", "").ToUpper() == "BAD") SendNotification(splitted[0].ToString());
							counter++; 
						}
					} catch {
						;	
					}
				}
				
				Response.Write(counter + " tracking numbers added."); 
			}
		}
	}
	
	private void SendNotification(string orderID) {
		Util u = new Util();
		u.SendMailServer("Customer Service", "customerservice@barclayswine.com", "Order " + orderID + " did not ship!", "There is something wrong with this order that caused it to not ship from Doretti, please fix this order ASAP. - Shipping Robot");	
	}
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload Tracking</title>
<link rel="stylesheet" type="text/css" href="../style.css" media="screen" />
<style>
	body {
		margin: 0px;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
</style>
</head>

<body style="padding: 5px;">
<form action="" method="post" enctype="multipart/form-data" runat="server" style="padding: 5px;">
	<p>Format: OrderID, TrackingNumber (CSV) </p>
	Select a file to upload:<br />
	<input id="file" name="file" type="file" runat="server" style="margin: 5px;">
	<br>
    <input type="hidden" value="upload" name="action" id="action" />
<input type="submit" value="Upload Tracking Numbers" style="margin: 5px;" />
</form>
</body>
</html>
