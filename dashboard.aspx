<%@  Page Language="C#" Debug="true" ClientIdMode="static" %>
<%@ Register TagPrefix="BB" TagName="Nav" Src="_navMain.ascx" %>
<%@ Assembly src="/bin/Report.cs" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Assembly src="/bin/Marketing.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Report report = new Report();
	Order o = new Order();
	Marketing m = new Marketing();
	DataTable dt;
	string startDate = System.DateTime.Now.Date.ToString("d");
	string endDate = System.DateTime.Now.Date.AddDays(1).ToString("d");
	string chartData = "";
		int orderTotal = 0;
		double revenueTotal = 0;
		double creditTotal = 0;
		double billedTotal = 0;
	
   	public void Page_Load(Object sender, EventArgs e)
 	{
		DisplayOrderReport();
	}
		
	private void DisplayOrderReport() {
		report.GetOrderReport(out dt, Request["Campaign_ID"], startDate, endDate);
		dt.DefaultView.Sort = "[Revenue] DESC";
		int inc = 0;
		foreach(DataRow r in dt.Select("", "Revenue desc")) {
			if (inc < 10) {
				if (orderTotal != 0) chartData += "\r\n,";
          		chartData += "['" + r["PartnerName"].ToString().Trim().Replace("'", "") + " " + r["Description"].ToString().Trim().Replace("'", "") + "'," + Convert.ToDouble(r["Revenue"]) + ", " + Convert.ToInt32(r["Orders"]) + "]";
			}
			orderTotal += Convert.ToInt32(r["Orders"]);
			billedTotal += Convert.ToDouble(r["Billed"]);
			creditTotal += Convert.ToDouble(r["Credits"]);
			revenueTotal += Convert.ToDouble(r["Revenue"]);
			inc++;
		}
	}
</script>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Dashboard</title>

<script src="//code.jquery.com/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/pepper-grinder/jquery-ui.min.css" media="screen" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


	<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['bar']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Campaign');
        data.addColumn('number', 'Revenue');
        data.addColumn('number', 'Orders');
        data.addRows([<%= chartData %>]);

        // Set chart options
        var options = {chart: {
						title: 'Top 10 Campaigns'
					  },
                       'width':'100%',
                       'height':500,
					   bars: 'horizontal',
          			   legend: {position: 'top', maxLines: 10},
					   series: {
						0: { axis: 'Revenue' }, // Bind series 0 to an axis named 'distance'.
						1: { axis: 'Orders' } // Bind series 1 to an axis named 'brightness'.
					  },
					  axes: {
						x: {
						  revenue: {label: 'today revenue'}, // Bottom x-axis.
						  orders: {side: 'top', label: 'total orders'} // Top x-axis.
						}
					  }
					   };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.charts.Bar(document.getElementById('revenue_chart'));
        chart.draw(data, options);
      }
    </script>
<script type="text/javascript">
	
	$(document).ready(function(){
		
  	});
	
  </script>

<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
<style>
	
	body {
		margin: 0 auto;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		padding: 0px;
	}
	
	#nav {
		margin: 0 auto;
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
		background-color: #efefef;
		min-width: 400px;
	}
	
	#workingArea {
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
	}
	
	th {
		text-align: left;
		font-weight: bold;
		cursor: s-resize;	
		background-color: #0CF;
		color: white;
	}
	
	th,td {
		padding: 5px;	
		border: 1px solid #efefef;
	}
	
	.summary {
		background-color: #efefef;
		border: 1px solid #ccc;
		box-shadow: #333 1px;	
	}
	
</style>
</head>

<body> 
<BB:Nav runat="server" id="nav_ctl" navId=1 />
    <div id="bodyWrapper">
        <div id="nav">
            <div style="font-size: 20px; margin-bottom: 10px;">Dashboard</div> 
        </div>
        
        <div id="workingArea" class="container">
        	<div class="row">
				<div class="col-sm-4 summary">
					<h4>Revenue</h4>
                    <h1><%= revenueTotal.ToString("c") %></h1>
				</div>
				<div class="col-sm-4 summary">
					<h4>Orders</h4>
                    <h1><%= orderTotal %></h1>
				</div>
				<div class="col-sm-4 summary">
					<h4>Profit</h4>
                    <h1>$$$$$</h1>
				</div>
			</div>
        	<div class="row">
            	<div class="col-sm-12" id="revenue_chart" />
            </div>
        </div>
    </div>
</body>
</html>
 