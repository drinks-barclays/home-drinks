<%@  Page Language="C#" Debug="true" ClientIdMode="static" %>
<%@ Register TagPrefix="BB" TagName="Nav" Src="_navMain.ascx" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	DataTable dt = new DataTable();
	Order o = new Order();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["action"] == "search") {
			resultSet.Visible = true;
			Hashtable parameters = new Hashtable();
			parameters.Add(Request["by"], Request["k"]);
			dt = o.OrderList(parameters);
			headline.InnerText = "Search Results";
			Display();
		} else {
			// show open orders
			resultSet.Visible = true;
			Hashtable parameters = new Hashtable();
			parameters.Add("StatusID", 1);
			dt = o.OrderList(parameters);
			//ds.Tables.Add(o.GetShipmentSummary(1));
			headline.InnerText = "Open Orders";			
			Display();
		}
	
		
		o = null;
	}
	
	private void Display() {
			if (dt != null && dt.Rows.Count > 0) {
				resultSet.InnerHtml += "<table id='myTable' class='tablesorter'><thead><tr><th>Ship To</th><th>Bill To</th><th>Total</th><th>Date</th><th colspan=3>Status</th></tr></thead><tbody>";
				foreach (DataRow r in dt.Rows) {
					resultSet.InnerHtml += "<tr id='" + r["InvoiceID"] + "'>";
					resultSet.InnerHtml += "<td>" + r["ShippingFirstName"] + " " + r["ShippingLastName"] + "</td>";
					resultSet.InnerHtml += "<td>" + r["BillingFirstName"] + " " + r["BillingLastName"] + "</td>";
					resultSet.InnerHtml += "<td>" + Convert.ToDouble(r["Total"]).ToString("c") + "</td>";
					resultSet.InnerHtml += "<td>" + r["DateAdded"] + "</td>";
					resultSet.InnerHtml += "<td>" + r["InvoiceStatus"] + "</td>";
					resultSet.InnerHtml += "<td><a href='_order.aspx?id=" + r["InvoiceID"] + "&placeValuesBeforeTB_=savedValues&TB_iframe=true&height=400&width=700&modal=true' class='modal'>View Order</a></td>";
					resultSet.InnerHtml += "<td><a href='customer.aspx?id=" + r["CustomerID"] + "'>View Account</a></td>";
					resultSet.InnerHtml += "</tr>";
				}
				resultSet.InnerHtml += "</tbody></table>";
			} else {
				resultSet.InnerHtml += "<div class='err'>No orders to display.</div>";
			}		
	}
</script>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Orders</title>

<script src="//code.jquery.com/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/pepper-grinder/jquery-ui.min.css" media="screen" />
<script src="/js/jquery.tablesorter.min.js"></script>

<script type="text/javascript" src="/js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="/js/jquery.fancybox.pack.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="/js/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript">
	
	$(document).ready(function(){
		$("#myTable").tablesorter(); 	
		$(".modal").fancybox({'autoSize':true, 'type' : 'iframe'});	
  	});
	
  </script>
<script language="javascript">
	function validateSearch() {
		var x = document.getElementById('k').value;
		var xx = document.getElementById('by').value;
		if (x.length < 1) {
			alert('Please enter a search value.');
			document.getElementById('k').focus();
			return false;
		} else if (xx.toLowerCase().indexOf('id') > -1) {
			var id = parseInt((x)*(-1)*(-1));
			if (isNaN(id)) {
				alert('Please enter a numeric value to search this field.');
				return false;	
			}
		}
		return true;
	}
</script>

<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
<style>
	
	body {
		margin: 0 auto;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		padding: 0px;
	}
	
	#nav {
		margin: 0 auto;
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
		background-color: #efefef;
		min-width: 400px;
	}
	
	#workingArea {
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
	}
	
	th {
		text-align: left;
		font-weight: bold;
		cursor: s-resize;	
		background-color: #0CF;
		color: white;
	}
	
	th,td {
		padding: 5px;	
		border: 1px solid #efefef;
	}
	
</style>
</head>

<body> 
    <BB:Nav runat="server" id="nav_ctl" navId=3 />
    <div id="bodyWrapper">
        <div id="nav">
            <div style="font-size: 20px; margin-bottom: 10px;">Orders</div> 
            <div>
            
            	<form action="" method="post" onSubmit="return validateSearch()">
                        <strong>Search:</strong>
                    <input type="text" name="k" id="k" />
                        by 
                        <select name="by" id="by">
                            <option value="InvoiceID">Order ID</option>
                            <option value="CustomerID">Customer ID</option>
                            <option value="Email">Email</option>
                            <option value="LastName">Last Name</option>
                            <option value="TransactionCode">Transaction ID</option>
                        </select>
                        <input type="hidden" value="search" name="action" id="action"  />
                        <input type="submit" name="Submit" value="Lookup" />
                  </form>
            </div>
        </div>
        
        <div id="workingArea">
            <div id="headline" style="font-size: 15px; font-weight: bold; padding: 5px;" runat="server" />
            <div id="resultSet" runat="server">
        </div>
    </div>
</body>
</html>
 