﻿<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Assembly src="/bin/LitlePayment.cs" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Net.Mime" %>
<script language="C#" runat="server">

	Order o = new Order();
	Payment_ p = new Payment_();
	Customer c = new Customer();
	DataTable dt = new DataTable();
	StringBuilder summary = new StringBuilder();
	DataTable goodShipments = new DataTable();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		
		
		// grab all orders that need to be charged
		
		// first ships and special orders
		o.GetOrdersInProcessingScott(out dt);
		
		Response.Write("Parsing orders to Authorize.net...");
		Response.Flush();
		
		if (dt.Rows.Count > 0) {			
			// add a column so we can track what charged correctly
			bool charged = false;
			
				// capture the $$$ using the transaction id
				foreach(DataRow r in dt.Rows) {
					// start fresh
					charged = false;
					p = new Payment_();
					
					SetBrand(Convert.ToInt32(r["BrandID"].ToString()));
					/*bool good = p.DoVoid(r["TransactionCode"].ToString());	
						
						if (good) {
							// add note to account
							string customerID = r["CustomerID"].ToString();
							c.AddNote(customerID, "Order #" + r["InvoiceID"].ToString() + " cancelled & payment authorization reversed by " + Session["operator"] + " - Note added by BOT");
							Response.Write("Order #" + r["InvoiceID"].ToString() + " cancelled & payment authorization reversed by " + Session["operator"] + " - Note added by BOT<br />");
						} else {
							Response.Write("Error: " + r["TransactionCode"].ToString() + " | " + r["InvoiceID"].ToString() + " " + p.DebugString);
							Response.Flush();
							
					}
					// update order status id
					o.UpdateStatus(r["InvoiceID"].ToString(), "6");*/
				}
				
		} else {
			Response.Write("No files to charge.<br />");	
		}
		
		
		
		dt = null;
		o = null;
		p = null;
	}
	
	void SetBrand(int id) {
			/*Barclays: 6105109, Heartwood: 6106204, Troon: 6105112*/
			switch  (id) {
				case 2:
					// troon
					p.MID = "6105112";
					break;
				case 7:
					// H&O
					p.MID = "6106204";
					break;
				default:
					// barclays
					p.MID = "6105109";
					break;
			}
	}
	
	
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
</body>
</html>
