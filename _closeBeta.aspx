﻿<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Assembly src="/bin/LitlePayment.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Net.Mime" %> 
<script language="C#" runat="server">

	Order o = new Order();
	Payment_ p = new Payment_();
	DataTable dt = new DataTable();
	StringBuilder summary = new StringBuilder();
	DataTable goodShipments = new DataTable();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		
		
		// grab all orders that need to be charged
		
		// first ships and special orders
		o.GetOrdersInProcessing(out dt);
		
		Response.Write("Parsing orders to Litle...");
		Response.Flush();
		
		if (dt.Rows.Count > 0) {			
			// add a column so we can track what charged correctly
			bool charged = false;
			
				// capture the $$$ using the transaction id
				foreach(DataRow r in dt.Rows) {
					// start fresh
					charged = false;
					p = new Payment_();
					
					/*Barclays: 6105109, Heartwood: 6106204, Troon: 6105112*/
					switch  (r["BrandID"].ToString()) {
						case "2":
							// troon
							p.MID = "6105109";
							break;
						case "7":
							// H&O
							p.MID = "6106204";
							break;
						default:
							p.MID = "6105109";
							break;
					}
					
					// do capture
					string amount = Convert.ToDouble(r["Amount"]).ToString("F");
					charged = p.DoCapture(r["TransactionCode"].ToString(), amount, r["InvoiceID"].ToString());
					
					// legacy capture on barclays
					if (charged == false) {
						p.MID = "6105109";
						charged = p.DoCapture(r["TransactionCode"].ToString(), amount, r["InvoiceID"].ToString());
					}
					
					if (charged) {
						// mark invoice as charged
						o.UpdateStatus(r["InvoiceID"].ToString(), "2", p.TransactionCode);
						//Response.Write("Charged order " + r["InvoiceID"].ToString());
						//Response.Flush();
					} else {
						
						// should send email to CS to resolve or void the order 
						// and contact customer.
						Response.Write("Error: " + r["TransactionCode"].ToString() + " | " + r["InvoiceID"].ToString() + " " + p.DebugString);
						Response.Flush();
						
					}
				}
		
		} else {
			Response.Write("No files to charge.<br />");	
		}
		
		/* send report
		dt = o.GetShipmentSummary(2);
		string reportSummary = "<table style='width: 500px; padding: 3px;'>";
		
					foreach (DataRow r in dt.Rows) {
						reportSummary += "<tr>";	
						reportSummary += "<td style='width: 100px;'>" + r["Code"].ToString().Trim() + "</td>";
						reportSummary += "<td style='width: 315px;'>" + r["Name"].ToString().Trim() + "</td>";
						reportSummary += "<td style='width: 75px;'>" + r["Items"].ToString().Trim() + "</td>";
						reportSummary += "</tr>";
					}
		
		reportSummary += "</table>";
		SendEmail(reportSummary, "Inventory Report");
			
		// setup dt for marking shipments as shipped
		goodShipments.Columns.Add("InvoiceID", typeof(string));
			
		// grab all of the orders that need to be shipped
		StringBuilder sb = new StringBuilder();
		string fileName = "";
		dt = null;
		dt = new DataTable();
		o.GetMiscBottlesToShip(out dt);
		
		// get single orders
		if (dt.Rows.Count > 0) {
			string currentInvoice = "";
			foreach(DataRow r in dt.Rows) {
				if (currentInvoice != r["InvoiceID"].ToString()) {
					try {
						// add 00 record
						sb.Append("00|" + r["InvoiceID"] + "|" + r["PhoneNumber"].ToString().Trim() +
										"|" + r["ShipName"].ToString().Trim().Replace("|", "") + "|" + r["Address"].ToString().Trim().Replace("|", "") + "|" + r["Address2"].ToString().Trim().Replace("|", "") + "|" + 
										r["City"].ToString().Trim().Replace("|", "") + "|" + r["State"] + "|" + r["PostalCode"].ToString().Trim() + "|" + r["BrandID"].ToString().Trim() + "\r\n");
						goodShipments.Rows.Add(r["InvoiceID"].ToString());
					}
					catch (Exception ex) {
						Response.Write(ex.ToString() + " ------ " + r["InvoiceID"] + "<br />");	
					}
					currentInvoice = r["InvoiceID"].ToString();
				}
				
				// add 10 record
				sb.Append("10|" + r["Name"].ToString().Trim().Replace("|", "") + "|" + r["Quantity"] + "\r\n");
			}
			SendToDoretti("Attached is the message.", "Pick and Pack", sb);
		}
		
		// get non wine
		sb = new StringBuilder();
		fileName = "";
		DataTable accessories = new DataTable();
		o.GetNonWineToShip(out accessories);
		
		sb = new StringBuilder();
		fileName = "";
		dt = null;
		dt = new DataTable();
		o.GetPacksToShip(out dt); 
		
		// get packs
		if (dt.Rows.Count > 0) {
			string currentSku = "";
			string body = "";
			string skuName = "";
			string tenRecord = "";
			int skuCount = 0;
			DataTable children = new DataTable();
			foreach(DataRow r in dt.Rows) {
				if (currentSku != r["ProductID"].ToString()) {
					if (currentSku != "") {
						body = skuCount + " shipments of " + skuName + "<br />\r\n<br />\r\n" + body;
						SendToDoretti(body, skuCount + " shipments of " + skuName, sb);
						tenRecord = "";
					}
					
					body = "";
					children = o.GetChildren(r["ProductID"].ToString());
					foreach (DataRow rr in children.Rows) {
						body += rr["Code"] + " - " + rr["Vintage"].ToString().Trim() + " " + rr["Name"].ToString().Trim() + " (" + rr["Quantity"] + " items)<br />\r\n";
						tenRecord += "10|" + rr["Vintage"].ToString().Trim() + " " + rr["Name"].ToString().Trim() + "|" + rr["Quantity"].ToString()  + "\r\n";
					}
					
					
					sb = new StringBuilder();
					currentSku = r["ProductID"].ToString();
					skuName = r["Name"].ToString();
					skuCount = 0;
				}
				
										
				for (int i = 0; i < Convert.ToInt32(r["Quantity"].ToString()); i++) {
					try {
						// add 00 record
						sb.Append("00|" + r["InvoiceID"] + "|" + r["PhoneNumber"].ToString().Trim() +
										"|" + r["ShipName"].ToString().Trim().Replace("|", "") + "|" + r["Address"].ToString().Trim().Replace("|", "") + "|" + r["Address2"].ToString().Trim().Replace("|", "") + "|" + 
										r["City"].ToString().Trim().Replace("|", "") + "|" + r["State"] + "|" + r["PostalCode"].ToString().Trim() + "|" + r["BrandID"].ToString().Trim() + "\r\n");
										
						// see if they have an accessory to add to their order
						DataRow[] accessory = accessories.Select("InvoiceID = " + r["InvoiceID"].ToString());
						if (accessory.Length > 0) {
							// add it to the package
							sb.Append("10|" + accessory[0]["Name"].ToString().Trim() + "|" + accessory[0]["Quantity"].ToString()  + "\r\n");
						}				
										
						// add 10 record
						sb.Append(tenRecord);
						goodShipments.Rows.Add(r["InvoiceID"].ToString());
						skuCount++;
					}
					catch (Exception ex) {
						Response.Write(ex.ToString() + " ------ " + r["InvoiceID"] + "<br />");	
					}
				}
			}
			body = skuCount + " shipments of " + skuName + "<br />\r\n<br />\r\n" + body;
			SendToDoretti(body, skuCount + " shipments of " + skuName + "(" + currentSku + ")", sb);
		}
		
		// mark shipments as packed
		foreach (DataRow r in goodShipments.Rows) {
			o.UpdateStatus(r["InvoiceID"].ToString(), "3");
		}*/
		
		dt = null;
		o = null;
		p = null;
	}
	
	private void Upload(string filename)
	{
	   FileInfo fileInf = new FileInfo(filename);
	   string uri = "ftp://ship.barclayswine.com/" + fileInf.Name;
	   FtpWebRequest reqFTP;
	
	   // Create FtpWebRequest object from the Uri provided
	   reqFTP = (FtpWebRequest)FtpWebRequest.Create
				(new Uri("ftp://ship.barclayswine.com/" + fileInf.Name));
	
	   // Provide the WebPermission Credintials
	   reqFTP.Credentials = new NetworkCredential("fedexuser", "FEDEXUSER");
	
	   // By default KeepAlive is true, where the control connection
	   // is not closed after a command is executed.
	   reqFTP.KeepAlive = false;
	
	   // Specify the command to be executed.
	   reqFTP.Method = WebRequestMethods.Ftp.UploadFile;
	
	   // Specify the data transfer type.
	   reqFTP.UseBinary = true;
	
	   // Notify the server about the size of the uploaded file
	   reqFTP.ContentLength = fileInf.Length;
	
	   // The buffer size is set to 2kb
	   int buffLength = 2048;
	   byte[] buff = new byte[buffLength];
	   int contentLen;
	
	   // Opens a file stream (System.IO.FileStream) to read the file
	   // to be uploaded
	   FileStream fs = fileInf.OpenRead();
	
	   try
	   {
		  // Stream to which the file to be upload is written
		  Stream strm = reqFTP.GetRequestStream();
	
		  // Read from the file stream 2kb at a time
		  contentLen = fs.Read(buff, 0, buffLength);
	
		  // Till Stream content ends
		  while (contentLen != 0)
		  {
			 // Write Content from the file stream to the FTP Upload
			 // Stream
			 strm.Write(buff, 0, contentLen);
			 contentLen = fs.Read(buff, 0, buffLength);
		  }
	
		  // Close the file stream and the Request Stream
		  strm.Close();
		  fs.Close();
	   }
	   catch(Exception ex)
	   {
		  ;
	   }
	}
	
	private void SendToDoretti(string s, string subject, StringBuilder sb) {
			string fileName = subject + "_" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + 
								"-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Ticks.ToString() +
								".csv";
			fileName = fileName.Replace(@"\", "");
			fileName = fileName.Replace(@"/", "");
			try {
				TextWriter tw = new StreamWriter(Server.MapPath("~/shipping_files/") + fileName);
				tw.Write(sb.ToString());
				tw.Close();
			}
			catch (Exception ex) {
				Response.Write(ex.ToString());	
			}
			
			Upload(Server.MapPath("~/shipping_files/") + fileName);
	}
	
	private void SendEmail(string s, string subject, StringBuilder sb) {
			string fileName = subject + "_" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + 
								"-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Ticks.ToString() +
								".csv";
			fileName = fileName.Replace(@"\", "");
			fileName = fileName.Replace(@"/", "");
			try {
				TextWriter tw = new StreamWriter(Server.MapPath("~/shipping_files/") + fileName);
				tw.Write(sb.ToString());
				tw.Close();
			}
			catch (Exception ex) {
				Response.Write(ex.ToString());	
			}
			
			// email file to jessica and cc devops
			
			try {
				Attachment data = new Attachment((Server.MapPath("~/shipping_files/") + fileName), MediaTypeNames.Application.Octet);
				MailMessage email = new MailMessage(
					   "shipping-robot@barclayswine.com",
					   "drinks-devops@drinks.com, jessica@barclayswine.com",
					   "**" + subject,
					   s);
				email.Attachments.Add(data);
				email.IsBodyHtml = true;
					
				NetworkCredential creds = new NetworkCredential("auth@voidme.com", "$m@ng0");
				SmtpClient client = new SmtpClient("127.0.0.1");
				client.UseDefaultCredentials = false;
				client.Credentials = creds;
				client.Send(email);
			} catch (Exception ex) {
				Response.Write(ex.ToString());	
			}
	}
	
	private void SendEmail(string s, string subject) {
			
			// email file to jessica and cc devops
			MailMessage email = new MailMessage(
				   "shipping-robot@barclayswine.com",
				   "drinks-devops@drinks.com, jessica@barclayswine.com, garrett@barclayswine.com",
				   "**" + subject,
				   s);
			email.IsBodyHtml = true;
				
			NetworkCredential creds = new NetworkCredential("auth@voidme.com", "$m@ng0");
			SmtpClient client = new SmtpClient("127.0.0.1");
			client.UseDefaultCredentials = false;
			client.Credentials = creds;
			try {
				client.Send(email);
			} catch (Exception ex) {
				;	
			}
	}
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
</body>
</html>
