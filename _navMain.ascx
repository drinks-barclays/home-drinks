<%@ Control Language="C#" %>

<script language="C#" runat="server">
	public int navId = 0;
	public void Page_Init(Object sender, EventArgs e)
 	{
		//if (Request.IsSecureConnection == false) Response.Redirect(Request.Url.ToString().Replace("http://", "https://"));
		if (Session["operator"] == null) Response.Redirect("/?r=" + Server.UrlEncode(Request.Url.ToString()));
		
		if (Session["operator"].ToString() == "chargebacks") {
			// hide something
			tools.Visible = false;	
			inventory.Visible = false;
			marketing.Visible = false;
			continuity.Visible = false;
			reporting.Visible = false;
		} else if (Session["operator"].ToString() == "finance") {
			// hide something
			tools.Visible = false;	
			inventory.Visible = false;
			marketing.Visible = false;
			continuity.Visible = false;
		}
		
		switch (navId) {
			case 1:
				reporting.Attributes["class"] = "active";
				break;
			case 2:
				inventory.Attributes["class"] = "active";
				break;
			case 3:
				orders.Attributes["class"] = "active";
				break;
			case 4:
				customers.Attributes["class"] = "active";
				break;
			case 5:
				marketing.Attributes["class"] = "active";
				break;
			case 6:
				continuity.Attributes["class"] = "active";
				break;
			case 7:
				tools.Attributes["class"] = "active";
				break;
			case 8:
				compliance.Attributes["class"] = "active";
				break;
			default:
				break;
		}
	}
</script>
<div id="masthead">
 <div id="wrapper" class="clearfix"> 
     <div class="tabs">   
        <ul>
          <li id="reporting" runat="server"><a href="report.aspx" class="navTitle">Reporting</a></li>
          <li id="inventory" runat="server"><a href="inventory-management.aspx" class="navTitle">Inventory</a></li>
          <li id="orders" runat="server"><a href="orders.aspx" class="navTitle">Orders</a></li>
          <li id="customers" runat="server"><a href="customers.aspx" class="navTitle">Customers</a></li>
          <li id="marketing" runat="server"><a href="marketing.aspx" class="navTitle">Marketing</a></li>
          <li id="continuity" runat="server"><a href="continuity.aspx" class="navTitle">Continuity</a></li>
           <li id="tools" runat="server"><a href="tools.aspx" class="navTitle">Tools</a></li>
           <li id="compliance" runat="server"><a href="compliance.aspx" class="navTitle">Compliance</a></li>
        </ul>
     </div>  
 </div> 
      
        <div style="padding-right: 10px; background: #fff; margin-top: 1px;" align="right">
            Logged in as <strong><%= Session["operator"] %></strong> - <a href="/logout.aspx">Logout</a>
        </div>
</div>
