<%@  Page Language="C#" Debug="true" ClientIdMode="static" %>
<%@ Register TagPrefix="BB" TagName="View" Src="customers/_view.ascx" %>
<%@ Register TagPrefix="BB" TagName="Manage" Src="customers/_manage.ascx" %>
<%@ Register TagPrefix="BB" TagName="Nav" Src="_navMain.ascx" %>
<script language="C#" runat="server">
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Session["operator"] == null) Response.Redirect("/security.aspx?r=" + Server.UrlEncode(Request.Url.ToString()));
		
		if (Request["id"] != null) {
			_manage.Visible = true;
		} else {
			_view.Visible = true;
		}
	}
	 
	
</script>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Customers</title>

<script src="//code.jquery.com/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/pepper-grinder/jquery-ui.min.css" media="screen" />
<script src="/js/jquery.validationEngine-en.js" type="text/javascript"></script>
<script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
<link rel="stylesheet" href="//heartwoodandoak.com/assets/css/validationEngine.jquery.css" type="text/css" media="screen" charset="utf-8" />
<script src="/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="/js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="/js/jquery.fancybox.pack.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="/js/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript">
	
	$(document).ready(function(){
		$("#myTable").tablesorter(); 	
		$(".modal").fancybox({'autoSize':true, 'type' : 'iframe'});	
  	});
	
  </script>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
<style>
	
	body {
		margin: 0 auto;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		padding: 0px;
	}
	
	#nav {
		margin: 0 auto;
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
		background-color: #efefef;
		min-width: 400px;
	}
	
	#workingArea {
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
	}
	
</style>
</head>

<body> 
    <BB:Nav runat="server" id="nav_ctl" navId=4 />
    <div id="bodyWrapper">
        <div id="nav">
            <div style="font-size: 20px; margin-bottom: 10px;">Customer Management</div> 
            <div>
                <a href="?action=view">Lookup</a>
        	</div>
        </div>
        
        <div id="workingArea">
        	<BB:View runat="server" id="_view" visible="false" />
        	<BB:Manage runat="server" id="_manage" visible="false" />
        </div>
    </div>
</body>
</html>
 