﻿<%@  Control Language="C#" ClientIdMode="static" %>
<%@ Assembly src="/bin/Marketing.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	DataSet ds = new DataSet();
	Marketing m = new Marketing();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		DataTable dt;
		int top = 25;
		try {
			top = Convert.ToInt16(Request["top"].ToString());	
		} catch  {
			;	
		}
		
		Hashtable parameters = new Hashtable();
		parameters.Add("Top", top);
		m.GetCampaignList(out dt, parameters);
		
		int totalRecs = 0;
		if (dt.Rows.Count > 0) totalRecs = Convert.ToInt32(dt.Rows[0]["TotalRecs"].ToString());
		
		double totalPages = 0;
		double t = ((double)totalRecs/(double)25);
		if (totalRecs > 25) totalPages = Math.Ceiling(t);
		for (int i=1; i<= totalPages; i++) {
			if (i != (top/25)) {
				paginate.InnerHtml += " <a href='?action=view&top=" + i*25 + "'>" + i + "</a> ";
			} else {
				paginate.InnerHtml += " <strong>" + i + "</strong> ";	
			}
		}
		
		
		dt.DefaultView.Sort = "CampaignID ASC";
		dt = dt.DefaultView.ToTable();
		DataTable dtT = dt.Rows.Cast<System.Data.DataRow>().Take(25).CopyToDataTable();
		dtT.DefaultView.Sort = "CampaignID DESC";
		dtT = dtT.DefaultView.ToTable();
		
		results.InnerHtml = "<table id='myTable' class='tablesorter'><thead><tr><th>ID</th><th>Type</th><th>Description</th><th>Free Shipping</th><th>DiscountType</th><th>StartDate</th><th>EndDate</th></tr></thead><tbody>";
		foreach(DataRow r in dtT.Rows) {
			results.InnerHtml += "<tr><td>" + r["CampaignID"] + "</td>";
			if (r["CampaignType"].ToString() == r["PartnerName"].ToString()) {
				results.InnerHtml += "<td>" + r["CampaignType"] + "</td>";
			} else {
				results.InnerHtml += "<td>" + r["CampaignType"] + " - " + r["PartnerName"] + "</td>";
			}
			results.InnerHtml += "<td>" + r["Description"] + "</td>";
			results.InnerHtml += "<td>" + r["FreeShipping"] + "</td>";
			results.InnerHtml += "<td>" + r["DiscountType"] + "</td>";
			results.InnerHtml += "<td>" + Convert.ToDateTime(r["StartDate"]).ToString("d") + "</td>";
			results.InnerHtml += "<td>" + Convert.ToDateTime(r["EndDate"]).ToString("d") + "</td></tr>";
		}
		results.InnerHtml += "</table>";
	}
</script>


          	<div style="font-size: 20px; margin-bottom: 10px;">Current Campaigns</div> 
          	<div id="results" name="results" runat="server"></div>
            <div id="paginate" runat="server" style="margin: 10px; text-align: center">Page: </div>