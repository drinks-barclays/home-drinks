﻿<%@ Control Language="C#" %>
<%@ Assembly src="/bin/Continuity.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<script language="C#" runat="server">

	public int continuityID = 0;
	public void Page_Load(Object sender, EventArgs e)
 	{
		if (continuityID != 0) {
			LookupContinuity();
		} else {
			// call to join
			joinNow.InnerHtml = "Please call (888) 380-2337 to join this program.";
		}
	}
	
	private void LookupContinuity() {
		// lookup items for the requested conti program
		Continuity c = new Continuity();
		DataTable dt;
		c.GetShipments(out dt, continuityID.ToString());
		
		// parse them out to the client
		string price = "";
		string defaultFrequency = "";
		string shipping = "";
		
		if (dt.Rows.Count > 0) {
			price = Convert.ToDouble(dt.Rows[0]["Price"].ToString()).ToString("c");
			shipping = Convert.ToDouble(dt.Rows[0]["Shipping"].ToString()).ToString("c");
			switch (dt.Rows[0]["Span"].ToString()) {
				case "4":
					defaultFrequency = "Monthly";
					break;
				case "8":
					defaultFrequency = "Bi-Monthly";
					break;
				case "12":
					defaultFrequency = "Quarterly";
					break;
				case "52":
					defaultFrequency = "Anually";
					break;
				default:
					break;
			}
		}
		
		// parse out beginning of form
		joinNow.InnerHtml += "<form action='shopping_cart.aspx' method=post>";
		
		int i = 0;
		foreach(DataRow r in dt.Rows) {
			joinNow.InnerHtml += "<input name='id' type='radio' id='id' value='" + r["ProductID"] + "'";
			if (i==0) { joinNow.InnerHtml += " checked />"; } else {  joinNow.InnerHtml += " />";}
			joinNow.InnerHtml += r["PreferenceType"] + " Wines<br />";
			i++;
		}
		
		// parse out pricing, button and end of form
		joinNow.InnerHtml += "<div align='center' style='font-size: 18px; padding: 5px;color: #694C6C;'>" + price + " " + defaultFrequency +
								"<br /><span style='font-size: 12px;'>(+ " + shipping + " shipping and tax)</span><br />" +
								"<input type=submit value='Join Now' style='margin-top: 10px;' />" +
								"<input type=hidden id='action' name='action' value='add_cart' /></div></form>";	
	}
	
</script>

<!-- begin join now -->
<div id="joinNow" style="padding: 5px;" runat="server">
	
</div>
<!-- end join now -->