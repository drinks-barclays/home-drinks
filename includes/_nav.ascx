﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="BB" TagName="User" Src="/includes/_user.ascx" %>
<script language="C#" runat="server">
	public int tabID = 0;
	public void Page_Load(Object sender, EventArgs e)
 	{
		switch (tabID) {
			case 1:
				// show wine shop as selected
				wineShopNav.Attributes["class"] = "tab_nav selected";
				break;
			case 2:
				// show wine plans as selected
				winePlanNav.Attributes["class"] = "tab_nav selected";				
				break;
			default:
				break;
		}
	}
</script>


	<div id="wineShopNav" class="tab_nav" style="position: absolute; top: 92px; left: 430px;" runat="server"><a href="/wine_shop.aspx" style="color: #E2DCCD" title="Wine Shop - Buy Wine Online">Wine Shop</a></div>
    <div id="wineMixedCases" class="tab_nav" style="position: absolute; top: 92px; left: 550px;" runat="server"><a href="/wine_shop.aspx?product=1003" style="color: #E2DCCD" title="Mixed Wine Cases">Mixed Cases</a></div>
    <div id="winePlanNav" class="tab_nav" style="position: absolute; top: 92px; left: 670px;" runat="server"><a href="/experience/<% if (Request["campaignID"] == "3559") { %>direct-mail-experience.aspx<% } else { %>full-experience.aspx<% } %>" style="color: #E2DCCD" title="Wine Club">Wine Club</a></div>
    <div class="fb-like" style="position: absolute; top: 90px; left: 815px; z-index: 1;" data-href="http://www.facebook.com/BarclaysWine" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false" data-font="trebuchet ms" align="right"></div>  
    <!-- <div id="userName" style="font-weight: bold; position: absolute; top: 98px; left: 690px; width: 220px;z-index: 1; height: 32px; text-align: right"><BB:User runat="server" id="user" /></div> -->

