﻿<%@ Control Language="C#" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<script language="C#" runat="server">

	bool checkMe = false;
	public void Page_Load(Object sender, EventArgs e)
 	{
		// decide which banner to show
		Random random = new Random();
		int randomNumber = random.Next(10, 19);
		
		if (randomNumber % 2 == 0) {
			// show amora
			checkMe = true;
		}
	}
	
</script>

<style>
	#rush-processing {
		border: 1px solid #333;
		color: #333;	
		padding: 5px;
		background-color: #efe;
		margin-top: 10px;
		margin-bottom: 10px;
		line-height: 1;
	}
</style>

<div id="rush-processing" class="clearfix">
    <div style="font-weight: bold;"><input name="rushProcessing" type="checkbox" id="rushProcessing" <% if (checkMe) Response.Write("checked='checked'"); %> /> RUSH my order to me!</div>
	<div>Get your order faster with same day processing for $5.99!</div>
</div>
<script language="javascript">
	$( document ).ready(function() {
	  if ($('#rushProcessing').attr('checked')) {
		  console.log('opt-out');
		  _gaq.push(['_trackEvent', 'RushProcessing', 'Viewed', 'Opt-Out']);
	  } else {
		  console.log('opt-in');
		  _gaq.push(['_trackEvent', 'RushProcessing', 'Viewed', 'Opt-In']);
	  }
	});
</script>

