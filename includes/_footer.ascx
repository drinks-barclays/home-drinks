﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="BB" TagName="Analytics" Src="/includes/_analytics.ascx" %>
<script language="C#" runat="server">
	public bool showUtilBar = true;
	public void Page_Load(Object sender, EventArgs e)
 	{
		if (!showUtilBar) utilBar.Visible = false;
	}
</script>
		<div id="utilBar" class="clearfix" runat="server">
        	<div style="border-bottom: 1px solid #603;font-size: 14px; color:#603; font-weight: bold; margin-bottom: 5px;">Useful links</div>
        	<div style="float: left; width: 175px; margin-left: 10px;">
                <div>
                	<a href="/customer_service.aspx" style="font-size: 12px;">Customer Service</a><br />
                	<a href="/newsletter.aspx" style="font-size: 12px;">Unsubscribe</a><br />
                	<a href="/my_account.aspx" style="font-size: 12px;">My Account</a>
                    
                </div>
            </div>
        	<div style="float: left; width: 175px; margin-left: 50px;">
                <div>
                	<a href="/about_us.aspx" style="font-size: 12px;">About Us</a><br />
                	<a href="/guarantee.aspx" style="font-size: 12px;">Our Guarantee</a><br />
                	<a href="/newsletter.aspx" style="font-size: 12px;">Email Sign Up</a>
                    
                </div>
            </div>
        	<div style="float: left; width: 160px; margin-left: 50px;">
                <div>
                	<a href="/privacy.aspx" style="font-size: 12px;">Privacy Policy</a><br />
                	<a href="/terms_service.aspx" style="font-size: 12px;">Terms of Service</a><br />
                	<a href="/shipping_policy.aspx" style="font-size: 12px;">Shipping Policy</a>
                    
                </div>
            </div>
        	<div style="float: left; width: 200px; margin-left: 20px;">
                <div style="font-size: 12px;" align="center">We accept the following:<br />
					<img src="/assets/images/logo_ccVisa.gif" />
					<img src="/assets/images/logo_ccMc.gif" />
					<img src="/assets/images/logo_ccAmex.gif" />
					<img src="/assets/images/logo_ccDiscover.gif" />
                    
                </div>
            </div>
        </div>
        <div id="footer" class="clearfix">
            <div style="float: left; width: 400px; margin-left: 10px;">© 2009-<%= System.DateTime.Now.Year %> Barclays Wine
            <BB:Analytics runat="server" id="analytics" /></div>
        </div>
 
 