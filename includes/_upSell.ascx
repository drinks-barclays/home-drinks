﻿<%@ Control Language="C#" %>
<script language="C#" runat="server">

	int id = 0;
	public void Page_Load(Object sender, EventArgs e)
 	{
		// decide which banner to show
		Random random = new Random();
		int randomNumber = random.Next(0, 5);
		
		if (Request.Cookies["upsellID"] == null) {
			// test different prices
			switch (randomNumber) {
				case 0:
					id = 5077; // $9.95	corkscrew
					break;
				case 1:
					id = 5091; // $8.95	corkscrew
					break;
				case 2:
					id = 5092; // $7.95	corkscrew
					break;
				case 3:
					id = 5093; // $6.95	corkscrew
					break;
				case 4:
					id = 5094; // $5.95	corkscrew
					break;
				default:
					break;
			}
			// drop a cookie to make sure we don't show this again
			HttpCookie cookie = new HttpCookie("upsellID");
			cookie.Value = id.ToString();
			Response.Cookies.Add(cookie);
		} else {
			// show what they've already seen
			id = Convert.ToInt32(Request.Cookies["upsellID"].Value);	
		}
		
	}
	
</script>


<script language="javascript">

//$(document).ready(function() {
	//$('#oForm').submit(function() {
	     // return upSell();
	
	//});
//})
</script>
<a href="/upsell/index.aspx?id=<%= id %>" id="upSellModal"></a>


