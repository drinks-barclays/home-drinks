﻿<%@ Control Language="C#" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Import Namespace="Wine" %>
<script language="C#" runat="server">

	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Session["UID"] != null && Session["First_Name"] != null) {
			welcome.InnerText = "Welcome, " + Session["First_Name"] + "!";
		}
	}
	
</script>

<span id="welcome" runat="server"></span>