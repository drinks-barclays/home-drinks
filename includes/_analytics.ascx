﻿<%@ Control Language="C#" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Assembly Src="/bin/Marketing.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<script language="C#" runat="server">

	public string trackMore = "";
	public void Page_Load(Object sender, EventArgs e)
 	{
		// see if we need to send order tracking to GA
		if (Request["orderID"] != null) {
			Order order = new Order();
			DataTable dt;
			
			order.GetGoogleOrder(out dt, Request["orderID"]);
			
			if (dt.Rows.Count > 0) {
				
				trackMore += "_gaq.push(['_addTrans', '" + dt.Rows[0]["oid"].ToString() + "', '', '" + dt.Rows[0]["amt"].ToString() + "', '0', '" + dt.Rows[0]["ShippingAmount"].ToString() + "', '', '', '']);\r\n";
				
				foreach (DataRow r in dt.Rows) {
					trackMore += "_gaq.push(['_addItem', '" + r["oid"].ToString() + "', '" + r["prdsku"].ToString().Trim() + "', '" + r["prdnm"].ToString().Replace("'", "\'") + "', '', '" + r["prdpr"].ToString() + "', '" + r["prdqn"].ToString() + "']);\r\n_gaq.push(['_trackTrans']);\r\n";	
					if (r["prdsku"].ToString().Trim() == "BWRUSH") trackMore += "_gaq.push(['_trackEvent', 'RushProcessing', 'Purchased']);\r\n";
				}
			}
		}
	}
	
</script>

<!-- begin analytics -->


<script type="text/javascript">

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  
  <% 
    int campaignID = 0;
    Int32.TryParse(Request["campaignID"], out campaignID);
    string pixel = "";
    if (campaignID != 0) pixel = new Marketing().GetPixel(campaignID);
  %>
  
  var dimensionValue = '<%= campaignID %>';
	ga('set', 'dimension1', dimensionValue);
	ga('set', 'pixel', pixel);
  ga('create', 'UA-1889996-9', 'barclayswine.com');
  ga('send', 'pageview');

</script>

<script type="text/javascript">
adroll_adv_id = "BLAA7UILZRBXHJMEOQUKX5";
adroll_pix_id = "WH7WRKRODJHJPPOIE3OP77";
(function () {
var oldonload = window.onload;
window.onload = function(){
   __adroll_loaded=true;
   var scr = document.createElement("script");
   var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
   scr.setAttribute('async', 'true');
   scr.type = "text/javascript";
   scr.src = host + "/j/roundtrip.js";
   ((document.getElementsByTagName('head') || [null])[0] ||
    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
   if(oldonload){oldonload()}};
}());
</script>

<!-- end analytics -->

