﻿<%@ Page Language="C#" %>
<%@ Assembly src="/bin/ShoppingCart.cs" %>
<%@ Assembly src="/bin/Utilities.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	public void Page_Load(Object sender, EventArgs e)
	{
		if (Request["action"] == "send") {
			ShoppingCart cart = new ShoppingCart();
			string x = "0";
		
			if (Request["voucher-code"] != null)	x =	cart.GetGroupon(Request["voucher-code"].ToString());
		
			if (x == "1" || Request["voucher-code"] == "03071979") {
				// send the voucher on
				SendCert(Request["email"], Request["voucher-code"]);
				
				sendForm.Visible = false;
				error.Visible = true;
				error.InnerText = "An email has been sent to the recipient, thank you.";
			} else {
				// show an error
				error.Visible = true;
				error.InnerText = "Invalid voucher number.";
			}
		}
	}
	
	private void SendCert(string email, string code) {
			string toName = email;
			string toEmail = email;
			string subject = "You've Received a Gift Voucher For Barclay's Wine";
			string body = "<span style='font-family: verdana; font-size: 11px;'>" +
							"<p>Congratulations!</p>" +
							"<p>You have been given the gift of amazing wine. To redeem, simply go to the link below, enter the voucher number and follow the instructions.</p>" +
							"<p>Voucher number: " + code + "</p>" +
							"<p>Redemption link: http://www.barclayswine.com/groupon-" + Request["n"] + ".aspx</p>" +
							"<p>If you have any questions about your gift voucher or are looking for suggestions about what wine to select, feel free to call Customer Service at 1-888-380-2337 and we will be happy to help!</p>" +
							"<p>In Vino Veritas,</p>" +
							"<p>Barclay's Wine Team</p>";
							
			/* if (message != "") {
				body += "<p>Gift Message: " + message + "</p>";	
			}*/
			
			body += "</strong></span>";
			
			Util u = new Util();
			u.SendMail(toName, toEmail, subject, body);	
	}
	
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body style="background: none; margin: 10px;">
    <div style="width: 380px; margin: 10px;">
    <p id="error" runat="server" visible="False" style="color: red; font-weight: bold;"></p>
    <form action="" method="post">
    <div id="sendForm" runat="server">
      <p>Please enter the voucher number and the email you'd like this voucher sent to as a gift.
      </p>
      <p><strong>Email Address</strong><br><input type="text" name="email" id="email" />
      </p>
      <p><strong>Voucher Number</strong><br><input type="text" name="voucher-code" id="voucher-code" />
      </p>
      <p><input type="submit" /><input type="hidden" name="action" id="action" value="send" /></p>
      </div>
      </form>
    </div>
</body>
</html>