﻿<%@ Control Language="C#" %>
<%@ Assembly src="/bin/ShoppingCart.cs" %>
<%@ Import Namespace="Wine" %>
<script language="C#" runat="server">
	
	public int nav = 1;
	public void Page_Load(Object sender, EventArgs e)
 	{
		 switch (nav) {
			case 0:
				normal.Visible = false;
				special.Visible = true;
				break;
			default:
				normal.Visible = true;
				special.Visible = false;
				GetShoppingCartItems();
				break;
		}
	}
	
	private void GetShoppingCartItems() {
		ShoppingCart cart = new ShoppingCart();
		items.InnerText = cart.GetItemCount();
	}
</script>
<script src='https://connect.facebook.net/en_US/all.js'></script>
<script>

      FB.init({appId: "113440958788733", status: true, cookie: true, channelUrl : '//www.barclaysbin.com/channel.html', xfbml : true });
	  
</script>

<div id="fb-root"></div>
<div id="masthead" class="clearfix">
    <div id="normal" runat="server" style="height: 40px; width: 240px; margin-right: 15px; float: right; padding-top: 10px;">
        <div style="float: left; width: 115px; line-height:normal">
        	<a href="/my_account.aspx" style="color: white; font-weight: bold">My Account</a><br />
			<% if (Session["UID"] != null) { %> <a href='/account/end.aspx' style="color: #CCC; font-size: 13px; margin-left: 20px;">Logout</a> <% } %>
        </div>
        <div style="color: #CCC;float: left; width: 115px; line-height:normal" align="center">
        	<a href="/shopping_cart.aspx" style="color: white; font-weight: bold">Shopping Cart</a><br />
        	<span id="items" runat="server"></span> items
        </div>
    </div> 
    
    <div id="special" runat="server" style="height: 40px; width: 230px; margin-right: 15px; float: right; padding-top: 10px; font-size: 16px; color: white;">
    	<% if (Request["campaignID"] != "1066" && Request["campaignID"] != "1264" && Request["campaignID"] != "1225" && Request["campaignID"] != "1357") { %> Customer Care: (888) 380-2337 <% } else { %> 
    	100% Satisfaction Guaranteed
    	<% } %>
    </div> 
    <div title="In Vino Veritas - In wine there is truth." style="height: 120px; width: 350px; margin-left:35px; float: left; cursor: pointer" onClick="document.location = '/index.aspx'">
                &nbsp;
    </div>    
</div> 