<%@  Page Language="C#" Debug="true" ClientIdMode="static" %>
<%@ Register TagPrefix="BB" TagName="View" Src="marketing/_view.ascx" %>
<%@ Register TagPrefix="BB" TagName="Add" Src="marketing/_add.ascx" %>
<%@ Register TagPrefix="BB" TagName="Nav" Src="_navMain.ascx" %>
<%@ Assembly src="/bin/Product.cs" %>
<script language="C#" runat="server">
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Session["operator"] == null) Response.Redirect("/security.aspx?r=" + Server.UrlEncode(Request.Url.ToString()));
		
		switch (Request["action"]) {
			case "view":
				_view.Visible = true;
				break;
			case "add":
				_add.Visible = true;
				break;
			default:
				_view.Visible = true;
				break;	
		}
	}
	 
	
</script>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Marketing</title>

<script src="//code.jquery.com/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/pepper-grinder/jquery-ui.min.css" media="screen" />
<script src="/js/jquery.validationEngine-en.js" type="text/javascript"></script>
<script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
<link rel="stylesheet" href="//heartwoodandoak.com/assets/css/validationEngine.jquery.css" type="text/css" media="screen" charset="utf-8" />
<script src="/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
	
	$(document).ready(function(){
		$("#myTable").tablesorter(); 	
  	});
	
  </script>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
<style>
	
	body {
		margin: 0 auto;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		padding: 0px;
	}
	
	#nav {
		margin: 0 auto;
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
		background-color: #efefef;
		min-width: 400px;
	}
	
	#workingArea {
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
	}
	
</style>
</head>

<body> 
    <BB:Nav runat="server" id="nav_ctl" navId=5 />
    <div id="bodyWrapper">
        <div id="nav">
            <div style="font-size: 20px; margin-bottom: 10px;">Marketing</div> 
            <div>
                <a href="?action=view">View Campaigns</a> | <a href="?action=add">Add Campaign</a>
        	</div>
        </div>
        
        <div id="workingArea">
        	<BB:View runat="server" id="_view" visible="false" />
        	<BB:Add runat="server" id="_add" visible="false" />
        </div>
    </div>
</body>
</html>
 