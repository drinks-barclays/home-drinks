<%@ language="C#" %>
<%@ Import Namespace="AOSMTPLib" %>
<%@ Import Namespace="System.Web.Http" %>
<%@ Assembly src="/bin/Utilities.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="DrinksAPI" %>
<script runat="server">

 void Application_Start(Object Sender, EventArgs e)
 {
	Application["Sessions"] = 0;
	GlobalConfiguration.Configure(WebApiConfig.Register);
 }


 void Application_BeginRequest(Object Sender, EventArgs e)
 {
  	if (HttpContext.Current.Request["campaignID"] != null && HttpContext.Current.Request["campaignID"] != "") {
	
			HttpCookie cookie = new HttpCookie("campaignID");
			
			// Set the cookies value
			cookie.Value = HttpContext.Current.Request["campaignID"].ToString();
			// Set the cookie to expire in 1 day
			System.DateTime dtNow = System.DateTime.Now;
			System.TimeSpan tsDay = new System.TimeSpan(30, 0, 0, 0);
			cookie.Expires = dtNow + tsDay;
			HttpContext.Current.Response.Cookies.Add(cookie);
	}
	
	if (Request["SubID"] != null && Request["SubID"] != "") {
		HttpCookie cookie = new HttpCookie("subID");
			
		// Set the cookies value
		cookie.Value = HttpContext.Current.Request["subID"].ToString();
		// Set the cookie to expire in 1 day
		System.DateTime dtNow = System.DateTime.Now;
		System.TimeSpan tsDay = new System.TimeSpan(30, 0, 0, 0);
		cookie.Expires = dtNow + tsDay;
		HttpContext.Current.Response.Cookies.Add(cookie);
	}
 }
 
  protected void Application_Error(Object sender, EventArgs e)
 {
   	System.Exception ex = Server.GetLastError();
   	string body = ex.InnerException+"\r\n------\r\n"+ex.Source+"\r\n------\r\n"+ex.Message+"\r\n------\r\n"+ex.StackTrace+"\r\n------\r\n"+ex.TargetSite+"\r\n------\r\n"+Request.Url.ToString()+"\r\n------\r\n"+Request.Form.ToString() + "\r\n" + Request.ServerVariables["HTTP_COOKIE"] + "\r\n" + Request.ServerVariables["ALL_HTTP"];
   
   	Util u = new Util();
	u.SendMailServer("Drinks DevOps", "drinks-devops@drinks.com", "home.drinks.com Error", body, "bot@barclayswine.com", "Bot");
	
	HttpCookie cookie = new HttpCookie("campaignID");
			
			// Set the cookies value
			cookie.Value = "";
			// Set the cookie to expire in 1 day
			System.DateTime dtNow = System.DateTime.Now;
			System.TimeSpan tsDay = new System.TimeSpan(30, 0, 0, 0);
			cookie.Expires = dtNow + tsDay;
			HttpContext.Current.Response.Cookies.Add(cookie);
	
	Response.Redirect("/error.html");
   
 }

void Session_Start(Object Sender, EventArgs e)
{
	if (HttpContext.Current.Request["email_"] != null && HttpContext.Current.Request["email_"].ToString() != "") {
		Util u = new Util();
		u.AddEmailLead(HttpContext.Current.Request["email_"]);
	}
}

void Session_End(Object Sender, EventArgs e) {
	
}
 
</script>