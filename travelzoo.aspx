<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	Customer c = new Customer();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request["grouponID"] != null && Request["grouponID"].ToString() != "") {
			c.ValidateTravelzoo(Request["grouponID"].ToString(), Request["val"].ToString());
			workingArea.InnerHtml += "<strong>Voucher changed successfully!</strong><br />";
		}
		
	}
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Travelzoo</title>
<script type="text/javascript" src="/includes/js/jquery.js"></script> 
<script type="text/javascript" src="/includes/js/thickbox.js"></script>
<style type="text/css" media="all">@import "/includes/thickbox.css";</style>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>

<body>
<div id="container">
    <div id="navigation">
      <WF:Nav runat="server" id="nav_ctl" navId=2 />
    </div>
    
    <div id="content">
    	<div id="header" runat="server">Voucher</div>
        <div id="workingArea" runat="server"> 
        <form action="" method="post" id="iForm">
        	<strong style="font-size: 14px;">Activate Voucher</strong>
        	<hr size="1" style="margin-top: 2px;" />
			Voucher ID: 
			<input id="grouponID" name="grouponID" type="text" class="required" />
            <select name="val">
            	<option value="1">Activate</option>
            	<option value="0">De-activate</option>
            </select>
       	  <input name="submit" value="Submit" type="submit" />
        </form>
        </div>
    </div>
    
    <div id="footer">
    
    </div>
</div>
</body>
</html>
