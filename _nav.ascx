<%@ Control Language="C#" %>

<script language="C#" runat="server">
	public int navId = 0;
	public void Page_Init(Object sender, EventArgs e)
 	{
		if (Request.IsSecureConnection == false) Response.Redirect(Request.Url.ToString().Replace("http://", "https://"));
		if (Session["operator"] == null) Response.Redirect("/?r=" + Server.UrlEncode(Request.Url.ToString()));
	}
</script>
<div id="nav">

	<div class="navItem" style="margin: 0px;"><img src="/images/barclays/barclays_logo_small.jpg" /><hr size="1" /></div>
    
	<div class="navItem">
   	  <a href="inventory-management.aspx" class="navTitle">Inventory Management</a>
        
    </div>
    
    
    <div class="navItem">
   	  <a href="customers.aspx" class="navTitle">Customers</a>
    </div>
    
    <div class="navItem">
   	  <a href="orders.aspx" class="navTitle">Orders</a>
    </div> 
      
    <div class="navItem">
   	  <a href="suppressions.aspx" class="navTitle">Suppressions</a>
  </div>
    
    <div class="navItem">
   	  <a href="marketing.aspx" class="navTitle">Marketing</a>
    </div>
    
    <div class="navItem">
   	  <a href="continuity.aspx" class="navTitle">Continuity</a>
    </div>  
    
        
	<div class="navItem">
   	  <a href="_continuityPerformance.aspx" class="navTitle">Continuity Performance</a>
        
    </div>
      
    <div class="navItem">
   	  <a href="report.aspx" class="navTitle">Report</a>
    </div>    
  <div class="navItem">
   	  <a href="voucher.aspx" class="navTitle">Voucher</a>
  </div>
  
    
    <hr size="1" />
	<div class="navItem">
    	Logged in as <strong><%= Session["operator"] %></strong> - <a href="/logout.aspx">Logout</a>
    </div>
    
</div>
