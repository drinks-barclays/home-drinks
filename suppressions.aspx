<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">

	DataSet ds = new DataSet();
	Order o = new Order();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		
			// show open orders
			resultSet.Visible = true;
			ds.Tables.Add(o.GetSuppressions());			
			Display();
	
		
		o = null;
	}
	
	private void Display() {
			if (ds.Tables[0].Rows.Count > 0) {
				foreach (DataRow r in ds.Tables[0].Rows) {
					resultSet.InnerHtml += "<div class='clearfix' id='" + r["InvoiceID"] + "' style='margin:5px;'>";
					resultSet.InnerHtml += "<div><a href='customer.aspx?id=" + r["CustomerID"] + "'>";
					resultSet.InnerHtml += r["FirstName"] + " " + r["LastName"] + "</a> - suppressed  ";
					resultSet.InnerHtml += r["DateAdded"] + "</div>";
					resultSet.InnerHtml += "</div>";
				}
			} else {
				resultSet.InnerHtml += "<div class='err'>No orders to display.</div>";
			}		
	}
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Suppressions</title>
<script type="text/javascript" src="/includes/js/jquery.js"></script> 
<script type="text/javascript" src="/includes/js/thickbox.js"></script>
<script language="javascript">
	function validateSearch() {
		var x = document.getElementById('k').value;
		var xx = document.getElementById('by').value;
		if (x.length < 1) {
			alert('Please enter a search value.');
			document.getElementById('k').focus();
			return false;
		} else if (xx.toLowerCase().indexOf('id') > -1) {
			var id = parseInt((x)*(-1)*(-1));
			if (isNaN(id)) {
				alert('Please enter a numeric value to search this field.');
				return false;	
			}
		}
		return true;
	}
</script>
<style type="text/css" media="all">@import "/includes/thickbox.css";</style>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>

<body>
<div id="container">
    <div id="navigation">
      <WF:Nav runat="server" id="nav_ctl" navId=2 />
    </div>
    
    <div id="content">
    	<div id="header">Suppressions</div>
        <div id="workingArea">
            <div id="resultSet" runat="server">
           	</div>
        </div>
    </div>
    
    <div id="footer">
    
    </div>
</div>
</body>
</html>
