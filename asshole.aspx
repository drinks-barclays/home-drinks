<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Report.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Net.Mime" %>
<script language="C#" runat="server">

	Report report = new Report();
	DataTable dt;
   	public void Page_Load(Object sender, EventArgs e)
 	{
		string body = "";
		string startDate = System.DateTime.Now.Date.ToString("d");
		string endDate = System.DateTime.Now.Date.AddDays(1).ToString("d");
		report.GetProfitLossSummary(out dt, startDate, endDate);
		double reality = 0;
		
		body = "<div style='margin-bottom: 10px; font-weight: bold; font-size: 18px;'>Daily Summary</div><table>";
		body += "<tr style='width: 610px; padding: 3px; border-bottom: solid 1px #ccc;' class='clearfix'>";	
		body += "<td style='width: 100px; float: left;'>Product Cost</td>";
		body += "<td style='width: 100px; float: left;'>Shipping Cost</td>";
		body += "<td style='width: 100px; float: left;'>CC Fees</td>";
		body += "<td style='width: 100px; float: left;'>Taxes</td>";
		body += "<td style='width: 100px; float: left;'>Billed</td>";
		body += "<td style='width: 100px; float: left;'>Net</td>";
		body += "</tr>";
		foreach (DataRow r in dt.Rows) {
			body += "<tr style='width: 610px; padding: 3px;";
			if (Convert.ToDouble(r["Net"]) < 0) body += " color: red;";
			body += "' class='clearfix'>";		
			body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["ProductCost"]).ToString("c") + "</td>";
			body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["ShippingCost"]).ToString("c") + "</td>";
			body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["CCFee"]).ToString("c") + "</td>";
			body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["Taxes"]).ToString("c") + "</td>";
			body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["Billed"]).ToString("c") + "</td>";
			body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["Net"]).ToString("c") + "</td>";
			reality += Convert.ToDouble(r["Net"]);
			body += "</tr>";
		}	
		body += "</table>";
		
		report.GetProfitLossSummaryByCampaign(out dt, startDate, endDate);
		
		body += "<div style='margin-bottom: 10px; margin-top: 30px; font-weight: bold; font-size: 18px;'>Campaign Breakdown</div><table>";
		body += "<tr style='width: 710px; padding: 3px; border-bottom: solid 1px #ccc;' class='clearfix'>";	
		body += "<td style='width: 50px; float: left;'>ID</td>";	
		body += "<td style='width: 100px; float: left;'>Product Cost</td>";
		body += "<td style='width: 100px; float: left;'>Shipping Cost</td>";
		body += "<td style='width: 60px; float: left;'>CC Fees</td>";
		body += "<td style='width: 60px; float: left;'>Taxes</td>";
		body += "<td style='width: 100px; float: left;'>Billed</td>";
		body += "<td style='width: 75px; float: left;'>Net</td>";
		body += "<td style='width: 75px; float: left;'>%</td>";
		body += "</tr>";
		foreach (DataRow r in dt.Rows) {
			body += "<tr style='width: 640px; padding: 3px;";
			if (Convert.ToDouble(r["Net"]) < 0) body += " color: red;";
			body += "' class='clearfix'>";	
			body += "<td style='width: 50px; float: left;'><a href='https://www.barclayswine.com/report.aspx?startDate=" + startDate + "&endDate=" + endDate + "&report=order&campaign_ID=" + r["CampaignID"].ToString() + "'>" + r["CampaignID"].ToString() + "</a></td>";
			body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["ProductCost"]).ToString("c") + "</td>";
			body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["ShippingCost"]).ToString("c") + "</td>";
			body += "<td style='width: 60px; float: left;'>" +  Convert.ToDouble(r["CCFee"]).ToString("c") + "</td>";
			body += "<td style='width: 60px; float: left;'>" +  Convert.ToDouble(r["Taxes"]).ToString("c") + "</td>";
			body += "<td style='width: 100px; float: left;'>" +  Convert.ToDouble(r["Billed"]).ToString("c") + "</td>";
			body += "<td style='width: 75px; float: left;'>" +  Convert.ToDouble(r["Net"]).ToString("c") + "</td>";
			body += "<td style='width: 75px; float: left;'>" + (Convert.ToDouble(r["Net"])/Convert.ToDouble(r["Billed"])).ToString("P") + "</td>";
			body += "</tr>";
		}	
		body += "</table>";
		
		string subject = "";
		
		if (reality > 0) {
			subject = "Congrats Genius, you made " + reality.ToString("c") + " today!";
		} else {
			subject = "Asshole, you lost " + reality.ToString("c") + " today!";	
		}
		
			MailMessage email = new MailMessage(
				   "reality-robot@barclayswine.com",
				   "drinks-devops@drinks.com, zac@quir.is, josiah@quir.is, joyce@drinks.com, chris@drinks.com",
				   subject,
				   body);
			email.IsBodyHtml = true;
				
			NetworkCredential creds = new NetworkCredential("auth@voidme.com", "$m@ng0");
			SmtpClient client = new SmtpClient("127.0.0.1"); 
			client.UseDefaultCredentials = false;
			client.Credentials = creds;
			try {
				client.Send(email);
			} catch (Exception ex) {
				;	
			}
			Response.Write(body);
			
		report.GetOutOfStock(out dt);
		body = "<table>";
		foreach (DataRow r in dt.Rows) {
			body += "<tr style='width: 210px; padding: 3px;' class='clearfix'>";	
			body += "<td style='width: 100px;'>" +  r["ParentProductID"] + "</td>";
			body += "<td style='width: 100px;'>" +  r["Code"] + "</td>";
			body += "</tr>";
		}
		body += "</table>";
		
		if (dt.Rows.Count > 0) {
			email = new MailMessage(
				   "inventory-robot@barclayswine.com",
				   "garrett@barclayswine.com, jessica@barclayswine.com, drinks-devops@drinks.com",
				   "Out Of Stock Packs",
				   body);
			email.IsBodyHtml = true;
			client = new SmtpClient("127.0.0.1");
			client.UseDefaultCredentials = false;
			client.Credentials = creds;
			try {
				client.Send(email);
			} catch (Exception ex) {
				;	
			}
		}
		
	}
</script>

