﻿<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="BB" TagName="Footer" Src="/includes/_footer.ascx" %>
<%@ Register TagPrefix="BB" TagName="Masthead" Src="/includes/_masthead.ascx" %>
<%@ Assembly src="/bin/Authentication.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="Newtonsoft.Json" %> 
<script language="C#" runat="server">

	public void Page_Load(Object sender, EventArgs e)
 	{
		if (Request.IsSecureConnection == false) Response.Redirect(Request.Url.ToString().Replace("http://", "https://"));
		
		if (Request["action"] == "login") {
			try {
				WebClient webClient = new WebClient();
				dynamic result = Newtonsoft.Json.Linq.JValue.Parse(webClient.DownloadString("https://www.googleapis.com/oauth2/v1/tokeninfo?id_token=" + Request["id_token"]));
				
				if (result.email_verified == true && result.email.ToString().IndexOf("@barclayswine.com") > -1 && result.email.ToString().IndexOf("cs@") < 0) {
					// drop session on them
					Session["operator"] = result.email.ToString().Replace("@barclayswine.com", "");
					
					// redirect
					if (Request["r"] != null && Request["r"] != "") {
						Response.Redirect(Server.UrlDecode(Request["r"].ToString()));	
					} else {
						if (Session["operator"].ToString() == "chargebacks") Response.Redirect("/customers.aspx");
						Response.Redirect("/report.aspx");	
					}
				} else {
					// show error
					Response.Redirect("/");	
				}
			} catch (Exception ex) {
					;	
			}
		}
	}

</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" type="image/ico" href="favicon.png" />
<link rel="stylesheet" href="/assets/main.css" type="text/css" media="screen" />
<title>Drinks Admin</title>
    <meta name="google-signin-scope" content="email">
    <meta name="google-signin-client_id" content="509686058821-t9ata2du3jnun3a1njf2ebj0cvg14lun.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="/includes/js/jquery.js"></script>
<script src="/includes/js/jquery.validate.js"></script>
<script> 

	
$(document).ready(function(){
	
    $("#loginForm").validate();
	
});
</script>
</head>

<body>

<div style="margin: 0 auto;" align="center" class="clearfix">
			<div style="font-weight: bold; color: #C30" id="err" runat="server"></div>
            <!--<div style="width: 300px; border: solid 1px #333; margin: 15px;" class="clearfix" align="left">
                <div class="csForm">Secure Login</div>
                        <div style="padding: 5px;">
                        <form method="post" action="" id="loginForm">
                        <div>Drinks ID: </div><div><input name="user_id" id="user_id" type="text" title="Please enter your ID." class="required" /> 
                        <span style="font-size: 12px"></span>
                        </div>
                        <div style="margin-top: 10px;">Password: </div><div><input name="password" id="password" type="password" class="required" />
                        </div>
                        <input name="Submit" type="submit" value="Login" style="margin-top: 10px; margin-right: 10px;" />
                        <input type="hidden" name="action" id="action" value="login" />
                        </form>
                        </div>
              </div>
        </div>-->
    <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark" style="margin: 0 auto;"></div>
    <script>
      function onSignIn(googleUser) {  
		  
        // Useful data for your client-side scripts:
        var profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log("Name: " + profile.getName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());
		
		
		if (profile.getEmail().indexOf("@barclayswine.com") == -1) 
		{
			$("#err").html("A valid Drinks account is required to login.");
		} else {
        	// The ID token you need to pass to your backend:
        	var id_token = googleUser.getAuthResponse().id_token;
        	document.location = 'index.aspx?r=<%= Request["r"] %>&action=login&id_token=' + id_token;
		}

      };
    </script>
</div>

</body>
</html>
