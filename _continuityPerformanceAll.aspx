<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Continuity.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<script language="C#" runat="server">
	
	public Continuity c = new Continuity();
	public System.DateTime currentMonth;
	public DataTable dt = new DataTable();
	
	public void Page_Load(Object sender, EventArgs e)
 	{
		Hashtable p = new Hashtable();
		
		programs.InnerHtml += "<select name='month'>";
		for (int i = -24; i < 3; i++) {
			programs.InnerHtml += "<option value='" + System.DateTime.Now.AddDays(System.DateTime.Now.Day*-1+1).AddMonths(i).ToString("d")  + "'";
			if (Request["month"] == System.DateTime.Now.AddDays(System.DateTime.Now.Day*-1+1).AddMonths(i).ToString("d")) programs.InnerHtml += " selected";
			programs.InnerHtml += ">" + System.DateTime.Now.AddDays(System.DateTime.Now.Day*-1+1).AddMonths(i).ToString("y") + "</option>";
		}
		programs.InnerHtml += "</select>";
		
		if (Request["month"] != null) {
			p = new Hashtable();
			p.Add("Month", Request["month"]);
			dt = new DataTable();
			c.GetContinuityPerformance(out dt, p);
			// parse out current shipments
			currentMonth = Convert.ToDateTime(Request["month"]);
			if (dt != null && dt.Rows.Count > 0) ParseMonth();
		}
	}

	private void ParseMonth() {
		err.InnerHtml = "<table id='myTable'><thead><tr><th>Club</th><th>Sign Ups</th><th>Charge Date</th><th>Success</th><th>Fails</th><th>Cancels</th><th>Revenue</th><th>Credits</th><th>Margin</th></thead></tr><tbody>";
		for (int i = 0; i < System.DateTime.DaysInMonth(currentMonth.Year, currentMonth.Month); i++) {
			DataRow[] group = dt.Select("DateAdded = '" + currentMonth.AddDays(i) + "'");
			if (group.Length > 0) {
				// write out day
				err.InnerHtml += "<tr><td>" + group[0]["Club"].ToString() + "</td><td>" + group.Length + "</td>";
				
				// write out second ships shit
				if (dt.Compute("MIN(SecondShipDate)", "DateAdded = '" + currentMonth.AddDays(i) + "'") != System.DBNull.Value) {
					err.InnerHtml += "<td>" + Convert.ToDateTime(dt.Compute("MIN(SecondShipDate)", "DateAdded = '" + currentMonth.AddDays(i) + "'")).ToString("d") + "</td>"; // charge date
				} else {
					err.InnerHtml += "<td>&nbsp;</td>";
				}
				DataRow[] sums = dt.Select("DateAdded = '" + currentMonth.AddDays(i) + "' AND OrderStatusID IS NOT NULL");
				err.InnerHtml += "<td>" + sums.Length + "</td>"; // successful charges
				
				if (dt.Compute("MIN(SecondShipDate)", "DateAdded = '" + currentMonth.AddDays(i) + "'") != System.DBNull.Value) {
					err.InnerHtml += "<td>" + (group.Length - sums.Length) + "</td>"; // fails
				} else {
					err.InnerHtml += "<td>0</td>";
				}
				
				sums = dt.Select("DateAdded = '" + currentMonth.AddDays(i) + "' AND Active = 0");
				err.InnerHtml += "<td>" + sums.Length + "</td>"; // cancellations
				
				err.InnerHtml += "<td>" + Convert.ToDouble(dt.Compute("SUM(SecondShipTotal)", "DateAdded = '" + currentMonth.AddDays(i) + "'")).ToString("c") + "</td>"; // revenue
				err.InnerHtml += "<td>" +( Convert.ToDouble(dt.Compute("SUM(Credit)", "DateAdded = '" + currentMonth.AddDays(i) + "'"))*-1).ToString("c") + "</td>"; // credits
				err.InnerHtml += "<td>" + Convert.ToDouble(dt.Compute("SUM(Margin)", "DateAdded = '" + currentMonth.AddDays(i) + "'")).ToString("c") + "</td></tr>"; // margin
			}
		}
		err.InnerHtml += "</tbody></table>";
	}
	
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Continuity Performance Report</title>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/pepper-grinder/jquery-ui.min.css" media="screen" />
<script src="//heartwoodandoak.com/assets/js/jquery.validationEngine-en.js" type="text/javascript"></script>
<script src="//heartwoodandoak.com/assets/js/jquery.validationEngine.js" type="text/javascript"></script>
<link rel="stylesheet" href="//heartwoodandoak.com/assets/css/validationEngine.jquery.css" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
<style>
	
	body {
		margin: 0 auto;
		width: 800px;
		padding: 5px;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	
	#nav {
		margin: 0 auto;
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
		background-color: #efefef;
		min-width: 400px;
	}
	
	#workingArea {
		margin-bottom: 25px;
		margin-top: 25px;
		margin-left: 0px;
		padding: 10px;
		border: 1px solid #ccc;
	}
	
	th {
		text-align: left;
		font-weight: bold;
		cursor: s-resize;	
		background-color: #0CF;
		color: white;
	}
	
	th,td {
		padding: 5px;	
		border: 1px solid #efefef;
	}
	
	.product:hover {
		background-color: #FFC;	
	}
	
</style>
<script src="/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
	
	$(document).ready(function(){
		$("#myTable").tablesorter({sortList: [[0,0]]}); 
		
  	});
	
  </script>
</head>

<body>
<form action=""><span id="programs" runat="server" style="font-weight; bold: font-size: 20px" /><input type="submit" /></form>
<div id="err" runat="server" style="width: 800px;" />
</body>
</html>
