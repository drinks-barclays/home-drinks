﻿<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Order.cs" %>
<%@ Assembly src="/bin/Payment.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Net.Mime" %>
<script language="C#" runat="server">

	Order o = new Order();
	Payment p = new Payment();
	DataTable dt = new DataTable();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		
		
		// grab all orders that need to be charged
		
		// first ships and special orders
		o.GetOrdersInProcessing(out dt);
		
		if (dt.Rows.Count > 0) {			
			// add a column so we can track what charged correctly
			bool charged = false;
			
				// capture the $$$ using the transaction id
				foreach(DataRow r in dt.Rows) {
					// start fresh
					charged = false;
					p.Reset();
					
					// do capture
					string amount = Convert.ToDouble(r["Amount"]).ToString("c").Replace("$", "");
					charged = p.DoCapture(r["TransactionCode"].ToString(), amount);
					
					if (!charged) {
						// try troon
						p.x_login = "8B45Bvbh";
						p.x_tran_key = "8xC2Jc2Qe5ak24VF";
						charged = p.DoCapture(r["TransactionCode"].ToString(), amount);
					}
					
					if (charged) {
						// mark invoice as charged
						o.UpdateStatus(r["InvoiceID"].ToString(), "2");
					} else {
						
						// should send email to CS to resolve or void the order 
						// and contact customer.
						
					}
				}
		
		} else {
			Response.Write("No files to charge.<br />");	
		}
			
			
		// grab all of the orders that need to be shipped
		StringBuilder sb = new StringBuilder();
		string fileName = "";
		dt = null;
		dt = new DataTable();
		o.GetMiscBottlesToShip(out dt);
		
		// get single orders
		if (dt.Rows.Count > 0) {
			string currentInvoice = "";
			string body = "";
			string shipName = "";
			int skuCount = 0;
			foreach(DataRow r in dt.Rows) {
				if (currentInvoice != r["InvoiceID"].ToString()) {
					if (currentInvoice != "") {
						SendEmail(body, shipName + "-" + currentInvoice, sb);
					}
					sb = new StringBuilder();
					sb.Append("order,attn,phone,Ship,name,add1,add2,City,State,Zip\r\n");
					sb.Append(r["InvoiceID"] + ",," + String.Format("{0:###-###-####}", Convert.ToInt64(r["PhoneNumber"])) +
									"," + r["ShipName"].ToString().Trim().Replace(",", "") + "," + r["Address"].ToString().Trim().Replace(",", "") + "," + r["Address2"].ToString().Trim().Replace(",", "") + "," + 
									r["City"].ToString().Trim().Replace(",", "") + "," + r["State"] + "," + r["PostalCode"].ToString().Trim() + "\r\n");
					o.UpdateStatus(r["InvoiceID"].ToString(), "3"); // mark as packed
					currentInvoice = r["InvoiceID"].ToString();
					shipName = r["ShipName"].ToString();
					body = "Shipment " + r["InvoiceID"] + " to " +  r["ShipName"] + "<br />\r\n<br />\r\n";
					skuCount = 0;
				}
				
				skuCount++;
				body += r["Code"] + " - " + r["Name"] + " (" + r["Quantity"] + " bottle)<br />\r\n";
			}
			SendEmail(body, shipName + "-" + currentInvoice, sb);
		}
		
		sb = new StringBuilder();
		fileName = "";
		dt = null;
		dt = new DataTable();
		o.GetPacksToShip(out dt); 
		
		// get packs
		if (dt.Rows.Count > 0) {
			string currentSku = "";
			string body = "";
			string skuName = "";
			int skuCount = 0;
			DataTable children = new DataTable();
			foreach(DataRow r in dt.Rows) {
				if (currentSku != r["ProductID"].ToString()) {
					if (currentSku != "") {
						body = skuCount + " shipments of " + skuName + "<br />\r\n<br />\r\n" + body;
						SendEmail(body, skuCount + " shipments of " + skuName, sb);
					}
					
					body = "";
					children = o.GetChildren(r["ProductID"].ToString());
					foreach (DataRow rr in children.Rows) {
						body += rr["Code"] + " - " + rr["Name"] + " (" + rr["Quantity"] + " items)<br />\r\n";
					}
					
					sb = new StringBuilder();
					sb.Append("order,attn,phone,Ship,name,add1,add2,City,State,Zip\r\n");
					currentSku = r["ProductID"].ToString();
					skuName = r["Name"].ToString();
					skuCount = 0;
				}
				
				for (int i = 0; i < Convert.ToInt32(r["Quantity"].ToString()); i++) {
					sb.Append(r["InvoiceID"] + ",," + String.Format("{0:###-###-####}", Convert.ToInt64(r["PhoneNumber"])) +
									"," + r["ShipName"].ToString().Trim().Replace(",", "") + "," + r["Address"].ToString().Trim().Replace(",", "") + "," + r["Address2"].ToString().Trim().Replace(",", "") + "," + 
									r["City"].ToString().Trim().Replace(",", "") + "," + r["State"] + "," + r["PostalCode"].ToString().Trim() + "\r\n");
					o.UpdateStatus(r["InvoiceID"].ToString(), "3"); // mark as packed
					skuCount++;
				}
			}
			body = skuCount + " shipments of " + skuName + "<br />\r\n<br />\r\n" + body;
			SendEmail(body, skuCount + " shipments of " + skuName, sb);
		}
		
		dt = null;
		o = null;
		p = null;
	}
	
	private void SendEmail(string summary, string subject, StringBuilder sb) {
			string fileName = subject + "_" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + 
								"-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Second.ToString() +
								".csv";
			fileName = fileName.Replace(@"\", "");
			fileName = fileName.Replace(@"/", "");
			try {
				TextWriter tw = new StreamWriter(Server.MapPath("~/secure/shipping_files/") + fileName);
				tw.Write(sb.ToString());
				tw.Close();
			}
			catch {
				;	
			}
			
			// email file to jessica and cc devops
			Attachment data = new Attachment((Server.MapPath("~/secure/shipping_files/") + fileName), MediaTypeNames.Application.Octet);
			MailMessage email = new MailMessage(
				   "shipping-robot@barclayswine.com",
				   "drinks-devops@drinks.com, jessica@barclayswine.com",
				   subject,
				   summary);
			email.Attachments.Add(data);
			email.IsBodyHtml = true;
				
			NetworkCredential creds = new NetworkCredential("auth", "$m@ng0");
			SmtpClient client = new SmtpClient("127.0.0.1");
			client.UseDefaultCredentials = false;
			client.Credentials = creds;
			try {
				client.Send(email);
			} catch (Exception ex) {
				;	
			}
	}
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
</body>
</html>
