﻿<%@  Page Language="C#" Debug="true" %>
<%@ Register TagPrefix="WF" TagName="Nav" Src="_nav.ascx" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Assembly src="/bin/Utilities.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<script language="C#" runat="server">

	Customer c = new Customer();
   	public void Page_Load(Object sender, EventArgs e)
 	{
		DataTable dt = c.Impersonate(Request["id"]);
		
		if (dt.Rows.Count > 0) {
					// send email
					byte[] key = {1, 2, 3, 5, 5, 6, 7, 8, 9, 10, 11, 12, 
							7, 14, 15, 9, 17, 18, 19, 7, 3, 22, 23, 24};
					byte[] iv = {3, 7, 5, 5, 5, 3, 7, 2};	
					
					// encrypt password
					cTripleDES crypto = new cTripleDES(key, iv);
					string password = crypto.Decrypt(dt.Rows[0]["Password"].ToString());
					
					Response.Redirect("https://www.barclaysbin.com/login.aspx?action=login&user=" + dt.Rows[0]["Email"].ToString() + "&password=" + password);
		}
	}
	
</script>
      <WF:Nav runat="server" id="nav_ctl" navId=2 />