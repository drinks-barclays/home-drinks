<%@  Page Language="C#" Debug="true" %>
<%@ Assembly src="/bin/Customer.cs" %>
<%@ Import Namespace="Wine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="ResponsysWrapper" %>
<%@ Import Namespace="ResponsysWrapper.Models" %>
<%@ Import Namespace="System.Configuration" %>

<script language="C#" runat="server">
	
	public void Page_Load(Object sender, EventArgs e)
 	{
		if(Request["data[email]"] != null && Request["folder"] != null)
		{
			ResponsysApi api = new ResponsysApi("drinks_api", "s?aRap6esTehapH+");

            Auth session = api.Connect();

            api._authToken = session.authToken;
            api.BaseUrl = new Uri(session.endPoint);
			
			DataTable dt = new DataTable();
			dt.Columns.Add("EMAIL_ADDRESS_", typeof(String));
			dt.Columns.Add("EMAIL_PERMISSION_STATUS_", typeof(String));
			dt.Rows.Add(Request["data[email]"], "O");
				
			try {
				UnsubscribeResult result = api.Unsubscribe(dt, Request["folder"]);
				ShowResults(result);
			} catch (Exception ex) {
				Response.Write(ex);
			}
		}
	}
	
	private void ShowResults(UnsubscribeResult result) {
            if (result.errorCode == null)
            {
                foreach (string s in result.recordData.fieldNames)
                {
                    Response.Write(s + "\t");
                    Response.Write("<br>");
                }
            }

            foreach (var records in result.recordData.records)
            {
                // make sure everything went through
                foreach (string s in records)
                {
                    Response.Write(s + "\t");
                    Response.Write("<br>");
                }
            }	
	}
	
</script>